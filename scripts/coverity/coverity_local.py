import os
from os import system
from time import time
from argparse import ArgumentParser

###################################### SYSTEM DEPENDENT #####################################

# These should only need to set once for your system, make them absolute paths and be mindful of spaces and quotes
IAR_BUILD_PATH = '\"C:/Program Files (x86)/IAR Systems/Embedded Workbench 7.4/common/bin/IarBuild.exe\"'
COV_BUILD_PATH = '\"C:/Program Files/Coverity 8/Coverity Static Analysis/bin/cov-build.exe\"'
COV_ANALY_PATH = '\"C:/Program Files/Coverity 8/Coverity Static Analysis/bin/cov-analyze.exe\"'
COV_OUTPT_PATH = '\"C:/Program Files/Coverity 8/Coverity Static Analysis/bin/cov-format-errors.exe\"'
COV_MISRA_PATH =   'cov-config/MISRA_c2012_7.config'
COV_CONFG_PATH =   'cov-config/coverity_config.xml'
COV_INTER_DIR  =   'C:/coverity/cov-intdir/'
COV_OUTPT_DIR  =   'C:/coverity/cov-output/'

#############################################################################################

# Get input arguments
parser = ArgumentParser(description='Run Coverity 8 locally.')  
parser.add_argument('-p', dest='projFile', type=str, required=True, help='IAR project file')
parser.add_argument('-i', dest='srcFiles', type=str, help='Input file containing list of source files to analyze', default='NULL')
parser.add_argument('--misra', dest='isMisra', action='store_true', help='Run MISRA 2012 checking', default=False)
parser.add_argument('--stat', dest='isStatic', action='store_true', help='Run Coverity static analysis', default=False)
args = parser.parse_args()

# Check input arguments
if (args.isMisra and args.isStatic) or ((not args.isMisra) and (not args.isStatic)):
    print "Error: Must run one and only one of MISRA or static analysis"
    exit(1)
if not (os.path.isfile(args.projFile)):
    print "Error: Project file does not exist"
    exit(1)
outFiles = []
if args.srcFiles != 'NULL':
    if not (os.path.isfile(args.srcFiles)):
        print "Error: Input file does not exist"
        exit(1)
    else:
        partPath,_ = os.path.split(args.srcFiles)
        with open(args.srcFiles, 'r') as f:
            for line in f:
                fullpath = os.path.join(partPath, line.strip())
                if not (os.path.isfile(fullpath)):
                    print "Error: Bad filepath in the input file"
                    exit(1) 
                else:
                    outFiles.append(fullpath)

# Just set the project name to the name of the project file (without extension)
path,name = os.path.split(args.projFile)
projName  = name.split('.')[0]

t0 = time()

# Clean
command = IAR_BUILD_PATH + ' ' + args.projFile + ' -clean Debug'
print(command)
system(command)

# Build
command = COV_BUILD_PATH + ' --dir ' + COV_INTER_DIR + projName  + ' --config ' + COV_CONFG_PATH + ' ' + IAR_BUILD_PATH + ' ' + args.projFile + ' -build Debug -log all'
print(command)
system(command)

# Analyze
if args.isMisra:
    command = COV_ANALY_PATH + ' --dir ' + COV_INTER_DIR + projName  + ' --config ' + COV_CONFG_PATH + ' --misra-config ' + COV_MISRA_PATH
else:
    command = COV_ANALY_PATH + ' --dir ' + COV_INTER_DIR + projName  + ' --config ' + COV_CONFG_PATH + ' --security --concurrency --enable-constraint-fpp --enable-fnptr --enable-callgraph-metrics'
print(command)
system(command)

# Output
if args.srcFiles != 'NULL':
    for outFile in outFiles:
        newDir = COV_OUTPT_DIR + str(outFiles.index(outFile)) + '_' + outFile.split('/')[-1]
        system('rm -rf ' + newDir)
        system('mkdir ' + newDir)
        command = COV_OUTPT_PATH + ' --dir ' + COV_INTER_DIR + projName + ' --html-output ' + newDir + ' --include-files ' + outFile
        print(command)
        system(command) 
else:
    newDir = COV_OUTPT_DIR + projName
    system('rm -rf ' + newDir)
    system('mkdir ' + newDir) 
    command = COV_OUTPT_PATH + ' --dir ' + COV_INTER_DIR + projName + ' --html-output ' + newDir
    print(command)
    system(command) 

t1 = time()

print('Total runtime is: %f seconds.' %(t1-t0))

exit(0)

