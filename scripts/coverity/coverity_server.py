from os import system, path
from argparse import ArgumentParser

###################################### SYSTEM DEPENDENT #####################################

# Set this once for the local system
COV_MISRA_PATH = 'cov-config\\MISRA_c2012_7.config'
COV_CONFG_PATH = 'cov-config\\coverity_config.xml'
COV_INTER_DIR  = 'C:\\Coverity\\cov-intdir\\'
COV_INSTL_DIR  = 'C:\\Coverity\\cov-analyze\\bin\\'

# Set this once for the host server
COV_HOST_INFO = '--host nwdcoverity1.spd.analog.com --dataport 9090 --user commit --password commit --stream Muska --strip-path /Jenkins/workspace/aducm4x50_coverity'

#############################################################################################

# Get input arguments
parser = ArgumentParser(description='Run Coverity. Intended for a host server, not local developer use.')
parser.add_argument('-p', dest='projName', type=str, required=True, help='Project name string')
parser.add_argument('-b', dest='buildFile', type=str, required=True, help='Compiler build script')
parser.add_argument('--misra', dest='isMisra', action='store_true', help='Run MISRA 2012 checking', default=False)
parser.add_argument('--stat', dest='isStatic', action='store_true', help='Run Coverity static analysis', default=False)
args = parser.parse_args()

# Check input arguments
if (args.isMisra and args.isStatic) or ((not args.isMisra) and (not args.isStatic)):
    print "Error: Must run one and only one of MISRA or static analysis"
    exit(1)
if not (path.isfile(args.buildFile)):
    print "Error: Compiler build script does not exist"
    exit(1)

# Build
command = COV_INSTL_DIR + 'cov-build.exe --dir ' + COV_INTER_DIR + args.projName  + ' --config ' + COV_CONFG_PATH + ' ' + args.buildFile
x = system(command)
if x == 1:
    exit(1)

# Analyze
if args.isMisra:
    command = COV_INSTL_DIR + 'cov-analyze.exe --dir ' + COV_INTER_DIR + args.projName  + ' --config ' + COV_CONFG_PATH + ' --misra-config ' + COV_MISRA_PATH
else:
    command = COV_INSTL_DIR + 'cov-analyze.exe --dir ' + COV_INTER_DIR + args.projName  + ' --config ' + COV_CONFG_PATH + ' --all --enable-constraint-fpp --enable-fnptr --enable-callgraph-metrics'
x = system(command)
if x == 1:
    exit(1)

# Commit
command = COV_INSTL_DIR + 'cov-commit-defects.exe --dir ' + COV_INTER_DIR + args.projName + ' ' + COV_HOST_INFO
system(command)
x = system(command)
if x == 1:
    exit(1)
else:
    exit(0) 
