# Build Process Documentation

## Overview

This document describes the build scripts and how they are used to generate packages and products. 

## Definitions

* Package: A ZIP or PACK file that contains a group of files that goes to a single destination during the installation.

* Product: A set of software packages that are delivered to customers.

## Build Process

The build process consists of creating several software packages, testing them, and eventually moving them to a location where they can be released. The main Python script is the same for all packages, called `master.py`, which consumes a main XML configuration file, called `master.xml`.

At a high-level, `master.py` will perform the following operations

1. Export documents from Confluence
2. Generate Doxygen documentation
3. Create a package consisting of regular and support manifests
4. Run the test hook script, if defined
5. Create a package consisting of regular manifests
6. Create the ZIP or PACK artifact

Packages are described in `master.xml`. The required fields for a package defined in `master.xml` are:

* name : The name of the package.

* type: This is a string set to "zip" or "pack". A ZIP package will simply be zipped after testing is complete. A PACK is more complicated, and will have several steps after testing including PDSC file generation, PDSC file verification, `examples.xml` file generation, and finally zipping.

* manifests : At least one file containing a list of files to go in the package.

There are also many optional fields for a package in `master.xml`, which allow the user to harness extra features in the Python script:

* testbuild : A hook script can be specified to run the desired test system on the package. This script will be called with a single input argument containing the relative path to the package from the Python script.

* support : Another set of manifests, but for files that are only needed for testing. This allows for simple paritioning between test files and release files.

* mappings : Sometimes the directory structure of the repository needs to be changed in the release package. This allows the user to provide a directory structure mapping. For instance, if all files in the directory `foo\` must be changed to `super\foo\bar` for the final product, this can be specified here.

* confluence : The Python script can export documents from Confluence if the proper parameters are specified here. 

* doxygen : Doxyfiles and Doxygen output files are specified here. These are run prior to building the package. The Doxygen exec must be specified at the top of `master.py`.

## Running on Your Machine

The Python script has the following dependencies:

* The "requests" package, can be installed with `pip`
* Doxygen (defined at the top of `master.py`)

For PACK files:
* Example Crawler (must be placed in the `scripts/` directory for proper importing)
* PackChk.exe (defined at the top of `master.py`, comes with Keil install)

Then, use the script like so:
`python master.py -n "MyPackageName" -v "12345" -m "path\to\manifests\folder" -p "path\to\package\folder" `

You can always skip the test build script by adding the `--skip-tests` option above. This is good for creating packages locally if you don't have the right hardware setup.
