CD %WORKSPACE%\TTH\src

SET PYTHONPATH=%WORKSPACE%\TTH\src
SET IAR_EW_HOME=C:\Progra~2\IARSys~1\Embedd~1.4
SET CSPY_PATH=%WORKSPACE%\aducm4x50\scripts\iar\cspy
SET ADUCM4x50_MICRIUM_DIR=C:\Micrium\Software
SET ADI_FREERTOS_PATH=C:\FreeRTOS\FreeRTOSv9.0.0\FreeRTOS\Source
SET ADI_FREERTOS_PATH_PLUS=C:\FreeRTOS\FreeRTOSv9.0.0\FreeRTOS-Plus\Source\FreeRTOS-Plus-Trace

REM *************** TTH ***************

python TTH.py -v4 -p ADuCM4150 -r %WORKSPACE%\aducm4x50\scripts\%1\ -x bsp_results_4150.xml

EXIT %ERRORLEVEL%
