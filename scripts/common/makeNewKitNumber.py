import time
import sys
import os
import re

def main():
    newKitNumber = 'bad_kit_name'
    kitsDirectory = 'bad_dir'
    if (len(sys.argv) > 1):
        kitsDirectory = sys.argv[1]
    if (os.path.isdir(kitsDirectory)):
        date = time.strftime("%y%m%d", time.localtime())
        allKits = os.listdir(kitsDirectory)
        allKits.sort()
        if (len(allKits) > 0):
            latestKit = allKits[len(allKits)-1]
            m = re.compile('w[0-9]+').search(latestKit)
            if (m != None):
                latestDate = m.group()[1:len(m.group())-1]
                kitNumber = m.group()[len(m.group())-1]
                if (latestDate == date):
                    newKitNumber = 'w' + date + str(int(kitNumber)+1)
                else:
                    newKitNumber = 'w' + date + '1'
        else: # kits directory is empty:
            newKitNumber = 'w' + date + '1'
    newKitNumber = newKitNumber + "-stage"
    outputFolder = kitsDirectory + "\\" + newKitNumber

    sys.stdout.write(outputFolder)
if __name__ == "__main__":
    main()
