import os
import copy
import shutil
import zipfile
import xml.etree.ElementTree as ET
from argparse import ArgumentParser


### NON-DEFAULT PACKAGES - MUST BE INSTALLED ###
import requests


### SYSTEM DEPENDENT - CHANGE THESE TO REFLECT YOUR MACHINE ###
DOXYGEN_EXEC_ABSOLUTE_PATH    = '\"C:\\Program Files\\doxygen\\bin\\doxygen.exe\"'
PACKCHK_EXEC_ABSOLUTE_PATH    = '\"C:\\Keil_v5\\ARM\\PACK\\ARM\\CMSIS\\4.5.0\\CMSIS\\Utilities\\PackChk.exe\"'


### CONSTANTS ###
LOG_FILE_NAME  = 'master_output.txt'
PACKNAME_FILE  = 'packname.txt'
PACKCHECK_FILE = 'packcheck_output.log'


### GLOBALS ###
PACKAGE_ROOT_RELATIVE_TO_CWD  = ""
MANIFEST_ROOT_RELATIVE_TO_CWD = ""


def log_output(log):
    """
    Debug log file
    """
    fp = open(LOG_FILE_NAME, 'a')
    fp.write(log + '\r\n')
    fp.close()


def dict_replace(s, words):
    """
    Simply doing a string replace(key, value) is not valid because the mapping is one directory
    to another directory. So we need to decompose the path into the various directories and 
    look for an exact match between the directory and the keys of the dictionary.
    """
    dpath,fname = os.path.split(s)
    p = os.path.normpath(dpath)
    path_list = p.split(os.sep)
    new_list  = []
    for element in path_list:
        for k, v in words.iteritems():
            if element == k:
                element = v
        new_list.append(element)
    new_list.append(fname)     
    return os.path.join(*new_list)


def manifest_copy(trueSrc, newSrc, dest):
    """
    Copy trueSrc to dest with the directory structure defined in newSrc
    """
    if os.path.isfile(trueSrc):
        newDir, _ = os.path.split(os.path.join(dest, newSrc))
        if not os.path.exists(newDir):
            os.makedirs(newDir)
        shutil.copy(trueSrc, newDir)
    else:
        shutil.copytree(trueSrc, os.path.join(dest, newSrc))


def confluence_export(output, rootPageId, versionId):
    """
    Export rootPageID at versionId from Confluence into "output"
    """
    exportSchemeId = '0A40AB5C01516940585088991BF862E8'
    url = 'https://labrea.ad.analog.com/confluence/rest/scroll-pdf/1.0/sync-export?exportSchemeId=-' + exportSchemeId + '&rootPageId=' + rootPageId
    if len(versionId) != 0:
        url += '&versionId=' + versionId

    auth = ('aditools' , '4web.reg')
    r = requests.get(url, auth=auth)

    if r.status_code == requests.codes.ok:
        if os.path.isfile(output):
            os.remove(output)
        with open(output, 'wb') as f:
            f.write(r.content)
        return 0
    else:
        return 1


def zipdir(name, path):
    ziph = zipfile.ZipFile(name, 'w', zipfile.ZIP_DEFLATED)
    for root, dirs, files in os.walk(path):
        for file in files:
                # Do not keep the first level in the zip (the first dir in root, i.e. the path)
                ziph.write(os.path.join(root, file), os.path.join(root, file)[len(path):])
    ziph.close()


class Package(object):
    """
    Simply mapping one of the XML "package" entries in master.xml to a data structure
    """
    __name       = ''
    __testbuild  = ''
    __type       = ''
    __support    = []
    __manifests  = []
    __mappings   = {}
    __confluence = []
    __doxygen    = []

    def __init__(self, name, fname):
        self.__support     = []
        self.__manifests   = []
        self.__mappings    = {}        
        self.__confluence  = []
        self.__doxygen     = []

        found = False
        for package in ET.parse(fname).getroot():
            if package.get('name') == name:
                self.__name      = package.get('name')
                self.__testbuild = package.get('testbuild')
                self.__type      = package.get('type')

                if self.__type != 'zip' and self.__type != 'pack':
                    log_output('Error parsing master.xml. The type field must be zip or pack!')
                    exit(1)

                for fields in package:
                    if fields.tag == 'manifests':
                        for m in fields:
                            self.__manifests.append(m.get('p'))

                    if fields.tag == 'mappings':
                        for m in fields:
                            d = m.attrib
                            self.__mappings[d.get('s')] = d.get('d')

                    if fields.tag == 'support':
                        for m in fields:
                            self.__support.append(m.get('p'))

                    if fields.tag =='confluence':
                        for c in fields:
                            vals = c.attrib
                            self.__confluence.append((vals.get('p'), vals.get('id'), vals.get('version')))

                    if fields.tag =='doxygen':
                        for c in fields:
                            vals = c.attrib
                            self.__doxygen.append((vals.get('i'), vals.get('o')))

                found = True
                break

        if not found:
            log_output('Could not read package configuration from master.xml. Bad package name?')
            exit(1)

    def GetName(self):
        return self.__name

    def GetTestBuild(self):
        return self.__testbuild

    def GetType(self):
        return self.__type

    def GetSupportFiles(self):
        return self.__support

    def GetManifests(self):
        return self.__manifests

    def GetMappings(self):
        return self.__mappings

    def GetConfluence(self):
        log_output('Starting Confluence export...')
        for tup in self.__confluence:
            output, page, ver = tup
            output = os.path.join(PACKAGE_ROOT_RELATIVE_TO_CWD, output)
            log_output('Exporting ' + output)
            ret = confluence_export(output, page, ver)
            if ret != 0:
                log_output('Confluence export failed. Exiting...')
                exit(1)
        log_output('Confluence export completed successfully.')

    def GenDoxygen(self):
        log_output('Starting Doxygen generation...')
        for tup in self.__doxygen:
            doxyfile, doxyout = tup
            doxydir_0, doxyfile = os.path.split(doxyfile)
            doxydir_1, doxyout = os.path.split(doxyout)
            if doxydir_0 != doxydir_1:
                log_output('Error in master.xml, Doxyfile and output must be in same directory. Exiting...')
                exit(1)
            log_output('Generating ' + doxyfile)
            cwd = os.getcwd()
            os.chdir(os.path.join(PACKAGE_ROOT_RELATIVE_TO_CWD, doxydir_0))
            ret = os.system(DOXYGEN_EXEC_ABSOLUTE_PATH + ' ' + doxyfile +' > ' + doxyout + ' 2>&1')
            os.chdir(cwd)
            if ret != 0:
                log_output('Doxygen generation failed. Exiting...')
                exit(1)
            log_output('Doxygen generation completed successfully.')

    def CreateFileList(self, bSupportFiles=False):
        flist = []
        manifest_list = copy.deepcopy(self.__manifests)
        if bSupportFiles:
            manifest_list += self.__support
        for m in manifest_list:
            for file in ET.parse(os.path.join(MANIFEST_ROOT_RELATIVE_TO_CWD, m)).getroot():
                flist.append(file.attrib.get('p'))
        return flist


def build_package(name, version, skip_tests):
    """
    Main function for package creation
    """
    log_output('Starting build for package: ' + name)

    package = Package(name, os.path.join(MANIFEST_ROOT_RELATIVE_TO_CWD, "master.xml"))

    tName = package.GetName() + '_' + version

    package.GetConfluence()
    package.GenDoxygen()

    if os.path.exists(tName):
        shutil.rmtree(tName)
    os.mkdir(tName)

    log_output('Creating package WITH support files...')
    for f in package.CreateFileList(True):
        manifest_copy(os.path.join(PACKAGE_ROOT_RELATIVE_TO_CWD, f), dict_replace(f, package.GetMappings()), tName)
    log_output('Package creation successul.')

    if os.path.exists(package.GetTestBuild()) and not skip_tests:
        ret = os.system(package.GetTestBuild() + " " + tName)
        if ret == 0:
            log_output('Test-build script completed successfully.')
        else:
            log_output('Test-build script failed. Exiting...')
            exit(1)
    else:
        log_output('No test-build script specified.')                

    shutil.rmtree(tName)
    os.mkdir(tName)

    log_output('Creating package WITHOUT support files...')
    for f in package.CreateFileList(False):
        manifest_copy(os.path.join(PACKAGE_ROOT_RELATIVE_TO_CWD, f), dict_replace(f, package.GetMappings()), tName)
    log_output('Package creation successul.')

    # ZIP Archive Generation
    if package.GetType() == 'zip':
        zipdir(tName + '.zip', tName)
        
    # PACK Archive Generation
    else:
        # Concat examples
        pdsc = ''
        pdsc_cnt = 0
        examples = ''
        for root, dirs, files in os.walk(tName):
            for file in files:
                if file.endswith('.pdsc'):
                    pdsc = os.path.join(root, file)
                    pdsc_cnt += 1

                if file == 'example_pdsc.txt':
                    example = os.path.join(root, file)
                    fp = open(example, 'r')
                    examples += fp.read()
                    fp.close()
                    os.remove(example)

        if len(pdsc) == 0:
            log_output("Error: Could not find PDSC file in the package. Exiting...")
            exit(1)

        if pdsc_cnt != 1:
            log_output("Error: Multiple PDSC files in the package. Exiting...")
            exit(1)

        fp = open(pdsc, 'r')
        template = fp.read()
        fp.close() 
        os.remove(pdsc)
        fp = open(pdsc, 'w')
        new_file = template.replace('%examples_pdsc%', examples) 
        fp.write(new_file)
        fp.close()

        # Generate examples.xml
        from examplecrawler.src.excrawler import run_excrawler
        ret = run_excrawler(tName, ".", True)
        if ret != 0:
            log_output("Error: Example Crawler failed. Exiting...")
            exit(1)
        else:
            log_output("Example Crawler completed successfully!")

        # Run PackChk.exe
        if os.path.exists(PACKNAME_FILE):
            os.remove(PACKNAME_FILE)
        if os.path.exists(PACKCHECK_FILE):
            os.remove(PACKCHECK_FILE)
        ret = os.system(PACKCHK_EXEC_ABSOLUTE_PATH + " " + pdsc + " -n " + PACKNAME_FILE + " >> " + PACKCHECK_FILE)
        if ret != 0:
            log_output("Error: PACK check failed, see " + PACKCHECK_FILE + " for details. Exiting...")
            exit(1)
        else:
            log_output("PACK check succeeded!")

        fp = open(PACKNAME_FILE, 'r')
        packname = fp.read()
        fp.close()

        # Create archive
        zipdir(packname, tName)


if __name__ == '__main__':
   
    parser = ArgumentParser(description='Top level script for package building')
    parser.add_argument('-n', dest='names', type=str, required=True, help='String of the package names to build (in order), white space seperated.')
    parser.add_argument('-m', dest='manifests', type=str, required=True, help='Path to "manifests" directory from "scripts"')
    parser.add_argument('-p', dest='package', type=str, required=True, help='Path to "package" directory from "scripts"')
    parser.add_argument('-v', dest='version', type=str, required=True, help='Version string (from Git or SVN)')
    parser.add_argument('--skip-tests', dest='skip', action='store_true', required=False, help='Do not run the test build script(s)', default=False)
    args = parser.parse_args()   

    PACKAGE_ROOT_RELATIVE_TO_CWD  = args.package
    MANIFEST_ROOT_RELATIVE_TO_CWD = args.manifests

    if os.path.exists(LOG_FILE_NAME):
        os.remove(LOG_FILE_NAME)

    for package_name in args.names.split():
        build_package(package_name, args.version, args.skip)

    exit(0)
