import os
import re
from argparse import ArgumentParser

emu_str = '-U228206875'
com_str = 'COM8\n'

names    = [os.path.join('sport', 'loopback_mixed'), os.path.join('sport', 'loopback_dma'), os.path.join('sport', 'loopback_int'), os.path.join('icc', 'icc_task_dma')]
emu_str2 = '-U228209887'
com_str2 = 'COM9\n'

def regex_find_replace(flag, regex, rep_str):
    lines = []
    with open(os.path.join(dirpath, f), 'r') as infile:
        for line in infile:
            if flag in line:
                line = line.replace(re.split(regex, line)[1], rep_str)
            lines.append(line)
    with open(os.path.join(dirpath, f), 'w') as outfile:
        for line in lines:
            outfile.write(line)

if __name__ == "__main__":

    parser = ArgumentParser(description='Modify Keil TTH files for an specific emulator and COM port.')
    parser.add_argument('-r', dest='root', type=str, required=True, help='Path to root directory to apply hardware configuration to"')
    args = parser.parse_args()   

    for dirpath,dirnames,filenames in os.walk(args.root):
        for f in filenames:
            if f == 'COM.cfg':
                if any(n in os.path.join(dirpath, f) for n in names):
                    regex_find_replace('COM', ': |\r', com_str2)
                else:
                    regex_find_replace('COM', ': |\r', com_str)
            if '.uvoptx' in f:
                if any(n in os.path.join(dirpath, f) for n in names):
                    regex_find_replace('<Name>-U', '<Name>| -O', emu_str2)
                else:
                    regex_find_replace('<Name>-U', '<Name>| -O', emu_str)
