SET PYTHONPATH=%WORKSPACE%\TTH\src
SET CRAWLPATH=%WORKSPACE%\examplecrawler\src
SET INSTALLERPATH=%WORKSPACE%\installer
SET ADUCM4x50_MICRIUM_DIR=C:\Micrium\Software
SET ADI_FREERTOS_PATH=C:\FreeRTOS\FreeRTOSv9.0.0\FreeRTOS\Source
SET ADI_FREERTOS_PATH_PLUS=C:\FreeRTOS\FreeRTOSv9.0.0\FreeRTOS-Plus\Source\FreeRTOS-Plus-Trace
SET KEIL_HOME=C:\Keil_v5
SET CCES_HOME=C:\Analog Devices\CrossCore Embedded Studio 2.5.1
SET CCES_WORK=C:\Users\testlab\cces\Examples

SET DFP_KEIL_PATH=%KEIL_HOME%\ARM\PACK\AnalogDevices\ADuCM4x50_DFP\1.0.0
SET DFP_CCES_PATH="%CCES_HOME%\ARM\packs\AnalogDevices\ADuCM4x50_DFP\1.0.0"
SET BSP_KEIL_PATH=%KEIL_HOME%\ARM\PACK\AnalogDevices\ADuCM4x50_EZ_KIT_BSP\1.0.0
SET BSP_CCES_PATH="%CCES_HOME%\ARM\packs\AnalogDevices\ADuCM4x50_EZ_KIT_BSP\1.0.0"

REM ************ HW CONFIG ************

CD %WORKSPACE%\aducm4x50\scripts\cmsis

python apply_hw_config.py -r %WORKSPACE%\aducm4x50\scripts\%1\
IF %ERRORLEVEL% NEQ 0 EXIT %ERRORLEVEL%

REM **** INSTALL DFP KIT INSTALLER ****

python %INSTALLERPATH%\KitInstaller.py install MSKUV-00P-1.0.0-R -m \\nwd1-kitserver\Kits\SoftwareModules --dry-run
IF %ERRORLEVEL% NEQ 0 EXIT %ERRORLEVEL%

ECHO "Installing DFP into Keil"
IF NOT EXIST %DFP_KEIL_PATH% MKDIR %DFP_KEIL_PATH%
CD %DFP_KEIL_PATH%
%WORKSPACE%\aducm4x50\scripts\common\7za.exe e %INSTALLERPATH%\temp\*.zip
%WORKSPACE%\aducm4x50\scripts\common\7za.exe x *.pack -y
DEL *.pack

ECHO "Installing DFP into CCES"
IF NOT EXIST %DFP_CCES_PATH% MKDIR %DFP_CCES_PATH%
CD %DFP_CCES_PATH%
%WORKSPACE%\aducm4x50\scripts\common\7za.exe e %INSTALLERPATH%\temp\*.zip
%WORKSPACE%\aducm4x50\scripts\common\7za.exe x *.pack -y
DEL *.pack

REM ******** INSTALL BSP XCOPY ********

ECHO "Installing BSP into Keil"
IF NOT EXIST %BSP_KEIL_PATH% MKDIR %BSP_KEIL_PATH%
XCOPY /E /Y %WORKSPACE%\aducm4x50\scripts\%1 %BSP_KEIL_PATH%
IF %ERRORLEVEL% NEQ 0 EXIT %ERRORLEVEL%

ECHO "Installing BSP into CCES"
IF NOT EXIST %BSP_CCES_PATH% MKDIR %BSP_CCES_PATH%
XCOPY /E /Y %WORKSPACE%\aducm4x50\scripts\%1 %BSP_CCES_PATH%
IF %ERRORLEVEL% NEQ 0 EXIT %ERRORLEVEL%

REM *************** TTH ***************

CD %WORKSPACE%\TTH\src

IF EXIST %CCES_WORK% RD /S/Q %CCES_WORK%

python TTH.py -v4 -p ADuCM4050 -t config -r %WORKSPACE%\aducm4x50\scripts\%1\ -x bsp_results_keil.xml --toolchain=keil
IF %ERRORLEVEL% NEQ 0 EXIT %ERRORLEVEL%

IF EXIST %WORKSPACE%\aducm4x50\scripts\%1\examples.xml DEL %WORKSPACE%\aducm4x50\scripts\%1\examples.xml

python TTH.py -v4 -p ADuCM4050 -t example -r %WORKSPACE%\aducm4x50\scripts\%1\ -x bsp_results_cces.xml -s target-aducm4050-2000 --debugger=gdb-openocd

EXIT %ERRORLEVEL%
