/*****************************************************************************
 * xisaddrfns.h
 *
 * Copyright (c) 2015 Analog Devices, Inc. All Rights Reserved.
 * This software is proprietary to Analog Devices, Inc. and its
 * licensors.
 *
 * This file is part of libldr, and declares inline functions for checking
 * the type of an address.
 *****************************************************************************/

#ifndef __ADI_LIBLDR_XISADDRFNS_H__
#define __ADI_LIBLDR_XISADDRFNS_H__

#include <stdint.h>

#if defined(_MISRA_2004_RULES)
#pragma diag(push)
#pragma diag(suppress:misra_rule_11_3:"We cast pointer to unsigned integer to compare with literal addresses.")
#endif /* _MISRA_2004_RULES */

#ifdef __ADSPLPBLACKFIN__

/* Return true if the address is within the code region of L1 memory
*/

#pragma inline
static bool is_l1_code_addr(void *_addr) {
  uint32_t targ = (uint32_t)_addr;
#if defined(__ADSPBF70x__)
  return ((0x11a00000uL <= targ) && (targ <= 0x11a0ffffuL));
#elif __NUM_CORES__ > 1
  return (   ((0xff600000uL <= targ) && (targ <= 0xff60ffffuL))
          || ((0xffa00000uL <= targ) && (targ <= 0xffa0ffffuL)));
#else
  return ((0xffa00000uL <= targ) && (targ <= 0xffa0ffffuL));
#endif
}

#endif /* __ADSPLPBLACKFIN__ */

#pragma inline
static bool may_be_cached_code_addr(void *_addr) {
  bool result = 0;
#if defined(__ADSPBF70x__)
  uint32_t targ = (uint32_t)_addr;
  result = !((0x11800000uL <= targ) && (targ <= 0x11b01fffuL)) /* L1 */;
#elif defined(__ADSPBLACKFIN__)
  uint32_t targ = (uint32_t)_addr;
  result = !((0xff400000uL <= targ) && (targ <= 0xffbfffffuL)) /* L1 */;
#elif defined(__ADSP215xx__)
  uint32_t targ = (uint32_t)_addr;
  #pragma word_addressed
  extern uint32_t __l2_uncached_start;
  #pragma word_addressed
  extern uint32_t __l2_uncached_end;
  if ((__l2_uncached_start <= targ) && (targ <= __l2_uncached_end)) {
    result = false;
  } else {
    /* Here we list SHARC's non-L1 execution space in system view.
    ** Don't bother with Mapped-IO, because that can't be in the loader stream.
    */
 #if defined(__ADSP2158x__)
    result = (   ((0x80000000uL <= targ) && (targ <= 0x805fffffuL))   /* NW DDR-A */
              || ((0xc0000000uL <= targ) && (targ <= 0xc05fffffuL))   /* NW DDR-B */
              || ((0x80a00000uL <= targ) && (targ <= 0x80ffffffuL))   /* SW DDR-A */
              || ((0xc0a00000uL <= targ) && (targ <= 0xc0ffffffuL))   /* SW DDR-B */
              || ((0x20080000uL <= targ) && (targ <= 0x200bffffuL))); /* NW and SW L2 RAM */
 #elif defined(__ADSP2157x__)
    result = (   ((0x80000000uL <= targ) && (targ <= 0x805fffffuL))   /* NW DDR-A */
              || ((0x80a00000uL <= targ) && (targ <= 0x80ffffffuL))   /* SW DDR-A */
              || ((0x20000000uL <= targ) && (targ <= 0x200fffffuL))); /* NW and SW L2 RAM */
 #else
  #error Please define non-L1 execution space for your target.
 #endif
  }
#else
// #error Please define is_uncached_addr for your target.
  result = 0; /* Nothing is considered cached */
#endif
  return result;
}

#if defined(_MISRA_2004_RULES)
#pragma diag(pop)
#endif /* _MISRA_2004_RULES */

#endif /* __ADI_LIBLDR_XISADDRFNS_H__ */
