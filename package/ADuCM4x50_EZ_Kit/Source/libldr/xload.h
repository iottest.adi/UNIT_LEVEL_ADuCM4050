/*****************************************************************************
 * xload.h
 *
 * Copyright (c) 2015 Analog Devices, Inc. All Rights Reserved.
 * This software is proprietary to Analog Devices, Inc. and its
 * licensors.
 *
 * This file is part of libldr, and declares support functions needed to load
 * a loader stream.
 *****************************************************************************/

#ifndef __ADI_LIBLDR_XLOAD_H__
#define __ADI_LIBLDR_XLOAD_H__

#include <stdint.h>
#include <stdbool.h>
#include <adi_libldr.h>
#include <adi_bootstream.h>

#include "xisaddrfns.h"
#include "xblocktypes.h"

/* Return true if the checksum for the header block is valid, and false if not.
*/
bool adi_libldr_checksum_okay(HDRTYPE *_hdr);

#if defined(__ADSPBLACKFIN__)
/* Load code to an address in the L1 code section on Blackfin.
*/
ADI_LIBLDR_RESULT write_l1_code(uint8_t *_addr, size_t _blksize, FILE *_fp);
#endif

#endif /* __ADI_LIBLDR_XLOAD_H__ */
