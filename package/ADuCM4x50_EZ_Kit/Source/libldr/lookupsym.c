/*****************************************************************************
 * lookupsym.c
 *
 * Copyright (c) 2015 Analog Devices, Inc. All Rights Reserved.
 * This software is proprietary to Analog Devices, Inc. and its
 * licensors.
 *
 * This file is part of libldr, and implements the LookUpSym() API, for locating
 * functions and data in the .ldr file.
 *****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <adi_processor.h>

#include <adi_libldr.h>

#if defined(_MISRA_2004_RULES)
#pragma diag(push)
#pragma diag(suppress:misra_rule_11_5:"We cast off the constness of pointer to const in the returned symbol address.")
#pragma diag(suppress:misra_rule_17_4:"The symtab parameter is a pointer and we can't avoid pointer arithmetic.")
#pragma diag(suppress:misra_rule_11_3:"Casts from integer to pointer are used for the symbol table's magic numbers.")
#endif /* _MISRA_2004_RULES */


ADI_LIBLDR_RESULT adi_libldr_LookUpSym(const char_t *_symbol,
                                       const void *_symtab,
                                       void **_addr) {
  const adi_libldr_Symbol *sym = (const adi_libldr_Symbol *)_symtab;
  ADI_LIBLDR_RESULT result = ADI_LIBLDR_NO_SYMBOL;
  void **addr = _addr;
  const char_t *symbol = _symbol;

  /* Sanity check on the pointers */
  if (   (symbol == NULL)
      || (sym == NULL)
      || (addr == NULL)) {
    result = ADI_LIBLDR_NULL_PTR;
  } else if (*symbol == '\0') {
    result = ADI_LIBLDR_BAD_SYMBOL;
  } else if (   (sym->symname != ADI_LIBLDR_MAGIC_SYMNAME)
             || (sym->symaddr != ADI_LIBLDR_MAGIC_SYMADDR)) {
    /* Sanity check on the symbol table */
    result = ADI_LIBLDR_NO_SYMTAB;
  } else {
    /* Okay, let's search for the given symbol.
    ** Skip the first entry, since it contains the magic numbers.
    */
    int32_t i;
    for (i = 1; sym[i].symname != NULL; i++) {
      if (strcmp(sym[i].symname, symbol) == 0) {
        *addr = (void *)sym[i].symaddr;
        result = ADI_LIBLDR_SUCCESS;
        break;
      }
    }
  }
  return result;
}

#if defined(_MISRA_2004_RULES)
#pragma diag(pop)
#endif /* _MISRA_2004_RULES */

