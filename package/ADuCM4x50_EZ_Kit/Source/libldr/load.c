/*****************************************************************************
 * load.c
 *
 * Copyright (c) 2015 Analog Devices, Inc. All Rights Reserved.
 * This software is proprietary to Analog Devices, Inc. and its
 * licensors.
 *
 * This file is part of libldr, and implements the Load() API, for processing
 * a boot stream and copying the content into application space.
 *****************************************************************************/


#if defined(_MISRA_2004_RULES)
#pragma diag(push)
#pragma diag(suppress:misra_rule_11_3:"We need to cast an address read from the stream to a pointer.")
#pragma diag(suppress:misra_rule_20_9:"I/O is used to read the loader stream.")
#ifdef LIBLDRDBG
#pragma diag(suppress:misra_rule_6_3:"To permit use of __word_addressed bool.")
#pragma diag(suppress:misra_rule_13_2:"To permit use of __word_addressed bool.")
#endif
#endif /* _MISRA_2004_RULES */

#include <adi_processor.h>
#include <string.h>

// #include "xlibdefs.h"
#ifdef __ADSPBLACKFIN__
#include <ccblkfn.h>
#endif
#if defined(__ADSPSHARC__)
 /* I-cache, BTB and/or conflict cache */
 #include <sys/cache.h>
#endif
#if defined(__ADSPBLACKFIN__)
 /* I-cache */
 #include <cplbtab.h>
#endif

#include "xload.h"

#ifdef LIBLDRDBG
static void log_block(FILE *logfp, const HDRTYPE *hdr, const char_t *msg);
static void log_block(FILE *logfp, const HDRTYPE *hdr, const char_t *msg) {
  if (logfp != NULL) {
    fprintf(logfp, "Code %08x Addr %08x Count %08x Arg %08x %s\n",
            hdr->BLKKIND, hdr->BLKADDR, hdr->BLKSIZE,
            hdr->BLKARGS, msg);
  }
}
#endif

/*
** This file is built in two ways: with and without LIBLDRDBG defined.
** With LIBLDRDBG defined, the function has additional parameters which
** are checked to determine whether:
** - block content should be copied to application space.
** - tracing information should be written to a file pointer.
** With LIBLDRDBG undefined, those parameters are not used, and all the
** tests of those parameters are avoided.
*/

#ifdef LIBLDRDBG
#ifdef __BA_SHARC__
ADI_LIBLDR_RESULT adi_libldr_LoadDbg(FILE *_fp, FILE *_logfp,
                                     __word_addressed bool _do_copy)
#else
ADI_LIBLDR_RESULT adi_libldr_LoadDbg(FILE *_fp, FILE *_logfp, bool _do_copy)
#endif
#else
ADI_LIBLDR_RESULT adi_libldr_Load(FILE *_fp)
#define do_copy (true)
#endif /* LIBLDRDBG */
{
  HDRTYPE hdr;
  ADI_LIBLDR_RESULT result = ADI_LIBLDR_SUCCESS;
  bool do_code_cache_invalidate = false;
  FILE *fp = _fp;
#ifdef LIBLDRDBG
  FILE *logfp = _logfp;
  bool do_copy = (bool)_do_copy;
#endif

  if (fp == NULL) {

    result = ADI_LIBLDR_NULL_PTR;

  } else {
    size_t n;

    do {
      n = fread(&hdr, sizeof(hdr), 1U, fp);
      if ((n != 0U) && (n != 1U)) {
        result =  ADI_LIBLDR_READ_ERROR;
      } else if (!adi_libldr_checksum_okay(&hdr)) {
#ifdef LIBLDRDBG
        log_block(logfp, &hdr, "(checksum)");
#endif
        result = ADI_LIBLDR_CHECKSUM_ERROR;
      } else {
        void *baddr = (void *)hdr.BLKADDR;
        uint32_t blksize = ((uint32_t)hdr.BLKSIZE / (uint32_t)BYTES_PER_WORD);
        if (blksize > 0U) {
          if (is_fill_block((uint32_t)hdr.BLKKIND)) {
#ifdef LIBLDRDBG
            log_block(logfp, &hdr, "(fill)");
#endif
            if (do_copy) {
              /* Fill a block with a given value */
              if (may_be_cached_code_addr(baddr)) {
                do_code_cache_invalidate = true;
              }
              memset(baddr, (int32_t)hdr.BLKARGS, blksize);
            }
          } else if (is_unsupported_block((uint32_t)hdr.BLKKIND)) {
#ifdef LIBLDRDBG
            log_block(logfp, &hdr, "(unsupported)");
            /* Treat as a skip block */
            if (fseek(fp, (int32_t)blksize, SEEK_CUR) < 0) {
              result = ADI_LIBLDR_SEEK_ERROR;
            }
#else
            result = ADI_LIBLDR_UNSUPP_BLOCK;
#endif
          } else if (is_skip_block((uint32_t)hdr.BLKKIND)) {
#ifdef LIBLDRDBG
            log_block(logfp, &hdr, "(skip)");
#endif
            /* Skip over this many bytes in the file. */
            if (fseek(fp, (int32_t)blksize, SEEK_CUR) < 0) {
              result = ADI_LIBLDR_SEEK_ERROR;
            }
          } else if (is_normal_block((uint32_t)hdr.BLKKIND)) {
#ifdef LIBLDRDBG
            log_block(logfp, &hdr, "(normal)");
#endif
            if (do_copy) {
              /* Copy the block contents from the file to memory */
#ifdef __ADSPSC58x__
              if (((uint32_t)baddr & 0xff000000uL) == 0x28000000uL) {
                /* workaround for CCES-13995 - use internal view address rather than
                ** system view, else fread doesn't work.
                */
                uint32_t iaddr = (uint32_t)baddr;
                iaddr = iaddr & (~(0x28000000uL));
                baddr = (void *)iaddr;
              }
#endif
#ifdef __ADSPBLACKFIN__
              if (is_l1_code_addr(baddr)) {
                result = write_l1_code(baddr, blksize, fp);
                do_code_cache_invalidate = true;
              } else
#endif
              {
                if (may_be_cached_code_addr(baddr)) {
                  do_code_cache_invalidate = true;
                }
                if (fread(baddr, 1U, blksize, fp) != blksize) {
                  result = ADI_LIBLDR_READ_ERROR;
                }
              }
            } else {
              /* Skip over this many bytes in the file. */
              if (fseek(fp, (int32_t)blksize, SEEK_CUR) < 0) {
                result = ADI_LIBLDR_SEEK_ERROR;
              }
            }
          } else {
            /* We wouldn't expect to get here. */
#ifdef LIBLDRDBG
            log_block(logfp, &hdr, "(unrecognized)");
#endif
            result = ADI_LIBLDR_UNSUPP_BLOCK;
          }
        }
      }
    } while ((result == ADI_LIBLDR_SUCCESS) && (n > 0U) &&
             (!is_final_block((uint32_t)hdr.BLKKIND)));

  }

  if (do_code_cache_invalidate) {
    adi_code_cache_invalidate();
  }

  return result;
}

#if defined(_MISRA_2004_RULES)
#pragma diag(pop)
#endif /* _MISRA_2004_RULES */
