/*****************************************************************************
 * checksum.c
 *
 * Copyright (c) 2015 Analog Devices, Inc. All Rights Reserved.
 * This software is proprietary to Analog Devices, Inc. and its
 * licensors.
 *
 * This file is part of libldr, and implements the checksum API, for verifying
 * that a block has a valid checksum.
 *****************************************************************************/

#include <stdio.h>
#include <sys/platform.h>
#include <adi_bootstream.h>
#include "xload.h"

#if defined(_MISRA_2004_RULES)
#pragma diag(push)
#pragma diag(suppress:misra_rule_11_4:"We need to treat the header block as a byte stream.")
#pragma diag(suppress:misra_rule_17_4:"We need to do pointer arithmetic since we can't use an array for the byte stream.")
#endif /* _MISRA_2004_RULES */

bool adi_libldr_checksum_okay(HDRTYPE *_hdr) {
  /* The checksum is the XOR of all 16 bytes in the header
  ** aside from the HDRCHK field itself.
  */
  uint32_t i;
#if !defined(__ADSP21000__) || defined(__BYTE_ADDRESSING__)
  const uint8_t *ptr = (const uint8_t *)_hdr;
  uint8_t cs = ptr[0] ^ ptr[1];
  for (i = 3U; i < sizeof(HDRTYPE); i++) {
    cs ^= ptr[i];
  }
  return cs == ptr[2];
#else
  /* bytes are packed four to a word. Not used for SHARC+, as we only need the
  ** byte-addressed entry point.
  */
  const uint32_t *ptr = (const uint32_t *)_hdr;
  uint32_t cs = ptr[0] ^ (ptr[0] >> 8U);
  cs ^= (ptr[0] >> 24);
  for (i = 1U; i < sizeof(HDRTYPE); i++) {
    cs ^= ptr[i];
    cs ^= (ptr[i] >> 8U);
    cs ^= (ptr[i] >> 16U);
    cs ^= (ptr[i] >> 24U);
  }
  cs &= 0xff;
  return cs == ((ptr[0] >> 16U) & 0xff);
#endif
}

#if defined(_MISRA_2004_RULES)
#pragma diag(pop)
#endif /* _MISRA_2004_RULES */
