/*****************************************************************************
 * xblocktypes.h
 *
 * Copyright (c) 2015 Analog Devices, Inc. All Rights Reserved.
 * This software is proprietary to Analog Devices, Inc. and its
 * licensors.
 *
 * This file is part of libldr, and declares inline functions for determining
 * the type of the block in a boot stream.
 *****************************************************************************/

#ifndef __ADI_LIBLDR_XBLOCKTYPES_H
#define __ADI_LIBLDR_XBLOCKTYPES_H

/* Identifying the kind of blocks. */

#pragma inline
#if !defined(ADI_ADSP_CM41X) && !defined(ADI_ADSP_CM4X5X)
#pragma const
#endif
static bool is_normal_block(uint32_t _flags) {
  uint32_t other_types =   BFLG_INDIRECT
                         | BFLG_INIT
                         | BFLG_IGNORE
                         | BFLG_CALLBACK
                         | BFLG_FILL
                         ;
  return (_flags & other_types) == 0U;
}

#pragma inline
#if !defined(ADI_ADSP_CM41X) && !defined(ADI_ADSP_CM4X5X)
#pragma const
#endif
static bool is_unsupported_block(uint32_t _flags) {
  uint32_t bad_types =   BFLG_INDIRECT
                       | BFLG_INIT
                       | BFLG_CALLBACK
                       | BFLG_QUICKBOOT
                       | BFLG_SAVE
                       /* Forward? */
                       ;
  return (_flags & bad_types) != 0U;
}

#pragma inline
#if !defined(ADI_ADSP_CM41X) && !defined(ADI_ADSP_CM4X5X)
#pragma const
#endif
static bool is_fill_block(uint32_t _flags) {
  return (_flags & BFLG_FILL) != 0U;
}

#pragma inline
#if !defined(ADI_ADSP_CM41X) && !defined(ADI_ADSP_CM4X5X)
#pragma const
#endif
static bool is_skip_block(uint32_t _flags) {
  return (_flags & BFLG_IGNORE) != 0U;
}

#pragma inline
#if !defined(ADI_ADSP_CM41X) && !defined(ADI_ADSP_CM4X5X)
#pragma const
#endif
static bool is_final_block(uint32_t _flags) {
  return (_flags & BFLG_FINAL) != 0U;
}

#endif /* __ADI_LIBLDR_XBLOCKTYPES_H__ */
