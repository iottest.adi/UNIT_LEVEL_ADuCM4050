/*****************************************************************************
 * move.c
 *
 * Copyright (c) 2016 Analog Devices, Inc. All Rights Reserved.
 * This software is proprietary to Analog Devices, Inc. and its
 * licensors.
 *
 * This file is part of libldr, and implements the Move() API, for processing
 * a boot stream and copying the content into application space.
 *****************************************************************************/


#if defined(_MISRA_2004_RULES)
#pragma diag(push)
#pragma diag(suppress:misra_rule_11_3:"We need to cast an address read from the stream to a pointer.")
#pragma diag(suppress:misra_rule_11_4:"We need to cast the stream address to a block pointer.")
#pragma diag(suppress:misra_rule_17_4:"We increment a pointer by the size of the block and the payload.")
#ifdef LIBLDRDBG
#pragma diag(suppress:misra_rule_20_9:"I/O is used for debugging output, in non-production code.")
#pragma diag(suppress:misra_rule_6_3:"To permit use of __word_addressed bool.")
#pragma diag(suppress:misra_rule_13_2:"To permit use of __word_addressed bool.")
#endif
#endif /* _MISRA_2004_RULES */

#if defined(__ADSPSHARC__) && !defined(__BYTE_ADDRESSING__)
#error adi_libldr_Move APIs only defined for byte-addressed builds
#endif

#include <stdio.h>
#include <adi_processor.h>
#include <string.h>

// #include "xlibdefs.h"
#ifdef __ADSPBLACKFIN__
#include <ccblkfn.h>
#endif
#if defined(__ADSPSHARC__)
 /* I-cache, BTB and/or conflict cache */
 #include <sys/cache.h>
#endif
#if defined(__ADSPBLACKFIN__)
 /* I-cache */
 #include <cplbtab.h>
#endif

#include "xload.h"

#ifdef LIBLDRDBG
static void log_block(FILE *logfp, const HDRTYPE *hdr, const char_t *msg);
static void log_block(FILE *logfp, const HDRTYPE *hdr, const char_t *msg) {
  if (logfp != NULL) {
    fprintf(logfp, "Code %08x Addr %08x Count %08x Arg %08x %s\n",
            hdr->BLKKIND, hdr->BLKADDR, hdr->BLKSIZE,
            hdr->BLKARGS, msg);
  }
}
#endif

/*
** This file is built in two ways: with and without LIBLDRDBG defined.
** With LIBLDRDBG defined, the function has additional parameters which
** are checked to determine whether:
** - block content should be copied to application space.
** - tracing information should be written to a file pointer.
** With LIBLDRDBG undefined, those parameters are not used, and all the
** tests of those parameters are avoided.
*/

#ifdef LIBLDRDBG
#ifdef __BA_SHARC__
ADI_LIBLDR_RESULT adi_libldr_MoveDbg(char_t *_cp, FILE *_logfp,
                                     __word_addressed bool _do_copy)
#else
ADI_LIBLDR_RESULT adi_libldr_MoveDbg(char_t *_cp, FILE *_logfp, bool _do_copy)
#endif
#else
ADI_LIBLDR_RESULT adi_libldr_Move(char_t *_cp)
#define do_copy (true)
#endif /* LIBLDRDBG */
{
  HDRTYPE *hdr;
  ADI_LIBLDR_RESULT result = ADI_LIBLDR_SUCCESS;
  bool do_code_cache_invalidate = false;
  char_t *cp = _cp;
#ifdef LIBLDRDBG
  FILE *logfp = _logfp;
  bool do_copy = (bool)_do_copy;
#endif

  if (cp == NULL) {

    result = ADI_LIBLDR_NULL_PTR;

  } else {

    do {
      hdr = (HDRTYPE *)cp;
      cp += sizeof(HDRTYPE);
      if (!adi_libldr_checksum_okay(hdr)) {
#ifdef LIBLDRDBG
        log_block(logfp, hdr, "(checksum)");
#endif
        result = ADI_LIBLDR_CHECKSUM_ERROR;
      } else {
        void *baddr = (void *)(hdr->BLKADDR);
        uint32_t blksize = ((uint32_t)(hdr->BLKSIZE) / (uint32_t)BYTES_PER_WORD);
        if (blksize > 0U) {
          if (is_fill_block((uint32_t)(hdr->BLKKIND))) {
#ifdef LIBLDRDBG
            log_block(logfp, hdr, "(fill)");
#endif
            if (do_copy) {
              /* Fill a block with a given value */
              if (may_be_cached_code_addr(baddr)) {
                do_code_cache_invalidate = true;
              }
              memset(baddr, (int32_t)(hdr->BLKARGS), blksize);
            }
          } else if (is_unsupported_block((uint32_t)(hdr->BLKKIND))) {
#ifdef LIBLDRDBG
            log_block(logfp, hdr, "(unsupported)");
            /* Treat as a skip block */
            cp += (size_t)blksize;
#else
            result = ADI_LIBLDR_UNSUPP_BLOCK;
#endif
          } else if (is_skip_block((uint32_t)(hdr->BLKKIND))) {
#ifdef LIBLDRDBG
            log_block(logfp, hdr, "(skip)");
#endif
            /* Skip over this many bytes in the file. */
            cp += (size_t)blksize;
          } else if (is_normal_block((uint32_t)(hdr->BLKKIND))) {
#ifdef LIBLDRDBG
            log_block(logfp, hdr, "(normal)");
#endif
            if (do_copy) {
              /* Copy the block contents from the file to memory */
#ifdef __ADSPBLACKFIN__
              if (is_l1_code_addr(baddr)) {
                (void)_memcpy_l1(baddr, cp, blksize);
                do_code_cache_invalidate = true;
              } else
#endif
              {
                if (may_be_cached_code_addr(baddr)) {
                  do_code_cache_invalidate = true;
                }
                (void)memcpy(baddr, cp, blksize);
              }
            }
            cp += (size_t)blksize;
          } else {
            /* We wouldn't expect to get here. */
#ifdef LIBLDRDBG
            log_block(logfp, hdr, "(unrecognized)");
#endif
            result = ADI_LIBLDR_UNSUPP_BLOCK;
          }
        }
      }
    } while ((result == ADI_LIBLDR_SUCCESS) &&
             (!is_final_block((uint32_t)(hdr->BLKKIND))));

  }

  if (do_code_cache_invalidate) {
    adi_code_cache_invalidate();
  }

  return result;
}

#if defined(_MISRA_2004_RULES)
#pragma diag(pop)
#endif /* _MISRA_2004_RULES */
