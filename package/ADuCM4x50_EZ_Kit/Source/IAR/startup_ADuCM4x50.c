/*!
 *****************************************************************************
 * @file:    startup_ADuCM4050.c
 * @brief:   Interrupt table and default handlers for ADuCM4x50
 *-----------------------------------------------------------------------------
 *
Copyright (c) 2016 Analog Devices, Inc.

All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
  - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  - Modified versions of the software must be conspicuously marked as such.
  - This software is licensed solely and exclusively for use with processors
    manufactured by or for Analog Devices, Inc.
  - This software may not be combined or merged with other code in any manner
    that would cause the software to become subject to terms and conditions
    which differ from those listed here.
  - Neither the name of Analog Devices, Inc. nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
  - The use of this software may or may not infringe the patent rights of one
    or more patent holders.  This license does not release you from the
    requirement that you obtain separate licenses from these patent holders
    to use this software.

THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT,
TITLE, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
NO EVENT SHALL ANALOG DEVICES, INC. OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF CLAIMS OF INTELLECTUAL
PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/

#include <startup_ADuCM4050.h>
#include <system_ADuCM4050.h>

/*
    Stack size considerations for RTOS environments:

    In the Cortex-M class processors, the hardware has two stacks: the main stack
    and the process stack.  The main stack is used at reset and by system exception
    handlers.  In simple applications (non-RTOS) you may only use this stack.  In
    more complex systems that use an RTOS you have a split between the main stack
    which is used by the kernel and exception handlers and the process stack which
    is used by the tasks and programmable interrupts (interrupts stack on top of
    the currently active task, so tasks stacks should be allocated for worst-case
    interrupt scenarios).  In the uCOS case, the process stack is the one that gets
    context-switched and used by the tasks, where we store the registers, etc.

    As RTOS tasks specify their own stack area, a minimal size of 4 for the process
    stack in order to retain symbols, etc., seems OK and we would not need any more.
    The main stack needs to be big enough to initialize the RTOS, run interrupts etc.

    As always, stack and heap size optimizations is a balancing act between usage
    and available memory.  It is entirely application-dependant.

*/


/*
    The minimal vector table for a Cortex M3.  Note that the proper constructs
    must be placed on this to ensure that it ends up at physical address
    0x0000.0000.
*/

#ifdef __ICCARM__
/*
* IAR MISRA C 2004 error suppressions:
*
* Pm093 (rule 18.4): use of union - overlapping storage shall not be used.
*    Required for interrupt vector table entries.
*
* Pm140 (rule 11.3): a cast should not be performed between a pointer type and an integral type
*   The rule makes an exception for memory-mapped register accesses.
*/
#pragma diag_suppress=Pm093,Pm140
#endif /* __ICCARM__ */

/* IVT typedefs. */
typedef void  ( *IntFunc )( void );

typedef union { void * __ptr;  IntFunc __ifunc; } IntVector;

/* Forward declaration of the system exceptions. */
WEAK_FUNCTION( NmiSR                        )
WEAK_FUNCTION( HardFault_Handler            )
WEAK_FUNCTION( MemManage_Handler            )
WEAK_FUNCTION( BusFault_Handler             )
WEAK_FUNCTION( UsageFault_Handler           )
WEAK_FUNCTION( SVC_Handler                  )
WEAK_FUNCTION( DebugMon_Handler             )
WEAK_FUNCTION( PENDSV_HANDLER               )
WEAK_FUNCTION( SYSTICK_HANDLER              )

/* Forward declaration of the programmable device interrupt handlers. */
WEAK_FUNCTION( RTC1_Int_Handler             )
WEAK_FUNCTION( Ext_Int0_Handler             )
WEAK_FUNCTION( Ext_Int1_Handler             )
WEAK_FUNCTION( Ext_Int2_Handler             )
WEAK_FUNCTION( Ext_Int3_Handler             )
WEAK_FUNCTION( WDog_Tmr_Int_Handler         )
WEAK_FUNCTION( Vreg_over_Int_Handler        )
WEAK_FUNCTION( Battery_Voltage_Int_Handler  )
WEAK_FUNCTION( RTC0_Int_Handler             )
WEAK_FUNCTION( GPIO_A_Int_Handler           )
WEAK_FUNCTION( GPIO_B_Int_Handler           )
WEAK_FUNCTION( GP_Tmr0_Int_Handler          )
WEAK_FUNCTION( GP_Tmr1_Int_Handler          )
WEAK_FUNCTION( Flash0_Int_Handler           )
WEAK_FUNCTION( UART0_Int_Handler            )
WEAK_FUNCTION( SPI0_Int_Handler             )
WEAK_FUNCTION( SPI2_Int_Handler             )
WEAK_FUNCTION( I2C0_Slave_Int_Handler       )
WEAK_FUNCTION( I2C0_Master_Int_Handler      )
WEAK_FUNCTION( DMA_Err_Int_Handler          )
WEAK_FUNCTION( DMA_SPIH_TX_Int_Handler      )
WEAK_FUNCTION( DMA_SPIH_RX_Int_Handler      )
WEAK_FUNCTION( DMA_SPORT0A_Int_Handler      )
WEAK_FUNCTION( DMA_SPORT0B_Int_Handler      )
WEAK_FUNCTION( DMA_SPI0_TX_Int_Handler      )
WEAK_FUNCTION( DMA_SPI0_RX_Int_Handler      )
WEAK_FUNCTION( DMA_SPI1_TX_Int_Handler      )
WEAK_FUNCTION( DMA_SPI1_RX_Int_Handler      )
WEAK_FUNCTION( DMA_UART0_TX_Int_Handler     )
WEAK_FUNCTION( DMA_UART0_RX_Int_Handler     )
WEAK_FUNCTION( DMA_I2C0_STX_Int_Handler     )
WEAK_FUNCTION( DMA_I2C0_SRX_Int_Handler     )
WEAK_FUNCTION( DMA_I2C0_MX_Int_Handler      )
WEAK_FUNCTION( DMA_AES0_IN_Int_Handler      )
WEAK_FUNCTION( DMA_AES0_OUT_Int_Handler     )
WEAK_FUNCTION( DMA_FLASH0_Int_Handler       )
WEAK_FUNCTION( SPORT0A_Int_Handler          )
WEAK_FUNCTION( SPORT0B_Int_Handler          )
WEAK_FUNCTION( Crypto_Int_Handler           )
WEAK_FUNCTION( DMA_ADC0_Int_Handler         )
WEAK_FUNCTION( GP_Tmr2_Int_Handler          )
WEAK_FUNCTION( Crystal_osc_Int_Handler      )
WEAK_FUNCTION( SPI1_Int_Handler             )
WEAK_FUNCTION( PLL_Int_Handler              )
WEAK_FUNCTION( RNG_Int_Handler              )
WEAK_FUNCTION( Beep_Int_Handler             )
/** Placeholder: IRQn = 46 is reserved on the ADuCM4x50  */
/** Placeholder: IRQn = 47 is reserved on the ADuCM4x50  */
/** Placeholder: IRQn = 48 is reserved on the ADuCM4x50  */
/** Placeholder: IRQn = 49 is reserved on the ADuCM4x50  */
/** Placeholder: IRQn = 50 is reserved on the ADuCM4x50  */
/** Placeholder: IRQn = 51 is reserved on the ADuCM4x50  */
/** Placeholder: IRQn = 52 is reserved on the ADuCM4x50  */
/** Placeholder: IRQn = 53 is reserved on the ADuCM4x50  */
/** Placeholder: IRQn = 54 is reserved on the ADuCM4x50  */
/** Placeholder: IRQn = 55 is reserved on the ADuCM4x50  */
WEAK_FUNCTION( DMA_SIP0_Int_Handler         )
WEAK_FUNCTION( DMA_SIP1_Int_Handler         )
WEAK_FUNCTION( DMA_SIP2_Int_Handler         )
WEAK_FUNCTION( DMA_SIP3_Int_Handler         )
WEAK_FUNCTION( DMA_SIP4_Int_Handler         )
WEAK_FUNCTION( DMA_SIP5_Int_Handler         )
WEAK_FUNCTION( DMA_SIP6_Int_Handler         )
WEAK_FUNCTION( DMA_SIP7_Int_Handler         )
WEAK_FUNCTION( Ext_Int4_Handler             )
WEAK_FUNCTION( Ext_Int5_Handler             )
WEAK_FUNCTION( UART1_Int_Handler            )
WEAK_FUNCTION( DMA_UART1_TX_Int_Handler     )
WEAK_FUNCTION( DMA_UART1_RX_Int_Handler     )
WEAK_FUNCTION( RGB_Tmr_Int_Handler          )
WEAK_FUNCTION( RESERVED_VECTOR              )
/* Interrupt Vector Table */
#pragma segment="CSTACK"
#pragma location = ".intvec"
const IntVector __vector_table[] =
{
    /* Grab stack pointer "end" address from ICF file */
    { .__ptr = __sfe( "CSTACK" )            },

    /* Exception mappings */
    { .__ifunc = ResetISR                   } ,  /* -15 */
    { .__ifunc = NmiSR                      } ,  /* -14 */
    { .__ifunc = HardFault_Handler          } ,  /* -13 */
    { .__ifunc = MemManage_Handler          } ,  /* -12 */
    { .__ifunc = BusFault_Handler           } ,  /* -11 */
    { .__ifunc = UsageFault_Handler         } ,  /* -10 */
    { .__ifunc = RESERVED_VECTOR            } ,  /* -09 */
    { .__ifunc = RESERVED_VECTOR            } ,  /* -08 */
    { .__ifunc = RESERVED_VECTOR            } ,  /* -07 */
    { .__ifunc = RESERVED_VECTOR            } ,  /* -06 */
    { .__ifunc = SVC_Handler                } ,  /* -05 */
    { .__ifunc = DebugMon_Handler           } ,  /* -04 */
    { .__ifunc = RESERVED_VECTOR            } ,  /* -03 */
    { .__ifunc = PENDSV_HANDLER             } ,  /* -02  (Micrium overrides weak binding if using uCOS)  */
    { .__ifunc = SYSTICK_HANDLER            } ,  /* -01  (Micrium overrides weak binding if using uCOS)  */
    
    /* Programmable interrupt mappings... */

    {.__ifunc = RTC1_Int_Handler            } ,  /* 0  */
    {.__ifunc = Ext_Int0_Handler            } ,  /* 1  */
    {.__ifunc = Ext_Int1_Handler            } ,  /* 2  */
    {.__ifunc = Ext_Int2_Handler            } ,  /* 3  */ 
    {.__ifunc = Ext_Int3_Handler            } ,  /* 4  */
    {.__ifunc = WDog_Tmr_Int_Handler        } ,  /* 5  */
    {.__ifunc = Vreg_over_Int_Handler       } ,  /* 6  */
    {.__ifunc = Battery_Voltage_Int_Handler } ,  /* 7  */
    {.__ifunc = RTC0_Int_Handler            } ,  /* 8  */
    {.__ifunc = GPIO_A_Int_Handler          } ,  /* 9  */
    {.__ifunc = GPIO_B_Int_Handler          } ,  /* 10 */
    {.__ifunc = GP_Tmr0_Int_Handler         } ,  /* 11 */
    {.__ifunc = GP_Tmr1_Int_Handler         } ,  /* 12 */
    {.__ifunc = Flash0_Int_Handler          } ,  /* 13 */
    {.__ifunc = UART0_Int_Handler           } ,  /* 14 */
    {.__ifunc = SPI0_Int_Handler            } ,  /* 15 */
    {.__ifunc = SPI2_Int_Handler            } ,  /* 16 */
    {.__ifunc = I2C0_Slave_Int_Handler      } ,  /* 17 */
    {.__ifunc = I2C0_Master_Int_Handler     } ,  /* 18 */
    {.__ifunc = DMA_Err_Int_Handler         } ,  /* 19 */
    {.__ifunc = DMA_SPIH_TX_Int_Handler     } ,  /* 20 */
    {.__ifunc = DMA_SPIH_RX_Int_Handler     } ,  /* 21 */ 
    {.__ifunc = DMA_SPORT0A_Int_Handler     } ,  /* 22 */
    {.__ifunc = DMA_SPORT0B_Int_Handler     } ,  /* 23 */
    {.__ifunc = DMA_SPI0_TX_Int_Handler     } ,  /* 24 */
    {.__ifunc = DMA_SPI0_RX_Int_Handler     } ,  /* 25 */
    {.__ifunc = DMA_SPI1_TX_Int_Handler     } ,  /* 26 */
    {.__ifunc = DMA_SPI1_RX_Int_Handler     } ,  /* 27 */
    {.__ifunc = DMA_UART0_TX_Int_Handler    } ,  /* 28 */
    {.__ifunc = DMA_UART0_RX_Int_Handler    } ,  /* 29 */
    {.__ifunc = DMA_I2C0_STX_Int_Handler    } ,  /* 30 */
    {.__ifunc = DMA_I2C0_SRX_Int_Handler    } ,  /* 31 */
    {.__ifunc = DMA_I2C0_MX_Int_Handler     } ,  /* 32 */
    {.__ifunc = DMA_AES0_IN_Int_Handler     } ,  /* 33 */
    {.__ifunc = DMA_AES0_OUT_Int_Handler    } ,  /* 34 */
    {.__ifunc = DMA_FLASH0_Int_Handler      } ,  /* 35 */
    {.__ifunc = SPORT0A_Int_Handler         } ,  /* 36 */
    {.__ifunc = SPORT0B_Int_Handler         } ,  /* 37 */
    {.__ifunc = Crypto_Int_Handler          } ,  /* 38 */
    {.__ifunc = DMA_ADC0_Int_Handler        } ,  /* 39 */    
    {.__ifunc = GP_Tmr2_Int_Handler         } ,  /* 40 */
    {.__ifunc = Crystal_osc_Int_Handler     } ,  /* 41 */
    {.__ifunc = SPI1_Int_Handler            } ,  /* 42 */
    {.__ifunc = PLL_Int_Handler             } ,  /* 43 */
    {.__ifunc = RNG_Int_Handler             } ,  /* 44 */
    {.__ifunc = Beep_Int_Handler            } ,  /* 45 */    
    {.__ifunc = RESERVED_VECTOR             } ,  /* 46 */
    {.__ifunc = RESERVED_VECTOR             } ,  /* 47 */
    {.__ifunc = RESERVED_VECTOR             } ,  /* 48 */
    {.__ifunc = RESERVED_VECTOR             } ,  /* 49 */
    {.__ifunc = RESERVED_VECTOR             } ,  /* 50 */
    {.__ifunc = RESERVED_VECTOR             } ,  /* 51 */
    {.__ifunc = RESERVED_VECTOR             } ,  /* 52 */
    {.__ifunc = RESERVED_VECTOR             } ,  /* 53 */
    {.__ifunc = RESERVED_VECTOR             } ,  /* 54 */
    {.__ifunc = RESERVED_VECTOR             } ,  /* 55 */    
    {.__ifunc = DMA_SIP0_Int_Handler        } ,  /* 56 */
    {.__ifunc = DMA_SIP1_Int_Handler        } ,  /* 57 */
    {.__ifunc = DMA_SIP2_Int_Handler        } ,  /* 58 */
    {.__ifunc = DMA_SIP3_Int_Handler        } ,  /* 59 */
    {.__ifunc = DMA_SIP4_Int_Handler        } ,  /* 60 */
    {.__ifunc = DMA_SIP5_Int_Handler        } ,  /* 61 */
    {.__ifunc = DMA_SIP6_Int_Handler        } ,  /* 62 */
    {.__ifunc = DMA_SIP7_Int_Handler        } ,  /* 63 */
    {.__ifunc = Ext_Int4_Handler            } ,  /* 64 */
    {.__ifunc = Ext_Int5_Handler            } ,  /* 65 */
    {.__ifunc = UART1_Int_Handler           } ,  /* 66 */
    {.__ifunc = DMA_UART1_TX_Int_Handler    } ,  /* 67 */
    {.__ifunc = DMA_UART1_RX_Int_Handler    } ,  /* 68 */
    {.__ifunc = RGB_Tmr_Int_Handler         }    /* 69 */
};

 #pragma location="ReadProtectedKeyHash"
__root const uint32_t ReadProKeyHsh[] =
    { 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu, 0xFFFFFFFFu };        

 #pragma location="CRC_ReadProtectedKeyHash"
__root const uint32_t CrcOfReadKey = 0xA79C3203u;        

 #pragma location="NumCRCPages"
__root const uint32_t NumOfCRCPages = 0u;  

#ifdef __ARMCC_VERSION
/* We want a warning if semi-hosting libraries are used. */
#pragma import(__use_no_semihosting_swi)
#endif

#ifdef __GNUC__
/* Symbols defined by the GNU linker. */
extern unsigned long _etext;
extern unsigned long _data;
extern unsigned long _edata;
extern unsigned long _bss ;
extern unsigned long _ebss;
#endif

#ifdef __ICCARM__
/* IAR system startup functions. */
WEAK_PROTO( void __iar_init_core (void));
WEAK_PROTO( void __iar_init_vfp  (void));
extern void __cmain(void);
#else
/* Application Entry point. */
extern int main(void);
#endif /* __ICCARM__*/


/******************************************************************************
* Function    : ResetISR (-15)
* Description : Reset event handler
******************************************************************************/

WEAK_FUNC (void ResetISR (void)) {

    /* Reset the main SP to clean up any leftovers from the boot rom. */
    __set_MSP( (uint32_t) __vector_table[0].__ptr );

#if 0 /*  TODO SRAM retention to be worked out. */   
    /* Unlock the PWRMOD register by writing the two keys to the PWRKEY register. */
    pADI_PMG0->PWRKEY = PWRKEY_VALUE_KEY;
    pADI_PMG0->SRAMRET &= ~(BITM_PMG_SRAMRET_RET1 | BITM_PMG_SRAMRET_RET2 | BITM_PMG_SRAMRET_RET3 | BITM_PMG_SRAMRET_RET4);
    /* Set the RAM0_RET bit so the entire 8K of SRAM Bank0 is hibernate-preserved. */

    adi_system_EnableRetention(ADI_SRAM_BANK_1, true);
#endif
    
/* To disable the instruction SRAM and entire 64K of SRAM is used as DSRAM. */    
#ifdef  ADI_DISABLE_INSTRUCTION_SRAM
    adi_system_EnableISRAM(false);
#endif    

    
/* To enable the instruction cache.  */    
#ifdef  ENABLE_CACHE
    adi_system_EnableCache(true);    
#endif    

#ifdef __GNUC__

    unsigned long *pulSrc, *pulDest;

    /* Copy initialised data from flash into RAM. */
    pulSrc = &_etext;
    for (pulDest = &_data; pulDest < &_edata; )
    {
        *pulDest++ = *pulSrc++;
    }

    /* Clear the bss segment. */
    for (pulDest = &_bss; pulDest < &_ebss; )
    {
        *pulDest++ = 0;
    }

    /* Clock initialization */
    SystemInit();
    
    /* Call application main directly. */
    main();

#elif __ICCARM__

    /* Clock initialization */
    SystemInit();
    
    /* Call IAR system startup. */
    __iar_init_core();
    __iar_init_vfp();
    __cmain();

#else

    /* Clock initialization */
    SystemInit();
    
    /* Call application main directly. */
    main();

#endif

    /* Stick here if main returns. */
    while(1) {}
}
