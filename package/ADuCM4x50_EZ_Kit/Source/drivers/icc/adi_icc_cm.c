/*! *****************************************************************************
 * @file    adi_icc_cm.c
 * @brief   Inter-core communication API implementation for Cortex.
 * @details The APIs in this file can only be called from the Cortex core. 
 *
 * @note    Downloading the code/data to SSP IRAM and DRAM is handled outside 
 *          the driver and is not in scope of the driver.
 -----------------------------------------------------------------------------
Copyright (c) 2016 Analog Devices, Inc.

All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
  - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  - Modified versions of the software must be conspicuously marked as such.
  - This software is licensed solely and exclusively for use with processors
    manufactured by or for Analog Devices, Inc.
  - This software may not be combined or merged with other code in any manner
    that would cause the software to become subject to terms and conditions
    which differ from those listed here.
  - Neither the name of Analog Devices, Inc. nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
  - The use of this software may or may not infringe the patent rights of one
    or more patent holders.  This license does not release you from the
    requirement that you obtain separate licenses from these patent holders
    to use this software.

THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-
INFRINGEMENT, TITLE, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL ANALOG DEVICES, INC. OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF
CLAIMS OF INTELLECTUAL PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*****************************************************************************/

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>
#include "adi_icc_cm_data.h"
#include <adi_icc_config.h>
#include <drivers/icc/adi_icc_cm.h>
#include "adi_icc_cm_data.c"

/*! @addtogroup ICC_Driver
 *  @{
 * 
 *  @brief    Inter Core Communication driver for Cortex Core.
 *  @note     The application must include drivers/icc/adi_icc_cm.h to use this driver
 *  @details  This file should be only included in the Cortex application. Including it
 *            in the SSP application will result in error. The SSP application should 
 *            include drivers/icc/adi_icc_ssp.h header file.
 *    
 */
 
#ifdef __ICCARM__
/*
* IAR MISRA C 2004 error suppressions.
*
* Pm073 (rule 14.7): A function should have a single point of exit.
* Pm143 (rule 14.7): A function should have a single point of exit at the end of the function.
*                    Multiple returns are used for error handling.
*       
* Pm088 (rule 17.4): Pointer arithmetic should not be used.
*                    Relying on pointer arithmetic for buffer handling.
*
* Pm123 (rule 18.5): There shall be no definition of objects in a header file.
*
* Pm140 (rule 11.3): A cast should not be performed between a pointer type and an integral type.
*                    MMR addresses are defined as simple constants. Accessing the MMR requires casting to a pointer type.
*
* Pm152 (rule 17.4): Array indexing shall only be applied to objects defined as an array type.
*                    Relying on pointer arithmetic for buffer handling is needed.
*
*/
#pragma diag_suppress=Pm073,Pm088,Pm123,Pm140,Pm143,Pm152
#endif /* __ICCARM__ */

/**
 * @brief    Open the ICC driver.
 *
 * @param [in]     nDeviceNum    The zero-based device instance number of ICC device to be opened.
 * @param [in]     pMemory       Application supplied memory space for use by the driver.
 * @param [in]     nMemorySize   Size of the application supplied memory (in bytes).
 * @param [in,out] phDevice      Pointer to a location where device handle is written.
 *
 * @return      Status
 *              - #ADI_ICC_SUCCESS                  The device is opened successfully.
 *              - #ADI_ICC_BAD_DEVICE_NUM       [D] The device number passed is invalid.
 *              - #ADI_ICC_NULL_POINTER         [D] Some pointer(s) passed to the function are pointing to NULL.
 *              - #ADI_ICC_ALREADY_INITIALIZED  [D] The device instance is already initialized and hence cannot be opened.
 *              - #ADI_ICC_INSUFFICIENT_MEM     [D] The memory passed to the driver is insufficient.
 *              - #ADI_ICC_SEMAPHORE_FAILED         Semaphore creation failed.
 *
 * Initialize an instance of the ICC device driver using default user configuration settings
 * (from adi_icc_config.h) and allocate the device for use.
 *
 * No other ICC APIs may be called until the device open function is called.  The returned
 * device handle is required to be passed to all subsequent ICC API calls to identify the
 * physical device instance in use.  The user device handle (pointed to by phDevice) is set
 * to NULL on failure.
 *
 * @sa      adi_icc_Close().
 */
ADI_ICC_RESULT adi_icc_Open(const uint32_t nDeviceNum, void *pMemory, const uint32_t nMemorySize, ADI_ICC_HANDLE *phDevice)
{
    ADI_ICC_DEVICE *pDevice;
    ADI_ICC_DEV_DATA *pDevData = (ADI_ICC_DEV_DATA *) pMemory;
    volatile int i;
    
    /* Initialize pointer to device handle with NULL so that we return NULL upon failure */
    *phDevice = NULL;
    
#ifdef ADI_DEBUG
    /* Check if the given device number is within number of given number of devices */
    if(nDeviceNum >= ADI_ICC_NUM_DEVICES)
    {
        return ADI_ICC_BAD_DEVICE_NUM;
    }
    
    /* Check if the given pointer is valid */
    if(pMemory == NULL)
    {
        return ADI_ICC_NULL_POINTER;
    }
    
    /* Check if the given memory is sufficient to operate the device */
    if(nMemorySize < ADI_ICC_MEM_SIZE)
    {
        return ADI_ICC_INSUFFICIENT_MEM;
    }
    
    /* Make sure that the ADI_ICC_MEM_SIZE macro is updated with correct size as ADI_ICC_DEV_DATA structure */
    assert(ADI_ICC_MEM_SIZE == sizeof(ADI_ICC_DEV_DATA));
    
    /* Check if the device is already opened */
    if(gICC_Devices[nDeviceNum].pDevData != NULL)
    {
        return ADI_ICC_ALREADY_INITIALIZED;
    }
    
#endif /* ADI_DEBUG */

    /* Get the pointer to the device instance */
    pDevice = &gICC_Devices[nDeviceNum];
    
    /* clear the given memory */
    memset(pMemory, 0, ADI_ICC_MEM_SIZE);
    
    /* Create the semaphore */
    SEM_CREATE(pDevData, "ICC Cortex Sem", ADI_ICC_SEMAPHORE_FAILED);

    pDevice->pDevData = pDevData;
    
    /* Assert Core and Debug reset */
    pDevice->pADI_CM->SSPSYS_CTRL = BITM_CM_SSPSYS_CTRL_DBG_RST | BITM_CM_SSPSYS_CTRL_CORE_RST;
 
    /* Wait for some time */
    for(i = 0; i < 100; i++){}

    /* De-Assert Core and Debug reset */
    pDevice->pADI_CM->SSPSYS_CTRL = BITM_CM_SSPSYS_CTRL_DBG_RST | BITM_CM_SSPSYS_CTRL_CORE_RST;
    
    /* Enable SSP System */
    pDevice->pADI_CM->SSPSYS_CTRL = BITM_CM_SSPSYS_CTRL_SSP_SYS_EN;

    
    /* 
    ** Set the MMRs based on the user configuration macros from configuration file    
    */
      
    /* Set the interrupt mask */   
    
#if (ADI_ICC_CFG_ENABLE_LOW_PWR_MODE == 1)

    /* 
      The wakeup interrupts and task valid interrupts are not mutually exclusive,
      when both are enabled, we get two interrupts. To avoid that based on the
      ADI_ICC_CFG_ENABLE_LOW_PWR_MODE configuration, enable only one of the 
      interrupts (wakeup interrupt or task valid interrupts)      
      
      When we want the SSP core to enter FLEXI mode when no tasks to execute, 
      enable wakeup interrupts and disable task valid interrupts
      */
    pDevice->pADI_CM->CM2SSP_VLD_IRQ_EN     = 0x0u;
    pDevice->pADI_CM->CM2SSP_VLD_WKUP_EN    = 0xFFFFFFFF;
#else
    /*
      When we want SSP to always stay in active mode, 
      disable wakeup interrupts and enable task valid interrupts
      */

    pDevice->pADI_CM->CM2SSP_VLD_IRQ_EN     = 0xFFFFFFFFu;
    pDevice->pADI_CM->CM2SSP_VLD_WKUP_EN    = 0x0u;
#endif
    
    /* Enable all the result interrupts back from SSP */
    pDevice->pADI_CM->SSP2CM_VLD_IRQ_EN     = 0xFFFFFFFFu;
        
    /* Configure SRAM control register */
    pDevice->pADI_SRAM->CTL = (uint32_t)( ( ADI_ICC_CFG_SRAM_PARITY_ERR_INT_CM_ENABLE      << BITP_SSP_SRAM_CTL_PAR_ERR_IEN2CM         ) |
                                          ( ADI_ICC_CFG_IRAM_WRITE_ERR_INT_CM_ENABLE       << BITP_SSP_SRAM_CTL_IRAMWR_DET_ERR_IEN2CM  ) |
                                          ( ADI_ICC_CFG_SRAM_OUT_OF_BOUND_INT_CM_ENABLE    << BITP_SSP_SRAM_CTL_OUT_BND_ERR_IEN2CM     ) |
                                          ( ADI_ICC_CFG_SRAM_PARITY_ERR_INT_SSP_ENABLE     << BITP_SSP_SRAM_CTL_PAR_ERR_IEN2SSP        ) |
                                          ( ADI_ICC_CFG_IRAM_WRITE_ERR_INT_SSP_ENABLE      << BITP_SSP_SRAM_CTL_IRAMWR_DET_ERR_IEN2SSP ) |
                                          ( ADI_ICC_CFG_SRAM_OUT_OF_BOUND_INT_SSP_ENABLE   << BITP_SSP_SRAM_CTL_OUT_BND_ERR_IEN2SSP    ) |
                                          ( ADI_ICC_CFG_ARBITRATION_WAIT_CNT               << BITP_SSP_SRAM_CTL_WAIT_CNT               ) |
                                          ( ADI_ICC_CFG_SRAM_MODE                          << BITP_SSP_SRAM_CTL_SSP_MODE               ) |
                                          ( ADI_ICC_CFG_SSP_SRAM_PRIO                      << BITP_SSP_SRAM_CTL_SSP_PRIO               ) |
                                          ( ADI_ICC_CFG_IRAM_WRITE_DETECT_ENABLE           << BITP_SSP_SRAM_CTL_IRAMWR_DET_EN          ) ); 
    /* 
    ** Enable the IRQs in NVIC 
    */
    NVIC_EnableIRQ(pDevice->eSSP2CM_IRQ0);
    NVIC_EnableIRQ(pDevice->eSSP2CM_IRQ1);
    NVIC_EnableIRQ(pDevice->eSSP2CM_ERR_IRQ);
    
    /* Return the device handle */
    *phDevice = pDevice;
    
    return ADI_ICC_SUCCESS;
}


/**
 * @brief    Close the ICC driver.
 *
 * @param [in]     hDevice    Handle to the ICC device.
 *
 * @return  Status
 *          - #ADI_ICC_SUCCESS                      Successfully closed the device.
 *          - #ADI_ICC_INVALID_HANDLE           [D] The given device handle is invalid.
 *          - #ADI_ICC_SEMAPHORE_FAILED             Semaphore delete failed.
 *
 * Once the device is closed none of the other APIs can be called.
 *
 * @sa      adi_icc_Open().
 */
ADI_ICC_RESULT adi_icc_Close(ADI_ICC_HANDLE hDevice)
{
    ADI_ICC_DEVICE *pDevice = (ADI_ICC_DEVICE *) hDevice;
    ADI_INT_STATUS_ALLOC();
    
#ifdef ADI_DEBUG    
    if(ValidateDeviceHandle(hDevice) != ADI_ICC_SUCCESS)
    {
        return ADI_ICC_INVALID_HANDLE;
    }
#endif /* ADI_DEBUG */

    /* Disable the IRQs in NVIC */
    NVIC_DisableIRQ(pDevice->eSSP2CM_IRQ0);
    NVIC_DisableIRQ(pDevice->eSSP2CM_IRQ1);
    NVIC_DisableIRQ(pDevice->eSSP2CM_ERR_IRQ);
    
    SEM_DELETE(pDevice->pDevData, ADI_ICC_SEMAPHORE_FAILED);   
    
    ADI_ENTER_CRITICAL_REGION();
    pDevice->pDevData = NULL;
    ADI_EXIT_CRITICAL_REGION();
    
    return ADI_ICC_SUCCESS;
}


/**
 * @brief    Starts the execution of the task on SSP.
 *
 * @param [in] hDevice      Handle to the ICC device.
 * @param [in] eTask        Task that should be started.
 * @param [in] pTaskArg     Pointer to the task argument. This argument will be passed to the
 *                          task running on SSP.
 * @param [in] nArgLen      Argument length in bytes.
 *
 * @return  Status
 *          - #ADI_ICC_SUCCESS              Successfully submitted the task for execution.
 *          - #ADI_ICC_INVALID_HANDLE   [D] The given device handle is invalid.
 *
 * Starts the execution of the task on SSP which is associated with the given task id (eTask). 
 * Application can use the #adi_icc_WaitForTaskDone API to wait until the task is done.
 *
 * @sa      adi_icc_WaitForTaskDone().
 * @sa      adi_icc_StartTaskOnDmaDone().
 *
 * @note    Only one task can be started at a time. After starting the task application should 
 *          wait (using API adi_icc_WaitForTaskDone) until the previously started task is completed.
 */
ADI_ICC_RESULT adi_icc_StartTask(ADI_ICC_HANDLE hDevice, const ADI_ICC_TASK eTask, void *pTaskArg, const uint32_t nArgLen)
{
    ADI_ICC_DEVICE *pDevice = (ADI_ICC_DEVICE *) hDevice;

#ifdef ADI_DEBUG    
    if(ValidateDeviceHandle(hDevice) != ADI_ICC_SUCCESS)
    {
        return ADI_ICC_INVALID_HANDLE;
    }
#endif /* ADI_DEBUG */
   
    /* Write the argument and argument length into scratch registers */
    pDevice->pADI_Scratch->CM0 = (uint32_t)pTaskArg;
    pDevice->pADI_Scratch->CM1 = nArgLen;
    
    /* Clear the event control, which might have been set by adi_icc_StartTaskOnDmaDone API */
    pDevice->pADI_CM->CM2SSP_VLD_EVENT_CTRL = 0u;
    
    /* Indicate to SSP to execute the task associated with the given task ID */
    pDevice->pADI_CM->CM2SSP_VLD_SET = ADI_TASK_ID_TO_MASK(eTask);
    
    return ADI_ICC_SUCCESS;
}

 
/**
 * @brief    Starts the execution of the task on SSP upon completion of DMA.
 *
 * @param [in] hDevice      Handle to the ICC device.
 * @param [in] eTask        Task that should be started.
 * @param [in] eEventCtrl   DMA event that will trigger the task to run.
 * @param [in] pTaskArg     Pointer to the task argument. This argument will be passed to the task running on SSP.
 * @param [in] nArgLen      Argument length in bytes.
 *
 * @return  Status
 *          - #ADI_ICC_SUCCESS                  Successfully submitted the task for execution.
 *          - #ADI_ICC_INVALID_HANDLE       [D] The given device handle is invalid.
 *          - #ADI_ICC_INVALID_PARAMETER    [D] The given task number is invalid.
 *
 * Starts the execution of the task on SSP which is associated with the given DMA channel (eTask/DMA channel)
 * Please refer HRM for mapping between the task number and the DMA channel/Peripheral associated
 * with it.
 *
 * @sa      adi_icc_WaitForTaskDone().
 * @sa      adi_icc_StartTask().
 * 
 * @note    Only one task can be started at a time. After starting the task application should wait 
 *          (using API #adi_icc_WaitForTaskDone) until the previously started task is completed.
 */ 
ADI_ICC_RESULT adi_icc_StartTaskOnDmaDone(ADI_ICC_HANDLE hDevice, const ADI_ICC_TASK eTask, const ADI_ICC_EVENT_CTRL eEventCtrl, void *pTaskArg, const uint32_t nArgLen)
{
    ADI_ICC_DEVICE *pDevice = (ADI_ICC_DEVICE *) hDevice;

#ifdef ADI_DEBUG
    if(ValidateDeviceHandle(hDevice) != ADI_ICC_SUCCESS)
    {
        return ADI_ICC_INVALID_HANDLE;
    }
    
    /* Only first 0-15 tasks are be mapped to DMA channels */
    if((uint32_t)eTask > (uint32_t)ADI_ICC_MAX_DMA_TASK)
    {
       return ADI_ICC_INVALID_PARAMETER;
    }
    
    /* When eEventCtrl = ADI_ICC_EVENT_CTRL_NONE, then it is not controlled by DMA trigger anymore */
    if(eEventCtrl == ADI_ICC_EVENT_CTRL_NONE)
    {
        return ADI_ICC_INVALID_PARAMETER; 
    }
#endif /* ADI_DEBUG */
   
    /* Write the argument and argument length into scratch registers */
    pDevice->pADI_Scratch->CM0 = (uint32_t)pTaskArg;
    pDevice->pADI_Scratch->CM1 = nArgLen;
    
    /* Set the event control to execute the task upon getting the DMA completion interrupt on the given channel */
    pDevice->pADI_CM->CM2SSP_VLD_EVENT_CTRL = (((uint32_t)eEventCtrl) << ( ( (uint32_t) eTask) << 1u) );
    
    return ADI_ICC_SUCCESS;
}

 

/**
 * @brief    Blocks until the completion of the previously started task.
 *
 * @param [in]  hDevice      Handle to the ICC device.
 * @param [out] pResult      Pointer to the task result. The result is passed back from SSP after executing the task.
 * @param [out] pResultLen   Pointer to a location where the result size is written. Result size is in terms of bytes.
 * @param [out] pHwError     Pointer to a location where hardware error are written
 *
 * @return  Status
 *          - #ADI_ICC_SUCCESS              Successfully submitted the task for execution.
 *          - #ADI_ICC_INVALID_HANDLE   [D] The given device handle is invalid.
 *          - #ADI_ICC_NULL_POINTER     [D] One of the given pointers are pointing to NULL.
 *          - #ADI_ICC_SEMAPHORE_FAILED     Semaphore Pend failed.
 *          - #ADI_ICC_HARDWARE_ERROR       A hardware error has occurred. The hardware error type is reported 
 *                                          through the \a pHwError parameter.
 *
 * Blocks until the completion of the previously started task or until a hardware error occurs. 
 * The pointer to the result of the task will be copied to pResult and its length to pResultLen. 
 * This API can be used for the tasks that are started directly (using API #adi_icc_StartTask) or 
 * the tasks that are started upon the completion of the DMA (using API #adi_icc_StartTaskOnDmaDone)
 *
 * This API also returns back without blocking when a hardware error occurs. The hardware error 
 * type is reported through the \a pHwError parameter. But the details regarding the hardware error 
 * is not reported through \a pHwError. The hardware error details (For example: memory location 
 * address that caused the error when a shared SRAM hardware #ADI_ICC_EVENT_ERROR_SHARED_SRAM occurs) 
 * are reported through the callback. 
 * 
 * @note The callback can be registered using the API #adi_icc_RegisterCallback.
 *
 * @sa      adi_icc_StartTask().
 * @sa      adi_icc_StartTaskOnDmaDone().
 * @sa      adi_icc_RegisterCallback().
 */  
ADI_ICC_RESULT adi_icc_WaitForTaskDone(ADI_ICC_HANDLE hDevice, void **pResult, uint32_t *pResultLen, uint32_t *pHwError)
{
    ADI_ICC_DEVICE *pDevice = (ADI_ICC_DEVICE *) hDevice;

#ifdef ADI_DEBUG
    if(ValidateDeviceHandle(hDevice) != ADI_ICC_SUCCESS)
    {
        return ADI_ICC_INVALID_HANDLE;
    }
    
    if((pResult == NULL) || (pResultLen == NULL))
    {
        return ADI_ICC_NULL_POINTER;
    }
#endif /* ADI_DEBUG */

    /* Wait until the task is done (semaphore is posted by interrupt handler) */
    SEM_PEND(pDevice->pDevData, ADI_ICC_SEMAPHORE_FAILED);
    
    /* Check if the semaphore is posted due to hardware error */
    if(pDevice->pDevData->hwError != ADI_ICC_EVENT_ERROR_NONE)
    {
        /* Report the hardware error and clear it */
        *pHwError = pDevice->pDevData->hwError;
         pDevice->pDevData->hwError = 0u;
        
        return ADI_ICC_HARDWARE_ERROR;
    }
    
    /* Copy the result and result length from scratch registers which are updated by SSP */
    *pResult    = (void *)pDevice->pADI_Scratch->SSP0;
    *pResultLen = pDevice->pADI_Scratch->SSP1;

    return ADI_ICC_SUCCESS;    
}


/**
 * @brief    Power up SSP and enable the clocks.
 *
 * @param [in] hDevice      Handle to the ICC device.
 * @param [in] bStall       Flag which indicates whether to power up SSP and stall execution.
 *                          'true'  - Power up SSP, but stall the execution. The execution can be
 *                                    started using the API #adi_icc_StartSSP.
 *                          'false' - Power up SSP and start executing the code.
 *
 * @return  Status
 *          - #ADI_ICC_SUCCESS              Successfully powered up the SSP.
 *          - #ADI_ICC_INVALID_HANDLE   [D] The given device handle is invalid.
 *
 * By default SSP will be in shut-down mode. This API can be used to power up the SSP. When powering
 * up, SSP can be powered up in stalled (set bStall parameter to true ) mode so that no code will start 
 * executing. Once it is powered up in stalled mode, the #adi_icc_StartSSP API can be used to start executing
 * the code in SSP. This is helpful in debugging the code on SSP where the user may want to set a breakpoint
 * at the first instruction on SSP. This will provide a chance to user to set a breakpoint after enabling the 
 * SSP in stalled mode. Please note that the JTAG can be attached to the SSP only after enabling it.
 *
 * @sa      adi_icc_DisableSSP().
 * @sa      adi_icc_StartSSP().
 */  
ADI_ICC_RESULT adi_icc_EnableSSP(ADI_ICC_HANDLE hDevice, const bool bStall)
{
     ADI_ICC_DEVICE *pDevice = (ADI_ICC_DEVICE *) hDevice;
     uint32_t ssp_ctrl_value;
     
#ifdef ADI_DEBUG
    if(ValidateDeviceHandle(hDevice) != ADI_ICC_SUCCESS)
    {
        return ADI_ICC_INVALID_HANDLE;
    }   
#endif /* ADI_DEBUG */   

    ssp_ctrl_value = pDevice->pADI_CM->SSPSYS_CTRL;
    
    if(bStall == true)
    {
        ssp_ctrl_value |= ( BITM_CM_SSPSYS_CTRL_USR_SSP_EN | 
                            BITM_CM_SSPSYS_CTRL_ROOT_CLK   | 
                            BITM_CM_SSPSYS_CTRL_SSP_STALL);
    }
    else
    {
        ssp_ctrl_value |= ( BITM_CM_SSPSYS_CTRL_USR_SSP_EN | 
                            BITM_CM_SSPSYS_CTRL_ROOT_CLK );    
    }
    
    pDevice->pADI_CM->SSPSYS_CTRL  = ssp_ctrl_value;
    
    /* Wait until SSP enable status gets set */
    while((pDevice->pADI_CM->SSPSYS_CTRL & (uint32_t)BITM_CM_SSPSYS_CTRL_SSP_EN_STS) == 0u) {}
    
    return ADI_ICC_SUCCESS;
}


/**
 * @brief    Power down SSP and disable the clocks.
 *
 * @param [in] hDevice      Handle to the ICC device.
 *
 * @return  Status
 *          - #ADI_ICC_SUCCESS              Successfully powered down SSP.
 *          - #ADI_ICC_INVALID_HANDLE   [D] The given device handle is invalid.
 *
 * This API can be used to power down and clock the SSP core.
 *
 * @sa      adi_icc_EnableSSP().
 */  
ADI_ICC_RESULT adi_icc_DisableSSP(ADI_ICC_HANDLE hDevice)
{
     ADI_ICC_DEVICE *pDevice = (ADI_ICC_DEVICE *) hDevice;
     const uint32_t ssp_ctrl_value = ~((uint32_t)BITM_CM_SSPSYS_CTRL_USR_SSP_EN | (uint32_t)BITM_CM_SSPSYS_CTRL_ROOT_CLK);
     
#ifdef ADI_DEBUG
    if(ValidateDeviceHandle(hDevice) != ADI_ICC_SUCCESS)
    {
        return ADI_ICC_INVALID_HANDLE;
    }   
#endif /* ADI_DEBUG */   

     pDevice->pADI_CM->SSPSYS_CTRL = ssp_ctrl_value;

     /* Wait until SSP enable status gets cleared */
    while((pDevice->pADI_CM->SSPSYS_CTRL & (uint32_t)BITM_CM_SSPSYS_CTRL_SSP_EN_STS) != 0u) {}

    return ADI_ICC_SUCCESS;    
}

/**
 * @brief    Start executing code in SSP.
 *
 * @param [in] hDevice      Handle to the ICC device.
 *
 * @return  Status
 *          - #ADI_ICC_SUCCESS              Successfully started code execution in SSP.
 *          - #ADI_ICC_INVALID_HANDLE   [D] The given device handle is invalid.
 *
 * When SSP is powered up (adi_icc_EnableSSP) the code execution can be stalled (bStall set to true) 
 * to set break points. The execution can be started by using this API.
 *
 * @sa      adi_icc_EnableSSP().
 * @sa      adi_icc_DisableSSP().
 */  
ADI_ICC_RESULT adi_icc_StartSSP(ADI_ICC_HANDLE hDevice)
{
    ADI_ICC_DEVICE *pDevice = (ADI_ICC_DEVICE *) hDevice;
     
#ifdef ADI_DEBUG
    if(ValidateDeviceHandle(hDevice) != ADI_ICC_SUCCESS)
    {
        return ADI_ICC_INVALID_HANDLE;
    }   
#endif /* ADI_DEBUG */   

    /* Start executing the code in SSP */
    pDevice->pADI_CM->SSPSYS_CTRL &= ~(BITM_CM_SSPSYS_CTRL_SSP_STALL);

    return ADI_ICC_SUCCESS;    
}

   
/**
 * @brief    Register/un-register the callback.
 *
 * @param [in] hDevice      Handle to the ICC device.
 * @param [in] pfCallback   Pointer to the application supplied callback.
 * @param [in] pCBParam     Pointer to the callback parameter which will be passed back
 *                          to the application when the callback is called.
 *
 * @return  Status
 *          - #ADI_ICC_SUCCESS              Successfully powered down SSP.
 *          - #ADI_ICC_INVALID_HANDLE   [D] The given device handle is invalid.
 *
 *  There are 3 interrupts from SSP to Cortex. Two interrupts are for reporting
 *  task done (tasks 0-15 are reported on SSP2CM_IRQ0 and tasks 15-31 are reported through
 *  SSP2CM_IRQ1). The third interrupt is to report errors.
 *  
 *  Only errors are reported through the callback. The task done interrupt is handled within
 *  the driver.
 *
 * @sa      adi_icc_EnableSSP().
 */     
ADI_ICC_RESULT adi_icc_RegisterCallback(ADI_ICC_HANDLE hDevice, ADI_CALLBACK pfCallback, void *pCBParam)
{
    ADI_ICC_DEVICE *pDevice = (ADI_ICC_DEVICE *) hDevice;
    ADI_INT_STATUS_ALLOC();
    
#ifdef ADI_DEBUG
    if(ValidateDeviceHandle(hDevice) != ADI_ICC_SUCCESS)
    {
        return ADI_ICC_INVALID_HANDLE;
    }   
#endif /* ADI_DEBUG */    
    
    ADI_ENTER_CRITICAL_REGION();
    
    pDevice->pDevData->pfCallback = pfCallback;
    pDevice->pDevData->pCBParam = pCBParam;
    
    ADI_EXIT_CRITICAL_REGION();
    
    return ADI_ICC_SUCCESS;
}

/**
 * @brief    Checks if SSP is ready to run tasks.
 *
 * @param [in]     hDevice      Handle to the ICC device.
 * @param [in]     pStatus      Pointer to a location where the ready status is written.
 *
 * @return  Status
 *          - #ADI_ICC_SUCCESS              Successfully read the SSP status.
 *          - #ADI_ICC_INVALID_HANDLE   [D] The given device handle is invalid.
 *          - #ADI_ICC_NULL_POINTER     [D] If the given pointer is pointing to NULL.
 *
 * After SSP is enabled to start executing the code, it requires some time to initialize
 * itself before it is ready to execute the tasks. The ready status can be queried
 * using this application.
 * 
 * @note    If the SSP core is enabled by stalling code execution, the code execution 
 *          should be started (using the API #adi_icc_StartSSP) before calling this API.
 *
 * @sa      adi_icc_EnableSSP().
 * @sa      adi_icc_StartSSP().
 */
ADI_ICC_RESULT adi_icc_QueryReadyStatus(ADI_ICC_HANDLE hDevice, bool *pStatus)
{
    ADI_ICC_DEVICE *pDevice = (ADI_ICC_DEVICE *) hDevice;
    
#ifdef ADI_DEBUG
    if(ValidateDeviceHandle(hDevice) != ADI_ICC_SUCCESS)
    {
        return ADI_ICC_INVALID_HANDLE;
    }
    
    if(pStatus == NULL)
    {
        return ADI_ICC_NULL_POINTER;
    }
#endif /* ADI_DEBUG */
    
    *pStatus = ((pDevice->pADI_SSP->SSP_STS & BITM_SSP_READY_STATUS) == 0u) ? (bool)false : (bool)true;
    
    return ADI_ICC_SUCCESS;
}

/*! \cond PRIVATE */

/*! SSP to Cortex IRQ0 Interrupt handler */
void SSP2CM_IRQ0_Handler(void)
{
    ISR_PROLOG()
    
    /* Clear all lower status bits, as IRQ0 will be generated for 0-15 task bits */
    gICC_Devices[0].pADI_CM->SSP2CM_VLD_CLR  = 0x0000FFFFu;  
    SEM_POST(gICC_Devices[0].pDevData);
    
    ISR_EPILOG()
}

/*! SSP to Cortex IRQ1 Interrupt handler */
void SSP2CM_IRQ1_Handler(void)
{
    ISR_PROLOG()

    /* Clear all upper status bits, as IRQ1 will be generated for 16-31 task bits */
    gICC_Devices[0].pADI_CM->SSP2CM_VLD_CLR  = 0xFFFF0000u;
 
    SEM_POST(gICC_Devices[0].pDevData);
    
    ISR_EPILOG()    
}

/*! SSP to Cortex Error Interrupt handler */
void SSP2CM_ERR_Handler(void)
{
    ADI_ICC_DEVICE *pDevice = &gICC_Devices[0];
    ADI_ICC_EVENT   hwError = ADI_ICC_EVENT_ERROR_NONE;
    void           *pEventArg;
    ADI_ICC_SRAM_ERROR_INFO SramErrInfo;
    
    ISR_PROLOG()
          
    /* Handle Task Overwrite error */
    if(pDevice->pADI_CM->CM2SSP_VLD_OWERR_STS != 0u)
    {
        uint32_t TaskOvrStatus = pDevice->pADI_CM->CM2SSP_VLD_OWERR_STS;
        
        pEventArg = (void *) TaskOvrStatus;
        
        /* Clear the error status (W1C) */
        pDevice->pADI_CM->CM2SSP_VLD_OWERR_STS = TaskOvrStatus;
        
        hwError = ADI_ICC_EVENT_ERROR_TASK_OVR;
    }
    /* Handle shared SRAM error */
    else if(pDevice->pADI_SRAM->STATUS != 0u)
    {        
        SramErrInfo.eError = (ADI_ICC_SRAM_ERR) pDevice->pADI_SRAM->STATUS;
        
        /* If the error occurred when Cortex is accessing the memory */
        if((SramErrInfo.eError & (ADI_ICC_SRAM_ERR_PARITY_CORTEX     | 
                                 ADI_ICC_SRAM_ERR_IRAM_WRITE_CORTEX  |
                                 ADI_ICC_SRAM_ERR_OUT_OF_BOUND_CORTEX)) != 0u)
        {
            /* Read the error address */
            SramErrInfo.pErrorAddr = (void *)pDevice->pADI_SRAM->ERR_ADDR2CM;            
        }
        else
        {
            SramErrInfo.pErrorAddr = (void *)pDevice->pADI_SRAM->ERR_ADDR2SSP;
        }
         
        pEventArg = &SramErrInfo;
        hwError = ADI_ICC_EVENT_ERROR_SHARED_SRAM;
        
        /* Clear the error status (W1C)*/
        pDevice->pADI_SRAM->STATUS = (uint32_t )SramErrInfo.eError;
    }
    /* Handle Expiry timer error */
    else if((pDevice->pADI_SSP->EXP_TMR_STS & BITM_SSP_EXP_TMR_STS_TIMEOUT) != 0u)
    {
       /* Clear the expiry error status (W1C) */
       pDevice->pADI_SSP->EXP_TMR_STS  = pDevice->pADI_SSP->EXP_TMR_STS;
       hwError      = ADI_ICC_EVENT_ERROR_TIMER_TIMEOUT;
       pEventArg    = NULL;
    }
    else
    {
        /* We should not reach here */
    }
    
    /* If there are any hardware error post the semaphore */
    if(hwError != ADI_ICC_EVENT_ERROR_NONE)
    {
        if(pDevice->pDevData->pfCallback != NULL)
        {
            pDevice->pDevData->pfCallback(pDevice->pDevData->pCBParam, (uint32_t)hwError, pEventArg);
        }
        pDevice->pDevData->hwError |= (uint32_t) hwError;
        SEM_POST(pDevice->pDevData);
    }
    
    
    ISR_EPILOG()
}

#ifdef ADI_DEBUG
/**
 * @brief    Validates the device handle.
 *
 * @param [in]     hDevice      Handle to the ICC device.
 *
 * @return  Status
 *          - #ADI_ICC_SUCCESS              If the given handle is valid.
 *          - #ADI_ICC_INVALID_HANDLE   [D] The given device handle is invalid.
 *
 */
static ADI_ICC_RESULT ValidateDeviceHandle(ADI_ICC_HANDLE hDevice)
{
    uint32_t i;
    ADI_ICC_DEVICE *pDevice = (ADI_ICC_DEVICE *) hDevice;
    
    for(i = 0u; i < ADI_ICC_NUM_DEVICES; i++)
    {
        if(pDevice == &gICC_Devices[i])
        {
            return ADI_ICC_SUCCESS;
        }
    }
    
    return ADI_ICC_INVALID_HANDLE;
}
#endif /* ADI_DEBUG */

/*! \endcond */

/*! @} */
