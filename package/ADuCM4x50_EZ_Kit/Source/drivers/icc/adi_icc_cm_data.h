/*! *****************************************************************************
 * @file    adi_icc_cm_data.h
 * @brief   Private header file for Inter-core communication Cortex driver.
 * @details This header file is used for private definitions used within the driver.
 -----------------------------------------------------------------------------
Copyright (c) 2016 Analog Devices, Inc.

All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
  - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  - Modified versions of the software must be conspicuously marked as such.
  - This software is licensed solely and exclusively for use with processors
    manufactured by or for Analog Devices, Inc.
  - This software may not be combined or merged with other code in any manner
    that would cause the software to become subject to terms and conditions
    which differ from those listed here.
  - Neither the name of Analog Devices, Inc. nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
  - The use of this software may or may not infringe the patent rights of one
    or more patent holders.  This license does not release you from the
    requirement that you obtain separate licenses from these patent holders
    to use this software.

THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-
INFRINGEMENT, TITLE, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL ANALOG DEVICES, INC. OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF
CLAIMS OF INTELLECTUAL PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*****************************************************************************/
#ifndef ADI_ICC_CM_DATA_H
#define ADI_ICC_CM_DATA_H

#include <adi_processor.h>
#include <adi_callback.h>
#include <rtos_map/adi_rtos_map.h>
#include <drivers/icc/adi_icc_cm.h>

/*! @addtogroup ICC_Driver
 *  @{
 */

 
/*! \cond PRIVATE */


/*! Structure which holds the device instance data */
typedef struct
{
    ADI_CALLBACK        pfCallback;    /*! Pointer to application supplied callback function */
    void               *pCBParam;      /*! Pointer to callback parameter */     
    SEM_VAR_DECLR                      /*! Semaphore variable declaration */
    uint32_t            hwError;       /*! Hardware error events */
    
} ADI_ICC_DEV_DATA;

/*! Structure which holds the pointers to the device data */
typedef struct
{
   ADI_CM_TypeDef       *pADI_CM;          /*! Pointer to Cortex writable registers */
   ADI_SSP_TypeDef      *pADI_SSP;         /*! Pointer to SSP writable registers */
   ADI_SCRATCH_TypeDef  *pADI_Scratch;     /*! Pointer to the scratch registers */
   ADI_SSP_SRAM_TypeDef *pADI_SRAM;        /*! Pointer to the shared SRAM registers */
   IRQn_Type             eSSP2CM_IRQ0;     /*! SSP to Cortex IRQ0 interrupt number */
   IRQn_Type             eSSP2CM_IRQ1;     /*! SSP to Cortex IRQ1 interrupt number */
   IRQn_Type             eSSP2CM_ERR_IRQ;  /*! SSP to Cortex Error IRQ interrupt number */
   ADI_ICC_DEV_DATA     *pDevData;         /*! Pointer to the local device data */
   
} ADI_ICC_DEVICE;

/*! Convert Task ID to bit mask */
#define ADI_TASK_ID_TO_MASK(eTask)     ((1u) << (uint32_t)(eTask))

/*! SSP to Cortex IRQ0 Interrupt handler */
void SSP2CM_IRQ0_Handler(void);

/*! SSP to Cortex IRQ1 Interrupt handler */
void SSP2CM_IRQ1_Handler(void);

/*! SSP to Cortex Error Interrupt handler */
void SSP2CM_ERR_Handler(void);

#ifdef ADI_DEBUG
static ADI_ICC_RESULT ValidateDeviceHandle(ADI_ICC_HANDLE hDevice);
#endif /* ADI_DEBUG */
 
 /*! \endcond */

/** @} */
 
#endif /* ADI_ICC_CM_DATA_H */


