/*! *****************************************************************************
 * @file    adi_icc_ssp_data.h
 * @brief   Private header file for Inter-core communication SSP driver.
 * @details This header file is used for private definitions used within the driver.
 -----------------------------------------------------------------------------
Copyright (c) 2012-2016 Analog Devices, Inc.

All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
  - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  - Modified versions of the software must be conspicuously marked as such.
  - This software is licensed solely and exclusively for use with processors
    manufactured by or for Analog Devices, Inc.
  - This software may not be combined or merged with other code in any manner
    that would cause the software to become subject to terms and conditions
    which differ from those listed here.
  - Neither the name of Analog Devices, Inc. nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
  - The use of this software may or may not infringe the patent rights of one
    or more patent holders.  This license does not release you from the
    requirement that you obtain separate licenses from these patent holders
    to use this software.

THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-
INFRINGEMENT, TITLE, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL ANALOG DEVICES, INC. OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF
CLAIMS OF INTELLECTUAL PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*****************************************************************************/

#ifndef ADI_ICC_SSP_DATA_H
#define ADI_ICC_SSP_DATA_H

#include <sys/platform.h>
#include <sys/adi_ADuCM4150_device.h>


/*! @addtogroup ICC_Driver
 *  @{
 */

 
 /*! \cond PRIVATE */

 /*! Structure which holds the task information */
 typedef struct
 {
     ADI_ICC_TASK_FUNC   pfTask;			/*! Pointer to task function */
     void *         	 pCBParam;			/*! Pointer to the callback parameter */

 } ADI_ICC_TASK_INFO;


 /*! Structure which holds the device instance data */
 typedef struct
 {
     ADI_ICC_TASK           eCurrTaskID;                 /*! Current task that is getting executed */
     volatile uint32_t      TaskInvoked;                 /*! Task that is invoked by Cortex */
     ADI_ICC_TASK_INFO      TaskTable[ADI_ICC_TASK_MAX]; /*! Table which holds the task information */

 } ADI_ICC_DEV_DATA;


 /*! Structure which holds the pointers to the device data */
 typedef struct
 {
    ADI_CM_TypeDef       *pADI_CM;          /*! Pointer to Cortex writeable registers */
    ADI_SSP_TypeDef      *pADI_SSP;         /*! Pointer to SSP writeable registers */
    ADI_SCRATCH_TypeDef  *pADI_Scratch;     /*! Pointer to the scratch registers */
    ADI_SSP_SRAM_TypeDef *pADI_SRAM;        /*! Pointer to the shared SRAM registers */
    uint32_t              nCM2SSP_IRQ0;     /*! Cortex to SSP IRQ0 interrupt number */
    uint32_t              nCM2SSP_IRQ1;     /*! Cortex to SSP IRQ1 interrupt number */
    uint32_t              nWakeupIRQ;       /*! Wakeup interrupt number */
    ADI_ICC_DEV_DATA     *pDevData;         /*! Pointer to the local device data */
    
 } ADI_ICC_DEVICE;

/*! Convert Task ID to bit mask */
#define ADI_TASK_ID_TO_MASK(eTask)     (1u << (uint32_t)eTask)

/*! Expiry Timer Clear Key. User writes 0xCCCCCCCC to reset/reload/restart 
     expiry timer or clear IRQ. */
#define ADI_EXPIRY_TMR_CLR_KEY 0xCCCCCCCC
 

/*! Invalid Task ID */
#define ADI_ICC_INVALID_TASK_ID 0x0u

/*! Disable all interrupts */
#define ADI_ENTER_CRITICAL_REGION()		XTOS_DISABLE_ALL_INTERRUPTS

 /*! Enable all interrupts */
#define ADI_EXIT_CRITICAL_REGION()		XTOS_ENABLE_INTERRUPTS

 
/*! Task dispatcher. Dispatcher the tasks when they are invoked by Cortex */
static void TaskDispatcher(ADI_ICC_DEVICE *pDevice);
 
/*! Cortex to SSP IRQ 0 interrupt handler */ 
static void CM2SSP_IRQ0_Handler(ADI_ICC_DEVICE *pDevice);

/*! Cortex to SSP IRQ 1 interrupt handler */
static void CM2SSP_IRQ1_Handler(ADI_ICC_DEVICE *pDevice); 

/*! Wakeup interrupt handler */
static void WakeupIRQ_Handler(ADI_ICC_DEVICE *pDevice);  

#ifdef ADI_DEBUG
static ADI_ICC_RESULT ValidateDeviceHandle(ADI_ICC_HANDLE hDevice);
#endif 

static inline unsigned long __cntlz (unsigned long x)
{
	int lz;
	asm ("nsau %0, %1" : "=r" (lz) : "r" (x));
	return 31 - lz;
	return lz;
}

 /*! \endcond */
 
/** @} */

#endif /* ADI_ICC_SSP_DATA_H */
