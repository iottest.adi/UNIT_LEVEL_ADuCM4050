/*! *****************************************************************************
 * @file    adi_icc_ssp.h
 * @brief   Inter-core communication API for SSP (Sensor Signal Processor).
 * @details The APIs in this file should only be called from the SSP.
 -----------------------------------------------------------------------------
Copyright (c) 2016 Analog Devices, Inc.

All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
  - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  - Modified versions of the software must be conspicuously marked as such.
  - This software is licensed solely and exclusively for use with processors
    manufactured by or for Analog Devices, Inc.
  - This software may not be combined or merged with other code in any manner
    that would cause the software to become subject to terms and conditions
    which differ from those listed here.
  - Neither the name of Analog Devices, Inc. nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
  - The use of this software may or may not infringe the patent rights of one
    or more patent holders.  This license does not release you from the
    requirement that you obtain separate licenses from these patent holders
    to use this software.

THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-
INFRINGEMENT, TITLE, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL ANALOG DEVICES, INC. OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF
CLAIMS OF INTELLECTUAL PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*****************************************************************************/

#include <drivers/icc/adi_icc_ssp.h>
#include <adi_icc_config.h>
#include "adi_icc_ssp_data.h"
#include "adi_icc_ssp_data.c"
#include <xtensa/xtruntime.h>
#include <stddef.h>
#include <assert.h>
#include <string.h>


/*! @addtogroup ICC_Driver
 *  @{
 *
 *  @brief    Inter Core Communication driver for SSP Core.
 *  @note     The application must include drivers/icc/adi_icc_ssp.h to use this driver
 *  @details  This file should be only included in the SSP application. Including it
 *            in Cortex application will result in error. Cortex application should include
 *            drivers/icc/adi_icc_cm.h header file.  
 */

/**
 * @brief    Open the ICC driver.
 *
 * @param [in]     nDeviceNum    The zero-based device instance number of flash controller to be opened.
 * @param [in]     pMemory       Application supplied memory space for use by the driver.
 * @param [in]     nMemorySize   Size of the application supplied memory (in bytes).
 * @param [in,out] phDevice      Pointer to a location where device handle is written.
 *
 * @return  Status
 *          - #ADI_ICC_SUCCESS                      The device is opened successfully.
 *          - #ADI_ICC_BAD_DEVICE_NUM           [D] The device number passed is invalid.
 *          - #ADI_ICC_NULL_POINTER             [D] Some pointer(s) passed to the function is pointing to NULL.
 *          - #ADI_ICC_ALREADY_INITIALIZED      [D] The device instance is already initialized and hence cannot be opened.
 *          - #ADI_ICC_INSUFFICIENT_MEM         [D] The memory passed to the driver is insufficient.
 *          - #ADI_ICC_SEMAPHORE_FAILED             Semaphore creation failed.
 *
 * Initialize an instance of the ICC device driver using default user configuration settings
 * (from adi_icc_config.h) and allocate the device for use.
 *
 * No other ICC APIs may be called until the device open function is called.  The returned
 * device handle is required to be passed to all subsequent ICC API calls to identify the
 * physical device instance in use.  The user device handle (pointed to by \a phDevice) is set
 * to NULL on failure.
 *
 * @sa      adi_icc_Close().
 */
ADI_ICC_RESULT adi_icc_Open(const uint32_t nDeviceNum, void *pMemory, const uint32_t nMemorySize, ADI_ICC_HANDLE *phDevice)
{
    ADI_ICC_DEVICE *pDevice;
    
    /* Initialize pointer to device handle with NULL so that we return NULL upon failure */
    *phDevice = NULL;
    
#ifdef ADI_DEBUG
    /* Check if the given device number is within number of given number of devices */
    if(nDeviceNum >= ADI_ICC_NUM_DEVICES)
    {
        return ADI_ICC_BAD_DEVICE_NUM;
    }
    
    /* Check if the given pointer is valid */
    if(pMemory == NULL)
    {
        return ADI_ICC_NULL_POINTER;
    }
    
    /* Check if the given memory is sufficient to operate the device */
    if(nMemorySize < ADI_ICC_MEM_SIZE)
    {
        return ADI_ICC_INSUFFICIENT_MEM;
    }
    
    /* Make sure that the ADI_ICC_MEM_SIZE macro is updated with correct size as ADI_ICC_DEV_DATA structure */
    assert(ADI_ICC_MEM_SIZE == sizeof(ADI_ICC_DEV_DATA));
    
    /* Check if the device is already opened */
    if(gICC_Devices[nDeviceNum].pDevData != NULL)
    {
        return ADI_ICC_ALREADY_INITIALIZED;
    }
    
#endif /* ADI_DEBUG */

    /* Get the pointer to the device instance */
    pDevice = &gICC_Devices[nDeviceNum];
    
    /* clear the given memory */
    memset(pMemory, 0, ADI_ICC_MEM_SIZE);
    
    pDevice->pDevData = (ADI_ICC_DEV_DATA *) pMemory;

    /*
     * Set the MMRs based on the user configuration macros from configuration file
     */

    /* Set the expiry timer timeout value*/
    pDevice->pADI_SSP->EXP_TMR_LD = ADI_ICC_CFG_EXPIRY_TMR_TIMEOUT_VALUE;

    
    /* Set the expiry timer configuration */
    pDevice->pADI_SSP->EXP_TMR_CTRL = ( (ADI_ICC_CFG_EXPIRY_TMR_PD_HLT     << BITP_SSP_EXP_TMR_CTRL_PD_STOP     ) |
                                        (ADI_ICC_CFG_EXPIRY_TMR_INT_ENABLE << BITP_SSP_EXP_TMR_CTRL_INTR_EN     ) |
                                        (ADI_ICC_CFG_EXPIRY_TMR_ENABLE     << BITP_SSP_EXP_TMR_CTRL_TMR_EN      ) |
                                        (ADI_ICC_CFG_EXPIRY_TMR_DBG_HLT    << BITP_SSP_EXP_TMR_CTRL_DBGHLT_STOP));
                                          
    /* No interrupt masks to be set on the SSP  */
    
    /* Install the interrupt handlers */
   _xtos_set_interrupt_handler_arg(pDevice->nCM2SSP_IRQ0, CM2SSP_IRQ0_Handler, pDevice);
   _xtos_set_interrupt_handler_arg(pDevice->nCM2SSP_IRQ1, CM2SSP_IRQ1_Handler, pDevice);
   _xtos_set_interrupt_handler_arg(pDevice->nWakeupIRQ,   WakeupIRQ_Handler, pDevice);
   
    /* Enable the interrupts in SSP */
    _xtos_ints_on((1u << pDevice->nCM2SSP_IRQ0) | (1u << pDevice->nCM2SSP_IRQ1) | (1u << pDevice->nWakeupIRQ));

    /* Return the device handle */
    *phDevice = pDevice;
    
    return ADI_ICC_SUCCESS;
}

/**
 * @brief    Close the ICC driver.
 *
 * @param [in]     hDevice    Handle to the ICC device.
 *
 * @return  Status
 *          - #ADI_ICC_SUCCESS                      Successfully closed the device.
 *          - #ADI_ICC_INVALID_HANDLE           [D] The given device handle is invalid.
 *
 * Once the device is closed none of the other APIs can be called.
 *
 * @sa      adi_icc_Open().
 */
ADI_ICC_RESULT adi_icc_Close(ADI_ICC_HANDLE hDevice)
{
    ADI_ICC_DEVICE *pDevice = (ADI_ICC_DEVICE *) hDevice;
    
#ifdef ADI_DEBUG    
    if(ValidateDeviceHandle(hDevice) != ADI_ICC_SUCCESS)
    {
        return ADI_ICC_INVALID_HANDLE;
    }
#endif /* ADI_DEBUG */

    /* Disable the interrupts in SSP */
    _xtos_ints_off((1u << pDevice->nCM2SSP_IRQ0) | (1u << pDevice->nCM2SSP_IRQ1) | (1u << pDevice->nWakeupIRQ));

    /* Disable interrupts */
    ADI_ENTER_CRITICAL_REGION();

    /* Reset device data */
    pDevice->pDevData = NULL;
    
    ADI_EXIT_CRITICAL_REGION();
    
    return ADI_ICC_SUCCESS;
}

/**
 * @brief    Indicates that the task is done and updates the result.
 *
 * @param [in]  hDevice     Handle to the ICC device.
 * @param [in]  pResult     Pointer to the task result. Note that this pointer should point
 *                          to the shared memory so that it is accessible to Cortex.
 * @param [in]  nResultLen  Size of the result in bytes.
 *
 * @return  Status
 *          - #ADI_ICC_SUCCESS                      Successfully updated the task result.
 *          - #ADI_ICC_INVALID_HANDLE       [D] The given device handle is invalid.
 *          - #ADI_ICC_NULL_POINTER         [D] The given pointer is pointing to NULL.
 *
 * Once the task is completed, SSP task should use this API to update the task result and
 * indicate to the Cortex that the task execution is completed. An interrupt will be generated
 * to Cortex to indicate that the task execution is completed.
 * 
 * @sa      adi_icc_RegisterTask().
 */     
ADI_ICC_RESULT adi_icc_TaskDone(ADI_ICC_HANDLE hDevice, void *pResult, const uint32_t nResultLen)
{
    ADI_ICC_DEVICE *pDevice = (ADI_ICC_DEVICE *) hDevice;
    
#ifdef ADI_DEBUG    
    if(ValidateDeviceHandle(hDevice) != ADI_ICC_SUCCESS)
    {
        return ADI_ICC_INVALID_HANDLE;
    }
#endif /* ADI_DEBUG */

    /* Update the result */
    pDevice->pADI_Scratch->SSP0 = (uint32_t)pResult;
    pDevice->pADI_Scratch->SSP1 = nResultLen;
    
    /* indicate to Cortex that the task is complete */
    pDevice->pADI_SSP->SSP2CM_VLD_SET = ADI_TASK_ID_TO_MASK(pDevice->pDevData->eCurrTaskID);
    
    return ADI_ICC_SUCCESS;
}


/**
 * @brief    Enables or disables the Expiry timer.
 *
 * @param [in]  hDevice    Handle to the ICC device.
 * @param [in]  bEnable    Flag which indicates whether to enable or disable the timer.
 *                          - true  :   Enable the timer.
 *                          - false :   Disable the timer.
 *
 * @return  Status
 *          - #ADI_ICC_SUCCESS              Successfully enabled the timer.
 *          - #ADI_ICC_INVALID_HANDLE   [D] The given device handle is invalid.
 *
 * The Expiry timer is similar to a watchdog timer in Cortex. 
 * When the timer is enabled it should be cleared (Using the API #adi_icc_ExpiryTmrRestart)
 * before the timer expires (similar to kicking the watchdog timer). 
 * If the timer timer expires an error interrupt is sent to Cortex.
 *
 * By default the Expiry timer is enabled when the SSP is reset. So application can disable 
 * it once to reconfigure the default time-out value. Once the timer is enabled back it 
 * cannot be disabled through out the life cycle of the application.
 *
 * @note : The Expiry timer is disabled in the default driver configuration 
 *         (#ADI_ICC_CFG_EXPIRY_TMR_ENABLE). So application does not have to call 
 *         this API to disable it. Application call this API to enable the Expiry timer.
 * \n
 * The time out value is configured through static configuration value
 * #ADI_ICC_CFG_EXPIRY_TMR_TIMEOUT_VALUE (defined in the file adi_icc_config.h).
 *
 * The timer can be halted during debug. This can be enabled/disabled through the configuration
 * macro #ADI_ICC_CFG_EXPIRY_TMR_DBG_HLT
 * 
 * @sa      adi_icc_ExpiryTmrRestart().
 */   
ADI_ICC_RESULT adi_icc_ExpiryTmrEnable(ADI_ICC_HANDLE hDevice, const bool bEnable)
{
    ADI_ICC_DEVICE *pDevice = (ADI_ICC_DEVICE *) hDevice;
    
    uint32_t TmrCtrl;
    
#ifdef ADI_DEBUG    
    if(ValidateDeviceHandle(hDevice) != ADI_ICC_SUCCESS)
    {
        return ADI_ICC_INVALID_HANDLE;
    }
#endif /* ADI_DEBUG */

    TmrCtrl = pDevice->pADI_SSP->EXP_TMR_CTRL;
    
    if(bEnable == true)
    {
        TmrCtrl |= BITM_SSP_EXP_TMR_CTRL_TMR_EN;
    }
    else
    {
        TmrCtrl &= ~BITM_SSP_EXP_TMR_CTRL_TMR_EN;
    }
    
    pDevice->pADI_SSP->EXP_TMR_CTRL = TmrCtrl;

    return ADI_ICC_SUCCESS;
}

/**
 * @brief    Restart the Expiry timer.
 *
 * @param [in]  hDevice    Handle to the ICC device.
 *
 * @return  Status
 *          - #ADI_ICC_SUCCESS              Successfully restarted the timer.
 *          - #ADI_ICC_INVALID_HANDLE   [D] The given device handle is invalid.
 *
 * Clear the Expiry timer so that it restarts counting. If the timer is not cleared
 * before time out an error interrupt (ADI_ICC_EVENT_ERROR_TIMER_TIMEOUT)
 * is generated (if the configuration macro #ADI_ICC_CFG_EXPIRY_TMR_INT_ENABLE is set to 1)
 * to Cortex.
 *\n
 * The time out value is configured through the static configuration value
 * #ADI_ICC_CFG_EXPIRY_TMR_TIMEOUT_VALUE (defined in the file adi_icc_config.h).
 *
 * @sa      adi_icc_ExpiryTmrEnable().
 */   
ADI_ICC_RESULT adi_icc_ExpiryTmrRestart(ADI_ICC_HANDLE hDevice)
{
    ADI_ICC_DEVICE *pDevice = (ADI_ICC_DEVICE *) hDevice;
        
#ifdef ADI_DEBUG    
    if(ValidateDeviceHandle(hDevice) != ADI_ICC_SUCCESS)
    {
        return ADI_ICC_INVALID_HANDLE;
    }
#endif /* ADI_DEBUG */

    /* Write the key to restart the timer */
    pDevice->pADI_SSP->EXP_TMR_CLR = ADI_EXPIRY_TMR_CLR_KEY;

    return ADI_ICC_SUCCESS;
}


/**
 * @brief    Start the ICC task dispatcher.
 *
 * @param [in]  hDevice    Handle to the ICC device.
 *
 * @return  Status
 *          - #ADI_ICC_SUCCESS              Successfully started the task dispatcher.
 *          - #ADI_ICC_INVALID_HANDLE   [D] The given device handle is invalid.
 *
 * This function will not return back to main.
 * This will start calling the application supplied tasks when they are invoked
 * by Cortex and put back SSP to sleep mode if ADI_ICC_CFG_ENABLE_LOW_PWR_MODE
 * is enabled.
 *\n
 * Before calling this API application should register the tasks with the 
 * dispatcher so that they will get called when Cortex invokes them.
 *
 * @sa      adi_icc_RegisterTask.
 */  
ADI_ICC_RESULT adi_icc_Start(ADI_ICC_HANDLE hDevice)
{
    ADI_ICC_DEVICE *pDevice = (ADI_ICC_DEVICE *) hDevice;
        
#ifdef ADI_DEBUG    
    if(ValidateDeviceHandle(hDevice) != ADI_ICC_SUCCESS)
    {
        return ADI_ICC_INVALID_HANDLE;
    }
#endif /* ADI_DEBUG */

    TaskDispatcher(hDevice);
    
    assert("Execution should not return");
}

/**
 * @brief    Register/un-register the task.
 *
 * @param [in]  hDevice    Handle to the ICC device.
 * @param [in]  eTask      Task number for which the function to be registered.
 * @param [in]  pfTask     Pointer to task function.
 * @param [in]  pCBParam   Callback parameter which will be passed back 
 *                         to the application when the task function is called.
 *
 * @return  Status
 *          - #ADI_ICC_SUCCESS              Successfully started the task dispatcher.
 *          - #ADI_ICC_INVALID_HANDLE   [D] The given device handle is invalid.
 *
 * Register the tasks with the SSP driver so that they will get called when 
 * Cortex invokes them.
 * 
 * Once the registration of all the tasks is complete the task dispatcher can 
 * started using the API #adi_icc_Start.
 *
 * @sa      adi_icc_Start.
 */  
ADI_ICC_RESULT adi_icc_RegisterTask(ADI_ICC_HANDLE hDevice, ADI_ICC_TASK eTask, ADI_ICC_TASK_FUNC pfTask, void *pCBParam)
{
    ADI_ICC_DEVICE *pDevice = (ADI_ICC_DEVICE *) hDevice;
        
#ifdef ADI_DEBUG    
    if(ValidateDeviceHandle(hDevice) != ADI_ICC_SUCCESS)
    {
        return ADI_ICC_INVALID_HANDLE;
    }
#endif /* ADI_DEBUG */    
    
    /* Disable interrupts while updating the task table */
    ADI_ENTER_CRITICAL_REGION();
    
    pDevice->pDevData->TaskTable[(uint32_t)eTask].pfTask    = pfTask;
    pDevice->pDevData->TaskTable[(uint32_t)eTask].pCBParam  = pCBParam;
    
    ADI_EXIT_CRITICAL_REGION();
    
    return ADI_ICC_SUCCESS;
}

/*! \cond PRIVATE */

/**
 * @brief    Start the ICC task dispatcher.
 *
 * @param [in]  hDevice    Handle to the ICC device.
 *
 * @return  Status
 *          - #ADI_ICC_SUCCESS              Successfully started the task dispatcher.
 *          - #ADI_ICC_INVALID_HANDLE   [D] The given device handle is invalid.
 *
 * This function will not return back to main.
 * This will start calling the application supplied tasks when they are invoked
 * by Cortex.  When there are no tasks to run and if the #ADI_ICC_CFG_ENABLE_LOW_PWR_MODE
 * is enabled, the SSP core is put to sleep.
 *\n
 * Before calling this API application should register the tasks with the 
 * dispatcher so that they will get called when Cortex invokes them.
 *
 * @sa      adi_icc_RegisterTask.
 */ 
void TaskDispatcher(ADI_ICC_DEVICE *pDevice)
{
    uint32_t TaskInvoked;
    ADI_ICC_DEV_DATA *pDevData = pDevice->pDevData;
    void             *pTaskArg = NULL;
    uint32_t          nArgLen = 0;
    uint32_t          NumZ = 0;
    /* indicate to Cortex that the SSP is ready to handle the tasks */
    pDevice->pADI_SSP->SSP_STS = BITM_SSP_READY_STATUS;
    
    while (1)
    {
#if (ADI_ICC_CFG_ENABLE_LOW_PWR_MODE == 1)
       
       /* 
        * Enter low power mode until we get the interrupt from Cortex 
        */
        __asm__ __volatile__(	"waiti 0" );

       /* Make sure we flush the pipeline before disabling the interrupts below */
       __asm__ __volatile__(	"nop; nop; nop;" );
        
#endif /*  ADI_ICC_CFG_ENABLE_LOW_PWR_MODE */

        XTOS_DISABLE_ALL_INTERRUPTS;
        
        TaskInvoked = pDevData->TaskInvoked;
        pDevData->TaskInvoked = ADI_ICC_INVALID_TASK_ID;

        XTOS_ENABLE_INTERRUPTS;

        
        /* If there is a task invoked from Cortex */
        if(TaskInvoked != ADI_ICC_INVALID_TASK_ID)
        {
            uint32_t nTask;
            ADI_ICC_TASK_INFO *pTaskInfo;

            /* Get the argument and argument length that is passed to the task */
            pTaskArg    = (void *)pDevice->pADI_Scratch->CM0;
            nArgLen     =         pDevice->pADI_Scratch->CM1;

            /* Find the task invoked by counting number of leading zeros.
             * It is assumed that tasks are invoked one at time.
             */
            nTask = __cntlz(TaskInvoked);
            pDevData->eCurrTaskID = (ADI_ICC_TASK) nTask;

            pTaskInfo = &pDevice->pDevData->TaskTable[nTask];

            /* Call the application registered task */
            if(pTaskInfo->pfTask != NULL)
            {
                pTaskInfo->pfTask(pTaskInfo->pCBParam, pTaskArg, nArgLen);
            }

        }
    }
}


/*! Cortex to SSP IRQ 0 interrupt handler */ 
static void CM2SSP_IRQ0_Handler(ADI_ICC_DEVICE *pDevice)
{
    uint32_t TaskInvoked = pDevice->pADI_SSP->CM2SSP_VLD_IRQ_STS;
    
    pDevice->pDevData->TaskInvoked      = TaskInvoked;
    
    /* Clear the interrupt */
    pDevice->pADI_SSP->CM2SSP_VLD_CLR   = TaskInvoked;
}

/*! Cortex to SSP IRQ 1 interrupt handler */
static void CM2SSP_IRQ1_Handler(ADI_ICC_DEVICE *pDevice)
{
    uint32_t TaskInvoked = pDevice->pADI_SSP->CM2SSP_VLD_IRQ_STS;
    
    pDevice->pDevData->TaskInvoked      = TaskInvoked;
    
    /* Clear the interrupt */
    pDevice->pADI_SSP->CM2SSP_VLD_CLR   = TaskInvoked;
}

/*! Wakeup interrupt handler */
static void WakeupIRQ_Handler(ADI_ICC_DEVICE *pDevice)
{
    uint32_t TaskInvoked = pDevice->pADI_CM->CM2SSP_VLD_SET;
    
    pDevice->pDevData->TaskInvoked      = TaskInvoked;
    
    /* Clear the interrupt */
    pDevice->pADI_SSP->CM2SSP_VLD_CLR   = TaskInvoked;
}

#ifdef ADI_DEBUG
/**
 * @brief    Validates the device handle.
 *
 * @param [in]     hDevice      Handle to the ICC device.
 *
 * @return  Status
 *          - #ADI_ICC_SUCCESS                      If the given handle is valid.
 *          - #ADI_ICC_INVALID_HANDLE           [D] The given device handle is invalid.
 *
 */
static ADI_ICC_RESULT ValidateDeviceHandle(ADI_ICC_HANDLE hDevice)
{
    uint32_t i;
    ADI_ICC_DEVICE *pDevice = (ADI_ICC_DEVICE *) hDevice;
    
    for(i = 0; i < ADI_ICC_NUM_DEVICES; i++)
    {
        if(pDevice == &gICC_Devices[i])
            return ADI_ICC_SUCCESS;
    }
    
    return ADI_ICC_INVALID_HANDLE;
}
#endif /* ADI_DEBUG */

 /*! \endcond */
 
 
/**@}*/
