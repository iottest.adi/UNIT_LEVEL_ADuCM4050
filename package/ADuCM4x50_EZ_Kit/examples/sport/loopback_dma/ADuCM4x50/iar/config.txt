CATEGORY = BSP,SPORT

PROCESSOR = ADuCM4050

#delete any previous build files
<CMD, del /Q/S "$testdir\Debug">
<CMD, del /Q/S "$testdir\Release">


###### Release Config ######

#build the test project
<CMD, %IAR_EW_HOME%\\common\\bin\\iarbuild sport_loopback_dma.ewp -build Release>$output/release_build.log>

#confirm build succeeded
<ASSERT_FINDINFILE, $output\release_build.log, Total number of errors: 0, Total number of warnings: 0>
<ASSERT_FILEEXISTS, $testdir\Release\Exe\sport_loopback_dma.out>


###### Debug Config ######

#build the test project
<CMD, %IAR_EW_HOME%\\common\\bin\\iarbuild sport_loopback_dma.ewp -build Debug>$output/debug_build.log>

#confirm build succeeded
<ASSERT_FINDINFILE, $output\debug_build.log, Total number of errors: 0, Total number of warnings: 0>
<ASSERT_FILEEXISTS, $testdir\Debug\Exe\sport_loopback_dma.out>


###### Run Debug ######

#load and run the test 
<CMD, TTH:TIMEOUT(120), "%CSPY_PATH%\cspy2.bat" "$testdir\Debug\Exe\sport_loopback_dma.out">

#look for test result printed in the terminal.
<BUILDFIND, All done!>


###### Run Release ######

#load and run the test 
<CMD, TTH:TIMEOUT(120), "%CSPY_PATH%\cspy2.bat" "$testdir\Release\Exe\sport_loopback_dma.out">

#look for test result printed in the terminal.
<BUILDFIND, All done!>
