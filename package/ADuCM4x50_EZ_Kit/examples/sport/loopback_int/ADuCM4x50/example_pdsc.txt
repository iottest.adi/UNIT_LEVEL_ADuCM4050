    <example name="sport_loopback_int" folder="Boards/ADuCM4050-EZ-KIT/Examples/sport/loopback_int" doc="ADuCM4x50/Readme_sport_loopback_int.txt" version="1.0.0">
    <description>Basic example demonstrating the SPORT driver (interrupt driven)</description>
    <board name="ADuCM4050 EZ-KIT" vendor="AnalogDevices" Dname="ADuCM4050"/>
        <project>
            <environment name="uv"   load="ADuCM4x50/keil/sport_loopback_int.uvprojx"/>
            <environment name="cces" load="ADuCM4x50/cces/.project" />
        </project>
        <attributes>
            <category>Driver Examples</category>
            <component Cclass="CMSIS"  Cgroup="CORE"/>
            <component Cclass="Device" Cgroup="Startup"/>
            <component Cclass="Device" Cgroup="Retarget" />
            <component Cclass="Device" Cgroup="Examples Support" />
            <component Cclass="Device" Cgroup="Drivers" Csub="SPORT"/>
            <keyword>SPORT</keyword>
        </attributes>
    </example>
