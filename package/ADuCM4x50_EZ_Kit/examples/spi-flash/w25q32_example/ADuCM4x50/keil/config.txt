CATEGORY=BSP,TOOLCHAINS

PROCESSOR = ADuCM4050

TOOLCHAIN = KEIL

#delete any previous build files 
<CMD, del /Q/S "$testdir\Listings">
<CMD, del /Q/S "$testdir\Objects">
<CMD, move /Y "$testdir\RTE\Device\ADuCM4050\adi_spi_config.h" "$testdir">
<CMD, del /Q/S "$testdir\RTE">
<CMD, move /Y "$testdir\adi_spi_config.h" "$testdir\RTE\Device\ADuCM4050" > 
<CMD, del /Q/S "$testdir\output">
 

###### Debug Config ######
<BUILDPROJECT, $testdir\w25q32_example.uvprojx, Debug >
<BUILDFIND, 0 Error(s) >
<BUILDFIND, 0 Warning(s) >
<ASSERT_FILEEXISTS, $testdir\Objects\w25q32_example.axf>
<LOAD, $testdir\w25q32_example.uvprojx, Debug>
<RUN>
<BUILDFIND,TTH:NOCASE:MATCHANY, All done>


###### Release Config ######
<BUILDPROJECT, $testdir\w25q32_example.uvprojx, Release >
<BUILDFIND, 0 Error(s) >
<BUILDFIND, 0 Warning(s) >
<ASSERT_FILEEXISTS, $testdir\Objects\w25q32_example.axf>
<LOAD, $testdir\w25q32_example.uvprojx, Release>
<RUN>
<BUILDFIND,TTH:NOCASE:MATCHANY, All done>
