    <example name="w25q32_example" folder="Boards/ADuCM4050-EZ-KIT/Examples/spi-flash/w25q32_example" doc="ADuCM4x50/Readme_w25q32_example.txt" version="1.0.0">
    <description>Basic example demonstrating the on-board SPI Flash</description>
    <board name="ADuCM4050 EZ-KIT" vendor="AnalogDevices" Dname="ADuCM4050"/>
        <project>
        <environment name="uv"   load="ADuCM4x50/keil/w25q32_example.uvprojx"/>
        <environment name="cces" load="ADuCM4x50/cces/.project" />
        </project>
        <attributes>
        <category>Driver Examples</category>
        <component Cclass="CMSIS"  Cgroup="CORE"/>
        <component Cclass="Device" Cgroup="Startup"/>
        <component Cclass="Device" Cgroup="Retarget" />
        <component Cclass="Device" Cgroup="Examples Support" />
        <component Cclass="Board Support" Cgroup="SPI Flash"/>
        <keyword>SPI Flash</keyword>
        </attributes>
    </example>
