/*********************************************************************************
Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.
*********************************************************************************/

/********************************************************************************
* @file    icc_task_cm.c
* @brief   Inter core communication task example for Cortex
********************************************************************************/

#include <stdint.h>
#include <common.h>
#include <config/adi_icc_config.h>
#include <drivers/icc/adi_icc_cm.h>
#include <drivers/pwr/adi_pwr.h>
#include <adi_libldr.h>


/* Set it to 1 to enable debugging the SSP code. When it is enabled, Lib loader
   will not be used to download the code to SSP. Code should be downloaded
   through the JTAG attached to P12 port on EZ-Kit.

   To download the code through JTAG follow the steps below:

   * Add a break point at adi_icc_StartSSP API call 
   * Run the code on Cortex, once the execution hit the breakpooint adi_icc_StartSSP
   * Run the xt-ocd.exe from the windows/cygwin terminal.
   * Build and download the SSP code through Xtensa tool.
   * Run the code in Xtensa
   * Continue running the code in IAR.
*/
#define DEBUG_SSP_CODE      0

/* Size of the input array that is passed to SSP tasks */
#define BUFFER_SIZE         (sizeof(SharedArray)/sizeof(uint32_t))

/* Expected Sum Result from the Sum task */
#define EXPECTED_SUM        (55u)

/* Expected Product result from the Product task */
#define EXPECTED_PRODUCT    (3628800u)

/* Pinmux */
extern int32_t adi_initpinmux(void);

/* List of hardware error messages */
static char ErrorMsgs[4][30] = 
{                              "\n",
                               "ICC Task overflow occurred \n",
                               "ICC Expiry timer timed out \n",
                               "Shared SRAM error occurred \n"
};

/* Callback event reported, initialize it to no errors */
static volatile uint32_t cbEvent = ADI_ICC_EVENT_ERROR_NONE;

#define CHECK_ERROR(result, expected, message) \
do {\
	if ( (expected) != (result))\
	{\
        common_Fail(message); \
        DEBUG_MESSAGE(ErrorMsgs[cbEvent]); \
        return 1; \
	}\
} while (0)

          
#if (DEBUG_SSP_CODE == 0)

static char SSP_code[] = {
#include "ssp_ldr.h"
}; 

#endif /* DEBUG_SSP_CODE */

static uint8_t  ICCMemory[ADI_ICC_MEM_SIZE];
static ADI_ICC_HANDLE ghDevice;

#if defined ( __ICCARM__ )

#pragma location = "shared_sram"
static volatile uint32_t SharedArray[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

#else
#error "The example is not ported to this toolchain"
#endif

/* Callback function to catch the errors */
void ErrorHandler(void *pCBParam, uint32_t Event, void  * pArg)
{
    cbEvent = Event;
}


/******************************************************************************************** 
                                            MAIN
*********************************************************************************************/
int main(void) 
{
    ADI_PWR_RESULT eResultPwr;
    ADI_ICC_RESULT eResultIcc;
    bool bSSPReadyStatus = false;
    void *pResult;
    uint32_t nResultLen;
    uint32_t HwError;
     
    adi_initpinmux();
    common_Init();
    
    /* Initialize clocks */
    eResultPwr = adi_pwr_Init();
    CHECK_ERROR(eResultPwr, ADI_PWR_SUCCESS, "adi_pwr_Init failed.");
    
    /* Open the ICC driver */
    eResultIcc = adi_icc_Open(0, ICCMemory, ADI_ICC_MEM_SIZE, &ghDevice);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_Open failed.");
      
#if (DEBUG_SSP_CODE == 0)
    
    /* Download the code to SSP */
    adi_libldr_MoveDbg(SSP_code,NULL,1);

#endif  /* DEBUG_SSP_CODE */

    /* Register the error handler */
    eResultIcc = adi_icc_RegisterCallback(ghDevice, ErrorHandler, NULL);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_RegisterCallback failed.");
    
    
    /* Enable SSP */
    eResultIcc = adi_icc_EnableSSP(ghDevice, true);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_EnableSSP failed.");
    
    /* At the point a break point can be placed in SSP code */
    
    /* Start executing the code in SSP */
    eResultIcc = adi_icc_StartSSP(ghDevice);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_StartSSP failed."); 
    
    /* Wait until SSP is ready to execute the task */
    while(bSSPReadyStatus == false)
    {
      eResultIcc = adi_icc_QueryReadyStatus(ghDevice, &bSSPReadyStatus);
      CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_QueryReadyStatus failed.");
    }
    
    /* 
    **  Start the Sum Task which computes the sum of the elements in the array 
    */
    
    /* Start the task on SSP. Task 5 is chosen as the Sum Task is registered as Task 5 in SSP.
       Please refer to adi_task_ssp.c  */
    eResultIcc = adi_icc_StartTask(ghDevice, ADI_ICC_TASK_5, (void *)SharedArray, BUFFER_SIZE);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_StartTask failed.");

    /* Wait until the task is done */
    eResultIcc = adi_icc_WaitForTaskDone(ghDevice, &pResult, &nResultLen, &HwError);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_WaitForTaskDone failed.");
    
    /* Check if the sum is equal to the expected result */
    CHECK_ERROR(*(uint32_t *) pResult, EXPECTED_SUM, "The Sum result did not match with the expected result");
    DEBUG_MESSAGE("Sum Result = %d\n", *(uint32_t *)pResult);

    /*
    **  Start the Product Task which computes the product of all the elements in the array 
    */
    
    /* Start the task on SSP. Task 16 is chosen as the Product Task is registered as Task 16 in SSP.
       Please refer to adi_task_ssp.c  */
    eResultIcc = adi_icc_StartTask(ghDevice, ADI_ICC_TASK_16, (void *)SharedArray, BUFFER_SIZE);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_StartTask failed.");
    
    /* Wait until the task is done */
    eResultIcc = adi_icc_WaitForTaskDone(ghDevice, &pResult, &nResultLen, &HwError);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_WaitForTaskDone failed.");

    /* Check if the product is equal to the expected result */
    CHECK_ERROR(*(uint32_t *) pResult, EXPECTED_PRODUCT, "The Product result did not match with the expected result");
    DEBUG_MESSAGE("Product Result = %d\n", *(uint32_t *)pResult);

    /* Disable SSP */
    eResultIcc = adi_icc_DisableSSP(ghDevice);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_DisableSSP failed.");

    /* Close the ICC driver */
    eResultIcc = adi_icc_Close(ghDevice);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_Close failed.");
    
    /* check if there were any hardware errors */
    CHECK_ERROR(cbEvent, ADI_ICC_EVENT_ERROR_NONE, ErrorMsgs[cbEvent]);

    common_Pass();
    
    return 0;
}