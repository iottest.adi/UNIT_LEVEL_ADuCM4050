/*
 **
 ** Source file generated on November 24, 2016 at 14:31:32.	
 **
 ** Copyright (C) 2016 Analog Devices Inc., All Rights Reserved.
 **
 ** This file is generated automatically based upon the options selected in 
 ** the Pin Multiplexing configuration editor. Changes to the Pin Multiplexing
 ** configuration should be made by changing the appropriate options rather
 ** than editing this file.
 **
 ** Selected Peripherals
 ** --------------------
 ** JTAG (TDI, TDO, TCK, TMS, TRST)
 **
 ** GPIO (unavailable)
 ** ------------------
 ** P1_11, P1_12, P1_13, P1_14, P1_15
 */

#include <adi_processor.h>

#define JTAG_TDI_PORTP1_MUX  ((uint32_t) ((uint32_t) 1<<22))
#define JTAG_TDO_PORTP1_MUX  ((uint32_t) ((uint32_t) 1<<24))
#define JTAG_TCK_PORTP1_MUX  ((uint32_t) ((uint32_t) 2<<26))
#define JTAG_TMS_PORTP1_MUX  ((uint32_t) ((uint32_t) 1<<28))
#define JTAG_TRST_PORTP1_MUX  ((uint32_t) ((uint32_t) 3<<30))

int32_t adi_initpinmux(void);

/*
 * Initialize the Port Control MUX Registers
 */
int32_t adi_initpinmux(void) {
    /* PORTx_MUX registers */
    *pREG_GPIO1_CFG = JTAG_TDI_PORTP1_MUX | JTAG_TDO_PORTP1_MUX
     | JTAG_TCK_PORTP1_MUX | JTAG_TMS_PORTP1_MUX | JTAG_TRST_PORTP1_MUX;

    return 0;
}

