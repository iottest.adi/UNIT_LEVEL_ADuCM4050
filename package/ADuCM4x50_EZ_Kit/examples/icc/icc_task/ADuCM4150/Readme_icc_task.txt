         Analog Devices, Inc. ADuCM4x50 Application Example

Project Name: icc_task

Description:  Demonstrates the use of ICC driver for assigning a task
              to SSP core and execute it.

Overview:
=========    
    This example uses the ICC driver for assigning some predefined task to
    the SSP and execute it on it while the Cortex goes into low power mode (Flexi).
    The example has two predefined tasks in the SSP, Sum and Product tasks.
    The Sum task adds all the elements in the given array and similarly the
    Product task finds the Product of all the elements in the given array.
    
    The example defines an array in the shared memory and passes the array 
    as an arguments to the Sum and Product tasks when they are started. 
    
    The tasks on the SSP can be started by the Cortex core or they can be started by
    trigger from DMA completion interrupt. This example demonstrates starting
    the task by the Cortex core.
    
    The example uses the libldr to load the code/data to the SSP core. 
    Alternatively the code/data can be downloaded to SSP using the second JTAG 
    which is attached to port P12 on the ADuCM4x50-EZ-Kit. Please refer the section
    "How to build and run" to know more about how to debug the SSP code.    

User Configuration Macros:
==========================
    DEBUG_SSP_CODE : This macro (which is in icc_task_cm.c) should be set to 1
                     for debugging the SSP code. When set to 0, the pre-compiled
                     SSP code (converted as header file ssp_ldr.h) will be 
                     downloaded using libldr.
        
Hardware Setup:
===============
    ADuCM4x50-EZ-Kit should be configured with default jumper 
    settings. Please refer ADuCM4x50 EZ-Kit Manual for default 
    jumper settings.
    
    Attach the J-Link lite emulator to port P12 for debugging the SSP code. It is
    not needed if the SSP code debugging is not required.
    
    For debugging code on the Cortex the J-Link lite should be attached to P4 port.

External connections:
=====================
    None.
    
How to build and run:
=====================    
    Prepare hardware as explained in the Hardware Setup section.
    Build the project, load the executable to ADuCM4x50, and run it.
    
    To debug the SSP code follow the steps below:
    
    * Set the DEBUG_SSP_CODE macro to 1.
    * Build the code on IAR.
    * Open Xtensa project (which is in the directory icc_task\ADuCM4050\xtensa)
      using Xtensa toolchain.
    * Build the SSP code in xtensa.
    * In IAR add a break point at adi_icc_StartSSP API call. 
    * Run the code on the Cortex, once the execution hit the breakpoint adi_icc_StartSSP
    * Run the xt-ocd.exe from the windows/cygwin terminal.
    * Download the C code through Xtensa tool.
    * Run the code in Xtensa
    * Continue running the code in IAR.
      
Expected Result:
=================    
    The Sum and Product results are printed onto the console as shown below:
    
    Sum Result = 55
    Product Result = 3628800
    All done!

References:
===========
    ADuCM4x50 Hardware Reference Manual.    