/*********************************************************************************
Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.
*********************************************************************************/

/*!
* @file      icc_task_dma.h
*
* @brief     This is a header file for ICC task execution with peripheral DMA example.
*/

#ifndef ICC_TASK_DMA_H
#define ICC_TASK_DMA_H

/* Memory required by the driver for bidirectional mode of operation. */
#define ADI_UART_MEMORY_SIZE    (ADI_UART_BIDIR_MEMORY_SIZE)

/* Size of the data buffers that will be processed. */
#define SIZE_OF_BUFFER  20u

#define EXPECTED_RESULT 210u

/* UART device number. */
#define UART_DEVICE_NUM 0u

/* Timeout value for receiving data. */
#define UART_GET_BUFFER_TIMEOUT 1000000u


/* Set it to 1 to enable debugging the SSP code. When it is enabled, Lib loader
   will not be used to download the code to SSP. Code should be downloaded
   through the JTAG attached to P12 port on EZ-Kit.

   To download the code through JTAG follow the steps below:

   * Add a break point at adi_icc_StartSSP API call 
   * Run the code on Cortex, once the execution hit the breakpooint adi_icc_StartSSP
   * Run the xt-ocd.exe from the windows/cygwin terminal.
   * Build and download the SSP code through Xtensa tool.
   * Run the code in Xtensa
   * Continue running the code in IAR.
*/
#define DEBUG_SSP_CODE   0

#endif /* ICC_TASK_DMA_H */
