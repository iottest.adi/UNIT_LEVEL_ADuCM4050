         Analog Devices, Inc. ADuCM4x50 Application Example

Project Name: icc_task_dma

Description:  Demonstrates the use of ICC driver for assigning a task
              to SSP core and start executing it when the peripheral DMA
              completes.

Overview:
=========    
    This example uses the ICC driver for assigning a predefined task to
    SSP and start executing it upon completion of a peripheral DMA.
    While the peripheral DMA is happening and the task is getting executed
    Cortex goes into low power mode (Flexi).
    
    To demonstrate it the example uses the UART loopback to copy a buffer 
    from Cortex memory to shared memory using the peripheral DMA. The
    example configures SSP to start the "Sum" task upon completion of the
    peripheral DMA.
    
    The Sum task adds all the elements in the given array and writes the result 
    back to the shared memory variable. The input to the Sum task is the
    buffer defined in Shared memory which will be filled by the UART peripheral
    DMA. The UART loopback copies the buffer which is in Cortex memory to 
    the shared memory buffer.
        
    The tasks on SSP can be started by Cortex core or they can be started by
    a trigger from DMA completion interrupt. This example demonstrates starting
    the task by completion of the DMA.
    
    The example uses the libldr to load the code/data to SSP core. Alternatively
    the code/data can be downloaded to SSP using the second JTAG which is 
    attached to port P12 on the ADuCM4x50-EZ-Kit. Please refer the section
    "How to build and run" to know more about how to debug SSP code.    

User Configuration Macros:
==========================
    DEBUG_SSP_CODE : This macro (which is in icc_task_dma.h) should be set to 1
                     for debugging SSP code. When set to 0, the pre-compiled
                     SSP code (converted as header file ssp_ldr.h) will be 
                     downloaded using libldr.
        
Hardware Setup:
===============
    ADuCM4x50-EZ-Kit should be configured with default jumper settings. 
    Please refer ADuCM4x50 EZ-Kit Manual for default 
    jumper settings.
    
    Attach the J-Link lite emulator to port P12 for debugging SSP code. It is
    not needed if SSP code debugging is not required.
    
    For debugging code on Cortex the J-Link lite should be attached to P4 port.

External connections:
=====================
    Connect Pin 0 and 1 on J6.
    
How to build and run:
=====================    
    Prepare hardware as explained in the Hardware Setup section.
    Build the project, load the executable to ADuCM4x50, and run it.
    
    To debug the SSP code follow the steps below:
    
    * Set the DEBUG_SSP_CODE macro (which is defined in icc_task_dma.h) to 1.
    * Build the code on IAR.
    * Open Xtensa project (which is in the directory icc_task_dma\ADuCM4050\xtensa)
      using Xtensa toolchain.
    * Build the SSP code in xtensa.
    * In IAR add a break point at adi_icc_StartSSP API call. 
    * Run the code on Cortex, once the execution hit the breakpoint adi_icc_StartSSP
    * Run the xt-ocd.exe from the windows/cygwin terminal.
    * Download the SSP code through Xtensa tool.
    * Run the code in Xtensa
    * Continue running the code in IAR.
      
Expected Result:
=================    
    The Sum result is printed onto the console as shown below:
    
    Result = 210

    ICC Task with UART DMA example completed successfully.

    All done!

References:
===========
    ADuCM4x50 Hardware Reference Manual.        