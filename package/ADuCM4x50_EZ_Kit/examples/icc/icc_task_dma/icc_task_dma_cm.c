/*********************************************************************************
Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*!
* @file      icc_task_dma_cm.c
*
* @brief     This file contains an example that demonstrates ICC task execution
*            upon completion of a peripheral (UART) DMA.
*
*/

#include <stdio.h>
#include <drivers/uart/adi_uart.h>
#include <drivers/pwr/adi_pwr.h>
#include <drivers/icc/adi_icc_cm.h>
#include <adi_libldr.h>
#include "icc_task_dma.h"
#include "common.h"


/* List of hardware error messages */
static char ErrorMsgs[4][30] = 
{                              "\n",
                               "ICC Task overflow occurred \n",
                               "ICC Expiry timer timed out \n",
                               "Shared SRAM error occurred \n"
};

/* Callback event reported, initialize it to no errors */
static volatile uint32_t cbEvent = ADI_ICC_EVENT_ERROR_NONE;

#define CHECK_ERROR(result, expected, message) \
do {\
	if ( (expected) != (result))\
	{\
        common_Fail(message); \
        DEBUG_MESSAGE(ErrorMsgs[cbEvent]);\
        return 1; \
	}\
} while (0)


extern void adi_initpinmux(void);

/* Handle for the UART device. */
static ADI_UART_HANDLE ghUART;

/* Memory for the UART driver. */
static uint8_t UartDeviceMem[ADI_UART_MEMORY_SIZE];

/* First Tx  Buffer. */
static uint8_t  nBufferTx[SIZE_OF_BUFFER]={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};

#if (DEBUG_SSP_CODE == 0)

static char SSP_code[] = {
#include "ssp_ldr.h"
}; 

#endif /* DEBUG_SSP_CODE */

static uint8_t  ICCMemory[ADI_ICC_MEM_SIZE];
static ADI_ICC_HANDLE ghICC;

#if defined ( __ICCARM__ )

#pragma location = "shared_sram"
/* First Rx  Buffer. */
static uint8_t nBufferRx[SIZE_OF_BUFFER];

#else
#error "The example is not ported to this toolchain"
#endif

/* Callback function to catch the errors */
void ErrorHandler(void *pCBParam, uint32_t Event, void  * pArg)
{
    cbEvent = Event;
}

/*********************************************************************
*
*   Function:   main
*
*********************************************************************/
int main(void)
{     
    bool bSSPReadyStatus = false;
    void *pResult;
    uint32_t nResultLen;
    ADI_PWR_RESULT eResultPwr;
    ADI_ICC_RESULT eResultIcc;
    ADI_UART_RESULT eResultUart;
    
    /* Variable to catch any errors when there is not a callback registered. */
    uint32_t HwError;

    /* Variable used to check if the Rx buffer has been filled. */
    bool bRxBufferComplete = false;

    /* Variable that keeps track of any errors found. */
    bool bResult = true;

    /* Variable to get the address of the processed buffer.*/
    void *pProcTxBuff;

    /* Variable to get the buffer size to compare the buffer contents at the end of this test. */
    uint32_t nCounter = 0u;

    /* Timeout nCounter for waiting on a receive buffer to be filled. */
    uint32_t nTimeout = 0u;

    /* Pinmux initialization. */
    adi_initpinmux();
        
    do
    {            
        /* Power initialization. */
        eResultPwr = adi_pwr_Init();
        CHECK_ERROR(eResultPwr, ADI_PWR_SUCCESS, "adi_pwr_Init failed.");

        /* System clock initialization. */
        eResultPwr = adi_pwr_SetClockDivider(ADI_CLOCK_HCLK, 1u);
        CHECK_ERROR(eResultPwr, ADI_PWR_SUCCESS, "adi_pwr_SetClockDivider failed.");
          
        /* Peripheral clock initialization. */
        eResultPwr = adi_pwr_SetClockDivider(ADI_CLOCK_PCLK, 1u);
        CHECK_ERROR(eResultPwr, ADI_PWR_SUCCESS, "adi_pwr_SetClockDivider failed.");
          
        /* Open the bidirectional UART device. */
        eResultUart = adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &ghUART);
        CHECK_ERROR(eResultUart, ADI_UART_SUCCESS, "adi_uart_Open failed.");
          
         /* Open the ICC driver */
        eResultIcc = adi_icc_Open(0, ICCMemory, ADI_ICC_MEM_SIZE, &ghICC);
        CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_Open failed.");
        
        /* Register the error handler */
        eResultIcc = adi_icc_RegisterCallback(ghICC, ErrorHandler, NULL);
        CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_RegisterCallback failed.");          
          
#if (DEBUG_SSP_CODE == 0)
    
        /* Download the code to SSP */
        adi_libldr_MoveDbg(SSP_code,NULL,1);

#endif  /* DEBUG_SSP_CODE */
           
        /* Enable SSP */
        eResultIcc = adi_icc_EnableSSP(ghICC, true);
        CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_EnableSSP failed.");
    
        /* At this point a break point can be placed in SSP code */
        
        /* Start executing the code in SSP */
        eResultIcc = adi_icc_StartSSP(ghICC);
        CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_StartSSP failed.");         

        /* Start the task    on SSP. Task 9 is used as Task 9 mapped to the UART Rx DMA channel (DMA Channel 9)
           in the hardware. */
        eResultIcc = adi_icc_StartTaskOnDmaDone(ghICC, ADI_ICC_TASK_9, ADI_ICC_EVENT_CTRL_BASIC_DMA_DONE, (void *)nBufferRx, SIZE_OF_BUFFER);
        CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_StartTask failed.");        

        /* Wait until SSP is ready to execute the task */
        while(bSSPReadyStatus == false)
        {
          eResultIcc = adi_icc_QueryReadyStatus(ghICC, &bSSPReadyStatus);
          CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_QueryReadyStatus failed.");
        }        

        /* Submit an empty buffer to the driver for receiving data using DMA mode. */
        eResultUart = adi_uart_SubmitRxBuffer(ghUART, nBufferRx, SIZE_OF_BUFFER, 1u);
        CHECK_ERROR(eResultUart, ADI_UART_SUCCESS, "Failed to submit the Rx buffer using dma mode.");
                     
        /* Submit a filled buffer to the driver using DMA mode. This data will be what fills an empty Rx buffer. */
        eResultUart = adi_uart_SubmitTxBuffer(ghUART, nBufferTx, SIZE_OF_BUFFER, 1u);
        CHECK_ERROR(eResultUart, ADI_UART_SUCCESS, "Failed to submit Tx buffer using dma mode.");            
               
        /* Return the buffer back to the API. "pProcTxBuff" should contain the address
           of "nBufferTx".
        */
        eResultUart = adi_uart_GetTxBuffer(ghUART, &pProcTxBuff, &HwError);
        CHECK_ERROR(eResultUart, ADI_UART_SUCCESS, "Failed to Get Tx buffer.");           
        
        /* Wait here until the processing of the first Rx buffer has completed or a timeout occured. */
         while((bRxBufferComplete == false) && (nTimeout != UART_GET_BUFFER_TIMEOUT))
          {   
            eResultUart = adi_uart_IsRxBufferAvailable(ghUART, &bRxBufferComplete);
            CHECK_ERROR(eResultUart, ADI_UART_SUCCESS, "Failed to check if the Rx buffer is available.");
            nTimeout ++;
          }
          
          /* Make sure there was not a timeout. */     
          if(nTimeout == UART_GET_BUFFER_TIMEOUT)
          { 
              DEBUG_MESSAGE("Timeout. Check loopback connection.");
              bResult = false;
              break;
          }
          else
          {
            nTimeout = 0u;
          }

        /* Wait until the task is done */
        eResultIcc = adi_icc_WaitForTaskDone(ghICC, &pResult, &nResultLen, &HwError);
        CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_WaitForTaskDone failed.");          

        CHECK_ERROR(*(uint32_t *)pResult, EXPECTED_RESULT, "The Sum result did not match with the expected result.");
        DEBUG_MESSAGE("Result = %d\n", *(uint32_t *)pResult);        
     
        /* Close the device. */
        eResultUart = adi_uart_Close(ghUART);
        CHECK_ERROR(eResultUart, ADI_UART_SUCCESS, "Failed to close the UART Device.");

        /* Disable SSP */
        eResultIcc = adi_icc_DisableSSP(ghICC);
        CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_DisableSSP failed.");

        /* Close ICC Driver */
        eResultIcc = adi_icc_Close(ghICC);
        CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_Close failed.");
        
    }while(0);
        
    if(bResult == true)
    {   
        /* Make sure the data transfers all worked properly. */
        for(nCounter = 0u; nCounter < SIZE_OF_BUFFER; nCounter++)
        {
           if((nBufferTx[nCounter] != nBufferRx[nCounter]))
           {
              bResult = false;
              DEBUG_MESSAGE("\nDetected a mismatch in Rx and Tx buffers.");
              break;
           }
        }  
    }

    /* check if there were any hardware errors */
    CHECK_ERROR(cbEvent, ADI_ICC_EVENT_ERROR_NONE, ErrorMsgs[cbEvent]);
    
    if(bResult == true)
    {
        DEBUG_MESSAGE("\n ICC Task with UART DMA example completed successfully.\n");
        common_Pass();
    }
    else
    {   
      common_Fail("ICC Task with UART DMA example failed.");
    }

    return(0);
}
