    <example name="SPI_MasterSlave_LoopBack_Example" folder="Boards/ADuCM4050-EZ-KIT/Examples/spi/MasterSlaveLoopBack" doc="ADuCM4x50/Readme_MasterSlave_loopback.txt" version="1.0.0">
    <description>Basic example demonstrating the SPI driver</description>
    <board name="ADuCM4050 EZ-KIT" vendor="AnalogDevices" Dname="ADuCM4050"/>
        <project>
        <environment name="uv"   load="ADuCM4x50/keil/MasterSlaveLoopback.uvprojx"/>
        <environment name="cces" load="ADuCM4x50/cces/.project" />
        </project>
        <attributes>
        <category>Driver Examples</category>
        <component Cclass="CMSIS"  Cgroup="CORE"/>
        <component Cclass="Device" Cgroup="Startup"/>
        <component Cclass="Device" Cgroup="Retarget" />
        <component Cclass="Device" Cgroup="Examples Support" />
        <component Cclass="Device" Cgroup="Drivers" Csub="SPI"/>
        <keyword>SPI</keyword>
        </attributes>
    </example>
