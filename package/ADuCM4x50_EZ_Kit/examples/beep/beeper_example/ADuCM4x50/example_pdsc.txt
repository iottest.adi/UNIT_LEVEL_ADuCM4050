    <example name="beeper_example" folder="Boards/ADuCM4050-EZ-KIT/Examples/beep/beeper_example" doc="ADuCM4x50/Readme_beeper_example.txt" version="1.0.0">
    <description>Basic example demonstrating the BEEP driver</description>
    <board name="ADuCM4050 EZ-KIT" vendor="AnalogDevices" Dname="ADuCM4050"/>
        <project>
        <environment name="uv"   load="ADuCM4x50/keil/beeper_example.uvprojx"/>
        <environment name="cces" load="ADuCM4x50/cces/.project" />
        </project>
        <attributes>
        <category>Driver Examples</category>
        <component Cclass="CMSIS"  Cgroup="CORE"/>
        <component Cclass="Device" Cgroup="Startup"/>
        <component Cclass="Device" Cgroup="Retarget" />
        <component Cclass="Device" Cgroup="Examples Support" />
        <component Cclass="Device" Cgroup="Drivers" Csub="BEEP"/>
        <keyword>BEEP</keyword>
        </attributes>
    </example>
