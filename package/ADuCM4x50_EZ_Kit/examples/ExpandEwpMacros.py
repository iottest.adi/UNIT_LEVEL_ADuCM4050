# fixProjectFiles.py
# expand IAR project file TOKENs so project files can be run manually under the IDE
# don't save (check-in) the expanded project files because it will break the Hudson regression testing

import os
import sys
import getopt
import string
import re
import tempfile

# Seperate dictionaries for each target
ewp_macro_dict = {
	"$_MICRIUM_DIR_$"	: os.path.expandvars("$MICRIUM_DIR")
	}

def getFileList( path , regex ):
	"""Returns a list of files from the directories starting
	at the path passed in"""

	head, tail = os.path.split(path)
	drive = os.path.splitdrive(path);
	drive = drive[0] + "\\";

	# ingest all elements on the path
	while head != drive:
		if head not in sys.path:
			sys.path.append(head)
		head, tail = os.path.split(head)

	# the matching callback
	def callback(filelist, directory, files):

		if directory not in sys.path:
			sys.path.append(directory)

		test = re.compile(regex, re.IGNORECASE)
		filteredFiles = filter(test.search, files)

		# only if we have matches...
		if len(filteredFiles) is not 0:

			# add if not in list already
			fileFound = False
			for file in filteredFiles:
				for f in filelist:
					if f == file:
						fileFound = True

				if( not fileFound ):
					filelist.append(os.path.join(directory, file))

	filelist = []
	os.path.walk (path, callback, filelist)
	return filelist


def translateText(text, dictionary):
    """Translate text of any and all key matches
    in the dictionary with the replacement value
    """
    for key in dictionary:
        text = text.replace(key, dictionary[key])
    return text


def usage():

	print "No arguments... just translate macros from internal dictionary."


def main():

	# default empty dictionary
	dictionary = {}

	# list of project file types to translate (including both IAR & Keil)
	projectFileTypeList = [".ewp$"]

	try:
		allowed = ''
		options, remainder = getopt.getopt( sys.argv[1:], allowed )

	except(getopt.error), msg:
	   sys.stderr.write( 'ERROR: %s\n' % str(msg) )
	   usage()
	   sys.exit(-1)

	dictionary = ewp_macro_dict

	# make sure we replaced our default empty dictionary
	if not dictionary:
		print "Unhandled or unspecified manditory option"
		usage()
		sys.exit(-1)

	# for each project file type...
	for projectFileType in projectFileTypeList:

		# get a list of all project files of that type under the cwd
		projectFileList = getFileList( os.getcwd() , projectFileType)

		# for each project file...
		for projectFile in projectFileList:

			print 'Fixing project file: %s\n' % projectFile

			# reset output array
			expandedLines = []

			# open project file in read-only mode
			fp = open(projectFile, 'r')

			# replace the lines
			for line in fp.readlines():

				# translate the line according to target dictionary
				newline = translateText(line, dictionary)

				# append the expanded line
				expandedLines.append(newline)

			# close the read-only project file
			fp.close()

			# rewrite the project file with the expanded lines
			fp = open(projectFile, 'w')
			fp.writelines(expandedLines)
			fp.close()


if __name__ == "__main__":
	main()

