    <example name="Autobaud" folder="Boards/ADuCM4050-EZ-KIT/Examples/uart/Autobaud" doc="ADuCM4x50/Readme_autobaud.txt" version="1.0.0">
    <description>UART autobaud example</description>
    <board name="ADuCM4050 EZ-KIT" vendor="AnalogDevices" Dname="ADuCM4050"/>
        <project>
        <environment name="uv"   load="ADuCM4x50/keil/Autobaud.uvprojx"/>
        <environment name="cces" load="ADuCM4x50/cces/.project" />
        </project>
        <attributes>
        <category>Driver Examples</category>
        <component Cclass="CMSIS"  Cgroup="CORE"/>
        <component Cclass="Device" Cgroup="Startup"/>
        <component Cclass="Device" Cgroup="Retarget" />
        <component Cclass="Device" Cgroup="Examples Support" />
        <component Cclass="Device" Cgroup="Drivers" Csub="UART"/>
        <keyword>UART</keyword>
        </attributes>
    </example>
