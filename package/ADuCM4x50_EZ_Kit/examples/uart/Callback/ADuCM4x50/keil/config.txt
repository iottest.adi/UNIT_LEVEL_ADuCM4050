CATEGORY=BSP,TOOLCHAINS

PROCESSOR = ADuCM4050

TOOLCHAIN = KEIL

#delete any previous build files 
<CMD, del /Q/S "$testdir\Listings">
<CMD, del /Q/S "$testdir\Objects">
<CMD, del /Q/S "$testdir\RTE">
<CMD, del /Q/S "$testdir\output">
 

###### Debug Config ######
<BUILDPROJECT, $testdir\uart_callback.uvprojx, Debug >
<BUILDFIND, 0 Error(s) >
<BUILDFIND, 0 Warning(s) >
<ASSERT_FILEEXISTS, $testdir\Objects\uart_callback.axf>
<LOAD, $testdir\uart_callback.uvprojx, Debug>
<RUN,TTH:TIMEOUT(20)>
<BUILDFIND,TTH:NOCASE:MATCHANY, All done>


###### Release Config ######
<BUILDPROJECT, $testdir\uart_callback.uvprojx, Release >
<BUILDFIND, 0 Error(s) >
<BUILDFIND, 0 Warning(s) >
<ASSERT_FILEEXISTS, $testdir\Objects\uart_callback.axf>
<LOAD, $testdir\uart_callback.uvprojx, Release>
<RUN,TTH:TIMEOUT(20)>
<BUILDFIND,TTH:NOCASE:MATCHANY, All done>
