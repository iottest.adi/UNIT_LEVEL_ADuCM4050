    <example name="systick_example" folder="Boards/ADuCM4050-EZ-KIT/Examples/SysTick/systick_example" doc="Readme_systick_example.txt" version="1.0.0">
    <description>Example to demonstrate SysTick timer to generate specific number of interrupts</description>
    <board name="ADuCM4050 EZ-KIT" vendor="AnalogDevices" Dname="ADuCM4050"/>
        <project>
        <environment name="uv"   load="ADuCM4x50/keil/systick_example.uvprojx"/>
        <environment name="cces" load="ADuCM4x50/cces/.project" />
        </project>
        <attributes>
        <category>Driver Examples</category>
        <component Cclass="CMSIS"  Cgroup="CORE"/>
        <component Cclass="Device" Cgroup="Startup"/>
        <component Cclass="Device" Cgroup="Retarget" />
        <component Cclass="Device" Cgroup="Examples Support" />
        <component Cclass="Device" Cgroup="Drivers" Csub="TMR"/>
        <keyword>SysTick</keyword>
        </attributes>
    </example>
