    <example name="temperature_sensor" folder="Boards/ADuCM4050-EZ-KIT/Examples/i2c/temperature_sensor" doc="ADuCM4x50/Readme_temperature_sensor.txt" version="1.0.0">
    <description>Temperature Sensor example using the I2C driver</description>
    <board name="ADuCM4050 EZ-KIT" vendor="AnalogDevices" Dname="ADuCM4050"/>
        <project>
        <environment name="uv"   load="ADuCM4x50/keil/temperature_sensor.uvprojx"/>
        <environment name="cces" load="ADuCM4x50/cces/.project" />
        </project>
        <attributes>
        <category>Driver Examples</category>
        <component Cclass="CMSIS"  Cgroup="CORE"/>
        <component Cclass="Device" Cgroup="Startup"/>
        <component Cclass="Device" Cgroup="Retarget" />
        <component Cclass="Device" Cgroup="Examples Support" />
        <component Cclass="Device" Cgroup="Drivers" Csub="I2C"/>
        <keyword>I2C</keyword>
        </attributes>
    </example>
