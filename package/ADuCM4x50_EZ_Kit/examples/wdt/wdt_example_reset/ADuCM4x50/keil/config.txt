CATEGORY=BSP,TOOLCHAINS

PROCESSOR = ADuCM4050

TOOLCHAIN = KEIL

#delete any previous build files 
<CMD, move /Y "$testdir\RTE\Device\ADuCM4050\adi_wdt_config.h" "$testdir">
<CMD, del /Q/S "$testdir\Listings">
<CMD, del /Q/S "$testdir\Objects">
<CMD, del /Q/S "$testdir\RTE">
<CMD, del /Q/S "$testdir\output">
<CMD, move /Y "$testdir\adi_wdt_config.h" "$testdir\RTE\Device\ADuCM4050" >
 

###### Debug Config ######
<BUILDPROJECT, $testdir\wdt_example_reset.uvprojx, Debug >
<BUILDFIND, 0 Error(s) >
<BUILDFIND, 0 Warning(s) >
<ASSERT_FILEEXISTS, $testdir\Objects\wdt_example_reset.axf>
<LOAD, $testdir\wdt_example_reset.uvprojx, Debug>
<RUN, TTH:TIMEOUT(60)>
<BUILDFIND,TTH:NOCASE:MATCHANY, All done>


###### Release Config ######
<BUILDPROJECT, $testdir\wdt_example_reset.uvprojx, Release >
<BUILDFIND, 0 Error(s) >
<BUILDFIND, 0 Warning(s) >
<ASSERT_FILEEXISTS, $testdir\Objects\wdt_example_reset.axf>
<LOAD, $testdir\wdt_example_reset.uvprojx, Release>
<RUN, TTH:TIMEOUT(60)>
<BUILDFIND,TTH:NOCASE:MATCHANY, All done>
