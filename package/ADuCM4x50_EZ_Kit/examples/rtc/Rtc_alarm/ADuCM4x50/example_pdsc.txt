    <example name="rtc_alarm" folder="Boards/ADuCM4050-EZ-KIT/Examples/rtc/Rtc_alarm" doc="ADuCM4x50/Readme_rtc_alarm.txt" version="1.0.0">
    <description>Example demonstrating the RTC alarm feature</description>
    <board name="ADuCM4050 EZ-KIT" vendor="AnalogDevices" Dname="ADuCM4050"/>
        <project>
        <environment name="uv"   load="ADuCM4x50/keil/rtc_alarm.uvprojx"/>
        <environment name="cces" load="ADuCM4x50/cces/.project" />
        </project>
        <attributes>
        <category>Driver Examples</category>
        <component Cclass="CMSIS"  Cgroup="CORE"/>
        <component Cclass="Device" Cgroup="Startup"/>
        <component Cclass="Device" Cgroup="Retarget" />
        <component Cclass="Device" Cgroup="Examples Support" />
        <component Cclass="Device" Cgroup="Drivers" Csub="RTC"/>
        <keyword>RTC, Alarm</keyword>
        </attributes>
    </example>
