# CollectFootprintData.py
# parse all IAR-generated linker map files for "MODULE SUMMARY" data on
# read-only code, read-only data, and read-write data usage by module
# and generate a report.

import os
import sys
import getopt
import string
import re
import tempfile

# object class
class ObjectEntry :
	def __init__(self, name, roc, rod, rwd) :
		self.filename = name
		self.roCode = roc
		self.roData = rod
		self.rwData = rwd

	def __lt__(self, other) :
		return self.filename < other.filename

# list of objects
ObjectList = []

# recursive file search
def getFileList( path , regex ) :
	"""Returns a list of files from the directories starting
	at the path passed in"""

	head, tail = os.path.split(path)
	drive = os.path.splitdrive(path);
	drive = drive[0] + "\\";

	# ingest all elements on the path
	while head != drive :
		if head not in sys.path :
			sys.path.append(head)
		head, tail = os.path.split(head)

	# the matching callback
	def callback(filelist, directory, files) :

		if directory not in sys.path :
			sys.path.append(directory)

		test = re.compile(regex, re.IGNORECASE)
		filteredFiles = filter(test.search, files)

		# only if we have matches...
		if len(filteredFiles) is not 0 :

			# add if not in list already
			fileFound = False
			for file in filteredFiles :
				for f in filelist :
					if f == file :
						fileFound = True

				if( not fileFound ) :
					filelist.append(os.path.join(directory, file))

	filelist = []
	os.path.walk (path, callback, filelist)
	return filelist


def usage() :

	print "No arguments... just parse map files from cwd recursively."


def main() :

	# regular expression describing the file types to parse
	MapFileTypeList = ['.map$']

	try :
		allowed = ''
		options, remainder = getopt.getopt( sys.argv[1:], allowed )

	except(getopt.error), msg :
	   sys.stderr.write( 'ERROR: %s\n' % str(msg) )
	   usage()
	   sys.exit(-1)

	# build a list of memory types and usage...

	# for each map file type...
	for MapFileType in MapFileTypeList :

		# get a list of all project files of that type under the cwd
		MapFileList = getFileList( os.getcwd() , MapFileType)

		# for each map file...
		for MapFile in MapFileList :

			if os.path.isdir(MapFile):
#				print MapFile
				continue
			print 'Parsing map file: %s' % MapFile

			# open the file
			fp = open(MapFile, 'r')

			# start with module parsing off
			parsing = False

			# load and parse the file lines
			for line in fp.readlines() :

				# pad the line to a nice consistent length for parsing
				line = line.ljust(60, ' ')

				# start parsing once we find the "Modules" section
				if "Module" in line :
					parsing = True

					# locate this file's first memory column (which changes from file to file... thank you very much, IAR guy)
					rocKey = 'ro code'
					rocColumn = line.find(rocKey) + len(rocKey) - 1
					continue

				# while parsing...
				if parsing :

					# disable parsing if its a debug build
					if 'DEBUG\OBJ:' in line.upper() :  # case invariant because it varies
						parsing = False
						print '...ignoring debug map file data'
						continue

					# reset memory array
					memory = []

					# collect object file listings reporting memory data
					if ".o" in line :

						# match lines containing an object filename of the form: "*.o"
						m = re.search('([a-zA-Z0-9_ \.\-]*\.o)', line)
						objectFile = m.group(1).lstrip()

						# parse line for all (optionally single-space-seperated) values
						# blank the line through ".o" in the filename to
						# avoid false re matches dur to numbers in filenames
						dot = line.find('.') + 2
						cleanLine = ' '*dot + line[dot:]
						reMatch = re.findall('\d+\s\d+|\d+', cleanLine)

						# scan the line for values/blanks
						charIndex = rocColumn
						eol = len(line)
						matchIndex = 0
						for memIndex in range(3) :

							# check for eol first
							if (charIndex >= eol) :

								# treat eol as zero
								memory.append(0)

							# check for blank fields
							elif (line[charIndex] == ' ') :

								# treat blanks as zero
								memory.append(0)

							else :

								# fetch non-blank strings from the re match
								matchItem = reMatch[matchIndex]
								matchIndex += 1

								# collapse any mid-string spaces (used instead of commas, thanks again, IAR guy)
								collapsed = matchItem.replace(' ','')

								# convert and append
								memory.append(int(collapsed))

							# index to next potential memory string on the line
							charIndex += 9

						# append new object file data to list
						ObjectList.append(ObjectEntry(objectFile, memory[0], memory[1], memory[2]))

					# stop parsing at end of "Modules" section
					if "Total:" in line :
						parsing = False

			# close the map file
			fp.close()


	# don't process null lists
	if not ObjectList :
		print 'No qualifying map files processed...'

	else :
		print "Average Memory Utilization by File:          (roCode, roData, rwData)"

		# sort the list
		ObjectList.sort()

		# initialize
		currentFilename = ObjectList[0].filename
		start = 0
		end = 0

		# process the memory use
		for index in range(len(ObjectList)) :

			# search for a name change or if we hit the last file
			if ( currentFilename != ObjectList[index].filename or index == (len(ObjectList) - 1) ):

				# name change... compute averages over number of files
				# get a sub list covering only this filename
				subList = ObjectList[start:end]

				# break out each memory type into seperate lists
				rocList = [o.roCode for o in subList]
				rodList = [o.roData for o in subList]
				rwdList = [o.rwData for o in subList]

				# average them each
				rocAve = sum(rocList)/len(rocList)
				rodAve = sum(rodList)/len(rodList)
				rwdAve = sum(rwdList)/len(rwdList)

				# log the results
				item = currentFilename + ' x %d:'%(end-start)
				item = item + ' '*(35-len(item))
				print '\t%s\t%d,\t%d,\t%d' % (item, rocAve, rodAve, rwdAve)

				# grab next filename
				currentFilename = ObjectList[index].filename
				start = index
				end = start + 1

			else :

				# same filename... just extend the end index
				end += 1


if __name__ == "__main__" :
	main()

