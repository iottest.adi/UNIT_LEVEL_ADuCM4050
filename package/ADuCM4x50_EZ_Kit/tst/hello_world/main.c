#include <common.h>
#include <adi_processor.h>

int32_t main(void)
{
  common_Init();
  
  common_Perf("Hello world\n");
  
  common_Pass();
  
  return 0;
}
