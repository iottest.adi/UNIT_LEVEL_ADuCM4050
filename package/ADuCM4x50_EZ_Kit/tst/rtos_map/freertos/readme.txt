Must have an environment variable called `ADI_FREERTOS_PATH` which points to the install directory of FreeRTOS, such as:
`C:\FreeRTOSv9.0.0\FreeRTOS\Source`
