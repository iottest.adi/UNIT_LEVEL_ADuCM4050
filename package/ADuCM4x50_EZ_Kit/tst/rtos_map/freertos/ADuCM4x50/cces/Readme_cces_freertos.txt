         Analog Devices, Inc. ADuCM4x50 Application Example

Project Name: freertos_test    

Description: Test to ensure that the RTOS Map file is correct
  
Overview:
=========
  This test was created using FreeRTOS 9.0.0
  

User Configuration Macros:
==========================
  None
  
Hardware Setup:
===============
  ADuCM4x50 EZ Kit configured with default settings.
    
External connections:
=====================
  None.
  
How to build and run:
=====================
  Download and copy the FreeRTOS Source directory into the same directory as the .project and .cproject files.
  Then delete all of the sources that are not applicable (or, disable them from within CCES:  Resource Configuration -> Exclude from build)
  
Expected Result:
=================
  The application will print out
  
    Semaphore Acquired!
   
References:
===========
    ADuCM4x50 Hardware Reference Manual
