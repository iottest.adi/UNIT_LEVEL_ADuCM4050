/*
 *******************************************************************************
 * @file:    main.c
 *
 * @brief:   Pre-RTOS startup file
 *******************************************************************************

 Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

 This software is proprietary and confidential.  By using this software you agree
 to the terms of the associated Analog Devices License Agreement.

 ******************************************************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <adi_processor.h>
#include <system_ADuCM4050.h>



#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/***************************** TEST RTOS MAP FILE *****************************/
#include <rtos_map/adi_rtos_map.h>


/* Helper macro */
#define CHECK_ERROR(result, expected, message) \
do {\
    if ( (expected) != (result))\
    {\
        printf(message); \
        while(1) {} \
    }\
} while (0);

/* FPGA LED L and C */
#define GPIO_PORT (pADI_GPIO2)
#define LED0      (2u)
#define LED1      (3u)


typedef struct {
    SEM_VAR_DECLR
} Special_Struct_t;


uint8_t testSemCreate(Special_Struct_t * pDev) {
    SEM_CREATE(pDev, "MySem", 1u);
    return 0u;

}

uint8_t testSemDelete(Special_Struct_t * pDev) {
    SEM_DELETE(pDev, 1u);
    return 0u;

}

uint8_t testSemPend  (Special_Struct_t * pDev) {
    SEM_PEND(pDev, 1u);
    return 0u;
}

void    testSemPost  (Special_Struct_t * pDev) {
    SEM_POST(pDev);
}

/******************************************************************************/

/* Global semaphore variable to be shared by the two tasks */
Special_Struct_t myStruct;

/* Blinks two LEDs on the FPGA and posts a semaphore every second */
void vBlinkyTask(void *pvParameters) {
    /* Initialize LEDs */
    GPIO_PORT->CFG &= ~(3 << LED0*2) | ~(3 << LED1*2);
    GPIO_PORT->OEN |=  (1 << LED0)   |  (1 << LED1);
    /* WHILE(forever) */
    while(1) {
        /* Delay for 1 second */
        vTaskDelay((TickType_t)(1000u / portTICK_PERIOD_MS));
        /* Toggle LEDs */
        GPIO_PORT->TGL = (1 << LED0) | (1 << LED1);
        /* Post the semaphore */
        testSemPost(&myStruct);
    } /* ENDIF */
}

/* Prints a message to the terminal when it acquires the semaphore */
void vSemTestTask(void *pvParameters) {
    uint8_t eResult;
    /* WHILE(forever) */
    while(1) {
        eResult = testSemPend(&myStruct);
        CHECK_ERROR(eResult, 0u, "Error pending on the semaphore")
        printf("Semaphore Acquired!\r\n");
    } /* ENDIF */
}

#define STACK_SZ  (configMINIMAL_STACK_SIZE + 50u)

static StackType_t blinkTaskStack[STACK_SZ];
static StackType_t semTestTaskStack[STACK_SZ];
static StaticTask_t xBlinkyTaskTCB;
static StaticTask_t xSemTestTaskTCB;
static TaskHandle_t blinkTaskHandle;
static TaskHandle_t semTestTaskHandle;

StaticTask_t idleTask;
StackType_t  idleTaskStack[STACK_SZ];

/* 
 * @brief      Application start function for low-level initialization and RTOS creation
 *
 * @retval     Returns 0u if failure occurs, should never return
 */
int32_t main(void)
{

    uint8_t             eResult;
  
    /* Check the semaphore size macro */
    CHECK_ERROR(ADI_SEM_SIZE, sizeof(SemaphoreHandle_t), "Error using the semaphore size macro")

    /* Create the semaphore */
    eResult = testSemCreate(&myStruct);
    CHECK_ERROR(eResult, 0u, "Error creating the semaphore")

    /* Delete the semaphore */
    eResult = testSemDelete(&myStruct);
    CHECK_ERROR(eResult, 0u, "Error deleting the semaphore")

    /* Create the semaphore again */
    eResult = testSemCreate(&myStruct);    
    CHECK_ERROR(eResult, 0u, "Error creating the semaphore")

    /* Create the tasks */
    blinkTaskHandle = xTaskCreateStatic(vBlinkyTask, "Blinky", STACK_SZ, NULL, tskIDLE_PRIORITY + 2u, &blinkTaskStack[0], &xBlinkyTaskTCB);
    if( blinkTaskHandle == 0)
    {
      printf("Task creation failed\n");
      return 1;
    }
    semTestTaskHandle = xTaskCreateStatic(vSemTestTask, "SemTest", STACK_SZ, NULL, tskIDLE_PRIORITY + 1u, &semTestTaskStack[0], &xSemTestTaskTCB);
    if( semTestTaskHandle == 0)
    {
      printf("Task creation failed\n");
      return 1;
    }

    /* Start the RTOS (does not return) */
    vTaskStartScheduler();
    
    return 0;
}


void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize )
{
  *ppxIdleTaskTCBBuffer = &idleTask;
  *ppxIdleTaskStackBuffer = &idleTaskStack[0];
  *pulIdleTaskStackSize = STACK_SZ;
}


