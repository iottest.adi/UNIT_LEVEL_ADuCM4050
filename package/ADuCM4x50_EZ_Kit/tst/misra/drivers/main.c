#include <drivers/uart/adi_uart.h>
#include <drivers/wdt/adi_wdt.h>
#include <drivers/spi/adi_spi.h>
#include <drivers/i2c/adi_i2c.h>
#include <drivers/xint/adi_xint.h>
#include <drivers/gpio/adi_gpio.h>
#include <drivers/pwr/adi_pwr.h>

extern const uint32_t ReadProKeyHsh[4];
extern const uint32_t CrcOfReadKey;
extern const uint32_t NumOfCRCPages;
extern uint32_t SystemCoreClock;

const void *var_refs[] = {
  &ReadProKeyHsh,
  &CrcOfReadKey,
  &NumOfCRCPages,
  &SystemCoreClock
};

void main(void) {
    adi_wdt_Kick();
}
