/*********************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*!
* @file      adc_tst.c
* @brief     Test parts of the ADC driver.
*
*/

/*=============  I N C L U D E S   =============*/

/* ADC example include */
#include "adc_tst.h"
/* Managed drivers and/or services include */
#include <drivers/adc/adi_adc.h>
#include "common.h"
#include "drivers/pwr/adi_pwr.h"
#include <assert.h>
#include <stdio.h>
#include <drivers/general/adi_drivers_general.h>
/*=============  D A T A  =============*/

/* ADC Handle */
ADI_ALIGNED_PRAGMA(4)
ADI_ADC_HANDLE hDevice ADI_ALIGNED_ATTRIBUTE(4);

/* Memory Required for adc driver */
ADI_ALIGNED_PRAGMA(4)
static uint8_t DeviceMemory[ADI_ADC_MEMORY_SIZE] ADI_ALIGNED_ATTRIBUTE(4);

/* ADC Data Buffer */
ADI_ALIGNED_PRAGMA(4)
static uint16_t ADC_DataBuffer[ADC_NUM_SAMPLES] ADI_ALIGNED_ATTRIBUTE(4) = {0};


/* Example configurations */
#define CHANNEL_TO_USE        (ADI_ADC_CHANNEL_0)
#define ADC_WAIT_FOR_LIMIT    (1)
#define ADC_GET_TEMP          (1)
#define ADC_GET_BATTERY       (1)

#define READ_CHANNEL_SAMPLES  (8)

volatile bool bLimitCrossed_Hi = false;
volatile bool bLimitCrossed_Lo = false;

/*=============  E X T E R N A L    F U N C T I O N S  =============*/
int32_t adi_initpinmux(void);

/*=============  L O C A L    F U N C T I O N S  =============*/

static void usleep(uint32_t usec);

#if ADC_WAIT_FOR_LIMIT  == 1
static void WaitForLimit(ADI_ADC_HANDLE hDevice);
#endif

/*=============  C O D E  =============*/

/* Callback from the device */
static void ADCCallback(void *pCBParam, uint32_t Event, void *pArg)
{
    switch (Event)
    {
    case ADI_ADC_EVENT_HIGH_LIMIT_CROSSED:
        bLimitCrossed_Hi = true;
        break;
    case ADI_ADC_EVENT_LOW_LIMIT_CROSSED:
        bLimitCrossed_Lo = true;
        break;

    default:
        break;
    }
}

/* Set up and wait for a comparitor setting to trigger.
 *
 * Only tests one channel
 */
static void WaitForLimit(ADI_ADC_HANDLE hDevice)
{
    ADI_ADC_RESULT  eResult = ADI_ADC_SUCCESS;
    
    /* Configure high limit for an ADC channel when it's used as a digital comparator. */
    eResult = adi_adc_SetLowLimit (hDevice, CHANNEL_TO_USE, true, 1000);
    DEBUG_RESULT("Failed to set the acquisition time ", eResult, ADI_ADC_SUCCESS);	

    /* Configure hysteresis for an ADC channel when it's used as a digital comparator. */
    eResult = adi_adc_SetHysteresis(hDevice, CHANNEL_TO_USE, true, 0);
    DEBUG_RESULT("Failed to set adi_adc_SetHysteresis ", eResult, ADI_ADC_SUCCESS);

    /* Configure number of monitor cycles for an ADC channel when it's used as a digital comparator. */
    eResult = adi_adc_SetNumMonitorCycles(hDevice, CHANNEL_TO_USE, 0);
    DEBUG_RESULT("Failed to set adi_adc_SetNumMonitorCycles ", eResult, ADI_ADC_SUCCESS);

    /*  Enable/Disable digital comparator for the given channel(s) */
    eResult = adi_adc_EnableDigitalComparator (hDevice, true);  
    DEBUG_RESULT("Failed to set adi_adc_EnableDigitalComparator ", eResult, ADI_ADC_SUCCESS);

    while(!bLimitCrossed_Lo);
    common_Perf("Limit crossed!\n");
    
    eResult = adi_adc_EnableDigitalComparator (hDevice,false);  
    DEBUG_RESULT("Failed to set adi_adc_EnableDigitalComparator ", eResult, ADI_ADC_SUCCESS);  
}


/* Simple test to get the current battery voltage. 
 * Assumes the battery voltage is around 3.15V
 *
 */
static void GetBatteryVoltage(ADI_ADC_HANDLE hDevice)
{
    ADI_ADC_RESULT  eResult = ADI_ADC_SUCCESS;
    uint32_t  voltage_read = 0u;
    
    /* The "safe" range of battery voltages is between 2.8V and 3.6V, use this
     * as a passsing range. */
    uint32_t  voltage_known_low  = (2u << 16u) + (uint32_t)(0.80 * 0xFFFFu);
    uint32_t  voltage_known_hi   = (3u << 16u) + (uint32_t)(0.60 * 0xFFFFu);
    
    eResult = adi_adc_GetBatteryVoltage(hDevice, (uint32_t)((2u << 16u) + (1u << 8u)), &voltage_read);
    DEBUG_RESULT("Failed to get voltage ", eResult, ADI_ADC_SUCCESS);	
    
    /* We expect the voltage to be around 3.15V, let's assume
     * around 10% variance. */
    if(voltage_read > voltage_known_low) {
      if(voltage_read < voltage_known_hi) {
         common_Perf("Voltage OK");
      }
      else {
         common_Fail("Measured battery voltage too high!");
      }
    }
    else {
       common_Fail("Measured battery voltage too low!");
      }
}


/* Simple test to get the current temperature of the chip. 
 * Assumes a fairly stable, room-temperature value (centigrade).
 *
 */
static void GetChipTemperature(ADI_ADC_HANDLE hDevice)
{
    ADI_ADC_RESULT  eResult = ADI_ADC_SUCCESS;
    int32_t  temp_read = 0;
    int32_t  temp_known_high = 30; /* degrees c */
    int32_t  temp_known_low  = 15; /* degrees c */
    
    eResult = adi_adc_EnableTemperatureSensor(hDevice, true);
    DEBUG_RESULT("Failed to get temperature ", eResult, ADI_ADC_SUCCESS);	
    
    eResult = adi_adc_GetTemperature (hDevice, (uint32_t)((2u<< 16u) + 5), &temp_read);
    DEBUG_RESULT("Failed to get temperature ", eResult, ADI_ADC_SUCCESS);	
    
    if(temp_read > temp_known_low) {
      if(temp_read < temp_known_high) {
         common_Perf("Temp just right");
      }
      else {
         common_Perf("Temp ok!");  
      }
    }
    else {
        common_Fail("Temperature too low!");
    }
    
    eResult = adi_adc_EnableTemperatureSensor(hDevice, false);
    DEBUG_RESULT("Failed to get temperature ", eResult, ADI_ADC_SUCCESS);	
}

    
void SetUpADC(ADI_ADC_HANDLE hDevice)
{
    ADI_ADC_RESULT  eResult = ADI_ADC_SUCCESS;
    bool  bCalibrationDone = false;	
    
    /* Power up ADC */
    eResult = adi_adc_PowerUp (hDevice, true);
    DEBUG_RESULT("Failed to power up ADC", eResult, ADI_ADC_SUCCESS);

    /* Set ADC reference */
    eResult = adi_adc_SetVrefSource (hDevice, ADI_ADC_VREF_SRC_INT_2_50_V);
    DEBUG_RESULT("Failed to set ADC reference", eResult, ADI_ADC_SUCCESS);

    /* Enable ADC sub system */
    eResult = adi_adc_EnableADCSubSystem (hDevice, true);
    DEBUG_RESULT("Failed to enable ADC sub system", eResult, ADI_ADC_SUCCESS);

    /* Wait for 5.0ms */
    usleep (5000);
    
    /* Start calibration */
    eResult = adi_adc_StartCalibration (hDevice);
    DEBUG_RESULT("Failed to start calibration", eResult, ADI_ADC_SUCCESS);

    /* Wait until calibration is done */
    while (!bCalibrationDone)
    {
        eResult = adi_adc_IsCalibrationDone (hDevice, &bCalibrationDone);
        DEBUG_RESULT("Failed to get the calibration status", eResult, ADI_ADC_SUCCESS);
    }	  
  
}

/* Simply read a given channel - an unconnected EZ-kit will have
 * a slightly wandering signal around 0x0, so hard to test for any accuracy.
 * Simply read, and if returned successfully, then return successfully. */
void ReadChannels(ADI_ADC_HANDLE hDevice)
{
  ADI_ADC_RESULT  eResult = ADI_ADC_SUCCESS;
  uint32_t data[READ_CHANNEL_SAMPLES];
  
  eResult = adi_adc_ReadChannels (hDevice, CHANNEL_TO_USE, READ_CHANNEL_SAMPLES*2, (void *)&data, sizeof(data));
  DEBUG_RESULT("Failed to Read Channels ", eResult, ADI_ADC_SUCCESS);
    
  common_Perf("Read channels done ");     
}


/* Sample the ADC */
void SampleData(ADI_ADC_HANDLE hDevice)
{
    ADI_ADC_BUFFER Buffer;
    ADI_ADC_RESULT  eResult = ADI_ADC_SUCCESS;
        
    /* Set the delay time */
    eResult = adi_adc_SetDelayTime ( hDevice, 10);
    DEBUG_RESULT("Failed to set the Delay time ", eResult, ADI_ADC_SUCCESS);

    /* Set the acquisition time. (Application need to change it based on the impedence) */
    eResult = adi_adc_SetAcquisitionTime ( hDevice, 10);
    DEBUG_RESULT("Failed to set the acquisition time ", eResult, ADI_ADC_SUCCESS);	

    /* Unregister callback */
    eResult = adi_adc_RegisterCallback (hDevice, NULL, NULL);
    DEBUG_RESULT("Failed to unregister callback ", eResult, ADI_ADC_SUCCESS);
    
    /* Populate the buffer structure */
    Buffer.nBuffSize = sizeof(ADC_DataBuffer);
    Buffer.nChannels = CHANNEL_TO_USE;
    Buffer.nNumConversionPasses = ADC_NUM_SAMPLES/2;
    Buffer.pDataBuffer = ADC_DataBuffer;
    
    /* Submit the buffer to the driver */
    eResult = adi_adc_SubmitBuffer (hDevice, &Buffer);
    DEBUG_RESULT("Failed to submit buffer ", eResult, ADI_ADC_SUCCESS);
        
    /* Enable the ADC */
    eResult = adi_adc_Enable (hDevice, true);
    DEBUG_RESULT("Failed to enable the ADC for sampling ", eResult, ADI_ADC_SUCCESS);

    ADI_ADC_BUFFER* pAdcBuffer;
    eResult = adi_adc_GetBuffer (hDevice, &pAdcBuffer);
    DEBUG_RESULT("Failed to Get Buffer ", eResult, ADI_ADC_SUCCESS);
    
}



/*********************************************************************
*
*   Function:   main
*
*********************************************************************/
int main (void)
{
    ADI_ADC_RESULT  eResult = ADI_ADC_SUCCESS;
    
    common_Init();

    if (adi_pwr_Init()!= ADI_PWR_SUCCESS)
    {
        DEBUG_MESSAGE("\n Failed to intialize the power service \n");
    }
    if (ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_HCLK,1))
    {
        DEBUG_MESSAGE("Failed to intialize the power service\n");
    }
    if (ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_PCLK,1))
    {
        DEBUG_MESSAGE("Failed to intialize the power service\n");
    }
    
    /* Configure the Pin Mux for ADC */
    adi_initpinmux();
      
    eResult = adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice);
    DEBUG_RESULT("Failed to open ADC device",eResult, ADI_ADC_SUCCESS);

    /* Register Callback */
    eResult = adi_adc_RegisterCallback (hDevice, ADCCallback, NULL);
    DEBUG_RESULT("Failed to register callback", eResult, ADI_ADC_SUCCESS);

    /* Go through the initial setup of the ADC */
    SetUpADC(hDevice);
    
#if ADC_WAIT_FOR_LIMIT  == 1
    WaitForLimit(hDevice);
#endif
    
    /* A test to see if the chip is at, or around, ambient office temperature */
#if ADC_GET_TEMP == 1
    GetChipTemperature(hDevice);
#endif
    
    /* The battery voltage should be fairly constant. */
#if ADC_GET_BATTERY == 1 
    GetBatteryVoltage(hDevice);
#endif
    
    /* Sample the ADC */
    SampleData(hDevice);
  
    ReadChannels(hDevice);
    
    /* Disable the ADC */
    eResult = adi_adc_Enable (hDevice, false);
    DEBUG_RESULT("Failed to disable ADC for sampling ", eResult, ADI_ADC_SUCCESS);

    /* Close the ADC */
    eResult = adi_adc_Close (hDevice);
    DEBUG_RESULT("Failed to close ADC", eResult, ADI_ADC_SUCCESS);
    
    /* Test Passed */
    common_Pass();
}

/* Approximately wait for minimum 1 usec */
static void usleep(uint32_t usec)
{
    volatile int y = 0;
    while (y++ < usec) {
        volatile int x = 0;
        while (x < 16) {
        x++;
        }
    }
}

/*****/
