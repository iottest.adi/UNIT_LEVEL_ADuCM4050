/******************************************************************************
 * @file:    BeepTest.c
 * @brief:   Beeper Driver Test forADuC302X
 *-----------------------------------------------------------------------------
 *
 Copyright (c) 2016 Analog Devices, Inc.

All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
  - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  - Modified versions of the software must be conspicuously marked as such.
  - This software is licensed solely and exclusively for use with processors
    manufactured by or for Analog Devices, Inc.
  - This software may not be combined or merged with other code in any manner
    that would cause the software to become subject to terms and conditions
    which differ from those listed here.
  - Neither the name of Analog Devices, Inc. nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
  - The use of this software may or may not infringe the patent rights of one
    or more patent holders.  This license does not release you from the
    requirement that you obtain separate licenses from these patent holders
    to use this software.

THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-
INFRINGEMENT, TITLE, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL ANALOG DEVICES, INC. OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF
CLAIMS OF INTELLECTUAL PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*****************************************************************************/

/** \addtogroup BEEP_Test BEEP Test
 *  Example code demonstrating use of the BEEP functions.
 *  @{
 */

/*==========  I N C L U D E  ==========*/

#include <stdio.h>
#include <drivers/pwr/adi_pwr.h>
#include <drivers/beep/adi_beep.h>
#include "common.h"

/* prototypes */
extern int32_t   adi_initpinmux (void);
void             beepCallback(void *pCBUnused, uint32_t Event, void *pvUnused);

/* Device handle */
ADI_BEEP_HANDLE hBeep = NULL;

/* required beeper driver memory */
uint8_t BeepMemory[ADI_BEEP_MEMORY_SIZE];


/* Various playlist lengths */
#define PLAY_TUNE1 1
#define PLAY_TUNE2 1
#define PLAY_TUNE3 1
#define PLAY_TUNE4 1
#define PLAY_TUNE5 1
ADI_BEEP_HANDLE hBeep1= NULL,hBeep2= NULL,hBeep3= NULL,hBeep4= NULL;
/* Basic functionality */
#define PLAY_NOTE     1
#define PLAY_TWONOTES 1
#define BEEP_OPEN
#define BEEP_REGISTER_CALLBACK
#define BEEP_PLAYSEQUENCE
#define BEEP_PLAYNOTE
#define BEEP_CLOSE

ADI_BEEP_RESULT PlayOne(void);
ADI_BEEP_RESULT PlayTwo(void);
ADI_BEEP_RESULT TuneTest(ADI_BEEP_NOTE tune[], uint8_t size);
ADI_BEEP_RESULT CheckStatic(void);


/* the playlist */
#if PLAY_TUNE1 == 1
ADI_BEEP_NOTE playList1[] = {
    /* Close Encounters: D-E-C-C(octave lower)-G */
    { ADI_BEEP_FREQ_D6,        ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_E6,        ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_C6,        ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_C5,        ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_G5,        ADI_BEEP_DUR_32_32 },
    { ADI_BEEP_FREQ_REST,      ADI_BEEP_DUR_1_32 },
};
uint8_t playLength1 = sizeof(playList1)/sizeof(playList1[0]);
#endif

#if PLAY_TUNE2 == 1
#define DURATION ADI_BEEP_DUR_1_32
ADI_BEEP_NOTE playList2[] = {
    /* Scales - step up two, then one down. */
    {ADI_BEEP_FREQ_C4  , DURATION},
    {ADI_BEEP_FREQ_D4  , DURATION},
    {ADI_BEEP_FREQ_Cs4 , DURATION},
    {ADI_BEEP_FREQ_Ds4 , DURATION},
    {ADI_BEEP_FREQ_F4  , DURATION},
    {ADI_BEEP_FREQ_E4  , DURATION},
    {ADI_BEEP_FREQ_Fs4 , DURATION},
    {ADI_BEEP_FREQ_Gs4 , DURATION},
    {ADI_BEEP_FREQ_G4  , DURATION},
    {ADI_BEEP_FREQ_A4  , DURATION},
    {ADI_BEEP_FREQ_B4  , DURATION},
    {ADI_BEEP_FREQ_As4 , DURATION},
    {ADI_BEEP_FREQ_C5  , DURATION},
    {ADI_BEEP_FREQ_D5  , DURATION},
    {ADI_BEEP_FREQ_Cs5 , DURATION},
    {ADI_BEEP_FREQ_Ds5 , DURATION},
    {ADI_BEEP_FREQ_F5  , DURATION},
    {ADI_BEEP_FREQ_E5  , DURATION},
    {ADI_BEEP_FREQ_Fs5 , DURATION},
    {ADI_BEEP_FREQ_Gs5 , DURATION},
    {ADI_BEEP_FREQ_G5  , DURATION},
    {ADI_BEEP_FREQ_A5  , DURATION},
    {ADI_BEEP_FREQ_B5  , DURATION},
    {ADI_BEEP_FREQ_As5 , DURATION},
    {ADI_BEEP_FREQ_C6  , DURATION},
    {ADI_BEEP_FREQ_D6  , DURATION},
    {ADI_BEEP_FREQ_Cs6 , DURATION},
    {ADI_BEEP_FREQ_Ds6 , DURATION},
    {ADI_BEEP_FREQ_F6  , DURATION},
    {ADI_BEEP_FREQ_E6  , DURATION},
    {ADI_BEEP_FREQ_Fs6 , DURATION},
    {ADI_BEEP_FREQ_Gs6 , DURATION},
    {ADI_BEEP_FREQ_G6  , DURATION},
    {ADI_BEEP_FREQ_A6  , DURATION},
    {ADI_BEEP_FREQ_B6  , DURATION},
    {ADI_BEEP_FREQ_As6 , DURATION},
    {ADI_BEEP_FREQ_C7  , DURATION},
    {ADI_BEEP_FREQ_D7  , DURATION},
    {ADI_BEEP_FREQ_Cs7 , DURATION},
    {ADI_BEEP_FREQ_Ds7 , DURATION},
    {ADI_BEEP_FREQ_F7  , DURATION},
    {ADI_BEEP_FREQ_E7  , DURATION},
    {ADI_BEEP_FREQ_Fs7 , DURATION},
    {ADI_BEEP_FREQ_Gs7 , DURATION},
    {ADI_BEEP_FREQ_G7  , DURATION},
    {ADI_BEEP_FREQ_A7  , DURATION},
    {ADI_BEEP_FREQ_B7  , DURATION},
    {ADI_BEEP_FREQ_As7 , DURATION},
    {ADI_BEEP_FREQ_C8  , DURATION},
    {ADI_BEEP_FREQ_D8  , DURATION},
    {ADI_BEEP_FREQ_Cs8 , DURATION},
    {ADI_BEEP_FREQ_Ds8 , DURATION},
    {ADI_BEEP_FREQ_F8  , DURATION},
    {ADI_BEEP_FREQ_E8  , DURATION},
    {ADI_BEEP_FREQ_Fs8 , DURATION},
    {ADI_BEEP_FREQ_G8  , DURATION},
};

uint8_t playLength2 = sizeof(playList2)/sizeof(playList2[0]);
#endif 

#if PLAY_TUNE3 == 1
ADI_BEEP_NOTE playList3[] = {
    /* Short fanfare */
    { ADI_BEEP_FREQ_REST,      ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_REST,      ADI_BEEP_DUR_16_32 },
};
uint8_t playLength3 = sizeof(playList3)/sizeof(playList3[0]);
#endif

#if PLAY_TUNE4 == 1
ADI_BEEP_NOTE playList4[] = {
    /* Fanfare */
    { ADI_BEEP_FREQ_C6,        ADI_BEEP_DUR_4_32 },
    { ADI_BEEP_FREQ_E6,        ADI_BEEP_DUR_4_32 },
    { ADI_BEEP_FREQ_G6,        ADI_BEEP_DUR_4_32 },
    { ADI_BEEP_FREQ_REST,      ADI_BEEP_DUR_2_32 },
    { ADI_BEEP_FREQ_E6,        ADI_BEEP_DUR_4_32 },
    { ADI_BEEP_FREQ_G6,        ADI_BEEP_DUR_16_32 },
};
uint8_t playLength4 = sizeof(playList4)/sizeof(playList4[0]);
#endif 

#if PLAY_TUNE5 == 1
ADI_BEEP_NOTE playList5[] = {
    /* Twinkle */
    { ADI_BEEP_FREQ_REST,      ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_E6,        ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_E6,        ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_B6,        ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_B6,        ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_Cs7,       ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_Cs7,       ADI_BEEP_DUR_16_32 },    
    { ADI_BEEP_FREQ_B6,        ADI_BEEP_DUR_32_32 },
    { ADI_BEEP_FREQ_REST,      ADI_BEEP_DUR_8_32 },
    { ADI_BEEP_FREQ_REST,      ADI_BEEP_DUR_8_32 },
};
uint8_t playLength5 = sizeof(playList5)/sizeof(playList5[0]);
#endif

/* Boolean flag indicating end of sequence */
volatile bool bDoneFlag     = false;
volatile bool bNoteDone     = false;
volatile bool bSequenceDone = false;

#ifdef 0
int main (void)
{
    ADI_BEEP_RESULT eResult;

    /* enable the beeper pins */
    adi_initpinmux();
    
    /* test system initialization. This functions will also add the 
       pinmuxing  required for the UART device. */
       
    common_Init();

    
    if(ADI_PWR_SUCCESS != adi_pwr_Init())
    {
      DEBUG_MESSAGE("adi_pwr_Init failed\n");
      return 1;
    }
    if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_HCLK,1))
    {
      DEBUG_MESSAGE("adi_pwr_SetClockDivider failed\n");
      return 1;
    }
    if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_PCLK,1))
    {
      DEBUG_MESSAGE("adi_pwr_SetClockDivider failed\n");
      return 1;
    }
    if(ADI_PWR_SUCCESS != adi_pwr_SetLFClockMux(ADI_CLOCK_MUX_LFCLK_LFOSC))
    {
      DEBUG_MESSAGE("adi_pwr_SetLFClockMux failed\n");
      return 1;
    }
    
    /* initialize the beeper driver */
    if (ADI_BEEP_SUCCESS != (eResult = adi_beep_Open(ADI_BEEP_DEVID_0,
                                                     &BeepMemory,
                                                     sizeof(BeepMemory),
                                                     &hBeep)))
    {
      DEBUG_MESSAGE("adi_beep_Open failed\n");
      return 1;
    }
    
    if (ADI_BEEP_SUCCESS != (eResult = adi_beep_RegisterCallback(hBeep,
                                                                 beepCallback,
                                                                 NULL)))
    {
      DEBUG_MESSAGE("adi_beep_RegisterCallback failed\n");
      return 1;
    }
    
    /* has to be first */
    DEBUG_MESSAGE("Playing static config \n");
    if (ADI_BEEP_SUCCESS != (eResult = CheckStatic()))
    {
      DEBUG_MESSAGE("Static config test failed\n");
      return eResult;
    }
    
    DEBUG_MESSAGE("Playing note \n");
    if (ADI_BEEP_SUCCESS != (eResult = PlayOne()))
    {
      DEBUG_MESSAGE("Playing single note failed\n");
      return eResult;
    }
    
    DEBUG_MESSAGE("Playing two tone \n");
    if (ADI_BEEP_SUCCESS != (eResult = PlayTwo()))
    {
      DEBUG_MESSAGE("Playing two tone failed\n");
      return eResult;
    }
    
#if PLAY_TUNE1 == 1       
    DEBUG_MESSAGE("Playing tune 1\n");
    if (ADI_BEEP_SUCCESS != (eResult = TuneTest(playList1, playLength1)))
    {
      DEBUG_MESSAGE("adi_beep_PlaySequence (PLAY_TUNE1) failed\n");
      return eResult;
    }        
#endif        
#if PLAY_TUNE2 == 1           
    DEBUG_MESSAGE("Playing tune 2\n");
    if (ADI_BEEP_SUCCESS != (eResult = TuneTest(playList2, playLength2)))
    {
      DEBUG_MESSAGE("adi_beep_PlaySequence (PLAY_TUNE2) failed\n");
      return eResult;
    }            
#endif        
#if PLAY_TUNE3 == 1         
    DEBUG_MESSAGE("Playing tune 3\n");
    if (ADI_BEEP_SUCCESS != (eResult = TuneTest(playList3, playLength3)))
    {
      DEBUG_MESSAGE("adi_beep_PlaySequence (PLAY_TUNE3) failed\n");
      return eResult;
    }   
#endif        
#if PLAY_TUNE4 == 1 
    DEBUG_MESSAGE("Playing tune 4\n");
    if (ADI_BEEP_SUCCESS != (eResult = TuneTest(playList4, playLength4)))
    {
      DEBUG_MESSAGE("adi_beep_PlaySequence (PLAY_TUNE4) failed\n");
      return eResult;
    }      
#endif
#if PLAY_TUNE5 == 1 
    DEBUG_MESSAGE("Playing tune 5\n");
    if (ADI_BEEP_SUCCESS != (eResult = TuneTest(playList5, playLength5)))
    {
      DEBUG_MESSAGE("adi_beep_PlaySequence (PLAY_TUNE5) failed\n");
      return eResult;
    }      
#endif
    
    
    if(ADI_BEEP_SUCCESS == eResult)
    {
      DEBUG_MESSAGE("Beep test completed successfully");
      DEBUG_MESSAGE("All done!");
    }
    
}

#endif

/* Application Callback */
void beepCallback(void *pCBUnused, uint32_t Event, void *pvUnused) {

    /* while the playlist is incomplete, program next ToneA with each ToneA start interrupt */
    if(Event & (ADI_BEEP_INTERRUPT_NOTE_END)) {  
       bNoteDone = true;
    }

    if(Event & ADI_BEEP_INTERRUPT_SEQUENCE_END) { 
       bSequenceDone = true;
    }
 
}

/*******
 * 
 *  TESTS
 *
 ********/


 ADI_BEEP_RESULT CheckStatic()
 {
        ADI_BEEP_RESULT eResult;
        
        if (ADI_BEEP_SUCCESS != (eResult = adi_beep_Enable(hBeep, true)))
        {
            DEBUG_MESSAGE("adi_beep_Enable failed for static config \n");
            return eResult;
        }
        
        if (ADI_BEEP_SUCCESS != (eResult = adi_beep_Wait(hBeep)))
        {
            DEBUG_MESSAGE("adi_beep_Wait failed\n");
            return eResult;
        }
           
        if (ADI_BEEP_SUCCESS != (eResult = adi_beep_Enable(hBeep, true)))
        {
            DEBUG_MESSAGE("adi_beep_Enable failed for static config \n");
            return eResult;
        }
        
        if (ADI_BEEP_SUCCESS != (eResult = adi_beep_Wait(hBeep)))
        {
            DEBUG_MESSAGE("adi_beep_Wait failed\n");
            return eResult;
        }
        return eResult;
 }


#if PLAY_NOTE == 1
ADI_BEEP_RESULT PlayOne(void)
{
        ADI_BEEP_RESULT eResult;
        ADI_BEEP_NOTE noise = {ADI_BEEP_FREQ_Gs6, ADI_BEEP_DUR_2_32};

        bNoteDone = false;
        bSequenceDone = false;
        
        
        if (ADI_BEEP_SUCCESS != (eResult = adi_beep_PlayNote(hBeep, noise)))
        {
            DEBUG_MESSAGE("adi_beep_PlayNote failed\n");
            return eResult;
        }

        if (ADI_BEEP_SUCCESS != (eResult = adi_beep_Wait(hBeep)))
        {
            DEBUG_MESSAGE("adi_beep_Wait failed\n");
            return eResult;
        }

        if ((bNoteDone != true) || (bSequenceDone != false))
        {
            DEBUG_MESSAGE("Note callback failed \n");
            return eResult;
        }
        
        noise.frequency = ADI_BEEP_FREQ_A6;
        noise.duration  = ADI_BEEP_DUR_8_32;

        if (ADI_BEEP_SUCCESS != (eResult = adi_beep_PlayNote(hBeep, noise)))
        {
            DEBUG_MESSAGE("adi_beep_PlayNote failed\n");
            return eResult;
        }
        
        if (ADI_BEEP_SUCCESS != (eResult = adi_beep_Wait(hBeep)))
        {
            DEBUG_MESSAGE("adi_beep_Wait failed\n");
            return eResult;
        }        
        return eResult;
}
#endif

#if PLAY_TWONOTES == 1
ADI_BEEP_RESULT PlayTwo(void)
{
        ADI_BEEP_RESULT eResult;
        ADI_BEEP_NOTE noise1, noise2;
        
        noise1.frequency = ADI_BEEP_FREQ_A4;
        noise1.duration  = ADI_BEEP_DUR_2_32;
        
        noise2.frequency = ADI_BEEP_FREQ_A7;
        noise2.duration  = ADI_BEEP_DUR_2_32;
        
        bNoteDone = false;
        bSequenceDone = false;
        
        if (ADI_BEEP_SUCCESS != (eResult = adi_beep_PlayTwoTone(hBeep, noise1, noise2, 5)))
        {
            DEBUG_MESSAGE("adi_beep_PlayTwoTone failed\n");
            return eResult;
        }
        
        if (ADI_BEEP_SUCCESS != (eResult = adi_beep_Wait(hBeep)))
        {
            DEBUG_MESSAGE("adi_beep_Wait failed\n");
            return eResult;
        }
        
        if ((bNoteDone != false) || (bSequenceDone != true))
        {
            DEBUG_MESSAGE("Twotone callback failed \n");
            return eResult;
        }        
        return eResult;
}
#endif 

#if ADI_BEEP_INCLUDE_PLAY_SEQUENCE == 1

ADI_BEEP_RESULT TuneTest(ADI_BEEP_NOTE tune[], uint8_t size)
{
        ADI_BEEP_RESULT eResult;
           
        bNoteDone     = false;
        bSequenceDone = false;
        if (ADI_BEEP_SUCCESS != (eResult = adi_beep_PlaySequence(hBeep, tune, size)))
        {
            DEBUG_MESSAGE("adi_beep_PlaySequence failed\n");
            return eResult;
        }
        
        if (ADI_BEEP_SUCCESS != (eResult = adi_beep_Wait(hBeep)))
        {
            DEBUG_MESSAGE("adi_beep_Wait failed\n");
            return eResult;
        }
        
        if ((bNoteDone != false) || (bSequenceDone != true))
        {
            DEBUG_MESSAGE("Sequence callback failed \n");
            return eResult;
        }     
        
        return ADI_BEEP_SUCCESS;
}

#endif    




int main (void)
{ 
  
ADI_BEEP_RESULT eResult;
ADI_CALLBACK          pfCallback=0;

ADI_BEEP_NOTE noise = {ADI_BEEP_FREQ_Gs6, ADI_BEEP_DUR_2_32};
    /* enable the beeper pins */
    adi_initpinmux();
    /* test system initialization. This functions will also add the 
       pinmuxing  required for the UART device. */
   int Test,Test1,Test2,Test3,Test4;    
   common_Init();  
   adi_pwr_Init();
   adi_pwr_SetClockDivider(ADI_CLOCK_HCLK,1);
   adi_pwr_SetClockDivider(ADI_CLOCK_PCLK,1);
   adi_pwr_SetLFClockMux(ADI_CLOCK_MUX_LFCLK_LFOSC);
     printf("===============BEEP TESTING===============\n\n ");
#ifdef BEEP_OPEN
   
 printf("===============BEEP OPEN TESTING===============\n" );
 /***************************************************************************/
 /*****************BEEP_TEST ADI_BEEP_BAD_DEV_ID *******************************/
 /**************************************************************************/
     
 Test=adi_beep_Open( ADI_BEEP_MAX_DEVID, &BeepMemory,sizeof(BeepMemory),&hBeep);
  if(Test==ADI_BEEP_BAD_DEV_ID)
  {
   printf("PASS:adi_beep_Open & ADI_BEEP_BAD_DEV_ID \n");  
  } 
  else
  {   
   printf("FAIL:adi_beep_Open & ADI_BEEP_BAD_DEV_ID \n");
  }
  
 /***************************************************************************/
 /*****************BEEP_TEST ADI_BEEP_NULL_PTR *******************************/
 /**************************************************************************/
  
 Test=adi_beep_Open( ADI_BEEP_DEVID_0, NULL,sizeof(BeepMemory),&hBeep);
  if(Test==ADI_BEEP_NULL_PTR)
  {
   printf("PASS:adi_beep_Open & ADI_BEEP_NULL_PTR \n");  
      
  } 
  else
  {   
   printf("FAIL:adi_beep_Open & ADI_BEEP_NULL_PTR \n");
  }
  
   /***************************************************************************/
  /*****************BEEP_TEST ADI_BEEP_SUCCESS*******************************/
  /**************************************************************************/
  
 Test=adi_beep_Open(ADI_BEEP_DEVID_0, &BeepMemory,sizeof(BeepMemory),&hBeep);
  if(Test==ADI_BEEP_SUCCESS)
  {
   printf("PASS:adi_beep_Open & ADI_BEEP_SUCCESS \n\n");  
  } 
  else
  {   
   printf("FAIL:adi_beep_Open & ADI_BEEP_SUCCESS \n");
  }

adi_beep_Close(hBeep);
 
#if 0
  /***************************************************************************/
  /*****************BEEP_TEST ADI_BEEP_ALREADY_INITIALIZED*******************************/
  /**************************************************************************/
/*adi_beep_Open(ADI_BEEP_DEVID_0, &BeepMemory,sizeof(BeepMemory),&hBeep);

printf ( " about to close the file \n");

Test2=adi_beep_Open(ADI_BEEP_DEVID_0, &BeepMemory,sizeof(BeepMemory),&hBeep1);
//printf("close _2 : %d \n",Test2); 

Test=adi_beep_Open(ADI_BEEP_DEVID_0, &BeepMemory,sizeof(BeepMemory),&hBeep1);*/

  Test=adi_beep_Open(ADI_BEEP_DEVID_0, &BeepMemory,sizeof(BeepMemory),&hBeep);
  
  if(Test==ADI_BEEP_ALREADY_INITIALIZED)
  {
   printf("PASS:adi_beep_Open & ADI_BEEP_ALREADY_INITIALIZED \n");  
  } 
  else
  {    
   printf("FAIL:adi_beep_Open & ADI_BEEP_ALREADY_INITIALIZED \n");
  }
#endif 

#endif    
#ifdef BEEP_CLOSE
 printf("===============BEEP CLOSE TESTING===============\n" );
  /***************************************************************************/
  /*********************ADI_BEEP_SUCCESS*******************************/
  /**************************************************************************/
  
#if 1
   adi_beep_Open( ADI_BEEP_DEVID_0, &BeepMemory, sizeof(BeepMemory),&hBeep1);
   Test2=adi_beep_Close(hBeep1);
   if(Test2==ADI_BEEP_SUCCESS)
  {
   printf("PASS:adi_beep_Close & ADI_BEEP_SUCCESS \n ");  
  } 
  else
  {   
   printf("FAIL:adi_beep_Close & ADI_BEEP_SUCCESS \n");
  }
 
#endif   
  /***************************************************************************/
  /*********************ADI_BEEP_NOT_INITIALIZED*******************************/
  /**************************************************************************/
                    /* Need to develop Logic */
 
 /***************************************************************************/
 /***************************ADI_BEEP_BAD_DEV_HANDLE *******************************/
 /**************************************************************************/
  adi_beep_Open(ADI_BEEP_DEVID_0, &BeepMemory,sizeof(BeepMemory),&hBeep2);
 Test1=adi_beep_Close(hBeep4);
  if(Test1==ADI_BEEP_BAD_DEV_HANDLE )
  {
   printf("PASS:adi_beep_Close & ADI_BEEP_BAD_DEV_HANDLE \n\n");  
  } 
  else
  {   
   printf("FAIL:adi_beep_Close & ADI_BEEP_BAD_DEV_HANDLE \n ");
  }
  adi_beep_Close(hBeep2);
#endif
#ifdef BEEP_REGISTER_CALLBACK
   
printf("===============BEEP_REGISTER_CALLBACK_TESTING===============\n" );
/***************************************************************************/
/*********************ADI_BEEP_SUCCESS*******************************/
/**************************************************************************/
 adi_beep_Open(ADI_BEEP_DEVID_0, &BeepMemory,sizeof(BeepMemory),&hBeep);
 Test2=adi_beep_RegisterCallback(hBeep, pfCallback,NULL);
                                                 
  if(Test2==ADI_BEEP_SUCCESS)
  {
   printf("PASS:adi_beep_RegisterCallback & ADI_BEEP_SUCCESS \n");  
  } 
  else
  {   
   printf("FAIL:adi_beep_RegisterCallback & ADI_BEEP_SUCCESS \n");
  }
  adi_beep_Close(hBeep);
  /***************************************************************************/
  /*********************ADI_BEEP_NOT_INITIALIZED*******************************/
  /**************************************************************************/
              /*  Need to develope logic */
  
  /***************************************************************************/
  /*****************ADI_BEEP_BAD_DEV_HANDLE*******************************/
  /**************************************************************************/
  adi_beep_Open( ADI_BEEP_MAX_DEVID, &BeepMemory,sizeof(BeepMemory),&hBeep2);
  Test2=adi_beep_RegisterCallback(hBeep2,pfCallback,NULL);                                                                                                                        
  if(Test2==ADI_BEEP_BAD_DEV_HANDLE)
  { 
   printf("PASS:adi_beep_RegisterCallback & ADI_BEEP_BAD_DEV_HANDLE \n\n");  
  } 
  else
  {   
   printf("FAIL:adi_beep_RegisterCallback & ADI_BEEP_BAD_DEV_HANDLE \n");
  }


 #endif
#ifdef BEEP_PLAYSEQUENCE
 printf("=============== BEEP_PLAYSEQUENCE_TESTING===============\n" );
  /***************************************************************************/
  /***************** ADI_BEEP_SUCCESS*******************************/
  /*************************************************************************/
 adi_beep_Open(ADI_BEEP_DEVID_0,&BeepMemory,sizeof(BeepMemory),&hBeep);
 Test=adi_beep_PlaySequence(hBeep,playList2, playLength2);                                                                                                 
  if(Test==ADI_BEEP_SUCCESS)
  {
    printf("PASS:adi_beep_PlaySequence & ADI_BEEP_SUCCESS \n");  
  } 
  else
  {   
     printf("FAIL:adi_beep_PlaySequence & ADI_BEEP_SUCCESS \n");
  }
  adi_beep_Close(hBeep);
  /***************************************************************************/
  /*****************ADI_BEEP_INVALID_COUNT *******************************/
  /**************************************************************************/
adi_beep_Open(ADI_BEEP_DEVID_0, &BeepMemory,sizeof(BeepMemory),&hBeep);
 Test=adi_beep_PlaySequence(hBeep,playList2,13); 
  if(Test==ADI_BEEP_INVALID_COUNT )
  {
   printf("PASS:adi_beep_PlaySequence & ADI_BEEP_INVALID_COUNT \n");  
  } 
  else
  {   
   printf("FAIL:adi_beep_PlaySequence & ADI_BEEP_INVALID_COUNT \n");
  }
   adi_beep_Close(hBeep); 
 /***************************************************************************/
 /*****************ADI_BEEP_NULL_PTR *******************************/
 /**************************************************************************/
 adi_beep_Open(ADI_BEEP_DEVID_0,&BeepMemory,sizeof(BeepMemory),&hBeep);
 Test=adi_beep_PlaySequence(hBeep,NULL,playLength2) ;  
  if(Test==ADI_BEEP_NULL_PTR)
  {
    printf("PASS:adi_beep_PlaySequence & ADI_BEEP_NULL_PTR \n");  
  } 
  else
  {   
    printf("FAIL:adi_beep_PlaySequence & ADI_BEEP_NULL_PTR \n");  
  }
  adi_beep_Close(hBeep); 
  /***************************************************************************/
  /*********************ADI_BEEP_BAD_DEV_HANDLE*******************************/
  /**************************************************************************/
adi_beep_Open( ADI_BEEP_MAX_DEVID, &BeepMemory,sizeof(BeepMemory),&hBeep);
Test=adi_beep_PlaySequence(hBeep,playList2, playLength2) ;
                                                            
  if(Test==ADI_BEEP_BAD_DEV_HANDLE)
  {
   printf("PASS:adi_beep_PlaySequence & ADI_BEEP_BAD_DEV_HANDLE \n\n" );  
  } 
  else
  {   
    printf("FAIL:adi_beep_PlaySequence & ADI_BEEP_BAD_DEV_HANDLE \n");
  }
  adi_beep_Close(hBeep); 
#endif 
  
#ifdef BEEP_PLAYNOTE   
  
 printf("===============BEEP PLAYNOTE TESTING===============\n" );
  /***************************************************************************/
  /***************************ADI_BEEP_SUCCESS*******************************/
  /**************************************************************************/
 Test2=adi_beep_Open(ADI_BEEP_DEVID_0, &BeepMemory,sizeof(BeepMemory),&hBeep);
  Test1=adi_beep_PlayNote(hBeep, noise);
  if(Test1==ADI_BEEP_SUCCESS)
  { 
   printf("PASS:adi_beep_PlayNote& ADI_BEEP_SUCCESS \n");  
  } 
  else
  {   
   printf("FAIL:adi_beep_PlayNote& ADI_BEEP_SUCCESS \n");
  }
  /***************************************************************************/
  /*********************ADI_BEEP_BAD_DEV_HANDLE*******************************/
  /**************************************************************************/
   adi_beep_Open( ADI_BEEP_MAX_DEVID, &BeepMemory,sizeof(BeepMemory),&hBeep);
   Test2=adi_beep_PlayNote(hBeep, noise);
  if(Test2==ADI_BEEP_BAD_DEV_HANDLE)
  {
    printf("PASS:adi_beep_PlayNote & ADI_BEEP_BAD_DEV_HANDLE \n");  
  } 
  else
  {  
     printf("FAIL:adi_beep_PlayNote & ADI_BEEP_BAD_DEV_HANDLE \n");
  }
  
  /***************************************************************************/
  /*********************ADI_BEEP_NOT_INITIALIZED*******************************/
  /**************************************************************************/
        /* Need To Develop Logic */
#endif  

}  

