/*******************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*******************************************************************************/
/**
* @file     sport_cc_loopback_dma.h
*
* @brief    Primary header file for SPORT driver test.
*/
#ifndef SPORT_CC_LOOPBACK_DMA_H
#define SPORT_CC_LOOPBACK_DMA_H

/** Sport Device identifier (in [0 .. ADI_SPORT_NUM_INSTANCES - 1]) */
#define SPORT_DEVICE_ID         (0u)

/** size of RX/TX buffers */
#define BUFFER_SIZE             (2048u)

extern uint8_t  nBufferTx[BUFFER_SIZE];
extern uint8_t  nBufferRx1[BUFFER_SIZE];
extern uint8_t  nBufferRx2[BUFFER_SIZE];
extern uint8_t  nBufferRx3[BUFFER_SIZE];
extern uint8_t  nBufferRx4[BUFFER_SIZE];

#if defined(ADI_CYCLECOUNT_ENABLED) && (ADI_CYCLECOUNT_ENABLED == 1u)

extern uint32_t cycleCountId_sport_Rx;
extern uint32_t cycleCountId_sport_Tx;

#endif

#endif /* SPORT_CC_LOOPBACK_DMA_H */
