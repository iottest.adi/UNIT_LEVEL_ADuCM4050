/*******************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*******************************************************************************/
/**
 * @file:       sport_loopback_dma_no_callback.c
 *
 * @brief       This file contains tests for the SPORT device driver executing
 *              DMA driven requests with  no callback function registered.
 */
#include <stdint.h>             /* for 'NULL' */
#include <system_ADuCM4050.h>
#include <common.h>

#include <drivers/general/adi_drivers_general.h>
#include <drivers/sport/adi_sport.h>
#include <drivers/pwr/adi_pwr.h>

#include "sport_test_support.h"
#include "sport_loopback_dma.h"

#if !defined (__CC_ARM)
#define TRACE_TEST	1
#endif

#define MAX_DATA_TRANSFER       (BUFFER_SIZE - 4u)
#define TEST_ITER               (4u)

extern int32_t adi_initpinmux(void);

extern ADI_SPORT_HANDLE hSportRx;                       /* Handle for Rx channel */
extern ADI_SPORT_HANDLE hSportTx;                       /* Handle for Tx channel */

/* Prototypes for function used in this example */

extern uint32_t test_sport_dma_without_callback(void);

static uint32_t test_case_1(void);
static uint32_t test_case_2(void);
static uint32_t test_case_3(void);
static uint32_t test_case_4(void);

uint32_t test_sport_dma_without_callback(void)
{
    ADI_SPORT_RESULT sportResult = ADI_SPORT_SUCCESS;   /* Variable for storing the return code from UART device */
    uint32_t err = 0u;

#if defined (TRACE_TEST)
    common_Perf("SPORT DMA - no callback");
    common_Perf("=======================");
#endif
    sportResult = adi_sport_RegisterCallback(hSportRx, NULL, NULL);
    DEBUG_RESULT("Failed to unregister a callback function for the Rx channel",sportResult,ADI_SPORT_SUCCESS);

    sportResult = adi_sport_RegisterCallback(hSportTx, NULL, NULL);
    DEBUG_RESULT("Failed to unregister a callback function for the Tx channel",sportResult,ADI_SPORT_SUCCESS);

    err += test_case_1();
    err += test_case_2();
    err += test_case_3();
    err += test_case_4();

#if defined (TRACE_TEST)
    common_Perf("");
#endif
    return err;
}

static uint8_t * dataRx[TEST_ITER] = {&nBufferRx1[0], &nBufferRx2[0], &nBufferRx3[0], &nBufferRx4[0]} ;
static size_t dataRxSize[TEST_ITER] = {sizeof(nBufferRx1), sizeof(nBufferRx2), sizeof(nBufferRx3), sizeof(nBufferRx4)} ;

/**
 * test_case_1 covers SPORT transfers with no multiple buffers submitted. In each iteration, there's
 * only one Rx buffer and one Tx buffer submitted, both with the same size. This makes sure that this
 * simple SPORT situation is fully functional.
 */
static uint32_t test_case_1(void)
{
    ADI_SPORT_RESULT sportResult = ADI_SPORT_SUCCESS;   /* Variable for storing the return code from UART device */
    bool useDma = true;                                 /* SPORT communication based on DMA, not interrupt */
    uint32_t cumulatedErr = 0u;
    uint32_t i;

    for (i=0; i<TEST_ITER; i++)
    {
        memset(dataRx[i],0u,dataRxSize[i]);             /* initialize Rx buffer with 0s */

        /* ================================================================================ */
        /* Always submit Rx buffers BEFORE Tx ones else data will be missed because Tx will */
        /* start sending data before Rx is ready. (Rx always wait for data to be received.) */
        /* Submit nBufferRx1 BEFORE nBufferTx                                               */
        /* ================================================================================ */
        
        sportResult = adi_sport_SubmitBuffer(hSportRx,dataRx[i],MAX_DATA_TRANSFER,useDma);
        DEBUG_RESULT("Failed to submit buffer for Rx channel",sportResult,ADI_SPORT_SUCCESS);

        sportResult = adi_sport_SubmitBuffer(hSportTx,&nBufferTx[0],MAX_DATA_TRANSFER,useDma);
        DEBUG_RESULT("Failed to submit buffer for Tx channel",sportResult,ADI_SPORT_SUCCESS);

        wait_for_buffer(hSportTx,&nBufferTx[0]);
        wait_for_buffer(hSportRx,dataRx[i]);

        cumulatedErr += check_transmitted_data(nBufferTx, dataRx[i], BUFFER_SIZE, MAX_DATA_TRANSFER);
    }

#if defined (TRACE_TEST)
    if (0u == cumulatedErr)
    {
      common_Perf(" - test case 1: ok");
    }
    else
    {
      common_Perf(" - test case 1: failed!!!");
    }
#endif
    return cumulatedErr;
}

/**
 * test_case_2 covers SPORT transfers with multiple Rx buffers submitted. In each iteration, there's
 * only one Tx buffer but two Rx buffers submitted, with the two Rx buffers sizes equalling the Tx
 * buffer size. This makes sure that the SPORT Rx interrupt properly supports pending Rx buffers when
 * an Rx transfer completes.
 */
static uint32_t test_case_2(void)
{
    uint32_t cumulatedErr = 0u;
    ADI_SPORT_RESULT sportResult = ADI_SPORT_SUCCESS;   /* Variable for storing the return code from UART device */
    bool useDma = true;                                 /* SPORT communication based on DMA, not interrupt */
    uint32_t i;

    for (i=0; i<TEST_ITER; i++)
    {
        memset(nBufferRx1,0,sizeof(nBufferRx1));        /* initialize Rx buffer with 0s */
        memset(nBufferRx2,0,sizeof(nBufferRx2));        /* initialize Rx buffer with 0s */

        /* ================================================================================ */
        /* Always submit Rx buffers BEFORE Tx ones else data will be missed because Tx will */
        /* start sending data before Rx is ready. (Rx always wait for data to be received.) */
        /* Submit nBufferRx1 and nBufferRx2 BEFORE nBufferTx                                */
        /* ================================================================================ */

        sportResult = adi_sport_SubmitBuffer(hSportRx,&nBufferRx1[0],MAX_DATA_TRANSFER,useDma);
        DEBUG_RESULT("Failed to submit buffer for Rx channel",sportResult,ADI_SPORT_SUCCESS);

        sportResult = adi_sport_SubmitBuffer(hSportRx,&nBufferRx2[0],MAX_DATA_TRANSFER,useDma);
        DEBUG_RESULT("Failed to submit buffer for Rx channel",sportResult,ADI_SPORT_SUCCESS);

        sportResult = adi_sport_SubmitBuffer(hSportTx,&nBufferTx[0],MAX_DATA_TRANSFER,useDma);
        DEBUG_RESULT("Failed to submit buffer for Tx channel",sportResult,ADI_SPORT_SUCCESS);

        wait_for_buffer(hSportRx,&nBufferRx1[0]);
        wait_for_buffer(hSportTx,&nBufferTx[0]);

        sportResult = adi_sport_SubmitBuffer(hSportTx,&nBufferTx[0],MAX_DATA_TRANSFER,useDma);
        DEBUG_RESULT("Failed to submit buffer for Tx channel",sportResult,ADI_SPORT_SUCCESS);

        wait_for_buffer(hSportRx,&nBufferRx2[0]);
        wait_for_buffer(hSportTx,&nBufferTx[0]);

        cumulatedErr += check_transmitted_data(nBufferTx, nBufferRx1, BUFFER_SIZE, MAX_DATA_TRANSFER);
        cumulatedErr += check_transmitted_data(nBufferTx, nBufferRx2, BUFFER_SIZE, MAX_DATA_TRANSFER);
    }

#if defined (TRACE_TEST)
    if (0u == cumulatedErr)
    {
      common_Perf(" - test case 2: ok");
    }
    else
    {
      common_Perf(" - test case 2: failed!!!");
    }
#endif
    return cumulatedErr;
}


/**
 * test_case_3 covers SPORT transfers with multiple Tx buffers submitted. In each iteration, there's
 * only one Rx buffer but two Tx buffers submitted, with the two Tx buffers sizes equalling the Rx
 * buffer size. This makes sure that the SPORT Tx interrupt properly supports pending Tx buffers when
 * an Tx transfer completes.
 */
static uint32_t test_case_3(void)
{
    uint32_t cumulatedErr = 0u;
    ADI_SPORT_RESULT sportResult = ADI_SPORT_SUCCESS;   /* Variable for storing the return code from UART device */
    bool useDma = true;                                 /* SPORT communication based on DMA, not interrupt */
    uint32_t i;

    for (i=0; i<TEST_ITER; i++)
    {
        const uint32_t batchSize1 = (MAX_DATA_TRANSFER >> 1u) & 0xFFFFFFFCu;    /* bytes number must be multiple of 4 */
        const uint32_t batchSize2 = MAX_DATA_TRANSFER - batchSize1;
        uint8_t * batchAddr1 = &nBufferTx[0];                                   /* start address of 1st Tx batch */
        uint8_t * batchAddr2 = &nBufferTx[batchSize1];                          /* start address of 2nd Tx batch */

        memset(nBufferRx1,0,sizeof(nBufferRx1));        /* initialize Rx buffer with 0s */
        memset(nBufferRx2,0,sizeof(nBufferRx1));        /* initialize Rx buffer with 0s */

        /* ================================================================================ */
        /* Always submit Rx buffers BEFORE Tx ones else data will be missed because Tx will */
        /* start sending data before Rx is ready. (Rx always wait for data to be received.) */
        /* Submit nBufferRx1 BEFORE batchAddr1 and batchAddr2                               */
        /* Submit nBufferRx2 and nBufferRx2 BEFORE nBufferTx                                */
        /* ================================================================================ */

        /* first buffer will receive data from two different Tx requests */
        sportResult = adi_sport_SubmitBuffer(hSportRx,&nBufferRx1[0],MAX_DATA_TRANSFER,useDma);
        DEBUG_RESULT("Failed to submit buffer for Rx channel",sportResult,ADI_SPORT_SUCCESS);

        /* second buffer will receive data from a third Tx request */
        sportResult = adi_sport_SubmitBuffer(hSportRx,&nBufferRx2[0],MAX_DATA_TRANSFER,useDma);
        DEBUG_RESULT("Failed to submit buffer for Rx channel",sportResult,ADI_SPORT_SUCCESS);

        /* first batch for nBufferRx1 */
        sportResult = adi_sport_SubmitBuffer(hSportTx,batchAddr1,batchSize1,useDma);
        DEBUG_RESULT("Failed to submit buffer for Tx channel",sportResult,ADI_SPORT_SUCCESS);

        /* 2nd batch for nBufferRx1 */
        sportResult = adi_sport_SubmitBuffer(hSportTx,batchAddr2,batchSize2,useDma);
        DEBUG_RESULT("Failed to submit buffer for Tx channel",sportResult,ADI_SPORT_SUCCESS);

        wait_for_buffer(hSportTx,batchAddr1);
        wait_for_buffer(hSportTx,batchAddr2);
        wait_for_buffer(hSportRx,&nBufferRx1[0]);

        /* batch for nBufferRx2 */
        sportResult = adi_sport_SubmitBuffer(hSportTx,&nBufferTx[0],MAX_DATA_TRANSFER,useDma);
        DEBUG_RESULT("Failed to submit buffer for Tx channel",sportResult,ADI_SPORT_SUCCESS);

        wait_for_buffer(hSportRx,&nBufferRx2[0]);
        wait_for_buffer(hSportTx,&nBufferTx[0]);

        cumulatedErr += check_transmitted_data(nBufferTx, nBufferRx1, BUFFER_SIZE, MAX_DATA_TRANSFER);
        cumulatedErr += check_transmitted_data(nBufferTx, nBufferRx2, BUFFER_SIZE, MAX_DATA_TRANSFER);
    }

#if defined (TRACE_TEST)
    if (0u == cumulatedErr)
    {
      common_Perf(" - test case 3: ok");
    }
    else
    {
      common_Perf(" - test case 3: failed!!!");
    }
#endif
    return cumulatedErr;
}

/**
 * test_case_4 covers invalid buffer submissions and checks that expected errors are returned.
 */
static uint32_t test_case_4(void)
{
    uint32_t cumulatedErr = 0u;
#ifdef ADI_DEBUG
    bool useDma = true;                                 /* SPORT communication based on interrupt, not DMA */
#if defined (TRACE_TEST)
    char msg[64];
#endif
    ADI_SPORT_HANDLE hDev[2] = { hSportRx, hSportTx };  /* Handle for Rx and Tx channels */
    uint32_t i;

    for (i=0u; i<2u; i++)
    {
        uint32_t j;

        /* ================================================================================ */
        /* Following buffer submissions should all be rejected because the data is          */
        /* mis-aligned or the number of bytes is not a multiple of 4 (32-bit data)          */
        /* ================================================================================ */
        for (j=1u; j<=3u; j++)
        {
            uint32_t size = MAX_DATA_TRANSFER - j;
            ADI_SPORT_RESULT sportResult;

            sportResult = adi_sport_SubmitBuffer(hDev[i],&nBufferRx1[0],size,useDma);
            if (ADI_SPORT_INVALID_PARAMETER != sportResult)
            {
#if defined (TRACE_TEST)
                sprintf(msg," - test_case_4 failure: invalid bytes number %lu not rejected", size);
                common_Perf(msg);
#endif
                cumulatedErr++;
            }

            sportResult = adi_sport_SubmitBuffer(hDev[i],&nBufferRx1[j],MAX_DATA_TRANSFER,useDma);
            if (ADI_SPORT_INVALID_PARAMETER != sportResult)
            {
#if defined (TRACE_TEST)
                sprintf(msg," - test_case_4 failure: invalid alignment %lu not rejected", j);
                common_Perf(msg);
#endif
                cumulatedErr++;
            }
        }
    }

#if defined (TRACE_TEST)
    if (0u == cumulatedErr)
    {
      common_Perf(" - test case 4: ok");
    }
    else
    {
      common_Perf(" - test case 4: failed!!!");
    }
#endif
#else
#if defined (TRACE_TEST)
      common_Perf(" - test case 4 skipped as it can only be run with ADI_DEBUG macro set");
#endif
#endif
    return cumulatedErr;
}
