/*******************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*******************************************************************************/
/**
* @file     sport_loopback_mixed.h
*
* @brief    Primary header file for SPORT driver test.
*/
#ifndef SPORT_LOOPBACK_MIXED_H
#define SPORT_LOOPBACK_MIXED_H

/** Sport Device identifier (in [0 .. ADI_SPORT_NUM_INSTANCES - 1]) */
#define SPORT_DEVICE_ID         (0u)

/** size of RX/TX buffers */
#define BUFFER_SIZE             (2048u)
#define TEST_ITER               (4u)
#define CFG_NUM                 (29u)
#define CFG_UNPACKED_NUM        (13u)

extern uint8_t  nBufferTx[BUFFER_SIZE];
extern uint8_t  nBufferRx1[BUFFER_SIZE];
extern uint8_t  nBufferRx2[BUFFER_SIZE];
extern uint8_t  nBufferRx3[BUFFER_SIZE];
extern uint8_t  nBufferRx4[BUFFER_SIZE];

extern uint8_t * dataRx[TEST_ITER];
extern size_t dataRxSize[TEST_ITER];

extern uint8_t wlen[CFG_NUM];
extern ADI_SPORT_PACKING_MODE pack[CFG_NUM];

extern bool dmaRx[TEST_ITER];
extern bool dmaTx[TEST_ITER];


#endif /* SPORT_LOOPBACK_MIXED_H */
