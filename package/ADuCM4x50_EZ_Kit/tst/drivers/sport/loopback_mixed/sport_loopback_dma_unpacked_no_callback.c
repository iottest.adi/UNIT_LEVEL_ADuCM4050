/*******************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*******************************************************************************/
/**
 * @file:       sport_loopback_dma_unpacked_no_callback.c
 *
 * @brief       This file contains tests for the SPORT device driver executing
 *              DMA driven requests with no callback function registered.
 */
#include <stdint.h>             /* for 'NULL' */
#include <system_ADuCM4050.h>
#include <common.h>

#include <drivers/general/adi_drivers_general.h>
#include <drivers/sport/adi_sport.h>
#include <drivers/pwr/adi_pwr.h>

#include "sport_test_support.h"
#include "sport_loopback_mixed.h"

#define MAX_DATA_TRANSFER       ((BUFFER_SIZE >> 1u) - 4u)

#if !defined (__CC_ARM)
#define TRACE_TEST	1
#endif

extern int32_t adi_initpinmux(void);

extern ADI_SPORT_HANDLE hSportRx;                       /* Handle for Rx channel */
extern ADI_SPORT_HANDLE hSportTx;                       /* Handle for Tx channel */

/* Prototypes for function used in this example */

extern uint32_t test_sport_dma_unpacked_without_callback(void);

static uint32_t test_case_1(void);
static uint32_t test_case_2(void);
static uint32_t test_case_3(void);

uint32_t test_sport_dma_unpacked_without_callback(void)
{
    ADI_SPORT_RESULT sportResult = ADI_SPORT_SUCCESS;   /* Variable for storing the return code from UART device */
    uint32_t err = 0u;

#if defined (TRACE_TEST)
    common_Perf("SPORT DMA unpacked - no callback");
    common_Perf("================================");
#endif
    sportResult = adi_sport_RegisterCallback(hSportRx, NULL, NULL);
    DEBUG_RESULT("Failed to unregister a callback function for the Rx channel",sportResult,ADI_SPORT_SUCCESS);

    sportResult = adi_sport_RegisterCallback(hSportTx, NULL, NULL);
    DEBUG_RESULT("Failed to unregister a callback function for the Tx channel",sportResult,ADI_SPORT_SUCCESS);

    err += test_case_1();
    err += test_case_2();
    err += test_case_3();

#if defined (TRACE_TEST)
    common_Perf("");
#endif
    return err;
}

/**
 * test_case_1 covers SPORT transfers with no multiple buffers submitted. In each iteration, there's
 * only one Rx buffer and one Tx buffer submitted, both with the same size. This makes sure that this
 * simple SPORT situation is fully functional.
 */
static uint32_t test_case_1(void)
{
    ADI_SPORT_RESULT sportResult = ADI_SPORT_SUCCESS;   /* Variable for storing the return code from UART device */
    uint32_t cumulatedErr = 0u;
    uint32_t i;
    uint32_t j;
    bool bLSBFirst = false;

    for (j=0u; j<CFG_UNPACKED_NUM; j++)
    {
        const uint32_t numBytesPerData = ((wlen[j] <= 8u) ? 1u : ((wlen[j] <= 16u) ? 2u : 4u));

        sportResult = adi_sport_ConfigData(hSportRx, wlen[j], ADI_SPORT_NO_PACKING, bLSBFirst);
        DEBUG_RESULT("Failed to configure data for Rx SPORT ",sportResult,ADI_SPORT_SUCCESS);

        sportResult = adi_sport_ConfigData(hSportTx, wlen[j], ADI_SPORT_NO_PACKING, bLSBFirst);
        DEBUG_RESULT("Failed to configure data for Tx SPORT ",sportResult,ADI_SPORT_SUCCESS);

        for (i=1u; i<4u; i++)
        {
            const uint32_t shift = (i * numBytesPerData);
            const uint32_t transferSize = MAX_DATA_TRANSFER - shift;

            memset(dataRx[i],0u,dataRxSize[i]);         /* initialize Rx buffer with 0s */

            sportResult = adi_sport_SubmitBuffer(hSportRx,&(dataRx[i][shift]),transferSize,true);
            DEBUG_RESULT("Failed to submit buffer for Rx channel",sportResult,ADI_SPORT_SUCCESS);

            sportResult = adi_sport_SubmitBuffer(hSportTx,&nBufferTx[shift],transferSize,true);
            DEBUG_RESULT("Failed to submit buffer for Tx channel",sportResult,ADI_SPORT_SUCCESS);

            wait_for_buffer(hSportTx,&nBufferTx[shift]);
            wait_for_buffer(hSportRx,&(dataRx[i][shift]));

            cumulatedErr += check_transmitted_data(nBufferTx, dataRx[i], BUFFER_SIZE, shift, transferSize, wlen[j]);
        }
    }

#if defined (TRACE_TEST)
    if (0u == cumulatedErr)
    {
      common_Perf(" - test case 1: ok");
    }
    else
    {
      common_Perf(" - test case 1: failed!!!");
    }
#endif
    return cumulatedErr;
}

/**
 * test_case_2 covers SPORT transfers with multiple Rx buffers submitted. In each iteration, there's
 * only one Tx buffer but two Rx buffers submitted, with the two Rx buffers sizes equalling the Tx
 * buffer size. This makes sure that the SPORT Rx interrupt properly supports pending Rx buffers when
 * an Rx transfer completes.
 */
static uint32_t test_case_2(void)
{
    uint32_t cumulatedErr = 0u;
    ADI_SPORT_RESULT sportResult = ADI_SPORT_SUCCESS;   /* Variable for storing the return code from UART device */
    uint32_t i;
    uint32_t j;
    bool bLSBFirst = false;

    for (j=0u; j<CFG_UNPACKED_NUM; j++)
    {
        const uint32_t numBytesPerData = ((wlen[j] <= 8u) ? 1u : ((wlen[j] <= 16u) ? 2u : 4u));

        sportResult = adi_sport_ConfigData(hSportRx, wlen[j], ADI_SPORT_NO_PACKING, bLSBFirst);
        DEBUG_RESULT("Failed to configure data for Rx SPORT ",sportResult,ADI_SPORT_SUCCESS);

        sportResult = adi_sport_ConfigData(hSportTx, wlen[j], ADI_SPORT_NO_PACKING, bLSBFirst);
        DEBUG_RESULT("Failed to configure data for Tx SPORT ",sportResult,ADI_SPORT_SUCCESS);

        for (i=1u; i<4u; i++)
        {
            const uint32_t shift = (i * numBytesPerData);
            const uint32_t transferSize = MAX_DATA_TRANSFER - shift;

            memset(nBufferRx1,0,sizeof(nBufferRx1));    /* initialize Rx buffer with 0s */
            memset(nBufferRx2,0,sizeof(nBufferRx2));    /* initialize Rx buffer with 0s */

            sportResult = adi_sport_SubmitBuffer(hSportRx,&nBufferRx1[shift],transferSize,true);
            DEBUG_RESULT("Failed to submit buffer for Rx channel",sportResult,ADI_SPORT_SUCCESS);

            sportResult = adi_sport_SubmitBuffer(hSportRx,&nBufferRx2[shift],transferSize,true);
            DEBUG_RESULT("Failed to submit buffer for Rx channel",sportResult,ADI_SPORT_SUCCESS);

            sportResult = adi_sport_SubmitBuffer(hSportTx,&nBufferTx[shift],transferSize,true);
            DEBUG_RESULT("Failed to submit buffer for Tx channel",sportResult,ADI_SPORT_SUCCESS);

            wait_for_buffer(hSportRx,&nBufferRx1[shift]);
            wait_for_buffer(hSportTx,&nBufferTx[shift]);

            sportResult = adi_sport_SubmitBuffer(hSportTx,&nBufferTx[shift],transferSize,true);
            DEBUG_RESULT("Failed to submit buffer for Tx channel",sportResult,ADI_SPORT_SUCCESS);

            wait_for_buffer(hSportRx,&nBufferRx2[shift]);
            wait_for_buffer(hSportTx,&nBufferTx[shift]);

            cumulatedErr += check_transmitted_data(nBufferTx, nBufferRx1, BUFFER_SIZE, shift, transferSize, wlen[j]);
            cumulatedErr += check_transmitted_data(nBufferTx, nBufferRx2, BUFFER_SIZE, shift, transferSize, wlen[j]);
        }
    }

#if defined (TRACE_TEST)
    if (0u == cumulatedErr)
    {
      common_Perf(" - test case 2: ok");
    }
    else
    {
      common_Perf(" - test case 2: failed!!!");
    }
#endif
    return cumulatedErr;
}


/**
 * test_case_3 covers SPORT transfers with multiple Tx buffers submitted. In each iteration, there's
 * only one Rx buffer but two Tx buffers submitted, with the two Tx buffers sizes equalling the Rx
 * buffer size. This makes sure that the SPORT Tx interrupt properly supports pending Tx buffers when
 * an Tx transfer completes.
 */
static uint32_t test_case_3(void)
{
    uint32_t cumulatedErr = 0u;
    ADI_SPORT_RESULT sportResult = ADI_SPORT_SUCCESS;   /* Variable for storing the return code from UART device */
    uint32_t i;
    uint32_t j;
    bool bLSBFirst = false;

    for (j=0u; j<CFG_UNPACKED_NUM; j++)
    {
        const uint32_t numBytesPerData = ((wlen[j] <= 8u) ? 1u : ((wlen[j] <= 16u) ? 2u : 4u));

        sportResult = adi_sport_ConfigData(hSportRx, wlen[j], ADI_SPORT_NO_PACKING, bLSBFirst);
        DEBUG_RESULT("Failed to configure data for Rx SPORT ",sportResult,ADI_SPORT_SUCCESS);

        sportResult = adi_sport_ConfigData(hSportTx, wlen[j], ADI_SPORT_NO_PACKING, bLSBFirst);
        DEBUG_RESULT("Failed to configure data for Tx SPORT ",sportResult,ADI_SPORT_SUCCESS);

        for (i=1u; i<4u; i++)
        {
            const uint32_t shift = (i * numBytesPerData);
            const uint32_t batchSize1 = ((MAX_DATA_TRANSFER >> 1u) & 0xFFFFFFFCu) - shift;
            const uint32_t batchSize2 = MAX_DATA_TRANSFER - batchSize1;
            uint8_t * batchAddr1 = &nBufferTx[shift];                   /* start address of 1st Tx batch */
            uint8_t * batchAddr2 = &nBufferTx[shift+batchSize1];        /* start address of 2nd Tx batch */

            memset(nBufferRx1,0,sizeof(nBufferRx1));    /* initialize Rx buffer with 0s */
            memset(nBufferRx2,0,sizeof(nBufferRx1));    /* initialize Rx buffer with 0s */


            /* first buffer will receive data from two different Tx requests */
            sportResult = adi_sport_SubmitBuffer(hSportRx,&nBufferRx1[shift],MAX_DATA_TRANSFER,true);
            DEBUG_RESULT("Failed to submit buffer for Rx channel",sportResult,ADI_SPORT_SUCCESS);

            /* second buffer will receive data from a third Tx request */
            sportResult = adi_sport_SubmitBuffer(hSportRx,&nBufferRx2[shift],MAX_DATA_TRANSFER,true);
            DEBUG_RESULT("Failed to submit buffer for Rx channel",sportResult,ADI_SPORT_SUCCESS);

            /* first batch for nBufferRx1 */
            sportResult = adi_sport_SubmitBuffer(hSportTx,batchAddr1,batchSize1,true);
            DEBUG_RESULT("Failed to submit buffer for Tx channel",sportResult,ADI_SPORT_SUCCESS);

            /* 2nd batch for nBufferRx1 */
            sportResult = adi_sport_SubmitBuffer(hSportTx,batchAddr2,batchSize2,true);
            DEBUG_RESULT("Failed to submit buffer for Tx channel",sportResult,ADI_SPORT_SUCCESS);

            wait_for_buffer(hSportTx,batchAddr1);
            wait_for_buffer(hSportTx,batchAddr2);
            wait_for_buffer(hSportRx,&nBufferRx1[shift]);

            /* batch for nBufferRx2 */
            sportResult = adi_sport_SubmitBuffer(hSportTx,&nBufferTx[shift],MAX_DATA_TRANSFER,true);
            DEBUG_RESULT("Failed to submit buffer for Tx channel",sportResult,ADI_SPORT_SUCCESS);

            wait_for_buffer(hSportRx,&nBufferRx2[shift]);
            wait_for_buffer(hSportTx,&nBufferTx[shift]);

            cumulatedErr += check_transmitted_data(nBufferTx, nBufferRx1, BUFFER_SIZE, shift, MAX_DATA_TRANSFER,  wlen[j]);
            cumulatedErr += check_transmitted_data(nBufferTx, nBufferRx2, BUFFER_SIZE, shift, MAX_DATA_TRANSFER,  wlen[j]);
        }
    }

#if defined (TRACE_TEST)
    if (0u == cumulatedErr)
    {
      common_Perf(" - test case 3: ok");
    }
    else
    {
      common_Perf(" - test case 3: failed!!!");
    }
#endif
    return cumulatedErr;
}
