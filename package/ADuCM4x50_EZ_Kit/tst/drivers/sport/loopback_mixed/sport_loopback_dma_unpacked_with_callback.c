/*******************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*******************************************************************************/
/**
 * @file:       sport_loopback_dma_unpacked_with_callback.c
 *
 * @brief       This file contains tests for the SPORT device driver executing
 *              DMA driven requests with callback functions registered.
 */
#include <stdint.h>             /* for 'NULL' */
#include <system_ADuCM4050.h>
#include <common.h>

#include <drivers/general/adi_drivers_general.h>
#include <drivers/sport/adi_sport.h>
#include <drivers/pwr/adi_pwr.h>

#include "sport_test_support.h"
#include "sport_loopback_mixed.h"

#if !defined (__CC_ARM)
#define TRACE_TEST	1
#endif

#define MAX_DATA_TRANSFER       ((BUFFER_SIZE >> 1u) - 4u)
#define MAX_CB_DATA             (3u)

struct tCbData
{
    void *      pBuff;
    uint32_t    evt;
};

extern int32_t adi_initpinmux(void);

extern ADI_SPORT_HANDLE hSportRx;                       /* Handle for Rx channel */
extern ADI_SPORT_HANDLE hSportTx;                       /* Handle for Tx channel */

static volatile uint32_t sportRxDone = 0u;              /* Number of Rx callback function calls by SPORT Rx driver */
static volatile uint32_t sportTxDone = 0u;              /* Number of Tx callback function calls by SPORT Tx driver */
static struct tCbData  cbData[2][MAX_CB_DATA];

/* Prototypes for function used in this example */

extern uint32_t test_sport_dma_unpacked_with_callback(void);

static uint32_t test_case_1(void);
static uint32_t test_case_2(void);
static uint32_t test_case_3(void);

static void initCallback(void);
static void sportCallback(void *pCBParam, uint32_t Event, void *pArg);

uint32_t test_sport_dma_unpacked_with_callback(void)
{
    ADI_SPORT_RESULT sportResult = ADI_SPORT_SUCCESS;   /* Variable for storing the return code from UART device */
    uint32_t err = 0u;

#if defined (TRACE_TEST)
    common_Perf("SPORT DMA unpacked - with callback");
    common_Perf("==================================");
#endif

    sportResult = adi_sport_RegisterCallback(hSportRx, sportCallback, hSportRx);
    DEBUG_RESULT("Failed to register a callback function for the Rx channel",sportResult,ADI_SPORT_SUCCESS);

    sportResult = adi_sport_RegisterCallback(hSportTx, sportCallback, hSportTx);
    DEBUG_RESULT("Failed to register a callback function for the Tx channel",sportResult,ADI_SPORT_SUCCESS);

    err += test_case_1();
    err += test_case_2();
    err += test_case_3();

#if defined (TRACE_TEST)
    common_Perf("");
#endif
    return err;
}

/**
 * test_case_1 covers SPORT transfers with no multiple buffers submitted. In each iteration, there's
 * only one Rx buffer and one Tx buffer submitted, both with the same size. This makes sure that this
 * simple SPORT situation is fully functional.
 */
static uint32_t test_case_1(void)
{
    ADI_SPORT_RESULT sportResult = ADI_SPORT_SUCCESS;   /* Variable for storing the return code from UART device */
    uint32_t cumulatedErr = 0u;
    uint32_t i;
    uint32_t j;
    bool bLSBFirst = false;

    for (j=0u; j<CFG_UNPACKED_NUM; j++)
    {
        const uint32_t numBytesPerData = ((wlen[j] <= 8u) ? 1u : ((wlen[j] <= 16u) ? 2u : 4u));

        sportResult = adi_sport_ConfigData(hSportRx, wlen[j], ADI_SPORT_NO_PACKING, bLSBFirst);
        DEBUG_RESULT("Failed to configure data for Rx SPORT ",sportResult,ADI_SPORT_SUCCESS);

        sportResult = adi_sport_ConfigData(hSportTx, wlen[j], ADI_SPORT_NO_PACKING, bLSBFirst);
        DEBUG_RESULT("Failed to configure data for Tx SPORT ",sportResult,ADI_SPORT_SUCCESS);

        for (i=1u; i<4u; i++)
        {
            const uint32_t shift = (i * numBytesPerData);
            const uint32_t transferSize = MAX_DATA_TRANSFER - shift;

            memset(dataRx[i],0u,dataRxSize[i]);         /* initialize Rx buffer with 0s */

            initCallback();

            sportResult = adi_sport_SubmitBuffer(hSportRx,&(dataRx[i][shift]),transferSize,true);
            DEBUG_RESULT("Failed to submit buffer for Rx channel",sportResult,ADI_SPORT_SUCCESS);

            sportResult = adi_sport_SubmitBuffer(hSportTx,&nBufferTx[shift],transferSize,true);
            DEBUG_RESULT("Failed to submit buffer for Tx channel",sportResult,ADI_SPORT_SUCCESS);

            while (0u == sportTxDone);                  /* wait for Tx callback */
            while (0u == sportRxDone);                  /* wait for Rx callback */

            if (! (  (ADI_SPORT_EVENT_TX_BUFFER_PROCESSED == cbData[ADI_SPORT_DIR_TX][0u].evt)
                  && (ADI_SPORT_EVENT_RX_BUFFER_PROCESSED == cbData[ADI_SPORT_DIR_RX][0u].evt)
                  && (cbData[ADI_SPORT_DIR_RX][0u].pBuff == &(dataRx[i][shift]))
                  && (cbData[ADI_SPORT_DIR_TX][0u].pBuff == &nBufferTx[shift])
                  )
               )
            {
#if defined (TRACE_TEST)
                common_Perf(" - test case 1: unexpected callback results");
#endif
            }
            cumulatedErr += check_transmitted_data(nBufferTx, dataRx[i], BUFFER_SIZE, shift, transferSize, wlen[j]);
        }
    }

#if defined (TRACE_TEST)
    if (0u == cumulatedErr)
    {
      common_Perf(" - test case 1: ok");
    }
    else
    {
      common_Perf(" - test case 1: failed!!!");
    }
#endif
    return cumulatedErr;
}

/**
 * test_case_2 covers SPORT transfers with multiple Rx buffers submitted. In each iteration, there's
 * only one Tx buffer but two Rx buffers submitted, with the two Rx buffers sizes equalling the Tx
 * buffer size. This makes sure that the SPORT Rx interrupt properly supports pending Rx buffers when
 * an Rx transfer completes.
 */
static uint32_t test_case_2(void)
{
    uint32_t cumulatedErr = 0u;
    ADI_SPORT_RESULT sportResult = ADI_SPORT_SUCCESS;   /* Variable for storing the return code from UART device */
    uint32_t i;
    uint32_t j;
    bool bLSBFirst = false;

    for (j=0u; j<CFG_UNPACKED_NUM; j++)
    {
        const uint32_t numBytesPerData = ((wlen[j] <= 8u) ? 1u : ((wlen[j] <= 16u) ? 2u : 4u));

        sportResult = adi_sport_ConfigData(hSportRx, wlen[j], ADI_SPORT_NO_PACKING, bLSBFirst);
        DEBUG_RESULT("Failed to configure data for Rx SPORT ",sportResult,ADI_SPORT_SUCCESS);

        sportResult = adi_sport_ConfigData(hSportTx, wlen[j], ADI_SPORT_NO_PACKING, bLSBFirst);
        DEBUG_RESULT("Failed to configure data for Tx SPORT ",sportResult,ADI_SPORT_SUCCESS);

        for (i=1u; i<4u; i++)
        {
            const uint32_t shift = (i * numBytesPerData);
            const uint32_t transferSize = MAX_DATA_TRANSFER - shift;

            memset(nBufferRx1,0,sizeof(nBufferRx1));    /* initialize Rx buffer with 0s */
            memset(nBufferRx2,0,sizeof(nBufferRx2));    /* initialize Rx buffer with 0s */

            initCallback();

            sportResult = adi_sport_SubmitBuffer(hSportRx,&nBufferRx1[shift],transferSize,true);
            DEBUG_RESULT("Failed to submit buffer for Rx channel",sportResult,ADI_SPORT_SUCCESS);

            sportResult = adi_sport_SubmitBuffer(hSportRx,&nBufferRx2[shift],transferSize,true);
            DEBUG_RESULT("Failed to submit buffer for Rx channel",sportResult,ADI_SPORT_SUCCESS);

            sportResult = adi_sport_SubmitBuffer(hSportTx,&nBufferTx[shift],transferSize,true);
            DEBUG_RESULT("Failed to submit buffer for Tx channel",sportResult,ADI_SPORT_SUCCESS);

            while (0u == sportRxDone);                  /* wait for Rx callback */
            while (0u == sportTxDone);                  /* wait for Tx callback */

            if (! (  (ADI_SPORT_EVENT_TX_BUFFER_PROCESSED == cbData[ADI_SPORT_DIR_TX][0u].evt)
                  && (ADI_SPORT_EVENT_RX_BUFFER_PROCESSED == cbData[ADI_SPORT_DIR_RX][0u].evt)
                  && (cbData[ADI_SPORT_DIR_RX][0u].pBuff == &nBufferRx1[shift])
                  && (cbData[ADI_SPORT_DIR_TX][0u].pBuff == &nBufferTx[shift])
                  )
                )
            {
#if defined (TRACE_TEST)
                common_Perf(" - test case 2: unexpected callback results");
#endif
            }

            sportResult = adi_sport_SubmitBuffer(hSportTx,&nBufferTx[shift],transferSize,true);
            DEBUG_RESULT("Failed to submit buffer for Tx channel",sportResult,ADI_SPORT_SUCCESS);

            while (1u == sportRxDone);                  /* wait for Rx callback */
            while (1u == sportTxDone);                  /* wait for Tx callback */

            if (! (  (ADI_SPORT_EVENT_TX_BUFFER_PROCESSED == cbData[ADI_SPORT_DIR_TX][1u].evt)
                  && (ADI_SPORT_EVENT_RX_BUFFER_PROCESSED == cbData[ADI_SPORT_DIR_RX][1u].evt)
                  && (cbData[ADI_SPORT_DIR_RX][1u].pBuff == &nBufferRx2[shift])
                  && (cbData[ADI_SPORT_DIR_TX][1u].pBuff == &nBufferTx[shift])
                   )
               )
            {
#if defined (TRACE_TEST)
                common_Perf(" - test case 2: unexpected callback results");
#endif
            }

            cumulatedErr += check_transmitted_data(nBufferTx, nBufferRx1, BUFFER_SIZE, shift, transferSize, wlen[j]);
            cumulatedErr += check_transmitted_data(nBufferTx, nBufferRx2, BUFFER_SIZE, shift, transferSize, wlen[j]);
        }
    }

#if defined (TRACE_TEST)
    if (0u == cumulatedErr)
    {
      common_Perf(" - test case 2: ok");
    }
    else
    {
      common_Perf(" - test case 2: failed!!!");
    }
#endif
    return cumulatedErr;
}


/**
 * test_case_3 covers SPORT transfers with multiple Tx buffers submitted. In each iteration, there's
 * only one Rx buffer but two Tx buffers submitted, with the two Tx buffers sizes equalling the Rx
 * buffer size. This makes sure that the SPORT Tx interrupt properly supports pending Tx buffers when
 * an Tx transfer completes.
 */
static uint32_t test_case_3(void)
{
    uint32_t cumulatedErr = 0u;
    ADI_SPORT_RESULT sportResult = ADI_SPORT_SUCCESS;   /* Variable for storing the return code from UART device */
    uint32_t i;
    uint32_t j;
    bool bLSBFirst = false;

    for (j=0u; j<CFG_UNPACKED_NUM; j++)
    {
        const uint32_t numBytesPerData = ((wlen[j] <= 8u) ? 1u : ((wlen[j] <= 16u) ? 2u : 4u));

        sportResult = adi_sport_ConfigData(hSportRx, wlen[j], ADI_SPORT_NO_PACKING, bLSBFirst);
        DEBUG_RESULT("Failed to configure data for Rx SPORT ",sportResult,ADI_SPORT_SUCCESS);

        sportResult = adi_sport_ConfigData(hSportTx, wlen[j], ADI_SPORT_NO_PACKING, bLSBFirst);
        DEBUG_RESULT("Failed to configure data for Tx SPORT ",sportResult,ADI_SPORT_SUCCESS);

        for (i=1u; i<4u; i++)
        {
            const uint32_t shift = (i * numBytesPerData);
            const uint32_t batchSize1 = ((MAX_DATA_TRANSFER >> 1u) & 0xFFFFFFFCu) - shift;
            const uint32_t batchSize2 = MAX_DATA_TRANSFER - batchSize1;
            uint8_t * batchAddr1 = &nBufferTx[shift];                   /* start address of 1st Tx batch */
            uint8_t * batchAddr2 = &nBufferTx[shift+batchSize1];        /* start address of 2nd Tx batch */

            memset(nBufferRx1,0,sizeof(nBufferRx1));    /* initialize Rx buffer with 0s */
            memset(nBufferRx2,0,sizeof(nBufferRx1));    /* initialize Rx buffer with 0s */

            initCallback();

            /* first buffer will receive data from two different Tx requests */
            sportResult = adi_sport_SubmitBuffer(hSportRx,&nBufferRx1[shift],MAX_DATA_TRANSFER,true);
            DEBUG_RESULT("Failed to submit buffer for Rx channel",sportResult,ADI_SPORT_SUCCESS);

            /* second buffer will receive data from a third Tx request */
            sportResult = adi_sport_SubmitBuffer(hSportRx,&nBufferRx2[shift],MAX_DATA_TRANSFER,true);
            DEBUG_RESULT("Failed to submit buffer for Rx channel",sportResult,ADI_SPORT_SUCCESS);

            /* first batch for nBufferRx1 */
            sportResult = adi_sport_SubmitBuffer(hSportTx,batchAddr1,batchSize1,true);
            DEBUG_RESULT("Failed to submit buffer for Tx channel",sportResult,ADI_SPORT_SUCCESS);

            /* 2nd batch for nBufferRx1 */
            sportResult = adi_sport_SubmitBuffer(hSportTx,batchAddr2,batchSize2,true);
            DEBUG_RESULT("Failed to submit buffer for Tx channel",sportResult,ADI_SPORT_SUCCESS);

            while (0u == sportRxDone);                  /* wait for Rx callback */
            while (1u >= sportTxDone);                  /* wait for Tx callbacks */

            if (! (  (ADI_SPORT_EVENT_TX_BUFFER_PROCESSED == cbData[ADI_SPORT_DIR_TX][0u].evt)
                  && (ADI_SPORT_EVENT_TX_BUFFER_PROCESSED == cbData[ADI_SPORT_DIR_TX][1u].evt)
                  && (ADI_SPORT_EVENT_RX_BUFFER_PROCESSED == cbData[ADI_SPORT_DIR_RX][0u].evt)
                  && (cbData[ADI_SPORT_DIR_RX][0u].pBuff == &nBufferRx1[shift])
                  && (cbData[ADI_SPORT_DIR_TX][0u].pBuff == batchAddr1)
                  && (cbData[ADI_SPORT_DIR_TX][1u].pBuff == batchAddr2)
                  )
               )
            {
#if defined (TRACE_TEST)
                common_Perf(" - test case 3: unexpected callback results");
#endif
            }

            /* batch for nBufferRx2 */
            sportResult = adi_sport_SubmitBuffer(hSportTx,&nBufferTx[shift],MAX_DATA_TRANSFER,true);
            DEBUG_RESULT("Failed to submit buffer for Tx channel",sportResult,ADI_SPORT_SUCCESS);

            while (1u == sportRxDone);                  /* wait for Rx callback */
            while (2u == sportTxDone);                  /* wait for Tx callbacks */

            if (! (  (ADI_SPORT_EVENT_TX_BUFFER_PROCESSED == cbData[ADI_SPORT_DIR_TX][2u].evt)
                  && (ADI_SPORT_EVENT_RX_BUFFER_PROCESSED == cbData[ADI_SPORT_DIR_RX][1u].evt)
                  && (cbData[ADI_SPORT_DIR_RX][1u].pBuff == &nBufferRx2[shift])
                  && (cbData[ADI_SPORT_DIR_TX][2u].pBuff == &nBufferTx[shift])
                  )
               )
            {
#if defined (TRACE_TEST)
                common_Perf(" - test case 3: unexpected callback results");
#endif
            }

            cumulatedErr += check_transmitted_data(nBufferTx, nBufferRx1, BUFFER_SIZE, shift, MAX_DATA_TRANSFER, wlen[j]);
            cumulatedErr += check_transmitted_data(nBufferTx, nBufferRx2, BUFFER_SIZE, shift, MAX_DATA_TRANSFER, wlen[j]);
        }
    }

#if defined (TRACE_TEST)
    if (0u == cumulatedErr)
    {
      common_Perf(" - test case 3: ok");
    }
    else
    {
      common_Perf(" - test case 3: failed!!!");
    }
#endif
    return cumulatedErr;
}

static void sportCallback(void *pCBParam, uint32_t Event, void *pArg)
{
    const ADI_SPORT_HANDLE hDev = (ADI_SPORT_HANDLE) pCBParam;  /* SPORT handle transmitted by SPORT driver */

    if (hDev == hSportTx)
    {
        if (ADI_SPORT_EVENT_TX_BUFFER_PROCESSED == Event)
        {
            cbData[ADI_SPORT_DIR_TX][sportTxDone].pBuff = pArg; /* record the buffer address value returned (NULL if an error occurred) */
            cbData[ADI_SPORT_DIR_TX][sportTxDone].evt = Event;  /* record the nature of the event that occurred in the callback function */
            sportTxDone++;                                      /* SPORT Tx task is done */
        }
    }
    else if (hDev == hSportRx)
    {
        if (ADI_SPORT_EVENT_RX_BUFFER_PROCESSED == Event)
        {
            cbData[ADI_SPORT_DIR_RX][sportRxDone].pBuff = pArg; /* record the buffer address value returned (NULL if an error occurred) */
            cbData[ADI_SPORT_DIR_RX][sportRxDone].evt = Event;  /* record the nature of the event that occurred in the callback function */
            sportRxDone++;                                      /* SPORT Tx task is done */
        }
    }
}

static void initCallback(void)
{
    memset(cbData,0u,sizeof(cbData));
    sportRxDone = 0u;
    sportTxDone = 0u;
}

