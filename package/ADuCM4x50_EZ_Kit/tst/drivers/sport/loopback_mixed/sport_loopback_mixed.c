/*******************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*******************************************************************************/
/**
 * @file:       sport_loopback_mixed.c
 * 
 * @brief       This file contains tests for the SPORT device driver executing
 *              DMA and core driven requests.
 */
#include <stddef.h>             /* for 'NULL' */
#include <system_ADuCM4050.h>
#include <common.h>

#include <drivers/general/adi_drivers_general.h>
#include <drivers/sport/adi_sport.h>
#include <drivers/pwr/adi_pwr.h>

#include "sport_loopback_mixed.h"

#define MAX_ITER        (3u)

extern uint32_t sport_SysDataErr;

extern int32_t adi_initpinmux(void);

extern uint32_t test_sport_mixed_without_callback(void);
extern uint32_t test_sport_mixed_with_callback(void);
extern uint32_t test_sport_dma_unpacked_without_callback(void);
extern uint32_t test_sport_dma_unpacked_with_callback(void);

ADI_ALIGNED_PRAGMA(4)
static unsigned char sportDriverTx[ADI_SPORT_MEMORY_SIZE] ADI_ALIGNED_ATTRIBUTE(4);     /* Memory required by the device for TX operation */

ADI_ALIGNED_PRAGMA(4)
static unsigned char sportDriverRx[ADI_SPORT_MEMORY_SIZE] ADI_ALIGNED_ATTRIBUTE(4);     /* Memory required by the device for RX operation */

ADI_ALIGNED_PRAGMA(4)
ADI_SPORT_HANDLE hSportRx ADI_ALIGNED_ATTRIBUTE(4);                                     /* Handle for Rx channel */

ADI_ALIGNED_PRAGMA(4)
ADI_SPORT_HANDLE hSportTx ADI_ALIGNED_ATTRIBUTE(4);                                     /* Handle for Tx channel */

/**
 * @brief       main function (application entry point)
 */
int main(void)
{
    ADI_PWR_RESULT ePwrResult = ADI_PWR_SUCCESS;
    uint32_t err = 0u;
    uint32_t iter = 0u;

    adi_initpinmux();
    common_Init();

    ePwrResult = adi_pwr_Init();
    DEBUG_RESULT("Failed to initialize power service",ePwrResult,ADI_PWR_SUCCESS);

    ePwrResult = adi_pwr_SetClockDivider(ADI_CLOCK_HCLK,1);
    DEBUG_RESULT("Failed to assign HCLK",ePwrResult,ADI_PWR_SUCCESS);

    ePwrResult = adi_pwr_SetClockDivider(ADI_CLOCK_PCLK,1);
    DEBUG_RESULT("Failed to assign PCLK",ePwrResult,ADI_PWR_SUCCESS);

    ePwrResult = adi_pwr_EnableClock(ADI_CLOCK_GATE_PCLK, true);
    DEBUG_RESULT("Failed to enable power clock",ePwrResult,ADI_PWR_SUCCESS);

    for (iter=0u; iter < MAX_ITER; iter++)
    {
        const uint32_t memSize = (uint32_t)(ADI_SPORT_MEMORY_SIZE);
        ADI_SPORT_RESULT sportResult = ADI_SPORT_SUCCESS;       /* Variable for storing the return code from SPORT device */

        /* SPORT-A and SPORT-B are configured through the configuration parameters
        * in adi_sport_config.h ; there's no need to call adi_sport_ConfigData,
        * adi_sport_ConfigClock and adi_sport_ConfigFrameSync for SPORT drivers
        * in this example. */

        sportResult = adi_sport_Open(SPORT_DEVICE_ID, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx);
        DEBUG_RESULT("Failed to open SPORT0 A in Rx mode",sportResult,ADI_SPORT_SUCCESS);

        sportResult = adi_sport_Open(SPORT_DEVICE_ID, ADI_HALF_SPORT_B, ADI_SPORT_DIR_TX, sportDriverTx, memSize, &hSportTx);
        DEBUG_RESULT("Failed to open SPORT0 B in Tx mode",sportResult,ADI_SPORT_SUCCESS);

        if (0u == (iter & 0x1u))
        {
            err += test_sport_mixed_without_callback();
            err += test_sport_mixed_with_callback();
        } else {
            err += test_sport_mixed_with_callback();
            err += test_sport_mixed_without_callback();
        }
        err += test_sport_dma_unpacked_without_callback();
        err += test_sport_dma_unpacked_with_callback();

        sportResult = adi_sport_Close(hSportTx);
        DEBUG_RESULT("Failed to close SPORT0 A",sportResult,ADI_SPORT_SUCCESS);

        sportResult = adi_sport_Close(hSportRx);
        DEBUG_RESULT("Failed to close SPORT0 B",sportResult,ADI_SPORT_SUCCESS);
    }

    if (0u < sport_SysDataErr)
    {
        char msg[64];

        sprintf(msg,"SYSDATAERR detected %lu time%s\n", sport_SysDataErr, ((1u < sport_SysDataErr) ? "s" : "") );
        common_Perf(msg);
    }

    if (0u == err)
    {
        common_Pass();
    } else {
        common_Fail("Test for SPORT loopback mixed");
    }
    return 0;
}

ADI_ALIGNED_PRAGMA(4)
uint8_t  nBufferTx[BUFFER_SIZE]  ADI_ALIGNED_ATTRIBUTE(4) =
{
#include "sport_data_buf.dat"
};

ADI_ALIGNED_PRAGMA(4)
uint8_t  nBufferRx1[BUFFER_SIZE]  ADI_ALIGNED_ATTRIBUTE(4);

ADI_ALIGNED_PRAGMA(4)
uint8_t  nBufferRx2[BUFFER_SIZE]  ADI_ALIGNED_ATTRIBUTE(4);

ADI_ALIGNED_PRAGMA(4)
uint8_t  nBufferRx3[BUFFER_SIZE]  ADI_ALIGNED_ATTRIBUTE(4);

ADI_ALIGNED_PRAGMA(4)
uint8_t  nBufferRx4[BUFFER_SIZE]  ADI_ALIGNED_ATTRIBUTE(4);


uint8_t * dataRx[TEST_ITER] = {&nBufferRx1[0], &nBufferRx2[0], &nBufferRx3[0], &nBufferRx4[0]} ;
size_t dataRxSize[TEST_ITER] = {sizeof(nBufferRx1), sizeof(nBufferRx2), sizeof(nBufferRx3), sizeof(nBufferRx4)} ;

bool dmaRx[TEST_ITER] = {false, true, false, true};
bool dmaTx[TEST_ITER] = {false, false, true, true};

/**
 * Different SPORT data size used in these tests. This means all the possible
 * SPORT data sizes are tested, from 4-bit to 32-bit.
 */
uint8_t wlen[CFG_NUM] = {  4u,  5u,  6u,  7u,  8u,  9u, 10u, 11u
                        , 12u, 13u, 14u, 15u, 16u, 17u, 18u, 19u
                        , 20u, 21u, 22u, 23u, 24u, 25u, 26u, 27u
                        , 28u, 29u, 30u, 31u, 32u};

/**
 * Different packing modes to be used with the SPORT data size tested.
 * Tests implemented in sport_loopback_dma_unpacked_no_callback.c and
 * in sport_loopback_dma_unpacked_with_callback.c cover the unpack
 * modes for SPORT data size 4-bit to 16-bit.
 */
ADI_SPORT_PACKING_MODE pack[CFG_NUM] = {
  ADI_SPORT_8BIT_PACKING,  ADI_SPORT_8BIT_PACKING,  ADI_SPORT_8BIT_PACKING,  ADI_SPORT_8BIT_PACKING,
  ADI_SPORT_8BIT_PACKING,  ADI_SPORT_16BIT_PACKING, ADI_SPORT_16BIT_PACKING, ADI_SPORT_16BIT_PACKING,
  ADI_SPORT_16BIT_PACKING, ADI_SPORT_16BIT_PACKING, ADI_SPORT_16BIT_PACKING, ADI_SPORT_16BIT_PACKING,
  ADI_SPORT_16BIT_PACKING, ADI_SPORT_NO_PACKING,   ADI_SPORT_NO_PACKING,     ADI_SPORT_NO_PACKING,
  ADI_SPORT_NO_PACKING,    ADI_SPORT_NO_PACKING,   ADI_SPORT_NO_PACKING,     ADI_SPORT_NO_PACKING,
  ADI_SPORT_NO_PACKING
};

