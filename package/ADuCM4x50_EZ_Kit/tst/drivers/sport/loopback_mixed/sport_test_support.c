/*******************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*******************************************************************************/
/**
 * @file:       sport_test_support.c
 */
#include <stdint.h>             /* for 'uint<N>_t' */
#include <common.h>
#include <drivers/sport/adi_sport.h>

#include "sport_test_support.h"

#if !defined (__CC_ARM)
#define TRACE_TEST	1
#endif

uint32_t sport_SysDataErr = 0u;

uint32_t check_transmitted_data(const uint8_t TxBuff[], const uint8_t RxBuff[], const uint32_t numData, const uint32_t start, const uint32_t numTrans, const uint32_t wlen)
{
    uint32_t i;
    uint32_t error_count = 0u;
    uint32_t mask[4] = {0xFFu, 0xFFu, 0xFFu, 0xFFu};
    const uint32_t lim1 = start;
    const uint32_t lim2 = start + numTrans;

    switch(wlen)
    {
    case  4u: mask[0] = mask[1] = mask[2] = mask[3] = 0x0F; break;
    case  5u: mask[0] = mask[1] = mask[2] = mask[3] = 0x1F; break;
    case  6u: mask[0] = mask[1] = mask[2] = mask[3] = 0x3F; break;
    case  7u: mask[0] = mask[1] = mask[2] = mask[3] = 0x7F; break;
    case  8u: break;
    case  9u: mask[1] = mask[3] = 0x01u; break;
    case 10u: mask[1] = mask[3] = 0x03u; break;
    case 11u: mask[1] = mask[3] = 0x07u; break;
    case 12u: mask[1] = mask[3] = 0x0Fu; break;
    case 13u: mask[1] = mask[3] = 0x1Fu; break;
    case 14u: mask[1] = mask[3] = 0x3Fu; break;
    case 15u: mask[1] = mask[3] = 0x7Fu; break;
    case 16u: break;
    case 17u: mask[2] = 0x01u; mask[3] = 0x00u; break;
    case 18u: mask[2] = 0x03u; mask[3] = 0x00u; break;
    case 19u: mask[2] = 0x07u; mask[3] = 0x00u; break;
    case 20u: mask[2] = 0x0Fu; mask[3] = 0x00u; break;
    case 21u: mask[2] = 0x1Fu; mask[3] = 0x00u; break;
    case 22u: mask[2] = 0x3Fu; mask[3] = 0x00u; break;
    case 23u: mask[2] = 0x7Fu; mask[3] = 0x00u; break;
    case 24u: mask[3] = 0x00u; break;
    case 25u: mask[3] = 0x01u; break;
    case 26u: mask[3] = 0x03u; break;
    case 27u: mask[3] = 0x07u; break;
    case 28u: mask[3] = 0x0Fu; break;
    case 29u: mask[3] = 0x1Fu; break;
    case 30u: mask[3] = 0x3Fu; break;
    case 31u: mask[3] = 0x7Fu; break;
    case 32u: break;
    }


    for (i = 0; i < lim1; i++)
    {
        if (0u != RxBuff[i])
        {
            error_count++;
        }
    }
    for (i = lim1; i < lim2; i++)
    {
        const uint8_t byte = TxBuff[i];
        const uint8_t expected = byte & mask[i%4];
        if (expected != RxBuff[i])
        {
            error_count++;
        }
    }
    for (i = lim2; i < numData; i++)
    {
        if (0u != RxBuff[i])
        {
            error_count++;
        }
    }
    return error_count;
}

void wait_for_buffer(ADI_SPORT_HANDLE hDevSport, const uint8_t pBuff[])
{
    ADI_SPORT_RESULT result = ADI_SPORT_SUCCESS;
    bool bBufferComplete = false;
    void *pProcessedBuffer = NULL;              /* Variable to get the buffer address */
    uint32_t hwErr = ADI_SPORT_HW_NO_ERR;

    /* wait for buffer availability */
    do
    {
        result = adi_sport_IsBufferAvailable(hDevSport,&bBufferComplete);
        DEBUG_RESULT("Failed to check buffer availability",result,ADI_SPORT_SUCCESS);
    }
    while (true != bBufferComplete);

    /* Get the address of the buffer processed by the SPORT channel.
    * Upon successful return "pProcessedBuffer" should contain the
    * address of the data buffer, pBuff and content of nSize should
    * be size of "pProcessedBuffer"
    */
    result = adi_sport_GetBuffer(hDevSport,&pProcessedBuffer,&hwErr);
    switch (result)
    {
    case ADI_SPORT_SUCCESS:
        break;

    case ADI_SPORT_HW_ERROR:
        if (ADI_SPORT_HW_ERR_SYSDATAERR == hwErr)
        {
            sport_SysDataErr++;
#if defined (TRACE_TEST)
        }else{
            char msg[64];

            sprintf(msg,"HW 0x%08X event detection 0x%X", (unsigned int)hDevSport, (unsigned int)hwErr);
            common_Perf(msg);
#endif
        }
        break;

    default:
        DEBUG_RESULT("Failed to get the processed buffer",result,ADI_SPORT_SUCCESS);
    }

    DEBUG_RESULT("Processed buffer is not the buffer initially submitted",(unsigned int)pProcessedBuffer,(unsigned int)pBuff);
}
