/*******************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*******************************************************************************/
/**
 * @file:       check_transmitted_data.c
 */
#include <stdint.h>             /* for 'uint<N>_t' */
#include <common.h>
#include <drivers/sport/adi_sport.h>

#include "sport_test_support.h"

#if !defined (__CC_ARM)
#define TRACE_TEST	1
#endif

uint32_t sport_SysDataErr = 0u;

uint32_t check_transmitted_data(const uint8_t TxBuff[], const uint8_t RxBuff[], const uint32_t numData, const uint32_t numTrans)
{
    uint32_t i;
    uint32_t error_count = 0u;

    for (i = 0u; i < numTrans; i++)
    {
        if (TxBuff[i] != RxBuff[i])
        {
            error_count++;
        }
    }
    for (i = numTrans; i < numData; i++)
    {
        if (0u != RxBuff[i])
        {
            error_count++;
        }
    }
    return error_count;
}

void wait_for_buffer(ADI_SPORT_HANDLE hDevSport, const uint8_t pBuff[])
{
    ADI_SPORT_RESULT result = ADI_SPORT_SUCCESS;
    bool bBufferComplete = false;
    void *pProcessedBuffer = NULL;                              /* Variable to get the buffer address */
    uint32_t hwErr = ADI_SPORT_HW_NO_ERR;

    /* wait for buffer availability */
    do
    {
        result = adi_sport_IsBufferAvailable(hDevSport,&bBufferComplete);
        DEBUG_RESULT("Failed to check buffer availability",result,ADI_SPORT_SUCCESS);
    }
    while (true != bBufferComplete);

    /* Get the address of the buffer processed by the SPORT channel.
    * Upon successful return "pProcessedBuffer" should contain the
    * address of the data buffer, pBuff and content of nSize should
    * be size of "pProcessedBuffer"
    */
    result = adi_sport_GetBuffer(hDevSport,&pProcessedBuffer,&hwErr);
    switch (result)
    {
    case ADI_SPORT_SUCCESS:
        break;

    case ADI_SPORT_HW_ERROR:
        if (ADI_SPORT_HW_ERR_SYSDATAERR == hwErr)
        {
            sport_SysDataErr++;
#if defined(TRACE_TEST)
        }else{
            char msg[64];

            sprintf(msg,"HW 0x%08X event detection 0x%X", (unsigned int)hDevSport, (unsigned int)hwErr);
            common_Perf(msg);
#endif
        }
        break;

    default:
        DEBUG_RESULT("Failed to get the processed buffer",result,ADI_SPORT_SUCCESS);
    }

    DEBUG_RESULT("Processed buffer is not the buffer initially submitted",(unsigned int)pProcessedBuffer,(unsigned int)pBuff);
}

void start_counting(void)
{
#if defined(ADI_CYCLECOUNT_ENABLED) && (ADI_CYCLECOUNT_ENABLED == 1u)
    ADI_CYCLECOUNT_RESULT ccRes = adi_cyclecount_start();
    DEBUG_RESULT("Failed to execute adi_cyclecount_start",ccRes,ADI_CYCLECOUNT_SUCCESS);
#endif
}

void stop_counting(uint32_t cycleCountId)
{
#if defined(ADI_CYCLECOUNT_ENABLED) && (ADI_CYCLECOUNT_ENABLED==1u)
    ADI_CYCLECOUNT_STORE(cycleCountId);
    ADI_CYCLECOUNT_RESULT ccRes = adi_cyclecount_stop();
    DEBUG_RESULT("Failed to execute adi_cyclecount_stop",ccRes,ADI_CYCLECOUNT_SUCCESS);
#endif
}
