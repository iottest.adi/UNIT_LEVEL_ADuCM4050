/*******************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*******************************************************************************/
/**
 * @file:       sport_test_support.h
 */
#ifndef SPORT_TEST_SUPPORT_H
#define SPORT_TEST_SUPPORT_H

extern uint32_t check_transmitted_data(const uint8_t TxBuff[], const uint8_t RxBuff[], const uint32_t numData, const uint32_t numTrans);
extern void wait_for_buffer(ADI_SPORT_HANDLE hDevSport, const uint8_t pBuff[]);

#endif /* SPORT_TEST_SUPPORT_H */
