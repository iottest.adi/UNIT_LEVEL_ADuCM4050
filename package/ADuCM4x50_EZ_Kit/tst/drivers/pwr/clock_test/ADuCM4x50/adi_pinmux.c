/*
 **
 ** Source file generated on August 8, 2016 at 15:35:08.	
 **
 ** Copyright (C) 2016 Analog Devices Inc., All Rights Reserved.
 **
 ** This file is generated automatically based upon the options selected in 
 ** the Pin Multiplexing configuration editor. Changes to the Pin Multiplexing
 ** configuration should be made by changing the appropriate options rather
 ** than editing this file.
 **
 ** Selected Peripherals
 ** --------------------
 ** SYS_CLK (CLOCK_OUT)
 **
 ** GPIO (unavailable)
 ** ------------------
 ** P2_11
 */

#include <adi_processor.h>
#include <stdint.h>

#define SYS_CLK_CLOCK_OUT_PORTP2_MUX  ((uint32_t) ((uint32_t) 2<<22))

int32_t adi_initpinmux(void);

/*
 * Initialize the Port Control MUX Registers
 */
int32_t adi_initpinmux(void) {
    /* PORTx_MUX registers */
    *((volatile uint32_t *)REG_GPIO2_CFG) = SYS_CLK_CLOCK_OUT_PORTP2_MUX;

    return 0;
}

