/*********************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/

#include <common.h>
#include <drivers/pwr/adi_pwr.h>
#include <stdint.h>

void adi_initpinmux(void);
#define CALLBACK_INFO_SIZE  	20

//#define TEST_EXTERNAL_CLK_IN
//#define TEST_LFXTAL_BYPASS

typedef struct 
{
	 volatile uint32_t nEvent;
	 volatile void *pEventData;

} CALLBACK_INFO;


CALLBACK_INFO CallbackInfo[CALLBACK_INFO_SIZE];
volatile uint32_t gNumCallbacks = 0;

/*
 * Power driver callback
 */
static void PowerDriverCallback(void* pCBParam, uint32_t nEvent,  void* pEventData)
{
    if(gNumCallbacks < CALLBACK_INFO_SIZE)
    {
        CallbackInfo[gNumCallbacks].nEvent = nEvent;
        CallbackInfo[gNumCallbacks].pEventData = pEventData;
	
        gNumCallbacks++;
    }
}

static int ChangeToHFXTAL(void)
{
    /* Enable HFXTAL */
    if(adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_HFXTAL, true) != ADI_PWR_SUCCESS)
    {
       DEBUG_MESSAGE("Failed to enable the HFXTAL as clock source\n");
       return -1;
    }
    
  
    /* Change Root Clock source to HFXTAL */
    if(adi_pwr_SetRootClockMux(ADI_CLOCK_MUX_ROOT_HFXTAL) != ADI_PWR_SUCCESS)
    {
       DEBUG_MESSAGE("Failed to set the HFXTAL as root clock source\n");
       return -1;        
    }
  
    return 0;
}

static int EnableLFXTAL(void)
{
    /* Enable HFXTAL */
    if(adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_LFXTAL, true) != ADI_PWR_SUCCESS)
    {
       DEBUG_MESSAGE("Failed to enable the LFXTAL as clock source\n");
       return -1;
    }
 
    return 0;
}

#ifdef TEST_EXTERNAL_CLK_IN
static int ChangeToGPIO(void)
{
  
    /* Change Root Clock source to HFXTAL */
    if(adi_pwr_SetRootClockMux(ADI_CLOCK_MUX_ROOT_GPIO) != ADI_PWR_SUCCESS)
    {
       DEBUG_MESSAGE("Failed to set the GPIO as root clock source\n");
       return -1;        
    }
  
    return 0;
}
#endif /* TEST_EXTERNAL_CLK_IN */

static int ChangeToSPLL(void)
{
    /* Set the Root clock source to HFOSC (PLL cannot be used when configuring it) */
    if(adi_pwr_SetRootClockMux(ADI_CLOCK_MUX_ROOT_HFOSC) != ADI_PWR_SUCCESS)
    {
       DEBUG_MESSAGE("Failed to set the HFOSC as root clock source\n");
       return -1;        
    }
    
    /* Disable the SPLL */
    if(adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_SPLL, false) != ADI_PWR_SUCCESS)
    {
       DEBUG_MESSAGE("Failed to disable the SPLL \n");
       return -1;        
    }

    /* Configure the PLL */
    if(adi_pwr_SetPll(26, /* nDivFactor*/
                      13, /* nMulFactor*/
                      1, /* bDiv2 */
                      0  /* bMul2 */) != ADI_PWR_SUCCESS)
    {
       DEBUG_MESSAGE("Failed to configure the SPLL \n");
       return -1;                
    }

    /* Set the source for SPLL as HFXTAL */
    if(adi_pwr_SetPLLClockMux(ADI_CLOCK_MUX_SPLL_HFOSC) != ADI_PWR_SUCCESS)
    {
       DEBUG_MESSAGE("Failed to set the HFOSC as SPLL clock source\n");
       return -1;        
    }
    
    
    /* Enable the SPLL (This API also waits until the SPLL stabilizes) */
    if(adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_SPLL, true) != ADI_PWR_SUCCESS)
    {
       DEBUG_MESSAGE("Failed to enable the SPLL \n");
       return -1;        
    }
     
    /* Change Root Clock source to SPLL */
    if(adi_pwr_SetRootClockMux(ADI_CLOCK_MUX_ROOT_SPLL) != ADI_PWR_SUCCESS)
    {
       DEBUG_MESSAGE("Failed to set the SPLL as root clock source\n");
       return -1;        
    }
  
    return 0;
}

#ifdef TEST_LFXTAL_BYPASS
static int LFXTALBypass(void)
{  
    /* We expect this to fail, we have not connected an external clock, so it isn't stable */
    if(adi_pwr_EnableLFXTALBypass(true) != ADI_PWR_FAILURE)
    {
       DEBUG_MESSAGE("Failed to Enable LFXTAL Bypass mode \n");
       return -1;                
    }
          
    if(adi_pwr_EnableLFXTALBypass(false) != ADI_PWR_SUCCESS)
    {
       DEBUG_MESSAGE("Failed to Disable LFXTAL Bypass mode \n");
       return -1;                
    }
    
    return 0;
}
#endif /* TEST_LFXTAL_BYPASS */

int main (void)
{    
    uint32_t i;
	
    adi_initpinmux();
    
    do
    {       
        /* Initialize power service */
        if(ADI_PWR_SUCCESS != adi_pwr_Init())
        {
            DEBUG_MESSAGE("Failed to initialize Power service");
            break;
        }
        
        if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_HCLK,1))
        {
            DEBUG_MESSAGE("Failed to set HCLK divider ");
            break;
        }

        if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_PCLK,1))
        {
            DEBUG_MESSAGE("Failed to set PCLK divider");
            break;
        }

        if(ADI_PWR_SUCCESS != adi_pwr_RegisterCallback(
               PowerDriverCallback,
               NULL
	       ))
        {
            DEBUG_MESSAGE("Failed to register callback \n");
            break;          
        }
                
        /* Set the HClk Divider */
        if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_HCLK, 1))
        {
            DEBUG_MESSAGE("Failed to set the HCLK  divide factor \n");
            break;
        }
        
        /* Set the PCLK Divider */
        if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_PCLK, 1))
        {
            DEBUG_MESSAGE("Failed to set the PCLK  divide factor \n");
            break;
        }

        /* Enable peripheral clock */
        if(ADI_PWR_SUCCESS != adi_pwr_EnableClock(ADI_CLOCK_GATE_PCLK,true))
        {
            DEBUG_MESSAGE("Failed to enable the PCLK ");
            break;
        }

        if(ADI_PWR_SUCCESS != adi_pwr_EnableClockInterrupt(ADI_PWR_ROOT_CLOCK_MON_IEN, true))
        {
            DEBUG_MESSAGE("Failed to enable the Root clock monitoring interrupt ");
            break;          
        }
        
        if(ADI_PWR_SUCCESS != adi_pwr_EnableClockInterrupt(ADI_PWR_LFXTAL_CLOCK_MON_IEN, true))
        {
            DEBUG_MESSAGE("Failed to enable the LFXTAL monitoring interrupt ");
            break;          
        }
        
        if(ADI_PWR_SUCCESS != adi_pwr_EnableClockInterrupt(ADI_PWR_LFXTAL_STATUS_IEN, true))
        {
            DEBUG_MESSAGE("Failed to enable the LFXTAL status interrupt ");
            break;          
        }
        
        if(ADI_PWR_SUCCESS != adi_pwr_EnableClockInterrupt(ADI_PWR_PLL_STATUS_IEN, true))
        {
            DEBUG_MESSAGE("Failed to enable the PLL status interrupt ");
            break;          
        }
        
        if(ADI_PWR_SUCCESS != adi_pwr_EnableClock(ADI_CLOCK_GATE_PCLK,true))
        {
            DEBUG_MESSAGE("Failed to enable the PCLK ");
            break;
        }

        if(ADI_PWR_SUCCESS != adi_pwr_SetGPIOClockOutput(ADI_CLOCK_OUTPUT_PCLK))
        {
            DEBUG_MESSAGE("Failed to set the clock output");
            break;          
        }
  
        if(EnableLFXTAL() != 0)
        {
          DEBUG_MESSAGE("LF XTAL test failed \n");
        }
       
        /* Test changing Root clock to HXTAL */
        if(ChangeToHFXTAL() != 0)
        {
            DEBUG_MESSAGE("HF XTAL test failed \n");
            break;            
        }

#ifdef TEST_EXTERNAL_CLK_IN
        /* Test changing Root clock to GPIO */
        if(ChangeToGPIO() != 0)
        {
            DEBUG_MESSAGE("GPIO Clock source test failed \n");
            break;            
        }
#endif /* TEST_EXTERNAL_CLK_IN */
   
#ifdef TEST_LFXTAL_BYPASS  
        if(LFXTALBypass() != 0)
        {
            DEBUG_MESSAGE("LFXTAL Bypass test failed \n");
            break;        
        }
#endif /* TEST_LFXTAL_BYPASS */       
        
        /* Test changing Root clock to SPLL */
        if(ChangeToSPLL() != 0)
        {
            DEBUG_MESSAGE("SPLL test failed \n");
            break;            
        }

        /* Set the Root clock source to HFOSC (PLL cannot be used when configuring it) */
        if(adi_pwr_SetRootClockMux(ADI_CLOCK_MUX_ROOT_HFOSC) != ADI_PWR_SUCCESS)
        {
            DEBUG_MESSAGE("Failed to set the HFOSC as root clock source\n");
            break;        
        }

        for(i = 0; i < gNumCallbacks; i++)
        {
            DEBUG_MESSAGE("Power Driver Callback Called: Event = 0x%08x : EventData = 0x%08x \n", (unsigned int)CallbackInfo[i].nEvent, (unsigned int)CallbackInfo[i].pEventData);
        }
				
        common_Pass();
        
        return 0;
        
    }while(0);

    
    common_Fail("Test Fail\n");
    
    return -1;
}

