CATEGORY=BSP,TOOLCHAINS

PROCESSOR = ADuCM4050

TOOLCHAIN = KEIL

#delete any previous build files
#preserve the config files that are modified
<CMD, move /Y "$testdir\RTE\Device\ADuCM4050\adi_wdt_config.h" "$testdir">
<CMD, del /Q/S "$testdir\Listings">
<CMD, del /Q/S "$testdir\Objects">
<CMD, del /Q/S "$testdir\RTE">
<CMD, del /Q/S "$testdir\output">
<CMD, move /Y "$testdir\adi_wdt_config.h" "$testdir\RTE\Device\ADuCM4050" > 

###### Debug Config ######
<BUILDPROJECT, $testdir\low_pwr_mode.uvprojx, Debug >
<BUILDFIND, 0 Error(s) >
<BUILDFIND, 0 Warning(s) >
<ASSERT_FILEEXISTS, $testdir\Objects\low_pwr_mode.axf>
<LOAD, $testdir\low_pwr_mode.uvprojx, Debug>
<RUN, TTH:TIMEOUT(60)>
<BUILDFIND, TTH:NOCASE:MATCHANY, All done>

###### Release Config ######
<BUILDPROJECT, $testdir\low_pwr_mode.uvprojx, Release >
<BUILDFIND, 0 Error(s) >
<BUILDFIND, 0 Warning(s) >
<ASSERT_FILEEXISTS, $testdir\Objects\low_pwr_mode.axf>
<LOAD, $testdir\low_pwr_mode.uvprojx, Release>
<RUN, TTH:TIMEOUT(60)>
<BUILDFIND, TTH:NOCASE:MATCHANY, All done>
