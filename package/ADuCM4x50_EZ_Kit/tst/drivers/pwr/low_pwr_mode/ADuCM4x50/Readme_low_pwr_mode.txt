         Analog Devices, Inc. ADuCM4x50 Application Example

Project Name: low_power_mode

Description:  Verifies that the transition from active state to hibernate
              and back works.


Overview:
=========    
    This test verifies that we cycle through the hibernate/wakeup successfully.
    It also verifies that the watchgod timer continues to work after several
    hibernate wakeup cycles.
    
    The test uses RTC interrupt to wakeup the processor from hibernate.
    
    LED3 is used to indicate wakeup/hibernate cycles. After 4 hibernate/wakeup
    cycles the LED stops toggling.
    
    LED4 is used to indicate WDOG timer timeouts. If the LED4 continues to blink
    after LED3 stops toggling, the test is successful.
    

User Configuration Macros:
==========================
    None.
        
Hardware Setup:
===============
    ADuCM4x50-EZ-Kit should be configured with default settings jumper 
    settings. Please refer ADuCM4050 EZ-Kit Manual for default 
    jumper settings.

External connections:
=====================
    None.
    
How to build and run:
=====================    
    Prepare hardware as explained in the Hardware Setup section.
    Build the project, load the executable to ADuCM4x50, and run it.

Expected Result:
=================    
    LED3 indicates wakeup/hibernate cycle. LED4 is used to indicate WDOG timer timeouts. 
    If the LED4 continues to blink after LED3 stops toggling, the test is successful.

References:
===========
    ADuCM4x50 Hardware Reference Manual
