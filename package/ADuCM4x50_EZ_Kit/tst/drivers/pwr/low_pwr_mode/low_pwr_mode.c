/*********************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/

/*
*  The test is to verify that we cycle through the hibernate/wakeup successfully.
*  It also verifies that the watchgod timer continues to work after several
*  hibernate wakeup cycles.
*/

#include <common.h>
#include <drivers/gpio/adi_gpio.h>
#include <drivers/pwr/adi_pwr.h>
#include <drivers/wdt/adi_wdt.h>
#include <drivers/rtc/adi_rtc.h>


#ifdef __ICCARM__
/* Suppress all the offending errors. It is ok suppress all of them as this is only used in test/example code */

/*
* IAR MISRA C 2004 error suppressions.
*
* Pm144 (rule 14.6): For any iteration there shall be at most on break statement.
* Pm108 (rule 20.11): The library functions abort, exit, getenv from stdlib.h shall not be used.
*/

#pragma diag_suppress=Pm144,Pm108
#endif /* __ICCARM__ */


/* used for exit timeout */
#define MAXCOUNT (10000000u)

/* Port and Pin number for LED which is turned on/off when the wakeup button is pressed */
#define WAKEUP_LED_PORT_NUM    ADI_GPIO_PORT0
#define WAKEUP_LED_PIN_NUM     ADI_GPIO_PIN_13

/* Port and Pin number for LED which is turned on/off when watch dog timer times out */
#define WDOG_LED_PORT_NUM    ADI_GPIO_PORT1
#define WDOG_LED_PIN_NUM     ADI_GPIO_PIN_12

#define RTC_DEVICE_NUM  0

/* Alarm offset from the current RTC count */
#define RTC_ALARM_OFFSET        1

/* Number of watchdog interrupts to wait for */
#define NUM_WDOG_INT            8

/* Number of hibernate cycles to go through */
#define NUM_HIBERNATE_CYCLES    4


static volatile uint64_t count;
static uint8_t gpioMemory[ADI_GPIO_MEMORY_SIZE];
static uint8_t aRtcDevMem0[ADI_RTC_MEMORY_SIZE];

/* Counters */
static volatile uint32_t gExitHibernate = 0;
static volatile uint32_t AlarmCount     = 0;
static volatile uint32_t gNumWdogInt    = 0;

/* Device handle for RTC device-0*/
static ADI_RTC_HANDLE hRTC0  = NULL;

/* Callback Function */
static void WdtCallback(void *pCBParam, uint32_t Event, void *pArg) 
{
    /* toggle LED 4 */
    adi_gpio_Toggle(WDOG_LED_PORT_NUM, WDOG_LED_PIN_NUM);
    
    gNumWdogInt++;
}


/* Update the alarm based on the current RTC count value */
ADI_RTC_RESULT rtc_UpdateAlarm (void) 
{

    ADI_RTC_RESULT eResult;
    uint32_t rtcCount;

    /* Disable RTC so that the count will stop incrementing while setting the alarm */
    eResult = adi_rtc_Enable(hRTC0, false);
    DEBUG_RESULT("Failed to enable the device",eResult,ADI_RTC_SUCCESS);
    
    /* 
      Get the current count and set the alarm with an offset 
    */
    eResult = adi_rtc_GetCount(hRTC0,&rtcCount);
    DEBUG_RESULT("\n Failed to get RTC Count %04d",eResult,ADI_RTC_SUCCESS);
    
    eResult = adi_rtc_SetAlarm(hRTC0, rtcCount + RTC_ALARM_OFFSET);
    DEBUG_RESULT("\n Failed to set RTC Alarm %04d",eResult,ADI_RTC_SUCCESS);

    /* Enable the RTC back */
    eResult = adi_rtc_Enable(hRTC0, true);
    DEBUG_RESULT("Failed to enable the device",eResult,ADI_RTC_SUCCESS);
    
    return eResult;
}


/* RTC-0 Callback handler */
static void rtc0Callback (void *pCBParam, uint32_t nEvent, void *EventArg) 
{

    if( 0 != (ADI_RTC_ALARM_INT & nEvent)) 
    {
        
        /* Update alarm count */
        AlarmCount++;

        /* exit hibernation on return from interrupt */
        adi_pwr_ExitLowPowerMode(&gExitHibernate);
    }
}

static ADI_RTC_RESULT rtc_Init (void) 
{

    ADI_RTC_RESULT eResult;
    
    /* Use both static configuration and dynamic configuration for illustrative purpsoes */
    do
    {
        eResult = adi_rtc_Open(RTC_DEVICE_NUM, aRtcDevMem0, ADI_RTC_MEMORY_SIZE, &hRTC0);
        DEBUG_RESULT("\n Failed to open the device %04d",eResult,ADI_RTC_SUCCESS);
        
        eResult = adi_rtc_RegisterCallback(hRTC0, rtc0Callback, hRTC0);
        DEBUG_RESULT("\n Failed to register callback",eResult,ADI_RTC_SUCCESS);
        
        /* force a reset to the latest build timestamp */
        eResult = adi_rtc_SetCount(hRTC0, RTC_ALARM_OFFSET);
        DEBUG_RESULT("Failed to set count",eResult,ADI_RTC_SUCCESS);
        
        /* enable alarm interrupting */
        eResult = adi_rtc_EnableInterrupts(hRTC0, ADI_RTC_ALARM_INT, true);
        DEBUG_RESULT("adi_RTC_EnableInterrupts failed",eResult,ADI_RTC_SUCCESS);
        
        eResult = adi_rtc_EnableAlarm(hRTC0,true);
        DEBUG_RESULT("Failed to enable alarm",eResult,ADI_RTC_SUCCESS);

        rtc_UpdateAlarm();
       
    } while(0); 
    
    return(eResult);
}

/*
 * main
 */
int main(void)
{
    
    /* test system initialization */
    common_Init();

    do
    {     

        /* Initialize power service */
        if(ADI_PWR_SUCCESS != adi_pwr_Init())
        {
            DEBUG_MESSAGE("Failed to initialize Power service");
            break;
        }        
        
        if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_HCLK,1))
        {
            DEBUG_MESSAGE("Failed to set the HCLK divider");
            break;
		}
  
        if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_PCLK,1))
        {
            DEBUG_MESSAGE("Failed to set the PCLK divider");
            break;
        }
						
        if(ADI_PWR_SUCCESS != adi_pwr_SetLFClockMux(ADI_CLOCK_MUX_LFCLK_LFXTAL))
        {
            DEBUG_MESSAGE("Failed to set the LF Clock Mux \n");
            break;                    
        }

        if(ADI_PWR_SUCCESS != adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_LFXTAL,true) )
        {
            DEBUG_MESSAGE("Failed to set the LF Clock as the clock source \n");
            break;                    
        }

        /* init the GPIO service */
        if(ADI_GPIO_SUCCESS != adi_gpio_Init(gpioMemory, ADI_GPIO_MEMORY_SIZE))
        {
            DEBUG_MESSAGE("adi_gpio_Init failed\n");
            break;
        }


        /* set GPIO output LED 4 */
        if(ADI_GPIO_SUCCESS != adi_gpio_OutputEnable(WDOG_LED_PORT_NUM, WDOG_LED_PIN_NUM, true))
        {
            DEBUG_MESSAGE("adi_gpio_SetDirection failed\n");
            break;
        }
        
        /* set GPIO output LED 3 */
        if(ADI_GPIO_SUCCESS != adi_gpio_OutputEnable(WAKEUP_LED_PORT_NUM, WAKEUP_LED_PIN_NUM, true))
        {
            DEBUG_MESSAGE("adi_gpio_SetDirection failed\n");
            break;
        }
        
        /* initialize driver */
        if(ADI_RTC_SUCCESS != rtc_Init())
        {
          DEBUG_MESSAGE("Failed to initialize RTC device ");
          break;
        }

        if(ADI_WDT_SUCCESS != adi_wdt_Enable(true, WdtCallback))
        {
            DEBUG_MESSAGE("Error enabling the WDT. \n");
            break;
        }      
    
        count = 0u;
        while(count < NUM_HIBERNATE_CYCLES)
        { 
            volatile uint16_t SavedWDTCtl;
            volatile uint16_t SavedWDTLoad;

            /* save WDT control register (which is not retained during hibernation) */
            SavedWDTCtl     =   pADI_WDT0->CTL;
            SavedWDTLoad    =   pADI_WDT0->LOAD;
    
            adi_pwr_EnterLowPowerMode(ADI_PWR_MODE_HIBERNATE, &gExitHibernate, 0);
 
            /* Restore WDT control register (which is not retained during hibernation) */
            pADI_WDT0->LOAD =   SavedWDTLoad;
            pADI_WDT0->CTL  =   SavedWDTCtl;
 
            /* Update RTC alarm */
            rtc_UpdateAlarm();
           
            /* toggle LED 3 */
            if(ADI_GPIO_SUCCESS != adi_gpio_Toggle(WAKEUP_LED_PORT_NUM, WAKEUP_LED_PIN_NUM))
            {
                common_Fail("Error toggling the Waketup LED GPIO pin \n");
                return 1;
            }

            count++;
        }        

        /* Wait for a while to test if watch dog is still functioning */
        while(count < MAXCOUNT)
        {           
            count++;
            
            /* If watchdog interrupts are being generated after several hibernate cycles, 
              the test is successful */
            if(gNumWdogInt >= NUM_WDOG_INT)
            {
                break;
            }
        }

        common_Pass();
    
        return 0;
        
    }while(0);
     
    common_Fail("Low Power mode test failed \n");
    
    return 1;
}
