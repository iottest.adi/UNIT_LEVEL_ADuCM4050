/*!****************************************************************************
 * @file    main.c
 * @brief   GP and RGB timer device driver test
******************************************************************************/


#include <stdint.h>
#include <common.h>
#include <adi_tmr_config.h>
#include <drivers/tmr/adi_tmr.h>
#include <drivers/pwr/adi_pwr.h>
#include <drivers/gpio/adi_gpio.h>

/* Test time in seconds, GP and RGB tests run in series so actual test is twice this time */
#define TEST_TIME_SEC  (10u)

/* Pinmux */
extern int32_t adi_initpinmux(void);

/* Helper macros */
#define CHECK_ERROR(result, expected, message) \
do {\
	if ( (expected) != (result))\
	{\
        common_Fail(message); \
        return 1; \
	}\
} while (0);

#define CHECK_ERROR_ISR(result, expected, message) \
do {\
	if ( (expected) != (result))\
	{\
        common_Fail(message); \
	}\
} while (0);

/* GPIO memory used for the push button */
static uint8_t GpioCallbackMemory[ADI_GPIO_MEMORY_SIZE];

/* Count the number of events that occur */
volatile static uint32_t gGP0TimerTimeouts;
volatile static uint32_t gGP1TimerTimeouts;
volatile static uint32_t gGP2TimerTimeouts;
volatile static uint32_t gRGBTimerTimeouts;
volatile static uint32_t gSysTimerTimeouts;
volatile static bool     gRldTimerTimeouts;

/* GP0 Callback function */
void GP0CallbackFunction(void *pCBParam, uint32_t Event, void  * pArg) {
  ADI_TMR_RESULT eResult;
  uint16_t       nTimer;
  
  if ((Event & ADI_TMR_EVENT_TIMEOUT) == ADI_TMR_EVENT_TIMEOUT) {
    eResult = adi_tmr_GetCurrentCount(ADI_TMR_DEVICE_GP0, &nTimer);
    CHECK_ERROR_ISR(eResult, ADI_TMR_SUCCESS, "GetCurrentCount function failed.")
    gGP0TimerTimeouts++;
  }
  
  if ((Event & ADI_TMR_EVENT_CAPTURE) == ADI_TMR_EVENT_CAPTURE) {
    eResult = adi_tmr_GetCaptureCount(ADI_TMR_DEVICE_GP0, &nTimer);
    CHECK_ERROR_ISR(eResult, ADI_TMR_SUCCESS, "GetCaptureCount function failed.") 
    common_Perf("GP0 Event Captured!\r\n");
  }
}

/* GP1 Callback function */
void GP1CallbackFunction(void *pCBParam, uint32_t Event, void  * pArg) {
  ADI_TMR_RESULT eResult;
  uint16_t       nTimer;
  
  if ((Event & ADI_TMR_EVENT_TIMEOUT) == ADI_TMR_EVENT_TIMEOUT) {
    eResult = adi_tmr_GetCurrentCount(ADI_TMR_DEVICE_GP1, &nTimer);
    CHECK_ERROR_ISR(eResult, ADI_TMR_SUCCESS, "GetCurrentCount function failed.")
    gGP1TimerTimeouts++;
  }
  
  if ((Event & ADI_TMR_EVENT_CAPTURE) == ADI_TMR_EVENT_CAPTURE) {
    eResult = adi_tmr_GetCaptureCount(ADI_TMR_DEVICE_GP1, &nTimer);
    CHECK_ERROR_ISR(eResult, ADI_TMR_SUCCESS, "GetCaptureCount function failed.")
    common_Perf("GP1 Event Captured!\r\n");
  }
}

/* GP2 Callback function */
void GP2CallbackFunction(void *pCBParam, uint32_t Event, void  * pArg) {
  ADI_TMR_RESULT eResult;
  uint16_t       nTimer;
  
  if ((Event & ADI_TMR_EVENT_TIMEOUT) == ADI_TMR_EVENT_TIMEOUT) {
    eResult = adi_tmr_GetCurrentCount(ADI_TMR_DEVICE_GP2, &nTimer);
    CHECK_ERROR_ISR(eResult, ADI_TMR_SUCCESS, "GetCurrentCount function failed.")
    gGP2TimerTimeouts++;
  }
  
  if ((Event & ADI_TMR_EVENT_CAPTURE) == ADI_TMR_EVENT_CAPTURE) {
    eResult = adi_tmr_GetCaptureCount(ADI_TMR_DEVICE_GP2, &nTimer);
    CHECK_ERROR_ISR(eResult, ADI_TMR_SUCCESS, "GetCaptureCount function failed.")  
    common_Perf("GP2 Event Captured!\r\n");
  }
}

/* RGB Callback function */
void RGBCallbackFunction(void *pCBParam, uint32_t Event, void  * pArg) {
  ADI_TMR_RESULT eResult;
  uint16_t       nTimer;
  
  if ((Event & ADI_TMR_EVENT_TIMEOUT) == ADI_TMR_EVENT_TIMEOUT) {
    eResult = adi_tmr_GetCurrentCount(ADI_TMR_DEVICE_RGB, &nTimer);
    CHECK_ERROR_ISR(eResult, ADI_TMR_SUCCESS, "GetCurrentCount function failed.")
    gRGBTimerTimeouts++;
  }
  
  if ((Event & ADI_TMR_EVENT_CAPTURE) == ADI_TMR_EVENT_CAPTURE) {
    eResult = adi_tmr_GetCaptureCount(ADI_TMR_DEVICE_RGB, &nTimer);
    CHECK_ERROR_ISR(eResult, ADI_TMR_SUCCESS, "GetCaptureCount function failed.")
    common_Perf("RGB Event Captured!\r\n"); 
  }
}

/* Callback function used to test the reloading feature */
void ErrorHandler(void *pCBParam, uint32_t Event, void  * pArg){
    /* If the reloading part of the test is working properly, we shouldn't get here */
    gRldTimerTimeouts = true;
}

/* SysTick timer used to profile the timers */
void SysTick_Handler(void)
{
    gSysTimerTimeouts++;
}

/******************************************************************************************** 
                                            MAIN
*********************************************************************************************/
 int main(void) {
    ADI_TMR_EVENT_CONFIG eventConfig;    
    ADI_TMR_PWM_CONFIG   pwmConfig;      
    ADI_TMR_CONFIG       tmrConfig;
    ADI_GPIO_RESULT      eResultGPIO;
    ADI_TMR_RESULT       eResult;
    ADI_PWR_RESULT       eResultPwr;
    uint32_t             nPinmuxError;
    uint32_t             nSysTickError;
    uint32_t             nSystemClock;
    uint32_t             nError;
    uint16_t             nTimerValue;
  
    common_Init();

    /* Initialize clocks */
    eResultPwr = adi_pwr_Init();
    CHECK_ERROR(eResultPwr, ADI_PWR_SUCCESS, "adi_pwr_Init failed.")

    /* Get core clock frequency in order to configure SysTick properly */
    eResultPwr = adi_pwr_GetClockFrequency(ADI_CLOCK_HCLK, &nSystemClock);
    CHECK_ERROR(eResultPwr, ADI_PWR_SUCCESS, "adi_pwr_GetClockFrequency failed.")
      
    /* External clocks require initialization */
#if 0u      
    eResultPwr = adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_LFXTAL, true);
    CHECK_ERROR(eResultPwr, ADI_PWR_SUCCESS, "adi_pwr_EnableClockSource failed.")
#endif      
      
    /* GPIO P1.14 is mapped to PB1 on the EZ-Kit, this will be used to test event capture */
    eResultGPIO = adi_gpio_Init                     (GpioCallbackMemory, ADI_GPIO_MEMORY_SIZE);
    CHECK_ERROR(eResultGPIO, ADI_GPIO_SUCCESS, "adi_gpio_Init failed.")
    eResultGPIO = adi_gpio_InputEnable              (ADI_GPIO_PORT1, ADI_GPIO_PIN_14, true);
    CHECK_ERROR(eResultGPIO, ADI_GPIO_SUCCESS, "adi_gpio_InputEnable failed.")
    eResultGPIO = adi_gpio_SetGroupInterruptPolarity(ADI_GPIO_PORT1, ADI_GPIO_PIN_14);
    CHECK_ERROR(eResultGPIO, ADI_GPIO_SUCCESS, "adi_gpio_SetGroupInterruptPolarity failed.")
    eResultGPIO = adi_gpio_SetGroupInterruptPins    (ADI_GPIO_PORT1, ADI_GPIO_INTA_IRQ, ADI_GPIO_PIN_14);
    CHECK_ERROR(eResultGPIO, ADI_GPIO_SUCCESS, "adi_gpio_SetGroupInterruptPins failed.")          
    eResultGPIO = adi_gpio_SetGroupInterruptPins    (ADI_GPIO_PORT1, ADI_GPIO_INTB_IRQ, ADI_GPIO_PIN_14);
    CHECK_ERROR(eResultGPIO, ADI_GPIO_SUCCESS, "adi_gpio_SetGroupInterruptPins failed.")      
      
    /* Pinmuxing */
    nPinmuxError = adi_initpinmux();
    CHECK_ERROR(nPinmuxError, 0u, "Pinmux failed.");      
    
//////////////////////// TEST GP TIMERS /////////////////////////
          
    /* Initialize the timers */  
    eResult = adi_tmr_Init(ADI_TMR_DEVICE_GP0, GP0CallbackFunction, NULL, true);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "Init function failed.")      
    eResult = adi_tmr_Init(ADI_TMR_DEVICE_GP1, GP1CallbackFunction, NULL, true);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "Init function failed.")      
    eResult = adi_tmr_Init(ADI_TMR_DEVICE_GP2, GP2CallbackFunction, NULL, true);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "Init function failed.")     
    
    /****************** Test GP0 with HFOSC ********************/

    /* PERIOD = 0.5 sec */
    tmrConfig.bCountingUp      = false;
    tmrConfig.bPeriodic        = true;
    tmrConfig.ePrescaler       = ADI_TMR_PRESCALER_256;
    tmrConfig.eClockSource     = ADI_TMR_CLOCK_HFOSC;
    tmrConfig.nLoad            = 50782u;
    tmrConfig.nAsyncLoad       = 50782u;    
    tmrConfig.bReloading       = false;
    tmrConfig.bSyncBypass      = false;
    eResult = adi_tmr_ConfigTimer(ADI_TMR_DEVICE_GP0, tmrConfig);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "ConfigTimer function failed.")

    /* EVENT = None */
    eventConfig.bEnable        = false;
    eventConfig.bPrescaleReset = false;
    eventConfig.nEventID       = 0u;
    eResult = adi_tmr_ConfigEvent(ADI_TMR_DEVICE_GP0, eventConfig);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "ConfigEvent function failed.")    
    
    /* DUTY CYCLE = 20% */
    pwmConfig.eOutput          = ADI_TMR_PWM_OUTPUT_0;
    pwmConfig.bMatch           = true;
    pwmConfig.bIdleHigh        = true;
    pwmConfig.nMatchValue      = 10156u;
    eResult = adi_tmr_ConfigPwm(ADI_TMR_DEVICE_GP0, pwmConfig);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "ConfigPwm function failed.")   

    /****************** Test GP1 with LFOSC ********************/

    /* PERIOD = 0.5 sec */
    tmrConfig.bCountingUp      = false;
    tmrConfig.bPeriodic        = true;
    tmrConfig.ePrescaler       = ADI_TMR_PRESCALER_256;
    tmrConfig.eClockSource     = ADI_TMR_CLOCK_LFOSC;
    tmrConfig.nLoad            = 60u;
    tmrConfig.nAsyncLoad       = 60u;    
    tmrConfig.bReloading       = false;
    tmrConfig.bSyncBypass      = false;
    eResult = adi_tmr_ConfigTimer(ADI_TMR_DEVICE_GP1, tmrConfig);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "ConfigTimer function failed.")

    /* EVENT = GPIOB */
    eventConfig.bEnable         = true;
    eventConfig.bPrescaleReset  = false;
    eventConfig.nEventID        = 28u;
    eResult = adi_tmr_ConfigEvent(ADI_TMR_DEVICE_GP1, eventConfig);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "ConfigEvent function failed.")    
    
    /* DUTY CYCLE = 50% */
    pwmConfig.eOutput           = ADI_TMR_PWM_OUTPUT_0;
    pwmConfig.bMatch            = true;
    pwmConfig.bIdleHigh         = true;
    pwmConfig.nMatchValue       = 30u;
    eResult = adi_tmr_ConfigPwm(ADI_TMR_DEVICE_GP1, pwmConfig);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "ConfigPwm function failed.")   

    /****************** Test GP2 with LFXTAL *******************/

    /* PERIOD = 0.5 sec */
    tmrConfig.bCountingUp      = false;
    tmrConfig.bPeriodic        = true;
    tmrConfig.ePrescaler       = ADI_TMR_PRESCALER_256;
    tmrConfig.eClockSource     = ADI_TMR_CLOCK_LFOSC;   /* @TODO: Can't get LFXTAL to work */
    tmrConfig.nLoad            = 60u;
    tmrConfig.nAsyncLoad       = 60u;    
    tmrConfig.bReloading       = false;
    tmrConfig.bSyncBypass      = false;
    eResult = adi_tmr_ConfigTimer(ADI_TMR_DEVICE_GP2, tmrConfig);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "ConfigTimer function failed.")

    /* EVENT = GPIOA */
    eventConfig.bEnable        = true;
    eventConfig.bPrescaleReset = false;
    eventConfig.nEventID       = 27u;
    eResult = adi_tmr_ConfigEvent(ADI_TMR_DEVICE_GP2, eventConfig);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "ConfigEvent function failed.")    
    
    /* DUTY CYCLE = 25% */
    pwmConfig.eOutput          = ADI_TMR_PWM_OUTPUT_0;
    pwmConfig.bMatch           = true;
    pwmConfig.bIdleHigh        = true;
    pwmConfig.nMatchValue      = 15u;
    eResult = adi_tmr_ConfigPwm(ADI_TMR_DEVICE_GP2, pwmConfig);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "ConfigPwm function failed.")       

    /******************** SysTick at 1.0 sec ********************/

    nSysTickError = SysTick_Config(nSystemClock);
    CHECK_ERROR(nSysTickError, 0u, "SysTick failed.");  

    /*********************** Begin test ************************/
    
    /* Turn on the timers */   
    eResult = adi_tmr_Enable(ADI_TMR_DEVICE_GP0, true);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "GP0 enable function failed.")
    eResult = adi_tmr_Enable(ADI_TMR_DEVICE_GP1, true);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "GP1 enable function failed.")
    eResult = adi_tmr_Enable(ADI_TMR_DEVICE_GP2, true);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "GP2 enable function failed.") 
          
    /* WHILE(Wait for test to end) */
    while(gSysTimerTimeouts < TEST_TIME_SEC) {
        ;
    } /* ENDWHILE */
    
    /* Turn off the timers */   
    eResult = adi_tmr_Enable(ADI_TMR_DEVICE_GP0, false);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "GP0 enable function failed.")
    eResult = adi_tmr_Enable(ADI_TMR_DEVICE_GP1, false);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "GP1 enable function failed.")
    eResult = adi_tmr_Enable(ADI_TMR_DEVICE_GP2, false);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "GP2 enable function failed.")
      
    /* Uninitialize the timers */
    eResult = adi_tmr_Init(ADI_TMR_DEVICE_GP0, NULL, NULL, false);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "Init function failed.")      
    eResult = adi_tmr_Init(ADI_TMR_DEVICE_GP1, NULL, NULL, false);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "Init function failed.")      
    eResult = adi_tmr_Init(ADI_TMR_DEVICE_GP2, NULL, NULL, false);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "Init function failed.") 

    /* Verify GP0 */
    nError = (2u*TEST_TIME_SEC > gGP0TimerTimeouts) ? (2u*TEST_TIME_SEC - gGP0TimerTimeouts) : (gGP0TimerTimeouts - 2u*TEST_TIME_SEC);
    if (nError > 2u) {
        common_Fail("GP0 test failed.");
        return 1;
    }

    /* Verify GP1 */
    nError = (2u*TEST_TIME_SEC > gGP1TimerTimeouts) ? (2u*TEST_TIME_SEC - gGP1TimerTimeouts) : (gGP1TimerTimeouts - 2u*TEST_TIME_SEC);
    if (nError > 2u) {
        common_Fail("GP1 test failed.");
        return 1;
    }    

    /* Verify GP2 */
    nError = (2u*TEST_TIME_SEC > gGP2TimerTimeouts) ? (2u*TEST_TIME_SEC - gGP2TimerTimeouts) : (gGP2TimerTimeouts - 2u*TEST_TIME_SEC);
    if (nError > 2u) {
        common_Fail("GP2 test failed.");
        return 1;
    }  

//////////////////////// TEST RGB TIMER /////////////////////////

    /* Divide the PCLK so we can get a slower timeout value - now it is at 812.5 kHz */
    eResultPwr = adi_pwr_SetClockDivider(ADI_CLOCK_PCLK, 32u);
    CHECK_ERROR(eResultPwr, ADI_PWR_SUCCESS, "adi_pwr_SetClockDivider failed.")   

    /* Initialize the timer */  
    eResult = adi_tmr_Init(ADI_TMR_DEVICE_RGB, RGBCallbackFunction, NULL, true);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "Init function failed.")      

    /****************** Test RGB with PCLK *******************/

    /* PERIOD = 322 ms */
    tmrConfig.bCountingUp      = true;
    tmrConfig.bPeriodic        = false;
    tmrConfig.ePrescaler       = ADI_TMR_PRESCALER_1;
    tmrConfig.eClockSource     = ADI_TMR_CLOCK_PCLK;
    tmrConfig.bReloading       = false;
    tmrConfig.bSyncBypass      = false;
    eResult = adi_tmr_ConfigTimer(ADI_TMR_DEVICE_RGB, tmrConfig);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "ConfigTimer function failed.")

    /* EVENT = GPIOA */
    eventConfig.bEnable        = true;
    eventConfig.bPrescaleReset = false;
    eventConfig.nEventID       = 0u;
    eResult = adi_tmr_ConfigEvent(ADI_TMR_DEVICE_RGB, eventConfig);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "ConfigEvent function failed.")    
    
    /* DUTY CYCLE 0 = 50% */
    pwmConfig.eOutput          = ADI_TMR_PWM_OUTPUT_0;
    pwmConfig.bMatch           = true;
    pwmConfig.bIdleHigh        = true;
    pwmConfig.nMatchValue      = 32768u;
    eResult = adi_tmr_ConfigPwm(ADI_TMR_DEVICE_RGB, pwmConfig);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "ConfigPwm function failed.")   

    /* DUTY CYCLE 1 = 25% */
    pwmConfig.eOutput          = ADI_TMR_PWM_OUTPUT_1;
    pwmConfig.bMatch           = true;
    pwmConfig.bIdleHigh        = true;
    pwmConfig.nMatchValue      = 16384u;
    eResult = adi_tmr_ConfigPwm(ADI_TMR_DEVICE_RGB, pwmConfig);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "ConfigPwm function failed.")   

    /* DUTY CYCLE 2 = 10% */
    pwmConfig.eOutput          = ADI_TMR_PWM_OUTPUT_2;
    pwmConfig.bMatch           = true;
    pwmConfig.bIdleHigh        = true;
    pwmConfig.nMatchValue      = 6554u;
    eResult = adi_tmr_ConfigPwm(ADI_TMR_DEVICE_RGB, pwmConfig);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "ConfigPwm function failed.")   

    /*********************** Begin test ************************/
    
    /* Turn on the timer */   
    eResult = adi_tmr_Enable(ADI_TMR_DEVICE_RGB, true);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "RGB enable function failed.")        

    /* WHILE(Wait for test to end) */
    gSysTimerTimeouts = 0u;
    while(gSysTimerTimeouts < TEST_TIME_SEC) {
        ;
    } /* ENDWHILE */

    /* Turn off the timer */   
    eResult = adi_tmr_Enable(ADI_TMR_DEVICE_RGB, false);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "RGB enable function failed.")
      
    /* Uninitialize the timer */
    eResult = adi_tmr_Init(ADI_TMR_DEVICE_RGB, NULL, NULL, false);
    CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "Init function failed.")         
 
    /* Verify RGB */
    nError = (3u*TEST_TIME_SEC > gRGBTimerTimeouts) ? (3u*TEST_TIME_SEC - gRGBTimerTimeouts) : (gRGBTimerTimeouts - 3u*TEST_TIME_SEC);
    if (nError > 3u) {
        common_Fail("RGB test failed.");
        return 1;
    }  

    /* Turn off SysTick */
    SysTick->CTRL = 0u;

//////////////////////// TEST RELOADING /////////////////////////

    /* Test all the devices */
    for (uint8_t i = ADI_TMR_DEVICE_GP0; i < ADI_TMR_DEVICE_NUM; i++) {
      
        /* Initialize timer */
        eResult = adi_tmr_Init((ADI_TMR_DEVICE) i, ErrorHandler, NULL, true);
        CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "Init function failed.")           

        /* Configure timer to count down, periodic mode, and use reloading */
        tmrConfig.bCountingUp    = false;
        tmrConfig.bPeriodic      = true;
        tmrConfig.ePrescaler     = ADI_TMR_PRESCALER_1;        
        tmrConfig.eClockSource   = ADI_TMR_CLOCK_LFOSC;
        tmrConfig.nLoad          = 0xFFFF;
        tmrConfig.nAsyncLoad     = 0xFFFF;             
        tmrConfig.bReloading     = true;   
        tmrConfig.bSyncBypass    = false;     
        eResult = adi_tmr_ConfigTimer((ADI_TMR_DEVICE) i, tmrConfig);
        CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "ConfigTimer function failed.")      
          
        /* Start the timer */
        eResult = adi_tmr_Enable((ADI_TMR_DEVICE) i, true);
        CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "Enable function failed.")
          
        /* Wait until the timer gets really low... */
        do {
            eResult = adi_tmr_GetCurrentCount((ADI_TMR_DEVICE) i, &nTimerValue);
            CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "GetCurrentCount failed.")
        } while (nTimerValue > 0x1000);
        
        /* Reload it! */
        eResult = adi_tmr_Reload((ADI_TMR_DEVICE) i);
        CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "Reload failed.")
        
        /* Grab the count to make sure the reload happened (it will get cleared after disabling) */
        eResult = adi_tmr_GetCurrentCount((ADI_TMR_DEVICE) i, &nTimerValue);
        CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "GetCurrentCount failed.")
                
         /* Stop the timer */
        eResult = adi_tmr_Enable((ADI_TMR_DEVICE) i, false);
        CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "Enable function failed.")       
                                        
        /* Uninitialize timer */
        eResult = adi_tmr_Init((ADI_TMR_DEVICE) i, NULL, NULL, false);
        CHECK_ERROR(eResult, ADI_TMR_SUCCESS, "Init function failed.")

        /* Verify the reload test */
        nError = 0xFFFF - nTimerValue;
        if ((gRldTimerTimeouts == true) || (nError != 0x0)) {
            common_Fail("Reload test failed.");
            return 1;
        }        
    }
		
    eResultPwr = adi_pwr_Init();
    DEBUG_RESULT("adi_pwr_Init failed.", eResultPwr, ADI_PWR_SUCCESS);

    eResultPwr = adi_pwr_SetClockDivider(ADI_CLOCK_HCLK, 1u);
    DEBUG_RESULT("adi_pwr_SetClockDivider (HCLK) failed.", eResultPwr, ADI_PWR_SUCCESS);

    eResultPwr = adi_pwr_SetClockDivider(ADI_CLOCK_PCLK, 1u);
    DEBUG_RESULT("adi_pwr_SetClockDivider (PCLK) failed.", eResultPwr, ADI_PWR_SUCCESS);    
    
    common_Pass();
    return 0;
}
