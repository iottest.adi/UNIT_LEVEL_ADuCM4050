/*
 **
 ** Source file generated on September 9, 2016 at 15:52:20. 
 **
 ** Copyright (C) 2016 Analog Devices Inc., All Rights Reserved.
 **
 ** This file is generated automatically based upon the options selected in 
 ** the Pin Multiplexing configuration editor. Changes to the Pin Multiplexing
 ** configuration should be made by changing the appropriate options rather
 ** than editing this file.
 **
 ** Selected Peripherals
 ** --------------------
 ** RGB_TMR0 (RGB_TMR0_1, RGB_TMR0_2, RGB_TMR0_3)
 ** TIMER0 (TMR0_OUT, TMR1_OUT, TMR2_OUT)
 **
 ** GPIO (unavailable)
 ** ------------------
 ** P0_14, P1_06, P1_07, P1_08, P1_11, P2_01
 */

#include <sys/platform.h>
#include <stdint.h>

#define RGB_TMR0_RGB_TMR0_1_PORTP1_MUX  ((uint16_t) ((uint16_t) 3<<12))
#define RGB_TMR0_RGB_TMR0_2_PORTP1_MUX  ((uint16_t) ((uint16_t) 3<<14))
#define RGB_TMR0_RGB_TMR0_3_PORTP1_MUX  ((uint32_t) ((uint32_t) 3<<16))
#define TIMER0_TMR0_OUT_PORTP0_MUX  ((uint32_t) ((uint32_t) 1<<28))
#define TIMER0_TMR1_OUT_PORTP1_MUX  ((uint32_t) ((uint32_t) 2<<22))
#define TIMER0_TMR2_OUT_PORTP2_MUX  ((uint16_t) ((uint16_t) 2<<2))

int32_t adi_initpinmux(void);

/*
 * Initialize the Port Control MUX Registers
 */
int32_t adi_initpinmux(void) {
    /* PORTx_MUX registers */
    *pREG_GPIO0_CFG = TIMER0_TMR0_OUT_PORTP0_MUX;
    *pREG_GPIO1_CFG = RGB_TMR0_RGB_TMR0_1_PORTP1_MUX | RGB_TMR0_RGB_TMR0_2_PORTP1_MUX
     | RGB_TMR0_RGB_TMR0_3_PORTP1_MUX | TIMER0_TMR1_OUT_PORTP1_MUX;
    *pREG_GPIO2_CFG = TIMER0_TMR2_OUT_PORTP2_MUX;

    return 0;
}

