/*********************************************************************************
Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/

/*!****************************************************************************
 * @file    icc_task_dma_ssp.c
 * @brief   Inter core communication dma test for SSP.
******************************************************************************/


#include <stdint.h>
#include <adi_icc_config.h>
#include <drivers/icc/adi_icc_ssp.h>


#define CHECK_ERROR(result, expected, message) \
do {\
	if ( (expected) != (result))\
	{\
        return 1; \
	}\
} while (0)

uint32_t ResultSum;

void SumTask(void *pCBParam, void *pArg, uint32_t nArgLen)
{    

	uint32_t i;
	uint8_t *pArray = (uint8_t *)pArg;

	ResultSum = 0;
	for(i = 0; i < nArgLen; i++)
	{
		ResultSum += *(pArray + i);
	}

	adi_icc_TaskDone(pCBParam, (void *)&ResultSum, 1);
    
    return;
}
    
    
static uint8_t  ICCMemory[ADI_ICC_MEM_SIZE];
static ADI_ICC_HANDLE ghDevice;

/******************************************************************************************** 
                                            MAIN
*********************************************************************************************/
int main(void) 
{
    ADI_ICC_RESULT eResultIcc;
    void *pResult;
    uint32_t nResultLen;
    
    /* Open the ICC driver */
    eResultIcc = adi_icc_Open(0, ICCMemory, ADI_ICC_MEM_SIZE, &ghDevice);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_Open failed.");
       
    /* Register the tasks */
    eResultIcc = adi_icc_RegisterTask(ghDevice, ADI_ICC_TASK_9, SumTask, (void *)ghDevice);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_RegisterTask failed.");
    
    /* Start SSP */
    eResultIcc = adi_icc_Start(ghDevice);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_EnableSSP failed.");
        
    return 0;
}
