CATEGORY = BSP,ICC, SSP

PROCESSOR = ADuCM4150

#delete any previous build files
<CMD, del /Q/S "$testdir\Debug">
<CMD, del /Q/S "$testdir\Release">


###### Release Config ######

#build the test project
<CMD, %IAR_EW_HOME%\\common\\bin\\iarbuild icc_task_dma.ewp -build Release>

#confirm build succeeded
<BUILDFIND, Total number of errors: 0, Total number of warnings: 0>
<ASSERT_FILEEXISTS, $testdir\Release\Exe\icc_task_dma.out>


###### Debug Config ######

#build the test project
<CMD, %IAR_EW_HOME%\\common\\bin\\iarbuild icc_task_dma.ewp -build Debug>

#confirm build succeeded
<BUILDFIND, Total number of errors: 0, Total number of warnings: 0>
<ASSERT_FILEEXISTS, $testdir\Debug\Exe\icc_task_dma.out>

###### Run Debug ######

#load and run the test 
<CMD, TTH:TIMEOUT(120), "%CSPY_PATH%\cspy2.bat" "$testdir\Debug\Exe\icc_task_dma.out">

#look for test result printed in the terminal.
<BUILDFIND, All done!>

#load and run the test 
<CMD, TTH:TIMEOUT(120), "%CSPY_PATH%\cspy2.bat" "$testdir\Release\Exe\icc_task_dma.out">

#look for test result printed in the terminal.
<BUILDFIND, All done!>

