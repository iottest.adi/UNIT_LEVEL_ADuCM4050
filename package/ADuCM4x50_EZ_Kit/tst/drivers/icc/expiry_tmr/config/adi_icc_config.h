/**
 *****************************************************************************
   @file:    adi_icc_config.h
   @brief:   Configuration options for ICC driver.
             This is specific to the ICC driver and will be included by the source file.
             It is not required for the application to include this header file.
  -----------------------------------------------------------------------------

Copyright (c) 2016 Analog Devices, Inc.

All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
  - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  - Modified versions of the software must be conspicuously marked as such.
  - This software is licensed solely and exclusively for use with processors
    manufactured by or for Analog Devices, Inc.
  - This software may not be combined or merged with other code in any manner
    that would cause the software to become subject to terms and conditions
    which differ from those listed here.
  - Neither the name of Analog Devices, Inc. nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
  - The use of this software may or may not infringe the patent rights of one
    or more patent holders.  This license does not release you from the
    requirement that you obtain separate licenses from these patent holders
    to use this software.

THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT,
TITLE, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
NO EVENT SHALL ANALOG DEVICES, INC. OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF CLAIMS OF INTELLECTUAL
PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*****************************************************************************/

#ifndef ADI_ICC_CONFIG_H
#define ADI_ICC_CONFIG_H

#ifdef __ICCARM__
/* IAR MISRA C 2004 error suppressions.
*
* Pm009 (rule 5.1): identifiers shall not rely on significance of more than 31 characters.
*   The YODA-generated headers rely on more. The IAR compiler supports that.
*/
#pragma diag_suppress=Pm009
#endif /* __ICCARM__ */

/** @addtogroup ICC_Driver_Config Static Configuration
 *  @ingroup ICC_Driver
 *  @{
 */

 /*! SSP can enter into flexi mode when there are no
   tasks to run. When enabled SSP driver puts the
   SSP core into sleep mode 
\n   
   - 1 : Enable low power mode
   - 0 : Disable low power mode 
*/
#define ADI_ICC_CFG_ENABLE_LOW_PWR_MODE         		0

/*!
   The expire timer helps to monitor the health of the
   SSP software. If the timer is not cleared before it times
   out the timer expires and generates an interrupt to Cortex core.
   The timeout duration is based on the following formula:
\n   
   Timeout duration = ADI_ICC_EXPIRE_TMR_TIMEOUT_VALUE/(Core Clock frequency)
   For example:
\n   
   0x80000000/26Mhz = 82.5 seconds
\n   
   The valid values are between 1 to 0xFFFFFFFF.  
*/
#define ADI_ICC_CFG_EXPIRY_TMR_TIMEOUT_VALUE   			0xFFF

/*!
   Enable or disable the Expiry timer.
\n
   - 1 : Enable the timer.
   - 0 : Disable the timer.
   
*/
#define ADI_ICC_CFG_EXPIRY_TMR_ENABLE         			0

/*!
   Enable or disable the Expiry timer interrupt.
\n
   - 1 : Enable the timer interrupt.
   - 0 : Disable the timer interrupt.
*/
#define ADI_ICC_CFG_EXPIRY_TMR_INT_ENABLE    			 1


/*!
   The Expiry timer can be configured to halt during debugging.
   This helps in avoiding the timer expiry during debug.
\n
   - 1 : Halt timer during debug.
   - 0 : Do not halt the timer during debug.
   
*/
#define ADI_ICC_CFG_EXPIRY_TMR_DBG_HLT         			1

/*!
   The Expiry timer can be configured to halt when SSP is not active.
   This helps in avoiding the timer expiry when SSP is in sleep mode.
\n
   - 1 : Halt timer during sleep mode.
   - 0 : Do not halt the timer during sleep mode.
   
*/
#define ADI_ICC_CFG_EXPIRY_TMR_PD_HLT         			1

/*! SSP Task Overwrite error enable.
    Starting a task when already started is treated as error. 
    Each bit in the 32 bit word enable mask corresponds to a task. 
    Application can selectively enable or disable the task that will generate 
    an error interrupt to Cortex.
*/
#define ADI_ICC_CFG_OVERWITE_ERROR_ENABLE       		0xFFFFFFFF

/***** Shared SRAM Configuration *****/

/*! Enable or disable Parity Error to Cortex 
    Enables or disables interrupt to Cortex on event of parity error in shared 
    memory region for which parity is enabled.

  - 1: Enable Parity Error interrupt.
  - 0: Disable Parity Error interrupt.
*/
#define ADI_ICC_CFG_SRAM_PARITY_ERR_INT_CM_ENABLE      	1

/*! Enable or disable Instruction RAM Write Error Interrupt to Cortex 
    Enables or disables interrupt to Cortex on event of write to IRAM shared 
    memory region when #ADI_ICC_CFG_IRAM_WRITE_DETECT_ENABLE is enabled.

  - 1: Enable IRAM Write Error interrupt.
  - 0: Disable IRAM Write Error interrupt.
*/
#define ADI_ICC_CFG_IRAM_WRITE_ERR_INT_CM_ENABLE        1


/*! Enable or disable Out of Bound Access Error Interrupt to Cortex.
    Enables or disables interrupt to Cortex on event of out of bound access to 
    shared memory region.

  - 1: Enable Out of Bound Access interrupt.
  - 0: Disable Out of Bound Access interrupt.
*/
#define ADI_ICC_CFG_SRAM_OUT_OF_BOUND_INT_CM_ENABLE    	1


/*! Enable or disable Parity Error to SSP.
    Enables or disables interrupt to SSP on event of parity error in shared 
    memory region for which parity is enabled.

  - 1: Enable Parity Error interrupt.
  - 0: Disable Parity Error interrupt.
*/
#define ADI_ICC_CFG_SRAM_PARITY_ERR_INT_SSP_ENABLE      0

/*! Enable or disable Instruction RAM Write Error Interrupt to SSP.
    Enables or disables interrupt to SSP on event of write to IRAM shared 
    memory region when #ADI_ICC_CFG_IRAM_WRITE_DETECT_ENABLE is enabled.

  - 1: Enable IRAM Write Error interrupt.
  - 0: Disable IRAM Write Error interrupt.
*/
#define ADI_ICC_CFG_IRAM_WRITE_ERR_INT_SSP_ENABLE       0


/*! Enable or disable Out of Bound Access Error Interrupt to SSP.
    Enables or disables interrupt to SSP on event of out of bound access to 
    shared memory region.

  - 1: Enable Out of Bound Access interrupt.
  - 0: Disable Out of Bound Access interrupt.
*/
#define ADI_ICC_CFG_SRAM_OUT_OF_BOUND_INT_SSP_ENABLE    0

/*! Wait Counter to Switch Arbitration Automatically.
    Specifies the number of clock cycles a master can be starved of access, when 
    there is a contention. Once the count is equal to this value, the arbitration 
    switches to the other master allowing it to access the memory.  

    Valid range is 1 to 255. 
    If the value is 0, then the arbitration switch never happens.
*/
#define ADI_ICC_CFG_ARBITRATION_WAIT_CNT                0

/*! SSP SRAM Mode.
   0 - SRAM Mode 0
   1 - SRAM Mode 1
   2 - SRAM Mode 2
*/
#define ADI_ICC_CFG_SRAM_MODE                           1

/*! Priority level for SSP.
    This specifies the priority level for SSP. If set, SSP has higher priority. 
    Else, the other masters have higher priority.

   0 - Other masters have higher priority to access SRAM
   1 - SSP has higher priority to access SRAM
*/
#define ADI_ICC_CFG_SSP_SRAM_PRIO                       1

/*! Enable or disable IRAM Write error detection.
    When IRAM Write error detection is enabled an interrupt will be generated
    to Cortex if #ADI_ICC_CFG_IRAM_WRITE_ERR_INT_CM_ENABLE is enabled and
    an interrupt will be generated to SSP if 
    #ADI_ICC_CFG_IRAM_WRITE_ERR_INT_SSP_ENABLE is enabled.

   0 - Disable IRAM Writer error detection
   1 - Enable IRAM Writer error detection
*/
#define ADI_ICC_CFG_IRAM_WRITE_DETECT_ENABLE            0


/* ************* Error Checking ****************************/

#if (ADI_ICC_CFG_ENABLE_LOW_PWR_MODE > 1)
#error The macro ADI_ICC_CFG_ENABLE_LOW_PWR_MODE is configured wrongly.
#endif 

#if (ADI_ICC_CFG_EXPIRY_TMR_TIMEOUT_VALUE < 1)
#error The macro ADI_ICC_CFG_EXPIRY_TMR_TIMEOUT_VALUE is configured wrongly.
#endif

#if (ADI_ICC_CFG_EXPIRY_TMR_ENABLE > 1)
#error The macro ADI_ICC_CFG_EXPIRY_TMR_ENABLE is configured wrongly.
#endif

#if (ADI_ICC_CFG_EXPIRY_TMR_INT_ENABLE > 1)
#error The macro ADI_ICC_CFG_EXPIRY_TMR_INT_ENABLE is configured wrongly.
#endif

#if (ADI_ICC_CFG_EXPIRY_TMR_DBG_HLT > 1)
#error The macro ADI_ICC_CFG_EXPIRY_TMR_DBG_HLT is configured wrongly.
#endif

#if (ADI_ICC_CFG_EXPIRY_TMR_PD_HLT > 1)
#error The macro ADI_ICC_CFG_EXPIRY_TMR_PD_HLT is configured wrongly.
#endif

#if (ADI_ICC_CFG_SRAM_PARITY_ERR_INT_CM_ENABLE > 1)
#error The macro ADI_ICC_CFG_SRAM_PARITY_ERR_INT_CM_ENABLE is configured wrongly.
#endif

#if (ADI_ICC_CFG_IRAM_WRITE_ERR_INT_CM_ENABLE > 1)
#error The macro ADI_ICC_CFG_IRAM_WRITE_ERR_INT_CM_ENABLE is configured wrongly.
#endif

#if (ADI_ICC_CFG_SRAM_PARITY_ERR_INT_SSP_ENABLE > 1)
#error The macro ADI_ICC_CFG_SRAM_PARITY_ERR_INT_SSP_ENABLE is configured wrongly.
#endif

#if (ADI_ICC_CFG_SRAM_OUT_OF_BOUND_INT_SSP_ENABLE > 1)
#error The macro ADI_ICC_CFG_SRAM_OUT_OF_BOUND_INT_SSP_ENABLE is configured wrongly.
#endif

#if (ADI_ICC_CFG_ARBITRATION_WAIT_CNT > 255)
#error The macro ADI_ICC_CFG_ARBITRATION_WAIT_CNT is configured wrongly.
#endif

#if (ADI_ICC_CFG_SRAM_MODE > 2)
#error The macro ADI_ICC_CFG_SRAM_MODE is configured wrongly.
#endif

#if (ADI_ICC_CFG_SSP_SRAM_PRIO > 1)
#error The macro ADI_ICC_CFG_SSP_SRAM_PRIO is configured wrongly.
#endif

#if (ADI_ICC_CFG_IRAM_WRITE_DETECT_ENABLE > 1)
#error The macro ADI_ICC_CFG_IRAM_WRITE_DETECT_ENABLE is configured wrongly.
#endif

/*! @} */

#ifdef __ICCARM__
#pragma diag_default=Pm009
#endif /* __ICCARM__ */

#endif /* ADI_ICC_CONFIG_H */
