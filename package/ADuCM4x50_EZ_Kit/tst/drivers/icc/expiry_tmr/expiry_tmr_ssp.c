/*********************************************************************************
Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/

/*!****************************************************************************
 * @file    expiry_tmr_ssp.c
 * @brief   Inter core communication expiry timer test for SSP.
******************************************************************************/


#include <stdint.h>
#include <adi_icc_config.h>
#include <drivers/icc/adi_icc_ssp.h>
#include <stdlib.h>


#define CHECK_ERROR(result, expected, message) \
do {\
	if ( (expected) != (result))\
	{\
        return 1; \
	}\
} while (0);


static uint8_t  ICCMemory[ADI_ICC_MEM_SIZE];
static ADI_ICC_HANDLE ghDevice;


void TestTask0(void *pCBParam, void *pArg, uint32_t nArgLen)
{    
	/* Delibrately hang in the task to make the Expiry timer timeout */
    while(1);
    
    return;
}

void TestTask1(void *pCBParam, void *pArg, uint32_t nArgLen)
{    

	adi_icc_ExpiryTmrRestart(ghDevice);
	adi_icc_TaskDone(ghDevice, NULL, 0);
    return;
}
    
    

/******************************************************************************************** 
                                            MAIN
*********************************************************************************************/
int main(void) 
{
    ADI_ICC_RESULT eResultIcc;
    void *pResult;
    uint32_t nResultLen;
    
    /* Open the ICC driver */
    eResultIcc = adi_icc_Open(0, ICCMemory, ADI_ICC_MEM_SIZE, &ghDevice);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_Open failed.");
       
    /* Register the tasks */
    eResultIcc = adi_icc_RegisterTask(ghDevice, ADI_ICC_TASK_0, TestTask0, (void *)ghDevice);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_RegisterTask failed.");

    eResultIcc = adi_icc_RegisterTask(ghDevice, ADI_ICC_TASK_1, TestTask1, (void *)ghDevice);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_RegisterTask failed.");
    
    eResultIcc = adi_icc_ExpiryTmrEnable(ghDevice, true);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_ExpiryTmrEnable failed.");

    /* Start SSP */
    eResultIcc = adi_icc_Start(ghDevice);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_EnableSSP failed.");
        
    return 0;
}
