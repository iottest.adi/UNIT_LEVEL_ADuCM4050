/*********************************************************************************
Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/

/*!****************************************************************************
 * @file    icc_task_cm.c
 * @brief   Inter core communication task test for Cortex
******************************************************************************/


#include <stdint.h>
#include <common.h>
#include <config/adi_icc_config.h>
#include <drivers/icc/adi_icc_cm.h>
#include <drivers/pwr/adi_pwr.h>
#include <adi_libldr.h>


/* Set it to 1 to enable debugging the SSP code. When it is enabled, Lib loader
   will not be used to download the code to SSP. Code should be downloaded
   through the JTAG attached to P12 port on EZ-Kit.

   To download the code through JTAG follow the steps below:

   * Add a break point at adi_icc_StartSSP API call 
   * Run the code on Cortex, once the execution hit the breakpooint adi_icc_StartSSP
   * Run the xt-ocd.exe from the windows/cygwin terminal.
   * Build and download the SSP code through Xtensa tool.
   * Run the code in Xtensa
   * Continue running the code in IAR.
*/
#define DEBUG_SSP_CODE   0

/* Pinmux */
extern int32_t adi_initpinmux(void);


#define CHECK_ERROR(result, expected, message) \
do {\
	if ( (expected) != (result))\
	{\
        common_Fail(message); \
        return 1; \
	}\
} while (0)


#if (DEBUG_SSP_CODE == 0)

static char SSP_code[] = {
#include "ssp_ldr.h"
}; 

#endif /* DEBUG_SSP_CODE */

static uint8_t  ICCMemory[ADI_ICC_MEM_SIZE];
static ADI_ICC_HANDLE ghDevice;

#pragma location = "shared_sram"
static volatile uint32_t SharedArray[10];

/* Callback function to catch the errors */
void ErrorHandler(void *pCBParam, uint32_t Event, void  * pArg)
{
    switch(Event)
    {
      /*! The task was started when the previously started task is not done.*/
      case ADI_ICC_EVENT_ERROR_TASK_OVR:
        printf("ICC Task overflow occurred \n");
        break;
    
    /*! The Expiry timer has timed out. This can because software running in SSP 
        stuck in a loop or crashed. The Event argument points to NULL in this case.
    */
      case ADI_ICC_EVENT_ERROR_TIMER_TIMEOUT:
        printf("ICC Expiry timer timedout \n");
        break;
    
    /*! An error occurred in the shared SRAM. The Event argument will point to the 
        ADI_ICC_SRAM_ERROR_INFO structure which will have more details regarding the error. */
      case ADI_ICC_EVENT_ERROR_SHARED_SRAM:
        printf("Shared SRAM error occurred \n");
        break;
    }
}

#if 0
void enable_tensilica_debug_mode()
{
  int rdata = 0;
  int* tssysctrl = (int *)0x400140a0;
  int ctrldata = 0;
  unsigned int i =0;
  
  // ROOT_CLK + SYS_RESET + CORE_RST + SSP_SYS_EN + USR_SSP_EN
  ctrldata = 0x10309;
  *tssysctrl = ctrldata;
  
  // Some delay
  for(i = 0;i<100;i++);
  
  // ROOT_CLK + SSP_SYS_EN + USR_SSP_EN (Bring out of reset)
  ctrldata = 0x10009;
  *tssysctrl = ctrldata;
    
  
  //Wait for Tensilica power up
  while(!(rdata & 0x02)) { 
	rdata = *tssysctrl;
  };
}
#endif


/******************************************************************************************** 
                                            MAIN
*********************************************************************************************/
int main(void) 
{
    ADI_PWR_RESULT eResultPwr;
    ADI_ICC_RESULT eResultIcc;
    bool bSSPReadyStatus = false;
    void *pResult;
    uint32_t nResultLen;
    uint32_t i;
    uint32_t HwError;
     
    adi_initpinmux();
    common_Init();
    
    /* Initialize clocks */
    eResultPwr = adi_pwr_Init();
    CHECK_ERROR(eResultPwr, ADI_PWR_SUCCESS, "adi_pwr_Init failed.");
    
    /* Open the ICC driver */
    eResultIcc = adi_icc_Open(0, ICCMemory, ADI_ICC_MEM_SIZE, &ghDevice);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_Open failed.");
    
#if (DEBUG_SSP_CODE == 0)
    
    /* Download the code to SSP */
    adi_libldr_MoveDbg(SSP_code,NULL,1);

#endif  /* DEBUG_SSP_CODE */
        
    /* Register the error handler */
    eResultIcc = adi_icc_RegisterCallback(ghDevice, ErrorHandler, NULL);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_RegisterCallback failed.");

    /* Enable SSP */
    eResultIcc = adi_icc_EnableSSP(ghDevice, true);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_EnableSSP failed.");
    
    /* At the point a break point can be placed in SSP code */
    
    /* Start executing the code in SSP */
    eResultIcc = adi_icc_StartSSP(ghDevice);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_StartSSP failed."); 
    
    /* Wait until SSP is ready to execute the task */
    while(bSSPReadyStatus == false)
    {
      eResultIcc = adi_icc_QueryReadyStatus(ghDevice, &bSSPReadyStatus);
      CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_QueryReadyStatus failed.");
    }
    
    /* 
    **  Start the Sum Task which computes the sum of the elements in the array 
    */
    
    for(i = 0; i < 10; i++)
      SharedArray[i] = i;

    /* Start a task on SSP */
    eResultIcc = adi_icc_StartTask(ghDevice, ADI_ICC_TASK_5, (void *)SharedArray, 10);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_StartTask failed.");

    /* Wait until the task is done */
    eResultIcc = adi_icc_WaitForTaskDone(ghDevice, &pResult, &nResultLen, &HwError);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_WaitForTaskDone failed.");
    
    if(*(uint32_t *)pResult != 45)
    {
       common_Fail("The Sum result did not match with the expected result");
       return 1;
    }
    printf("Sum Result = %d\n", *(uint32_t *)pResult);

    /*
    **  Start the Product Task which computes the sum of the elements in the array 
    */
    SharedArray[0] = 5;
    SharedArray[1] = 10;
    SharedArray[2] = 20;
      
    /* Start a task on SSP */
    eResultIcc = adi_icc_StartTask(ghDevice, ADI_ICC_TASK_16, (void *)SharedArray, 3);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_StartTask failed.");
    
    /* Wait until the task is done */
    eResultIcc = adi_icc_WaitForTaskDone(ghDevice, &pResult, &nResultLen, &HwError);
    CHECK_ERROR(eResultIcc, ADI_ICC_SUCCESS, "adi_icc_WaitForTaskDone failed.");

    if(*(uint32_t *)pResult != 1000)
    {
       common_Fail("The Product result did not match with the expected result");
       return 1;
    }

    printf("Product Result = %d\n", *(uint32_t *)pResult);
    
    common_Pass();
    
    return 0;
}