

         Analog Devices, Inc. ADuCM4x50 Application Test

Project Name: RNG Test

Description: This test demonstrates how to configure the random number
             generator to generate random numbers in different modes.



Overview:
=========
The test configures the random number generator, collects random numbers from
the device and prints them to the terminal.
It does this with the default configuration and with a runtime configuration
both in callback and non-callback modes.


User Configuration Macros:
==========================
Set NUM_RANDOM_NUMS to indicate how many random numbers should be calculated.
Set RNG_DEV_LEN_PRESCALER or RNG_DEV_LEN_RELOAD to configure the data length 
register.

Hardware Setup:
===============
None required.

External connections:
=====================
None.

How to build and run:
=====================
1.	Build the project, load the executable to the EZ-Kit, and run it
2.	Look for the debug information displayed on terminal I/O window

Expected Result:
=================
A set of random numbers are printed in the terminal and to finish the test
the test prints "All done".
References:
===========
    ADuCM4x50 Hardware Reference Manual.
