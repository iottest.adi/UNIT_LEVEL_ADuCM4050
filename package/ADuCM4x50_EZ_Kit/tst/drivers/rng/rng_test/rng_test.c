/*********************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*!
 * @file      rng_test.c
 * @brief     Test to demonstrate RNG driver to generate random numbers
 *
 * @details
 *            This is the primary source file for the RNG test
 *
 */

/*=============  I N C L U D E S   =============*/

/* RNG test include */
#include "rng_test.h"
/* Managed drivers and/or services include */
#include <drivers/rng/adi_rng.h>
#include <drivers/pwr/adi_pwr.h>
#include <common.h>
/*=============  D A T A  =============*/

/* RNG Device Handle */
static ADI_RNG_HANDLE     hDevice;
/* Memory to handle CRC Device */
static uint8_t            RngDevMem[ADI_RNG_MEMORY_SIZE];
/* Data buffers for Random numbers */
static uint32_t           RNBuff[NUM_RANDOM_NUMS] = {0};

static volatile uint32_t  nNumRNGen = 0u;

static volatile uint32_t failure_detected = 0u;
static volatile bool stuck_status_detected = false;

/*=============  L O C A L    F U N C T I O N S  =============*/

void  run_all_configurations(void);

/*=============  C O D E  =============*/

/* IF (Callback mode enabled) */

/*
 *  Callback from RNG Driver
 *
 * Parameters
 *  - [in]  pCBParam    Callback parameter supplied by application
 *  - [in]  Event       Callback event
 *  - [in]  pArg        Callback argument
 *
 * Returns  None
 *
 */
static void rngCallback(void *pCBParam, uint32_t Event, void *pArg)
{
    ADI_RNG_RESULT eResult;
    if (Event == ADI_RNG_EVENT_READY)
    {
      uint32_t nRandomNum;
      eResult = adi_rng_GetRngData(hDevice, &nRandomNum);
      if (ADI_RNG_SUCCESS != eResult) {
        /* A failure has been detected. Since we cannot print this from the ISR
         * we set a variable to indicate the problem.
         */
        failure_detected++ ;
        return;
      }
      /* Make sure that we do not overflow the array allocated */
      if (nNumRNGen < NUM_RANDOM_NUMS) {
        RNBuff[nNumRNGen++] = nRandomNum;
      }
    }
    else if (Event == ADI_RNG_EVENT_STUCK)
    {
        stuck_status_detected = true;
    }
    else
    {
        /* Unknown event */
        failure_detected =true;
    }
}


/*********************************************************************
*
*   Function:   main
*
*********************************************************************/
int main (void)
{
    /* RNG Return code */
    ADI_RNG_RESULT	eResult = ADI_RNG_SUCCESS;
    uint16_t lenPrescaler, lenReload;

    /* test system initialization */
    common_Init();

    if(adi_pwr_Init()!= ADI_PWR_SUCCESS)
    {
        DEBUG_MESSAGE("\n Failed to intialize the power service \n");
    }
    if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_HCLK,1))
    {
        DEBUG_MESSAGE("Failed to intialize the power service\n");
    }
    if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_PCLK,1))
    {
        DEBUG_MESSAGE("Failed to intialize the power service\n");
    }

    eResult = adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&hDevice);
    DEBUG_RESULT("Failed to open RNG device",eResult,ADI_RNG_SUCCESS);

    DEBUG_MESSAGE("****Default static config tests****");
    /* run the default configuration with callback and buffering options */
    run_all_configurations();

    /* change the default configuration */
    eResult = adi_rng_SetSampleLen(hDevice,RNG_DEV_LEN_PRESCALER,RNG_DEV_LEN_RELOAD);
    DEBUG_RESULT("Failed to set sample length",eResult,ADI_RNG_SUCCESS);

    DEBUG_MESSAGE("****Runtime sample length tests****");
    /* run the new configuration with callback and buffering options */
    run_all_configurations();

    DEBUG_MESSAGE("****Check runtime sample length ****");

    adi_rng_GetSampleLen (hDevice, &lenPrescaler, &lenReload);
    DEBUG_RESULT("Failed to get the sample length",eResult,ADI_RNG_SUCCESS);
    DEBUG_RESULT("Incorrect sample length",lenReload,RNG_DEV_LEN_RELOAD);
    DEBUG_RESULT("Incorrect sample length",lenPrescaler,RNG_DEV_LEN_PRESCALER);

    eResult = adi_rng_Close(hDevice);
    DEBUG_RESULT("Failed to close the device",eResult,ADI_RNG_SUCCESS);

    if ( failure_detected == true)
    {
      DEBUG_MESSAGE("Error detected. Test failed");
    }
    else {
      DEBUG_MESSAGE("All done!. Random number generator test completed successfully");
    }

    return 0;
}

void run_current_configuration(bool callback_enabled, bool buffering_enabled) {
    ADI_RNG_RESULT eResult;

    /* clear the result buffer */
    nNumRNGen = 0u;
    memset(RNBuff, 0, NUM_RANDOM_NUMS*sizeof(uint32_t));
    DEBUG_MESSAGE("\nRNG test with args callback enabled %d buffering %d", callback_enabled, buffering_enabled);

    if (callback_enabled) {
      eResult = adi_rng_RegisterCallback(hDevice, rngCallback,hDevice);
      DEBUG_RESULT("Failed to register callback",eResult,ADI_RNG_SUCCESS);
    }

    eResult = adi_rng_EnableBuffering(hDevice, buffering_enabled);
    DEBUG_RESULT("Failed to enable buffering",eResult,ADI_RNG_SUCCESS);

    eResult = adi_rng_Enable(hDevice, true);
    DEBUG_RESULT("Failed to enable device",eResult,ADI_RNG_SUCCESS);
    while (nNumRNGen < NUM_RANDOM_NUMS)
    {
        if (false == callback_enabled) {
            bool bRNGRdy;
            eResult = adi_rng_GetRdyStatus(hDevice, &bRNGRdy);
            DEBUG_RESULT("Failed to get ready status",eResult,ADI_RNG_SUCCESS);
            if (bRNGRdy) {
                uint32_t nRandomNum;
                eResult = adi_rng_GetRngData(hDevice, &nRandomNum);
                DEBUG_RESULT("Failed to enable device",eResult,ADI_RNG_SUCCESS);

                RNBuff[nNumRNGen++] = nRandomNum;
            }
         }
    }

    eResult = adi_rng_Enable(hDevice, false);
    DEBUG_RESULT("Failed to disable device",eResult,ADI_RNG_SUCCESS);

    eResult = adi_rng_RegisterCallback(hDevice, NULL ,hDevice);
    DEBUG_RESULT("Failed to unregister callback",eResult,ADI_RNG_SUCCESS);

    for (int x=0; x < NUM_RANDOM_NUMS; x++) {
      DEBUG_MESSAGE("Random number %d : 0x%08X",x,(uint16_t)RNBuff[x]);
      if (false == buffering_enabled) {
        /* in the non-buffered case we should get one byte of a result.
         * Since we still return 32-bits we make sure that the top ones are 0.
         */
        if (RNBuff[x] > 255u) {
          DEBUG_RESULT("Failure.Random number was bigger than expected.", true, false);
        }
      }
      else {
        /* in the buffered case we should get more than one byte of a result.
         * It is very unlikely that the 3 top bytes would be 0 so we check for that
         */
        if (RNBuff[x] < 255u) {
          DEBUG_RESULT("Failure.Random number was smaller than expected.", true, false);
        }
      }
      /* the numbers are random so we cannot be sure that they are OK but it is
       * unlikely that three in a row match so we check that at least
       */
      if (x > 1) {
        if (RNBuff[x] == RNBuff[x -1] && RNBuff[x] == RNBuff[x -2]) {
          DEBUG_RESULT("Failure.Random number was not random.", true, false);
        }
      }
    }

    DEBUG_RESULT("Error detected. Test failed", (uint16_t)failure_detected, false);

}

void run_all_configurations() {
    /* run default config no callback no buffering */
    run_current_configuration(false, false);

    /* run default config callback no buffering */
    run_current_configuration(true, false);

    /* run default config no callback buffering */
    run_current_configuration(false, true);

    /* run default config callback buffering */
    run_current_configuration(true, true);

}

/*****/
