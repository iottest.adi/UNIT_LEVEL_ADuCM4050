# This script is not run as part of the test automatically, it is just used
# to generate the test vectors that are then copied into the sha_test.c file.
# It is included with the test as a reference to how the test vectors were
# generated

from hashlib import sha256

def sha256_readable(message):
    """
    Just a regular SHA-256, with some input and output formatting.
    The function takes a list of values between 0-255 as a input and
    returns a list of the same type.
    """       

    # Format input 
    values = bytearray(message)

    # Use hashlib
    m = sha256()
    m.update(values)
    result = m.digest()

    # Format output
    result = [int(hex(ord(x)),16) for x in result]
    return result

print sha256_readable([i for i in range(60)])
print sha256_readable([i for i in range(64)])
print sha256_readable([i for i in range(68)])
print sha256_readable([i for i in range(72)])
print sha256_readable([i for i in range(121)])
print sha256_readable([i for i in range(128)])
