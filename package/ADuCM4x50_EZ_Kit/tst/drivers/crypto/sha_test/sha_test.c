/*********************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/


#include <common.h>
#include <drivers/pwr/adi_pwr.h>
#include <drivers/crypto/adi_crypto.h>


#define SHA_256_DIGEST_SIZE (32u)
#define SHA_256_INPUT_SIZE  (128u)


static ADI_CRYPTO_HANDLE hDeviceCrypto;                            
static uint8_t           DeviceMemoryCrypto[ADI_CRYPTO_MEMORY_SIZE];


static uint8_t gaActualInput [SHA_256_INPUT_SIZE];
static uint8_t gaActualOutput[SHA_256_DIGEST_SIZE];


static uint8_t gaExpectedOutput60 [SHA_256_DIGEST_SIZE] = 
{13, 221, 226, 142, 64, 131, 142, 246, 249, 133, 62, 136, 127, 89, 125, 106, 219, 95, 64, 235, 53, 213, 118, 60, 82, 225, 230, 77, 139, 163, 191, 255};
static uint8_t gaExpectedOutput64 [SHA_256_DIGEST_SIZE] = 
{253, 234, 185, 172, 243, 113, 3, 98, 189, 38, 88, 205, 201, 162, 158, 143, 156, 117, 127, 207, 152, 17, 96, 58, 140, 68, 124, 209, 217, 21, 17, 8};
static uint8_t gaExpectedOutput68 [SHA_256_DIGEST_SIZE] = 
{125, 237, 151, 156, 1, 83, 235, 185, 239, 40, 161, 90, 49, 77, 11, 39, 180, 28, 79, 142, 237, 112, 11, 84, 151, 75, 72, 235, 62, 202, 249, 28};
static uint8_t gaExpectedOutput72 [SHA_256_DIGEST_SIZE] =
{16, 125, 226, 188, 120, 142, 17, 2, 159, 120, 81, 248, 225, 176, 181, 175, 180, 227, 67, 121, 199, 9, 252, 132, 6, 137, 235, 211, 209, 245, 27, 91};
static uint8_t gaExpectedOutput121[SHA_256_DIGEST_SIZE] = 
{51, 90, 70, 22, 146, 179, 11, 186, 29, 100, 124, 199, 22, 4, 232, 142, 103, 108, 144, 228, 194, 36, 85, 208, 184, 200, 63, 75, 215, 200, 172, 155};
static uint8_t gaExpectedOutput128[SHA_256_DIGEST_SIZE] = 
{71, 31, 185, 67, 170, 35, 197, 17, 246, 247, 47, 141, 22, 82, 217, 200, 128, 207, 163, 146, 173, 128, 80, 49, 32, 84, 119, 3, 229, 106, 43, 229};


bool hash_and_verify(uint8_t * pExpectedResult, uint32_t nInputSize, bool bUseDma)
{
    ADI_CRYPTO_TRANSACTION  sTransaction;
    ADI_CRYPTO_TRANSACTION *pTransaction;
    ADI_CRYPTO_RESULT       eResult;
    bool                    bNoFailure;

    /* Error checking */
    if (nInputSize > SHA_256_INPUT_SIZE)
    {
        return false;
    }

    /* Clear globals */
    memset(gaActualInput, 0u, SHA_256_INPUT_SIZE);
    memset(gaActualOutput, 0u, SHA_256_DIGEST_SIZE);

    /* Generate the input */
    for (uint32_t i = 0u; i < nInputSize; i++)
    {
        /* In the future it might be better to randomly generate the inputs instead of something simple like this */
        gaActualInput[i] = i;
    }

    /* Use the crypto driver */
    eResult = adi_crypto_Open(0u, DeviceMemoryCrypto, sizeof(DeviceMemoryCrypto), &hDeviceCrypto);
    DEBUG_RESULT("Crypto driver failed.", eResult, ADI_CRYPTO_SUCCESS);
      
    eResult = adi_crypto_EnableDmaMode(hDeviceCrypto, bUseDma);
    DEBUG_RESULT("Crypto driver failed.", eResult, ADI_CRYPTO_SUCCESS);
    
    memset(&sTransaction, 0u, sizeof(ADI_CRYPTO_TRANSACTION));

    sTransaction.eCipherMode    = ADI_CRYPTO_MODE_SHA;
    sTransaction.eCodingMode    = ADI_CRYPTO_ENCODE;
    sTransaction.eShaByteSwap   = ADI_CRYPTO_SHA_BIG_ENDIAN;
    sTransaction.pInputData     = (uint32_t *) gaActualInput;
    sTransaction.numInputBytes  = nInputSize;
    sTransaction.numShaBits     = nInputSize*8u;
    sTransaction.pOutputData    = (uint32_t *) gaActualOutput;
    sTransaction.numOutputBytes = SHA_256_DIGEST_SIZE; 

    eResult = adi_crypto_SubmitBuffer(hDeviceCrypto, &sTransaction);
    DEBUG_RESULT("Crypto driver failed.", eResult, ADI_CRYPTO_SUCCESS);
    
    eResult = adi_crypto_Enable(hDeviceCrypto, true);
    DEBUG_RESULT("Crypto driver failed.", eResult, ADI_CRYPTO_SUCCESS);  

    eResult = adi_crypto_GetBuffer(hDeviceCrypto, &pTransaction);
    DEBUG_RESULT("Crypto driver failed.", eResult, ADI_CRYPTO_SUCCESS);

    eResult = adi_crypto_Enable(hDeviceCrypto, false);
    DEBUG_RESULT("Crypto driver failed.", eResult, ADI_CRYPTO_SUCCESS);

    eResult = adi_crypto_Close(hDeviceCrypto);     
    DEBUG_RESULT("Crypto driver failed.", eResult, ADI_CRYPTO_SUCCESS);   

    bNoFailure = true;
    for (uint8_t i = 0u; i < SHA_256_DIGEST_SIZE; i++)
    {
        if (pExpectedResult[i] != gaActualOutput[i])
        {
            bNoFailure = false;
            break;
        }
    }

    return (bNoFailure);

}


int main(void) 
{
    common_Init();

    ADI_PWR_RESULT ePwrResult = adi_pwr_Init();
    DEBUG_RESULT("adi_pwr_Init failed.", ePwrResult, ADI_PWR_SUCCESS);

    ePwrResult = adi_pwr_SetClockDivider(ADI_CLOCK_HCLK, 1u);
    DEBUG_RESULT("adi_pwr_SetClockDivider (HCLK) failed.", ePwrResult, ADI_PWR_SUCCESS);

    ePwrResult = adi_pwr_SetClockDivider(ADI_CLOCK_PCLK, 1u);
    DEBUG_RESULT("adi_pwr_SetClockDivider (PCLK) failed.", ePwrResult, ADI_PWR_SUCCESS);        

    bool bResult = false;

    bResult = hash_and_verify(gaExpectedOutput60,  60u, false);
    DEBUG_RESULT("SHA-256 PIO with input size = 60 failed.", bResult, true);
    bResult = hash_and_verify(gaExpectedOutput64,  64u, false);
    DEBUG_RESULT("SHA-256 PIO with input size = 64 failed.", bResult, true);
    bResult = hash_and_verify(gaExpectedOutput68,  68u, false);
    DEBUG_RESULT("SHA-256 PIO with input size = 68 failed.", bResult, true);
    bResult = hash_and_verify(gaExpectedOutput121, 121u, false);
    DEBUG_RESULT("SHA-256 PIO with input size = 121 failed.", bResult, true);
    bResult = hash_and_verify(gaExpectedOutput128, 128u, false);
    DEBUG_RESULT("SHA-256 PIO with input size = 128 failed.", bResult, true);

    bResult = hash_and_verify(gaExpectedOutput60,  60u, true);
    DEBUG_RESULT("SHA-256 DMA with input size = 60 failed.", bResult, true);
    bResult = hash_and_verify(gaExpectedOutput64,  64u, true);
    DEBUG_RESULT("SHA-256 DMA with input size = 64 failed.", bResult, true);
    bResult = hash_and_verify(gaExpectedOutput68,  68u, true);
    DEBUG_RESULT("SHA-256 DMA with input size = 68 failed.", bResult, true);
    bResult = hash_and_verify(gaExpectedOutput72, 72u, true);
    DEBUG_RESULT("SHA-256 DMA with input size = 72 failed.", bResult, true);    
    bResult = hash_and_verify(gaExpectedOutput121, 121u, true);
    DEBUG_RESULT("SHA-256 DMA with input size = 121 failed.", bResult, true);
    bResult = hash_and_verify(gaExpectedOutput128, 128u, true);
    DEBUG_RESULT("SHA-256 DMA with input size = 128 failed.", bResult, true);    

    common_Pass();
    return 0;
}
