CATEGORY=BSP,TOOLCHAINS

PROCESSOR = ADuCM4050

TOOLCHAIN = KEIL

#delete any previous build files 
<CMD, del /Q/S "$testdir\Listings">
<CMD, del /Q/S "$testdir\Objects">
<CMD, del /Q/S "$testdir\RTE">
<CMD, del /Q/S "$testdir\output">
 

###### Debug Config ######
<BUILDPROJECT, $testdir\sha_test.uvprojx, Debug >
<BUILDFIND, 0 Error(s) >
<BUILDFIND, 0 Warning(s) >
<ASSERT_FILEEXISTS, $testdir\Objects\sha_test.axf>
<LOAD, $testdir\sha_test.uvprojx, Debug>
<RUN>
<BUILDFIND,TTH:NOCASE:MATCHANY, All done>


###### Release Config ######
<BUILDPROJECT, $testdir\sha_test.uvprojx, Release >
<BUILDFIND, 0 Error(s) >
<BUILDFIND, 0 Warning(s) >
<ASSERT_FILEEXISTS, $testdir\Objects\sha_test.axf>
<LOAD, $testdir\sha_test.uvprojx, Release>
<RUN>
<BUILDFIND,TTH:NOCASE:MATCHANY, All done>
