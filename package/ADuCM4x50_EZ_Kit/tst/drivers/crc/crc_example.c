/*********************************************************************************

Copyright(c) 2014-2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*!
 * @file      crc_example.c
 * @brief     Example to demonstrate CORE/DMA driven CRC for computing the CRC of given block of data.
 * @details
 *            This is the primary source file for the CRC example that shows
 *            how to use CORE/DMA to compute the CRC.
 *
 */

/*=============  I N C L U D E S   =============*/

#include <drivers/crc/adi_crc.h>
#include <drivers/pwr/adi_pwr.h>
#include <drivers/dma/adi_dma.h>
#include <stddef.h>		/* for 'NULL' */
#include <system_ADuCM4050.h>

#include "adi_crc_config.h"
#include "common.h"

#include "crc_example.h"

/*=============  D A T A  =============*/

static ADI_CRC_HANDLE   hCrcDev;                        /*!< CRC Device Handle */
static uint8_t          CrcDevMem[ADI_CRC_MEMORY_SIZE]; /*!< Memory to handle CRC Device */
#if defined (__ICCARM__)
#pragma location="bank1_retained_ram"
#endif
static uint8_t          DataBuf1[CRC_BUF_NUM_BYTES];    /*!< Data buffers for CRC DMA transfers */

bool volatile bCRCBusy= true;

/*=============  L O C A L    F U N C T I O N S  =============*/

/* Prepares data buffers for CRC operation */
static void PrepareDataBuffers (void);


#ifdef ENABLE_CALLBACK_SUPPORT        /* IF (Callback mode enabled) */

/* callback function to be used by the interrupt handler associated with
 * the software DMA channel driving the CRC in CRC DMA driven operations */
static void CrcCallback(void *AppHandle, uint32_t Event, void *pArg);

#endif /* ENABLE_CALLBACK_SUPPORT */

static uint32_t CrcBlock32(uint8_t *StartAddr, uint32_t NumBytes, uint32_t NumBits, const bool bLsbFirst);
static uint16_t CrcBlock16(uint8_t *StartAddr, uint32_t NumBytes, uint32_t NumBits, const bool bLsbFirst);
static uint8_t CrcBlock8(uint8_t *StartAddr, uint32_t NumBytes, uint32_t NumBits, const bool bLsbFirst);

/*=============  C O D E  =============*/

/* IF (Callback mode enabled) */
#ifdef ENABLE_CALLBACK_SUPPORT

/*
 *  Callback from CRC Driver in DMA driven operations
 *
 * Parameters
 *  - [in]  pCBParam    Callback parameter supplied by application
 *  - [in]  Event       Callback event
 *  - [in]  pArg        Callback argument
 *
 * Returns  None
 *
 */
static void CrcCallback(void *pCBParam, uint32_t Event, void *pArg)
{
    switch(Event)
    {
    case ADI_CRC_EVENT_BUFFER_PROCESSED:        /* request was fully processed */
        break;

    case ADI_CRC_EVENT_ERROR:                   /* CRC encountered a problem */
        DEBUG_MESSAGE("CRC Callback - CRC error reported");
        break;

    default:                                    /* unknown event */
        DEBUG_MESSAGE("CRC Callback - unknown event");
        break;
    }
    bCRCBusy = false;   /* CRC is entering IDLE state */
}

#endif /* ENABLE_CALLBACK_SUPPORT */


void TestCrc32 (uint8_t *StartAddr, uint32_t NumBytes, uint32_t NumBits, const bool bLsbFirst)
{
    ADI_CRC_RESULT eResult;
    uint32_t nComputedCrc, nRefCrc;

    /* Open a CRC device instance */
    eResult = adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev);
    DEBUG_RESULT("Failed to open CRC device",eResult,ADI_CRC_SUCCESS);

    eResult = adi_crc_SetLSBFirst(hCrcDev, bLsbFirst);
    DEBUG_RESULT("Failed to set LSBFIRST bit",eResult,ADI_CRC_SUCCESS);

    /* set the polynomial */
    if (bLsbFirst)
    {
        eResult = adi_crc_SetPolynomialVal (hCrcDev, CRC32_POLYNOMIAL_LE);
    }
    else
    {
        eResult = adi_crc_SetPolynomialVal (hCrcDev, CRC32_POLYNOMIAL_BE);
    }
    DEBUG_RESULT("Failed to configure 32-bit CRC polynomial",eResult,ADI_CRC_SUCCESS);

    /* Set the Seed value */
    eResult = adi_crc_SetCrcSeedVal (hCrcDev, CRC32_SEED_VALUE);
    DEBUG_RESULT("Failed to configure 32-bit seed value for the CRC computation",eResult,ADI_CRC_SUCCESS);

    /* FOR call back mode of operation*/
#ifdef ENABLE_CALLBACK_SUPPORT
    bCRCBusy= true;
    eResult = adi_crc_RegisterCallback(hCrcDev, CrcCallback, hCrcDev);
    DEBUG_RESULT("Failed to set CRC callback",eResult,ADI_CRC_SUCCESS);
#endif /* ENABLE_CALLBACK_SUPPORT */

    eResult = adi_crc_Compute (hCrcDev, StartAddr, NumBytes, NumBits);
    DEBUG_RESULT("Failed to start CRC computation",eResult,ADI_CRC_SUCCESS);
#ifndef ENABLE_CALLBACK_SUPPORT
    {
       bool bCRCProgress=true;
       while(bCRCProgress == true)
       {
          eResult = adi_crc_IsCrcInProgress(hCrcDev, &bCRCProgress);
          /* IF (Failure) */
          if (eResult != ADI_CRC_SUCCESS)
          {
              DEBUG_RESULT("Failed to read the CRC computation",eResult,ADI_CRC_SUCCESS);
          }
       }
    }
#else
    while(bCRCBusy == true);    /* bCRCBusy: volatile updated by CrcCallback */
#endif

    /* Get the final result */
    eResult = adi_crc_GetFinalCrcVal(hCrcDev, &nComputedCrc);
    DEBUG_RESULT("Failed to get reset CRC registers",eResult,ADI_CRC_SUCCESS);

    /* Closes a CRC device instance */
    eResult = adi_crc_Close (hCrcDev);
    DEBUG_RESULT("Failed to close CRC device",eResult,ADI_CRC_SUCCESS);

    /* compute the CRC using mathematical formula */
    nRefCrc = CrcBlock32(StartAddr, NumBytes, NumBits, bLsbFirst);

    if(nRefCrc != nComputedCrc)
    {
        DEBUG_MESSAGE("CRC mismatch NumBytes = %lu, NumBits = %lu, %s, nRefCrc = 0x%08lX nComputedCrc = 0x%08lX", NumBytes, NumBits, bLsbFirst ? "LSBFIRST" : "MSBFIRST", nRefCrc, nComputedCrc);
        DEBUG_RESULT("CRC test failed",ADI_CRC_FAILURE,ADI_CRC_SUCCESS);
    }
}

void TestCrc16 (uint8_t *StartAddr, uint32_t NumBytes, uint32_t NumBits, const bool bLsbFirst)
{
    ADI_CRC_RESULT eResult;
    uint32_t nComputedCrc, nRefCrc;

    /* Open a CRC device instance */
    eResult = adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev);
    DEBUG_RESULT("Failed to open CRC device",eResult,ADI_CRC_SUCCESS);

    eResult = adi_crc_SetLSBFirst(hCrcDev, bLsbFirst);
    DEBUG_RESULT("Failed to set LSBFIRST bit",eResult,ADI_CRC_SUCCESS);

    /* set the polynomial */
    if (bLsbFirst)
    {
        eResult = adi_crc_SetPolynomialVal (hCrcDev, CRC16CCITT_POLYNOMIAL_LE);
    }
    else
    {
        eResult = adi_crc_SetPolynomialVal (hCrcDev, CRC16CCITT_POLYNOMIAL_BE << 16);
    }
    DEBUG_RESULT("Failed to configure 16-bit CRC polynomial",eResult,ADI_CRC_SUCCESS);

    /* Set the Seed value */
    if (bLsbFirst)
    {
        eResult = adi_crc_SetCrcSeedVal (hCrcDev, CRC16_SEED_VALUE);
    }
    else
    {
        eResult = adi_crc_SetCrcSeedVal (hCrcDev, CRC16_SEED_VALUE << 16);
    }
    DEBUG_RESULT("Failed to configure 16-bit seed value for the CRC computation",eResult,ADI_CRC_SUCCESS);

    /* FOR call back mode of operation*/
#ifdef ENABLE_CALLBACK_SUPPORT
    bCRCBusy= true;    
    eResult = adi_crc_RegisterCallback(hCrcDev, CrcCallback, hCrcDev);
    DEBUG_RESULT("Failed to set CRC callback",eResult,ADI_CRC_SUCCESS);
#endif /* ENABLE_CALLBACK_SUPPORT */

    eResult = adi_crc_Compute (hCrcDev, StartAddr, NumBytes, NumBits);
    DEBUG_RESULT("Failed to start CRC computation",eResult,ADI_CRC_SUCCESS);

#ifndef ENABLE_CALLBACK_SUPPORT
    {
       bool bCRCProgress=true;
       while(bCRCProgress == true)
       {
          eResult = adi_crc_IsCrcInProgress(hCrcDev, &bCRCProgress);
          /* IF (Failure) */
          if (eResult != ADI_CRC_SUCCESS)
          {
              DEBUG_RESULT("Failed to read the CRC computation",eResult,ADI_CRC_SUCCESS);
          }
       }
    }
#else
    while(bCRCBusy == true);    /* bCRCBusy: volatile updated by CrcCallback */
#endif

    /* Get the final result */
    eResult = adi_crc_GetFinalCrcVal(hCrcDev, &nComputedCrc);
    DEBUG_RESULT("Failed to get reset CRC registers",eResult,ADI_CRC_SUCCESS);

    /* Closes a CRC device instance */
    eResult = adi_crc_Close (hCrcDev);
    DEBUG_RESULT("Failed to close CRC device",eResult,ADI_CRC_SUCCESS);

    if (!bLsbFirst)
    {
        nComputedCrc >>= 16;
    }

    /* compute the CRC using mathematical formula */
    nRefCrc = CrcBlock16(StartAddr, NumBytes, NumBits, bLsbFirst);

    if(nRefCrc != nComputedCrc)
    {
        DEBUG_MESSAGE("CRC mismatch NumBytes = %lu, NumBits = %lu, %s, nRefCrc = 0x%04lX nComputedCrc = 0x%04lX", NumBytes, NumBits, bLsbFirst ? "LSBFIRST" : "MSBFIRST", nRefCrc, nComputedCrc);
        DEBUG_RESULT("CRC test failed",ADI_CRC_FAILURE,ADI_CRC_SUCCESS);
    }
}

void TestCrc8 (uint8_t *StartAddr, uint32_t NumBytes, uint32_t NumBits, const bool bLsbFirst)
{
    ADI_CRC_RESULT eResult;
    uint32_t nComputedCrc, nRefCrc;

    /* Open a CRC device instance */
    eResult = adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev);
    DEBUG_RESULT("Failed to open CRC device",eResult,ADI_CRC_SUCCESS);

    eResult = adi_crc_SetLSBFirst(hCrcDev, bLsbFirst);
    DEBUG_RESULT("Failed to set LSBFIRST bit",eResult,ADI_CRC_SUCCESS);

    /* set the polynomial */
    if (bLsbFirst)
    {
        eResult = adi_crc_SetPolynomialVal (hCrcDev, CRC8CCITT_POLYNOMIAL_LE);
    }
    else
    {
        eResult = adi_crc_SetPolynomialVal (hCrcDev, CRC8CCITT_POLYNOMIAL_BE << 24);
    }
    DEBUG_RESULT("Failed to configure 8-bit CRC polynomial",eResult,ADI_CRC_SUCCESS);

    /* Set the Seed value */
    if (bLsbFirst)
    {
        eResult = adi_crc_SetCrcSeedVal (hCrcDev, CRC8_SEED_VALUE);
    }
    else
    {
        eResult = adi_crc_SetCrcSeedVal (hCrcDev, CRC8_SEED_VALUE << 24);
    }
    DEBUG_RESULT("Failed to configure 8-bit seed value for the CRC computation",eResult,ADI_CRC_SUCCESS);

    /* FOR call back mode of operation*/
#ifdef ENABLE_CALLBACK_SUPPORT
    bCRCBusy= true;    
    eResult = adi_crc_RegisterCallback(hCrcDev, CrcCallback, hCrcDev);
    DEBUG_RESULT("Failed to set CRC callback",eResult,ADI_CRC_SUCCESS);
#endif /* ENABLE_CALLBACK_SUPPORT */

    eResult = adi_crc_Compute (hCrcDev, StartAddr, NumBytes, NumBits);
    DEBUG_RESULT("Failed to start CRC computation",eResult,ADI_CRC_SUCCESS);

#ifndef ENABLE_CALLBACK_SUPPORT
    {
       bool bCRCProgress=true;
       while(bCRCProgress == true)
       {
          eResult = adi_crc_IsCrcInProgress(hCrcDev, &bCRCProgress);
          /* IF (Failure) */
          if (eResult != ADI_CRC_SUCCESS)
          {
              DEBUG_RESULT("Failed to read the CRC computation",eResult,ADI_CRC_SUCCESS);
          }
       }
    }
#else
    while(bCRCBusy == true);    /* bCRCBusy: volatile updated by CrcCallback */
#endif

    /* Get the final result */
    eResult = adi_crc_GetFinalCrcVal(hCrcDev, &nComputedCrc);
    DEBUG_RESULT("Failed to get reset CRC registers",eResult,ADI_CRC_SUCCESS);

    /* Closes a CRC device instance */
    eResult = adi_crc_Close (hCrcDev);
    DEBUG_RESULT("Failed to close CRC device",eResult,ADI_CRC_SUCCESS);

    if (!bLsbFirst)
    {
        nComputedCrc >>= 24;
    }

    /* compute the CRC using mathematical formula */
    nRefCrc = CrcBlock8(StartAddr, NumBytes, NumBits, bLsbFirst);

    if(nRefCrc != nComputedCrc)
    {
        DEBUG_MESSAGE("CRC mismatch NumBytes = %lu, NumBits = %lu, %s, nRefCrc = 0x%02lX nComputedCrc = 0x%02lX", NumBytes, NumBits, bLsbFirst ? "LSBFIRST" : "MSBFIRST", nRefCrc, nComputedCrc);
        DEBUG_RESULT("CRC test failed",ADI_CRC_FAILURE,ADI_CRC_SUCCESS);
    }
}

/*********************************************************************
*
*   Function:   main
*
*********************************************************************/

int main (void)
{
    /* CRC Return code */
    ADI_PWR_RESULT  eResult;
    const bool bLsbFirst = true;
    const bool bMsbFirst = false;

    common_Init();

    eResult= adi_pwr_Init();
    DEBUG_RESULT("Failed to intialize the power service",eResult,ADI_PWR_SUCCESS);

    if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_HCLK,1))
    {
        DEBUG_MESSAGE("Failed to intialize the power service\n");
        exit(0);
    }
    if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_PCLK,1))
    {
        DEBUG_MESSAGE("Failed to intialize the power service\n");
        exit(0);
    }

    /* Fill data buffer with data to be verified with */
    PrepareDataBuffers();

    /* 32-bit CRC LSBFIRST */
    TestCrc32(DataBuf1, CRC_BUF_NUM_BYTES, 0, bLsbFirst);

    TestCrc32(DataBuf1 + 1, CRC_BUF_NUM_BYTES - 1, 0, bLsbFirst);
    TestCrc32(DataBuf1 + 2, CRC_BUF_NUM_BYTES - 2, 0, bLsbFirst);
    TestCrc32(DataBuf1 + 3, CRC_BUF_NUM_BYTES - 3, 0, bLsbFirst);

    TestCrc32(DataBuf1, CRC_BUF_NUM_BYTES - 1, 0, bLsbFirst);
    TestCrc32(DataBuf1, CRC_BUF_NUM_BYTES - 2, 0, bLsbFirst);
    TestCrc32(DataBuf1, CRC_BUF_NUM_BYTES - 3, 0, bLsbFirst);

    TestCrc32(DataBuf1 + 1, CRC_BUF_NUM_BYTES - 2, 0, bLsbFirst);
    TestCrc32(DataBuf1 + 2, CRC_BUF_NUM_BYTES - 4, 0, bLsbFirst);
    TestCrc32(DataBuf1 + 3, CRC_BUF_NUM_BYTES - 6, 0, bLsbFirst);

    TestCrc32(DataBuf1, CRC_BUF_NUM_BYTES - 1, 1, bLsbFirst);
    TestCrc32(DataBuf1, CRC_BUF_NUM_BYTES - 2, 3, bLsbFirst);
    TestCrc32(DataBuf1, CRC_BUF_NUM_BYTES - 3, 7, bLsbFirst);

    TestCrc32(DataBuf1, 0, 1, bLsbFirst);
    TestCrc32(DataBuf1, 0, 5, bLsbFirst);
    TestCrc32(DataBuf1, 0, 6, bLsbFirst);

    /* 32-bit CRC ! LSBFIRST */
    TestCrc32(DataBuf1, CRC_BUF_NUM_BYTES, 0, bMsbFirst);

    TestCrc32(DataBuf1 + 1, CRC_BUF_NUM_BYTES - 1, 0, bMsbFirst);
    TestCrc32(DataBuf1 + 2, CRC_BUF_NUM_BYTES - 2, 0, bMsbFirst);
    TestCrc32(DataBuf1 + 3, CRC_BUF_NUM_BYTES - 3, 0, bMsbFirst);

    TestCrc32(DataBuf1, CRC_BUF_NUM_BYTES - 1, 0, bMsbFirst);
    TestCrc32(DataBuf1, CRC_BUF_NUM_BYTES - 2, 0, bMsbFirst);
    TestCrc32(DataBuf1, CRC_BUF_NUM_BYTES - 3, 0, bMsbFirst);

    TestCrc32(DataBuf1 + 1, CRC_BUF_NUM_BYTES - 2, 0, bMsbFirst);
    TestCrc32(DataBuf1 + 2, CRC_BUF_NUM_BYTES - 4, 0, bMsbFirst);
    TestCrc32(DataBuf1 + 3, CRC_BUF_NUM_BYTES - 6, 0, bMsbFirst);

    TestCrc32(DataBuf1, CRC_BUF_NUM_BYTES - 1, 1, bMsbFirst);
    TestCrc32(DataBuf1, CRC_BUF_NUM_BYTES - 2, 3, bMsbFirst);
    TestCrc32(DataBuf1, CRC_BUF_NUM_BYTES - 3, 7, bMsbFirst);

    TestCrc32(DataBuf1, 0, 1, bMsbFirst);
    TestCrc32(DataBuf1, 0, 5, bMsbFirst);
    TestCrc32(DataBuf1, 0, 6, bMsbFirst);

    /* 16-bit CRC LSBFIRST */
    TestCrc16(DataBuf1, CRC_BUF_NUM_BYTES, 0, bLsbFirst);

    TestCrc16(DataBuf1 + 1, CRC_BUF_NUM_BYTES - 1, 0, bLsbFirst);
    TestCrc16(DataBuf1 + 2, CRC_BUF_NUM_BYTES - 2, 0, bLsbFirst);
    TestCrc16(DataBuf1 + 3, CRC_BUF_NUM_BYTES - 3, 0, bLsbFirst);

    TestCrc16(DataBuf1, CRC_BUF_NUM_BYTES - 1, 0, bLsbFirst);
    TestCrc16(DataBuf1, CRC_BUF_NUM_BYTES - 2, 0, bLsbFirst);
    TestCrc16(DataBuf1, CRC_BUF_NUM_BYTES - 3, 0, bLsbFirst);

    TestCrc16(DataBuf1 + 1, CRC_BUF_NUM_BYTES - 2, 0, bLsbFirst);
    TestCrc16(DataBuf1 + 2, CRC_BUF_NUM_BYTES - 4, 0, bLsbFirst);
    TestCrc16(DataBuf1 + 3, CRC_BUF_NUM_BYTES - 6, 0, bLsbFirst);

    TestCrc16(DataBuf1, CRC_BUF_NUM_BYTES - 1, 1, bLsbFirst);
    TestCrc16(DataBuf1, CRC_BUF_NUM_BYTES - 2, 3, bLsbFirst);
    TestCrc16(DataBuf1, CRC_BUF_NUM_BYTES - 3, 7, bLsbFirst);

    TestCrc16(DataBuf1, 0, 1, bLsbFirst);
    TestCrc16(DataBuf1, 0, 5, bLsbFirst);
    TestCrc16(DataBuf1, 0, 6, bLsbFirst);

    /* 16-bit CRC ! LSBFIRST */
    TestCrc16(DataBuf1, CRC_BUF_NUM_BYTES, 0, bMsbFirst);

    TestCrc16(DataBuf1 + 1, CRC_BUF_NUM_BYTES - 1, 0, bMsbFirst);
    TestCrc16(DataBuf1 + 2, CRC_BUF_NUM_BYTES - 2, 0, bMsbFirst);
    TestCrc16(DataBuf1 + 3, CRC_BUF_NUM_BYTES - 3, 0, bMsbFirst);

    TestCrc16(DataBuf1, CRC_BUF_NUM_BYTES - 1, 0, bMsbFirst);
    TestCrc16(DataBuf1, CRC_BUF_NUM_BYTES - 2, 0, bMsbFirst);
    TestCrc16(DataBuf1, CRC_BUF_NUM_BYTES - 3, 0, bMsbFirst);

    TestCrc16(DataBuf1 + 1, CRC_BUF_NUM_BYTES - 2, 0, bMsbFirst);
    TestCrc16(DataBuf1 + 2, CRC_BUF_NUM_BYTES - 4, 0, bMsbFirst);
    TestCrc16(DataBuf1 + 3, CRC_BUF_NUM_BYTES - 6, 0, bMsbFirst);

    TestCrc16(DataBuf1, CRC_BUF_NUM_BYTES - 1, 1, bMsbFirst);
    TestCrc16(DataBuf1, CRC_BUF_NUM_BYTES - 2, 3, bMsbFirst);
    TestCrc16(DataBuf1, CRC_BUF_NUM_BYTES - 3, 7, bMsbFirst);

    TestCrc16(DataBuf1, 0, 1, bMsbFirst);
    TestCrc16(DataBuf1, 0, 5, bMsbFirst);
    TestCrc16(DataBuf1, 0, 6, bMsbFirst);

    /* 8-bit CRC LSBFIRST */
    TestCrc8(DataBuf1, CRC_BUF_NUM_BYTES, 0, bLsbFirst);

    TestCrc8(DataBuf1 + 1, CRC_BUF_NUM_BYTES - 1, 0, bLsbFirst);
    TestCrc8(DataBuf1 + 2, CRC_BUF_NUM_BYTES - 2, 0, bLsbFirst);
    TestCrc8(DataBuf1 + 3, CRC_BUF_NUM_BYTES - 3, 0, bLsbFirst);

    TestCrc8(DataBuf1, CRC_BUF_NUM_BYTES - 1, 0, bLsbFirst);
    TestCrc8(DataBuf1, CRC_BUF_NUM_BYTES - 2, 0, bLsbFirst);
    TestCrc8(DataBuf1, CRC_BUF_NUM_BYTES - 3, 0, bLsbFirst);

    TestCrc8(DataBuf1 + 1, CRC_BUF_NUM_BYTES - 2, 0, bLsbFirst);
    TestCrc8(DataBuf1 + 2, CRC_BUF_NUM_BYTES - 4, 0, bLsbFirst);
    TestCrc8(DataBuf1 + 3, CRC_BUF_NUM_BYTES - 6, 0, bLsbFirst);

    TestCrc8(DataBuf1, CRC_BUF_NUM_BYTES - 1, 1, bLsbFirst);
    TestCrc8(DataBuf1, CRC_BUF_NUM_BYTES - 2, 3, bLsbFirst);
    TestCrc8(DataBuf1, CRC_BUF_NUM_BYTES - 3, 7, bLsbFirst);

    TestCrc8(DataBuf1, 0, 1, bLsbFirst);
    TestCrc8(DataBuf1, 0, 5, bLsbFirst);
    TestCrc8(DataBuf1, 0, 6, bLsbFirst);

    /* 8-bit CRC ! LSBFIRST */
    TestCrc8(DataBuf1, CRC_BUF_NUM_BYTES, 0, bMsbFirst);

    TestCrc8(DataBuf1 + 1, CRC_BUF_NUM_BYTES - 1, 0, bMsbFirst);
    TestCrc8(DataBuf1 + 2, CRC_BUF_NUM_BYTES - 2, 0, bMsbFirst);
    TestCrc8(DataBuf1 + 3, CRC_BUF_NUM_BYTES - 3, 0, bMsbFirst);

    TestCrc8(DataBuf1, CRC_BUF_NUM_BYTES - 1, 0, bMsbFirst);
    TestCrc8(DataBuf1, CRC_BUF_NUM_BYTES - 2, 0, bMsbFirst);
    TestCrc8(DataBuf1, CRC_BUF_NUM_BYTES - 3, 0, bMsbFirst);

    TestCrc8(DataBuf1 + 1, CRC_BUF_NUM_BYTES - 2, 0, bMsbFirst);
    TestCrc8(DataBuf1 + 2, CRC_BUF_NUM_BYTES - 4, 0, bMsbFirst);
    TestCrc8(DataBuf1 + 3, CRC_BUF_NUM_BYTES - 6, 0, bMsbFirst);

    TestCrc8(DataBuf1, CRC_BUF_NUM_BYTES - 1, 1, bMsbFirst);
    TestCrc8(DataBuf1, CRC_BUF_NUM_BYTES - 2, 3, bMsbFirst);
    TestCrc8(DataBuf1, CRC_BUF_NUM_BYTES - 3, 7, bMsbFirst);

    TestCrc8(DataBuf1, 0, 1, bMsbFirst);
    TestCrc8(DataBuf1, 0, 5, bMsbFirst);
    TestCrc8(DataBuf1, 0, 6, bMsbFirst);
    TestCrc8(DataBuf1, 1, 2, bMsbFirst);

    common_Pass();
    return 0;
}

/*
 * Prepares data buffers for CRC operation.
 *
 * Parameters
 *  None
 *
 * Returns
 *  None
 *
 */
static void PrepareDataBuffers (void)
{
    uint32_t i;

    /* Set seed value to generate test data */
    srand(CRC_RAND_SEED_VAL);

    /* Fill data buffer with 32-bit data */
    for (i = 0; i < CRC_BUF_NUM_BYTES; i++)
    {
        DataBuf1[i] = rand() & 0xff;
    }
}

/* Software implementation of the hardware CRC computation */
static uint32_t crc32_le(uint32_t crc, uint8_t *buf, uint32_t numbytes, uint32_t numbits, uint32_t poly)
{
    uint32_t i, j, len;

    len = numbytes + (numbits > 0 ? 1 : 0);
    for (i = 0; i < len; i++)
    {
        uint32_t bits;

        if (i == numbytes)
        {
            bits = numbits;
            crc ^= buf[i] & ((1 << bits) - 1);
        }
        else
        {
            bits = 8;
            crc ^= buf[i];
        }

        for (j = 0; j < bits; j++)
        {
            if (crc & 0x1)
            {
                crc >>= 1;
                crc ^= poly;
            }
            else
            {
                crc >>= 1;
            }
        }
    }

    return crc;
}

static uint32_t crc32_be(uint32_t crc, uint8_t *buf, uint32_t numbytes, uint32_t numbits, uint32_t poly)
{
    uint32_t i, j, len;

    len = numbytes + (numbits > 0 ? 1 : 0);
    for (i = 0; i < len; i++)
    {
        uint32_t bits;

        if (i == numbytes)
        {
            bits = numbits;
            crc ^= buf[i] >> (8 - bits) << (32 - bits);
        }
        else
        {
            bits = 8;
            crc ^= buf[i] << 24;
        }

        for (j = 0; j < bits; j++)
        {
            if (crc & 0x80000000)
            {
                crc <<= 1;
                crc ^= poly;
            }
            else
            {
                crc <<= 1;
            }
        }
    }

    return crc;
}

static uint32_t CrcBlock32(uint8_t *StartAddr, uint32_t NumBytes, uint32_t NumBits, const bool bLsbFirst)
{
    uint32_t result;
    uint32_t crc;

    /* Initialize CRC with the same seed as in TestCrc */
    crc = CRC32_SEED_VALUE;

    if (bLsbFirst)
    {
        result = crc32_le(crc, StartAddr, NumBytes, NumBits, CRC32_POLYNOMIAL_LE);
    }
    else
    {
        result = crc32_be(crc, StartAddr, NumBytes, NumBits, CRC32_POLYNOMIAL_BE);
    }
    return result;
}

static uint16_t crc16_le(uint16_t crc, uint8_t *buf, uint32_t numbytes, uint32_t numbits, uint16_t poly)
{
    uint32_t i, j, len;

    len = numbytes + (numbits > 0 ? 1 : 0);
    for (i = 0; i < len; i++)
    {
        uint32_t bits;
        if (i == numbytes)
        {
            bits = numbits;
            crc ^= buf[i] & ((1 << bits) - 1);
        }
        else
        {
            bits = 8;
            crc ^= buf[i];
        }

        for (j = 0; j < bits; j++)
        {
            if (crc & 0x1)
            {
                crc >>= 1;
                crc ^= poly;
            }
            else
            {
                crc >>= 1;
            }
        }
    }

    return crc;
}

static uint16_t crc16_be(uint16_t crc, uint8_t *buf, uint32_t numbytes, uint32_t numbits, uint16_t poly)
{
    uint32_t i, j, len;

    len = numbytes + (numbits > 0 ? 1 : 0);
    for (i = 0; i < len; i++)
    {
        uint32_t bits;
        if (i == numbytes)
        {
            bits = numbits;
            crc ^= buf[i] >> (8 - bits) << (16 - bits);
        }
        else
        {
            bits = 8;
            crc ^= buf[i] << 8;
        }

        for (j = 0; j < bits; j++)
        {
            if (crc & 0x8000)
            {
                crc <<= 1;
                crc ^= poly;
            }
            else
            {
                crc <<= 1;
            }
        }
    }

    return crc;
}

static uint16_t CrcBlock16(uint8_t *StartAddr, uint32_t NumBytes, uint32_t NumBits, const bool bLsbFirst)
{
    uint16_t result;
    uint16_t crc = CRC16_SEED_VALUE;    /* Initialize CRC with the same seed as in TestCrc */
    

    if (bLsbFirst)
    {
        result = crc16_le(crc, StartAddr, NumBytes, NumBits, CRC16CCITT_POLYNOMIAL_LE);
    }
    else
    {
        result = crc16_be(crc, StartAddr, NumBytes, NumBits, CRC16CCITT_POLYNOMIAL_BE);
    }
    return result;
}

static uint8_t crc8_le(uint8_t crc, uint8_t *buf, uint32_t numbytes, uint32_t numbits, uint8_t poly)
{
    uint32_t i, j, len;

    len = numbytes + (numbits > 0 ? 1 : 0);
    for (i = 0; i < len; i++)
    {
        uint32_t bits;
        if (i == numbytes)
        {
            bits = numbits;
            crc ^= buf[i] & ((1 << bits) - 1);
        }
        else
        {
            bits = 8;
            crc ^= buf[i];
        }

        for (j = 0; j < bits; j++)
        {
            if (crc & 0x1)
            {
                crc >>= 1;
                crc ^= poly;
            }
            else
            {
                crc >>= 1;
            }
        }
    }

    return crc;
}

static uint8_t crc8_be(uint8_t crc, uint8_t *buf, uint32_t numbytes, uint32_t numbits, uint8_t poly)
{
    uint32_t i, j, len;

    len = numbytes + (numbits > 0 ? 1 : 0);
    for (i = 0; i < len; i++)
    {
        uint32_t bits;
        if (i == numbytes)
        {
            bits = numbits;
            crc ^= buf[i] >> (8 - bits) << (8 - bits);
        }
        else
        {
            bits = 8;
            crc ^= buf[i];
        }

        for (j = 0; j < bits; j++)
        {
            if (crc & 0x80)
            {
                crc <<= 1;
                crc ^= poly;
            }
            else
            {
                crc <<= 1;
            }
        }
    }

    return crc;
}

static uint8_t CrcBlock8(uint8_t *StartAddr, uint32_t NumBytes, uint32_t NumBits, const bool bLsbFirst)
{
    uint8_t result;
    uint8_t crc = CRC8_SEED_VALUE;      /* Initialize CRC with the same seed as in TestCrc */
    if (bLsbFirst)
    {
        result = crc8_le(crc, StartAddr, NumBytes, NumBits, CRC8CCITT_POLYNOMIAL_LE);
    }
    else
    {
        result = crc8_be(crc, StartAddr, NumBytes, NumBits, CRC8CCITT_POLYNOMIAL_BE);
    }
    return result;
}
/*****/
