/*

Copyright (c) 2011-2016 Analog Devices, Inc.

All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
  - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  - Modified versions of the software must be conspicuously marked as such.
  - This software is licensed solely and exclusively for use with processors
    manufactured by or for Analog Devices, Inc.
  - This software may not be combined or merged with other code in any manner
    that would cause the software to become subject to terms and conditions
    which differ from those listed here.
  - Neither the name of Analog Devices, Inc. nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
  - The use of this software may or may not infringe the patent rights of one
    or more patent holders.  This license does not release you from the
    requirement that you obtain separate licenses from these patent holders
    to use this software.

THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT,
TITLE, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
NO EVENT SHALL ANALOG DEVICES, INC. OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF CLAIMS OF INTELLECTUAL
PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/
#if 1
#include <stddef.h>		/* for 'NULL' */
#include <string.h>		/* for strlen */
#endif
#include <system_ADuCM4050.h>
#include "common.h"

#include <drivers/crc/adi_crc.h>
#include <drivers/pwr/adi_pwr.h>
#include <drivers/gpio/adi_gpio.h>
#include <drivers/dma/adi_dma.h>
#include <adi_cyclecount.h>

#include "crc_cyclecounting.h"


#if defined(ADI_CYCLECOUNT_ENABLED) && (ADI_CYCLECOUNT_ENABLED==1u)
static uint32_t cycleCountId_crc = 0u;                  /* Cycle counting ID for application specified API for which cycle counts are to be obtained */
#endif

#if ADI_CRC_CFG_ENABLE_DMA_SUPPORT==0
#define CC_NAME         "CORE-CRC"
#elifdef ENABLE_CALLBACK_SUPPORT
#define CC_NAME         "DMA-CRC-CB"
#else
#define CC_NAME         "DMA-CRC"
#endif

/*=============  D A T A  =============*/

static ADI_CRC_HANDLE   hCrcDev;                        /*!< CRC Device Handle */
static uint8_t          CrcDevMem[ADI_CRC_MEMORY_SIZE]; /*!< Memory to handle CRC Device */
static uint32_t         crc_data_buf[CRC_BUF_NUM_DATA]= /*!< Data to be sent to the CRC hardware engine */
{
#include "crc_data_buf.dat"
};

#ifdef ENABLE_CALLBACK_SUPPORT
static volatile bool bCRCBusy = true;
static volatile uint32_t cbEvent = 0u;
#else
static bool bCRCProgress = true;
#endif

/*=============  L O C A L    F U N C T I O N S  =============*/

#ifdef ENABLE_CALLBACK_SUPPORT
/* callback function to be used by the interrupt handler associated with
 * the software DMA channel driving the CRC in CRC DMA driven operations */
static void CrcCallback(void *AppHandle, uint32_t Event, void *pArg);
#endif

/*=============  C O D E  =============*/

#ifdef ENABLE_CALLBACK_SUPPORT
/*
 *  Callback from CRC Driver in DMA driven operations
 *
 * Parameters
 *  - [in]  pCBParam    Callback parameter supplied by application
 *  - [in]  Event       Callback event
 *  - [in]  pArg        Callback argument
 *
 * Returns  None
 *
 */
static void CrcCallback(void *pCBParam, uint32_t Event, void *pArg)
{
    cbEvent = Event;    /* record the nature of the event that occurred in the callback function */
    bCRCBusy = false;   /* CRC is entering IDLE state */
}
#endif

int main (void)
{
    uint8_t * StartAddr = (uint8_t*) crc_data_buf;
    const bool bLsbFirst = true;                                        /* used to set CRC LSB/MSB first calculation */
    const uint32_t crc_polynomial = (bLsbFirst ? CRC32_POLYNOMIAL_LE : CRC32_POLYNOMIAL_BE);
    const uint32_t NumBytes = sizeof(crc_data_buf) / sizeof(uint8_t);   /* number of bytes in the data buffer to be processed by the CRC engine */
    const uint32_t NumBits = 0;                                         /* no partial byte */

    common_Init();

#if defined(ADI_CYCLECOUNT_ENABLED) && (ADI_CYCLECOUNT_ENABLED == 1u)
    ADI_CYCLECOUNT_INITIALIZE();                                        /* Initialize cycle counting */
    
    /* Obtain cycle counting IDs for DMA driven CRC with callback function  */
    ADI_CYCLECOUNT_RESULT eCyclecountResult = adi_cyclecount_addEntity(CC_NAME, &cycleCountId_crc);
    DEBUG_RESULT("Failed to add cycle counting ID for cycleCountId_crc", eCyclecountResult, ADI_CYCLECOUNT_SUCCESS);
#endif

    if (ADI_PWR_SUCCESS != adi_pwr_Init())
    {
        common_Fail("adi_pwr_Init failed");
    }
    else if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_HCLK,1))
    {
        common_Fail("adi_pwr_SetClockDivider ADI_CLOCK_HCLK failed");
    }
    else if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_PCLK,1))
    {
        common_Fail("adi_pwr_SetClockDivider ADI_CLOCK_PCLK failed");
    }
    /* open a CRC device instance */
    else if (ADI_CRC_SUCCESS != adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev))
    {
        common_Fail("adi_crc_Open failed");
    }
    else if (ADI_CRC_SUCCESS != adi_crc_SetLSBFirst(hCrcDev, bLsbFirst))
    {
        common_Fail("adi_crc_SetLSBFirst failed");
    }
    else if (ADI_CRC_SUCCESS != adi_crc_SetPolynomialVal (hCrcDev, crc_polynomial))
    {
        common_Fail("adi_crc_SetPolynomialVal failed");
    }
    /* set the Seed value */
    else if (ADI_CRC_SUCCESS != adi_crc_SetCrcSeedVal (hCrcDev, CRC32_SEED_VALUE))
    {
        common_Fail("adi_crc_SetCrcSeedVal failed");
    }
    else
#ifdef ENABLE_CALLBACK_SUPPORT
    if (ADI_CRC_SUCCESS != adi_crc_RegisterCallback(hCrcDev, CrcCallback, hCrcDev))
    {
        common_Fail("adi_crc_RegisterCallback failed");
    }else{
        bCRCBusy = true;        /* init the variable used by the callback function */
#else
    {
#endif

#if defined(ADI_CYCLECOUNT_ENABLED) && (ADI_CYCLECOUNT_ENABLED==1u)
        if( adi_cyclecount_start() != ADI_CYCLECOUNT_SUCCESS)
        {
            common_Fail("adi_cyclecount_start failed");
        }
        else
#endif
        /* kick off the DMA driven CRC operation */
        if (ADI_CRC_SUCCESS != adi_crc_Compute (hCrcDev, StartAddr, NumBytes, NumBits))
        {
            common_Fail("adi_crc_Compute failed");
        } else {
#if defined(ADI_CYCLECOUNT_ENABLED) && (ADI_CYCLECOUNT_ENABLED==1u)
            ADI_CYCLECOUNT_STORE(cycleCountId_crc);
            if( adi_cyclecount_stop() != ADI_CYCLECOUNT_SUCCESS)
            {
                common_Fail("adi_cyclecount_stop failed");
            } else {
#endif
                const uint32_t expectedCRCValue = (bLsbFirst ? 0xAAB2E79Fu : 0x270EECAFu);  /* expected CRC value depending on LSB setting */
                uint32_t computedCRCValue;

#ifdef ENABLE_CALLBACK_SUPPORT
                while(bCRCBusy == true);                        /* bCRCBusy: volatile updated by CrcCallback */

                /**
                 * cbEvent records events which occur in the CRC callback function
                 * we simply report the unexpected ones here
                 */
                switch(cbEvent)
                {
                case ADI_CRC_EVENT_BUFFER_PROCESSED:            /* request was fully processed */
                    break;
                case ADI_CRC_EVENT_ERROR:                       /* CRC encountered a problem */
                    common_Fail("CrcCallback: unexpected ADI_CRC_EVENT_ERROR");
                    break;
                default:                                        /* unknown event */
                    common_Fail("CrcCallback: unknown event");
                    break;
                }
#else
                while ((ADI_CRC_SUCCESS == adi_crc_IsCrcInProgress(hCrcDev, &bCRCProgress)) && (true == bCRCProgress));
                if (true == bCRCProgress)
                {
                    common_Fail("adi_crc_IsCrcInProgress failed");
                }
#endif
                /* get the final result */
                if (ADI_CRC_SUCCESS != adi_crc_GetFinalCrcVal(hCrcDev, &computedCRCValue))
                {
                    common_Fail("adi_crc_GetFinalCrcVal failed");
                }
                /* close the CRC device instance */
                else if (ADI_CRC_SUCCESS != adi_crc_Close (hCrcDev))
                {
                    common_Fail("adi_crc_Close failed");
                }
                else if (expectedCRCValue != computedCRCValue)
                {
                    common_Fail("unexpected CRC result");
                }else{
                    ADI_CYCLECOUNT_REPORT();                    /* Print the cycle counts obtained */
                    common_Pass();                              /* The example has successfully completed */
                }
            }
#if defined(ADI_CYCLECOUNT_ENABLED) && (ADI_CYCLECOUNT_ENABLED==1u)
        }
#endif
    }
    return 0;
}

