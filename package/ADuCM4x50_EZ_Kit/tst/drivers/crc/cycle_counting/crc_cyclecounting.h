/*********************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*
*
*           Primary header file for CRC driver example which contains the
*           processor specific defines.
*
*/

#ifndef CRC_CYCLECOUNTINT_H
#define CRC_CYCLECOUNTINT_H

/*=============  I N C L U D E S   =============*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*=============  D E F I N E S  =============*/

/* CRC Device number to work on */
#define	CRC_DEV_NUM                     (0u)

/* 32-bit data used to test CRC Data fill mode */
#define CRC32_POLYNOMIAL_BE             (0x04C11DB7)
#define CRC32_POLYNOMIAL_LE             (0xEDB88320)

/* 32-bit data used to test CRC Data fill mode */
#define CRC32_SEED_VALUE                (0xFFFFFFFF)

/* Number of bytes in CRC Buffer used for testing (requires multiple DMA requests) */
#define CRC_BUF_NUM_DATA                (2048)

#endif /* CRC_CYCLECOUNTINT_H */
