         Analog Devices, Inc. ADuCM4x50 Application Example

Project Name: Readme_crc_example

Description: This example demonstrates how to use the CRC driver. It executes
===========  CRC operations on 32-bit, 16-bit and 8-bit data. The CRC driver
             can be statically configured to execute either core driven CRC
             operations or DMA driven CRC operations using a software DMA
             channel (channel SIP7 by default).

             This example configures the CRC driver for dma driven operations
             with CRC callback for testing purpose

Overview:
=========
             This example shows how to use a CRC device on buffers of data.
             It opens a CRC device, configures this device to calculate the
             CRC, using buffers containing random data, and verify the CRC
             calculated.

             The CRC driver supports both core driven operations, DMA driven
             operations with no callback function and DMA driven operations
             with a callback function registered.

User Configuration Macros:
==========================
             The following macros can be used to configure the CRC example.
             They can be found in adi_crc_config.h
             
             ADI_CRC_CFG_ENABLE_DMA_SUPPORT : 
               - 0    : core driven CRC operations
               - else : DMA driven CRC operations

             ADI_CFG_ENABLE_CALLBACK_SUPPORT :
               - 0    : do not use callback functions
               - else : use registered callback function
                        (ADI_CRC_CFG_ENABLE_DMA_SUPPORT must be set)


            ADI_CFG_CRC_SOFTWARE_DMA_CHANNEL_ID:
              expects a value between 0 and 7 to set the software DMA channel
              to be used when executing DMA driven CRC operations. (Default
              channel used is SIP7.)

Hardware Setup:
===============  
             By default, this example runs on HW with no specific setup required.

External connections:
=====================
             None.             

How to build and run:
=====================    
             Prepare hardware as explained in the Hardware Setup section.
             Build the project, load the executable to ADuCM4x50, and run it.


Expected Result:
=================
             Message "All done!" should appear when the test executes
             all the CRC operations successfully.

References:
===========
             ADuCM4x50 Hardware Reference Manual
