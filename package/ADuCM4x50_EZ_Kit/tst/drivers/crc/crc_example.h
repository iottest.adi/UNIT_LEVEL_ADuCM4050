/*********************************************************************************
Copyright(c) 2013-2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*!
 * @file        crc_example.h
 * @brief       Example to demonstrate CORE/DMA driven for computing CRC.
 * @details     This is the primary include file for the CRC example that shows
 *              how to use  CRC engine to compute CRC of the specified polynomial
 *
 */

/*=============  I N C L U D E S   =============*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*=============  D E F I N E S  =============*/

/* CRC Device number to work on */
#define	CRC_DEV_NUM                     (0u)

/* 32-bit data used to test CRC Data fill mode */
#define CRC32_POLYNOMIAL_BE             (0x04C11DB7)
#define CRC32_POLYNOMIAL_LE             (0xEDB88320)

/* 32-bit data used to test CRC Data fill mode */
#define CRC32_SEED_VALUE                (0xFFFFFFFF)

/* 16-bit data used to test CRC Data fill mode */
#define CRC16CCITT_POLYNOMIAL_BE        (0x1021)
#define CRC16CCITT_POLYNOMIAL_LE        (0x8408)

/* 16-bit data used to test CRC Data fill mode */
#define CRC16_SEED_VALUE                (0xFFFFu)

/* 8-bit data used to test CRC Data fill mode */
#define CRC8CCITT_POLYNOMIAL_BE         (0x07)
#define CRC8CCITT_POLYNOMIAL_LE         (0xE0)

/* 8-bit data used to test CRC Data fill mode */
#define CRC8_SEED_VALUE                 (0xFFu)

/* 32-bit Seed value used to generate test data for CRC Memory Scan Compute Compare */
#define CRC_RAND_SEED_VAL               (0x32765321u)

/* Element number with in the CRC buffer to inject an error */
#define CRC_INJECT_ERROR_INTO_WORD      (120u)

/* Number of bytes in CRC Buffer used for testing (requires multiple DMA requests) */
#define CRC_BUF_NUM_BYTES               (8192)

/*****/
