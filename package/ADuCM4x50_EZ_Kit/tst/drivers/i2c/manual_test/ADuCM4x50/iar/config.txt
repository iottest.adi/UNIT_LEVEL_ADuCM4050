CATEGORY = BSP,I2C

PROCESSOR = ADuCM4050

#delete any previous build files
<CMD, del /Q/S "$testdir\Debug">
<CMD, del /Q/S "$testdir\Release">


###### Debug Config ######

#build the test project
<CMD, %IAR_EW_HOME%\\common\\bin\\iarbuild manual_test.ewp -build Debug>

#confirm build succeeded
<BUILDFIND, Total number of errors: 0, Total number of warnings: 0>
<ASSERT_FILEEXISTS, $testdir\Debug\Exe\manual_test.out>


###### Release Config ######

#build the test project
<CMD, %IAR_EW_HOME%\\common\\bin\\iarbuild manual_test.ewp -build Release>

#confirm build succeeded
<BUILDFIND, Total number of errors: 0, Total number of warnings: 0>
<ASSERT_FILEEXISTS, $testdir\Release\Exe\manual_test.out>

