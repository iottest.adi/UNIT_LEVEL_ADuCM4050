README.TXT file for I2C Manual Testing

This is a manual test for doing initial driver bring-up and verification. This test can be automated if and only
if the AARDVARK target is used - which the lab setup currently supports. 

The I2C driver interface is the new "lean-and-mean" API which bears little resemblance to previous SS/DD APIs.

There are three I2C slave devices used in the test, each of which needs to be enabled/disabled and run manually.

All three targets use the same pinmux settings, configured in PinMic.c.

DO NOT CONNECT THE TARGETS TO THE BREAKOUT BOARD "VIN" PIN BECAUSE IT IS +5VDC AND WILL OVERDRIVE THE TARGET
VCC INPUT AND LOGIC LEVEL OF +3.3 VDC.  USE THE BREAKOUT BOARD 3.3 VDC PIN LABLED "VIO" TO POWER THE TARGETS.

The three targets are:
	AARDVARK:	ATMEL AT24C02 2048-bit (256x8-byte pages) I2C EEPROM (U2) on Aardvark I2C/SPI Activity Board.
	STAMP:		Microchip 24LC512 512k-bit (512x128-byte pages) I2C EEPROM on ADI STAMP SPI/TWI Interface Card.
	TEMP:		ADI ADT7420 I2C temperature sensor (U6) on Muska EZ-Kit.

Look for the enable macros (by the same name) for each of these targets.

Run each test in isolation, as follows...



AARDVARK TESTING
	HW SETUP FOR AARDVARK
	Connect the Total Phase "Aardvark I2C/SPI Activity Board" to the EZ-Kit via jumper wires to the
	"ADZS-BRKOUT-EX3 EI3 Probing Adapter Rev. 1.0" breakout board.

	Connect the breakout board J1 connector to the EZ-Kit P1A connector.  (It is reccommended to bolt the
	breakout connector for secure connection.)

	Install the following jumber wires between the breakout board and the Aardvark board:

		Signal	Aardvark	Breakout
		GND		J5:GND		any GND pin
		Vcc		J5:+5		P6:Vio (NOT Vin!)
		SCL		J5:SCL		P5:79
		SDA		J5:SDA		P5:80


	TEST PROCEEDURE FOR AARDVARK
	Enable the AARDVARK macro and disable the others.  Build, load and run the example.

	Note: during the polling phase between the block write and block read, the program
	will iterate on a "dummy", single-byte write operation (without bRepeatStart set
	and without bAbortOnError enabled, waiting for ANAK (address not acknowledge) to clear.

	The test:
		1. Writes a block of data to the EEPROM,
		2. Polls for the programming phase to complete,
		3. Reads back the data.
		4. Hangs at the "SUCCESS" or "FAILURE" while loop and does not exit main.

	You may set a breakpoint after any of these operations to verify the data.
	You may also set up a logic analyzer to caprute the I2C bus and verify these operations.



STAMP TESTING
	HW SETUP FOR STAMP
	Connect the ADI "STAMP SPI/TWI Interface Card" to the Muska EZ-Kit via jumper wires to the
	"ADZS-BRKOUT-EX3 EI3 Probing Adapter Rev. 1.0" breakout board.

	Connect the breakout board J1 connector to the EZ-Kit P1A connector.  (It is reccommended to bolt the
	breakout connector for secure connection.)

	Install the following jumber wires between the breakout board and the Stamp board:
	
		Signal	Stamp		Breakout
		GND		J7:20		any GND pin
		Vcc		J7:2		P6:Vio (NOT Vin!)
		SCL		J7:5		P5:79
		SDA		J7:6		P5:80

	TEST PROCEEDURE FOR STAMP
	Enable the STAMP macro and disable the others.  Build, load and run the example.

	Note: during the polling phase between the block write and block read, the program
	will iterate on a "dummy", single-byte write operation (without bRepeatStart set
	and without bAbortOnError enabled, waiting for ANAK (address not acknowledge) to clear.

	The test:
		1. Writes a block of data to the EEPROM,
		2. Polls for the programming phase to complete,
		3. Reads back the data.
		4. Hangs at the "SUCCESS" or "FAILURE" while loop and does not exit main.

	You may set a breakpoint after any of these operations to verify the data.
	You may also set up a logic analyzer to caprute the I2C bus and verify these operations.



TEMP TESTING
	HW SETUP FOR TEMP
	There is no external connections to make for the temperature test; ADI ADT7420 I2C temperature
	sensor in an "on-board" component, external to the Muska, but hardwired on the EZ-Kit.
	
	This test requires a live debugger to run because it uses semihosted printf output to write to
	the Terminal I/O window.  Therefore, the test will hard fault on the first printf write attampt
	if no debugger is attached.  This can be seen on the logic analyzer as a failure in steps 2 and 3,
	below, due to the missing debugger interrupt handler (i.e., emulator attached) to handle the
	semihosted printf calls (debugger interrupts).

	TEST PROCEEDURE FOR TEMP
	Enable the TEMP macro and disable the others.  Build, load and run the example.

	The test:
		1. Reads/displays the ADT7420 device ID register,
		2. Reads/displays the ADT7420 device configuration register,
		3. Reads/computes/displays the ADT7420 device temperature,
		4. Hangs at the "SUCCESS" or "FAILURE" while loop and does not exit main.
