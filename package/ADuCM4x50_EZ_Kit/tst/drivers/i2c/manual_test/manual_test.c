/*********************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*!
 * @file      manual_test.c
 * @brief     Manual test to demonstrate I2C Driver
 *
 * @details
 *            This is the primary source file for the I2C example demonstrating
 *            a master to slave transfer.
 *
 */

/*=============  I N C L U D E S   =============*/


/* Managed drivers and/or services include */
#include <drivers/i2c/adi_i2c.h>

#include "system_ADuCM4050.h"
#include "drivers/pwr/adi_pwr.h"
#include "common.h"

#ifndef SUCCESS
/*! Local I2C define for result SUCCESS (if not already). */
#define SUCCESS 0
#endif
#ifndef FAILURE
/*! Local I2C define for result FAILURE (if not already). */
#define FAILURE 1
#endif

#define ADT7420_ID 0xcb
#define MAX_TEMP 100
#define MIN_TEMP 50

extern int32_t adi_initpinmux(void);

#define AARDVARK
#define xSTAMP
#define xFPGA
#define xTEMP

#ifdef AARDVARK
/* EEPROM 8-byte page size */
#define DATASIZE 8
#endif

#ifdef FPGA
/* EEPROM 128-byte page size */
#define DATASIZE 16
#endif

#ifdef STAMP
/* EEPROM 128-byte page size */
#define DATASIZE 128
#endif

#ifdef TEMP
/* temperature register set readback size */
#define DATASIZE 8
#endif

 /* data array statics */
uint8_t txData[DATASIZE];
uint8_t rxData[DATASIZE];


/*********************************************************************
*
*   Function:   main
*
*********************************************************************/
int main (void)
{
    /* device memory */
    uint8_t devMem[ADI_I2C_MEMORY_SIZE];

    /* device handle */
    ADI_I2C_HANDLE hDevice;

    /* addressing/command phase "prologue" array */
    uint8_t prologueData[5];

    /* transaction structure */
    ADI_I2C_TRANSACTION xfr;

    /* result */
    ADI_I2C_RESULT result = ADI_I2C_SUCCESS;

    /* HW Error result */
    uint32_t hwErrors;

    /* poll and fault counts */
    int poll = 0;
    int fault = 0;

    /* test system initialization */
    common_Init();

    /* power init */
    adi_pwr_Init();

    /* pinmux init */
    adi_initpinmux();

    if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_HCLK, 1))
    {
        DEBUG_MESSAGE("Failed to set clock divider for HCLK.");
        return FAILURE;
    }

    if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_PCLK, 1))
    {
        DEBUG_MESSAGE("Failed to set clock divider for PCLK.");
        return FAILURE;
    }

    /* test "loop" */
    while (1) {

        /* open driver */
        if (adi_i2c_Open(0, &devMem, ADI_I2C_MEMORY_SIZE, &hDevice))
            break;

        /* check reset flow */
        if (adi_i2c_Reset(hDevice))
            break;

#if 1
        /* check set speed API */
        if (adi_i2c_SetBitRate(hDevice, 400000))  // top-end
            break;
#else
        /* check set speed API */
        if (adi_i2c_SetBitRate(hDevice, 55000))  // bottom-end
            break;
#endif

#ifdef AARDVARK
        /* Test EEPROM write/read using ATMEL AT24C02 I2C EEPROM (U2) on Aardvark I2C/SPI Activity Board */
        /* this is a 2048-bit memory device organized as 256x8-bits (256 bytes).                         */
        /* It uses single-byte addressing and max write page size of 8 bytes.                            */

        /* announce test */
        DEBUG_MESSAGE("Running ATMEL AT24C02 I2C EEPROM test on Aardvark I2C/SPI Activity Board...");

        /* set slave address for AMTEL part */
        result = adi_i2c_SetSlaveAddress(hDevice, 0x50);
        if (result) {
            fault++;
            break;
        }

        /* initialize data buffers */
        for (int i = 0; i < DATASIZE; i++) {
            txData[i] = i;
            rxData[i] = 0;
        }

        prologueData[0]     = 0;  /* write address */
        xfr.pPrologue       = &prologueData[0];
        xfr.nPrologueSize   = 1;  /* 8-bit addressing */
        xfr.pData           = txData;
        xfr.nDataSize       = DATASIZE;
        xfr.bReadNotWrite   = false;
        xfr.bRepeatStart    = false;

        /* blocking write */
        result = adi_i2c_ReadWrite(hDevice, &xfr, &hwErrors);
        if (result) {
            fault++;
            break;
        }

        /* poll for READ-READY acknowledge (next operation) until previous write page-programming cycle completes */
        xfr.nPrologueSize   = 0;
        xfr.pData           = rxData;
        xfr.nDataSize       = 1;
        xfr.bReadNotWrite   = true;
        xfr.bRepeatStart    = false;

        /* device returns address-not-acknowledge (ADI_I2C_HW_ERROR_NACK_ADDR) during programming period */
        hwErrors = ADI_I2C_HW_ERROR_NACK_ADDR;
        while (ADI_I2C_HW_ERROR_NACK_ADDR & hwErrors) {
            poll++;
            result = adi_i2c_ReadWrite(hDevice, &xfr, &hwErrors);  /* blocking write */
        }
        if (result) {
            fault++;
            break;
        }

        /* prepare readback transaction */
        prologueData[0]     = 0;  /* read address */
        xfr.pPrologue       = &prologueData[0];
        xfr.nPrologueSize   = 1;  /* 8-bit addressing */
        xfr.pData           = rxData;
        xfr.nDataSize       = DATASIZE;
        xfr.bReadNotWrite   = true;
        xfr.bRepeatStart    = true;

        /* blocking read */
        result = adi_i2c_ReadWrite(hDevice, &xfr, &hwErrors);
        if (result) {
            fault++;
            break;
        }

        /* compare data */
        for (int i = 0; i < DATASIZE; i++) {
            if (rxData[i] != txData[i]) {
                result = ADI_I2C_FAILURE;
                break;
            }
        }

        if (result) {
            fault++;
            break;
        }

#endif  /* AARDVARK */


#ifdef FPGA

        /* test EEPROM write/read using ST Micro M24C02 I2C EEPROM on Muska FPGA expansion connector */
        /* this EEPROM is a 2k-bit (256-byte) device with a 16-byte page size and single-byte address */

        /* announce test */
        DEBUG_MESSAGE("Running ST Micro M24C02 I2C EEPROM test on Muska FPGA expansion connector...");

        /* set slave address for ST Micro M24C02 */
        result = adi_i2c_SetSlaveAddress(hDevice, 0x50);
        if (result) {
            fault++;
            break;
        }

        /* initialize data buffers */
        for (int i = 0; i < DATASIZE; i++) {
            txData[i] = i;
            rxData[i] = 0;
        }

        prologueData[0]     = 0;  /* write address */
        xfr.pPrologue       = &prologueData[0];
        xfr.nPrologueSize   = 1;  /* 8-bit addressing */
        xfr.pData           = txData;
        xfr.nDataSize       = DATASIZE;
        xfr.bReadNotWrite   = false;
        xfr.bRepeatStart    = false;

        /* blocking write */
        result = adi_i2c_ReadWrite(hDevice, &xfr, &hwErrors);
        if (result) {
            fault++;
            break;
        }

        /* poll for READ-READY acknowledge (next operation) until previous write page-programming cycle completes */
        xfr.nPrologueSize   = 0;
        xfr.pData           = rxData;
        xfr.nDataSize       = 1;
        xfr.bReadNotWrite   = false;
        xfr.bRepeatStart    = false;

        /* device returns address-not-acknowledge (ADI_I2C_HW_ERROR_NACK_ADDR) during programming period */
        hwErrors = ADI_I2C_HW_ERROR_NACK_ADDR;
        while (ADI_I2C_HW_ERROR_NACK_ADDR &hwErrors) {
            poll++;
            result = adi_i2c_ReadWrite(hDevice, &xfr, &hwErrors);  /* blocking write */
        }

        if (result) {
            fault++;
            break;
        }
        /* prepare readback transaction */
        prologueData[0]     = 0;  /* read address */
        xfr.pPrologue       = &prologueData[0];
        xfr.nPrologueSize   = 1;  /* 8-bit addressing */
        xfr.pData           = rxData;
        xfr.nDataSize       = DATASIZE;
        xfr.bReadNotWrite   = true;
        xfr.bRepeatStart    = true;

        /* blocking read */
        result = adi_i2c_ReadWrite(hDevice, &xfr, &hwErrors);
        if (result) {
            fault++;
            break;
        }

        /* compare data */
        for (int i = 0; i < DATASIZE; i++) {
            if (rxData[i] != txData[i]) {
                result = ADI_I2C_FAILURE;
                break;
            }
        }

        if (result) {
            fault++;
            break;
        }

#endif  /* FPGA */


#ifdef STAMP

        /* test EEPROM write/read using Microchip 24LC512 I2C EEPROM on ADI STAMP SPI/TWI Interface Card */

        /* announce test */
        DEBUG_MESSAGE("Running Microchip 24LC512 I2C EEPROM test on ADI STAMP SPI/TWI Interface Card...");

        /* set slave address for Microchip part */
        result = adi_i2c_SetSlaveAddress(hDevice, 0x55);
        if (result) {
            fault++;
            break;
        }

        /* initialize data buffers */
        for (int i = 0; i < DATASIZE; i++) {
            txData[i] = i;
            rxData[i] = 0;
        }

        /* prepare transaction data */
        prologueData[0]     = 0;  /* MS write address */
        prologueData[1]     = 0;  /* LS write address */
        xfr.pPrologue       = &prologueData[0];
        xfr.nPrologueSize   = 2;  /* 16-bit addressing */
        xfr.pData           = txData;
        xfr.nDataSize       = DATASIZE;
        xfr.bReadNotWrite   = false;
        xfr.bRepeatStart    = false;

        /* submit as a blocking write */
        result = adi_i2c_ReadWrite(hDevice, &xfr, &hwErrors);
        if (result) {
            fault++;
            break;
        }

        /* poll for READ-READY acknowledge (next operation) until previous write page-programming cycle completes */
        xfr.nPrologueSize   = 0;
        xfr.pData           = rxData;
        xfr.nDataSize       = 1;
        xfr.bReadNotWrite   = true;
        xfr.bRepeatStart    = false;

        /* device returns address-not-acknowledge (ADI_I2C_HW_ERROR_NACK_ADDR) during programming period */
        hwErrors = ADI_I2C_HW_ERROR_NACK_ADDR;
        while (ADI_I2C_HW_ERROR_NACK_ADDR &hwErrors) {
            poll++;
            result = adi_i2c_ReadWrite(hDevice, &xfr, &hwErrors);  /* blocking write */
        }
        if (result) {
            fault++;
            break;
        }

        /* prepare non-blocking readback transaction */
        prologueData[0]     = 0;  /* MS read address */
        prologueData[1]     = 0;  /* LS read address */
        xfr.pPrologue       = &prologueData[0];
        xfr.nPrologueSize   = 2;  /* 16-bit addressing */
        xfr.pData           = rxData;
        xfr.nDataSize       = DATASIZE;
        xfr.bReadNotWrite   = true;
        xfr.bRepeatStart    = true;

        /* non-blocking read */
        result = adi_i2c_SubmitBuffer(hDevice, &xfr);
        if (result) {
            fault++;
            break;
        }

// conditional polling code test... (not required)
#if 1
        /* poll on completion */
        bool bComplete = false;
        int poll_count = 0;
        while (false == bComplete) {
            result = adi_i2c_IsBufferAvailable(hDevice, &bComplete);
            if (result)
                break;  /* break out of loop on error */
            poll_count++;

            /* break out prior to transaction completion */
            if (100 == poll_count)
                break;  /* break out of loop on timeput*/
        }
        if (result) {
            fault++;
            break;
        }
#endif

        /* issue final get buffer call to get result */
        result = adi_i2c_GetBuffer(hDevice, &hwErrors);
        if (result) {
            fault++;
            break;
        }

        /* compare data */
        for (int i = 0; i < DATASIZE; i++) {
            if (rxData[i] != txData[i]) {
                result = (ADI_I2C_RESULT)-1;
                break;
            }
        }
        if (result) {
            fault++;
            break;
        }

#endif  /* STAMP */


#ifdef TEMP

        /* test ADT7420 I2C temperature sensor (U6) on Muska Micro EZ-Kit */

        /* announce test */
        DEBUG_MESSAGE("Running ADI ADT7420 I2C Temperature Sensor test on Muska EZ-Kit...");

        /* set slave address for ADI ADT7420 */
        if (adi_i2c_SetSlaveAddress(hDevice, 0x48))
            break;

        /* do a simple one-byte read of chip ID */
        prologueData[0]     = 0x0b;  /* address of ID register */
        xfr.pPrologue       = &prologueData[0];
        xfr.nPrologueSize   = 1;
        xfr.pData           = rxData;
        xfr.nDataSize       = 1;
        xfr.bReadNotWrite   = true;
        xfr.bRepeatStart    = true;

        /* clear chip ID readback value in receive buffer */
        rxData[0] = 0;

        /* blocking read */
        result = adi_i2c_ReadWrite(hDevice, &xfr, &hwErrors);
        if (result) {
            fault++;
            break;
        }

        /* Note: this DEBUG_MESSAGE introduces an approximate 32ms gap between the previous and subsequent read APIs. */
        DEBUG_MESSAGE("Chip ID register: 0x%02x.", rxData[0]);

        /* verify expected ID */
        if (ADT7420_ID != rxData[0]) {
            DEBUG_MESSAGE("ADI ADT7420 chip ID register does not match expected value.");
            fault++;
            break;
        }

        /* report config register */
        prologueData[0]     = 0x03;  /* address of config register */
        xfr.pPrologue       = &prologueData[0];
        xfr.nPrologueSize   = 1;
        xfr.pData           = rxData;
        xfr.nDataSize       = 1;
        xfr.bReadNotWrite   = true;
        xfr.bRepeatStart    = true;

        /* blocking read */
        result = adi_i2c_ReadWrite(hDevice, &xfr, &hwErrors);
        if (result) {
            fault++;
            break;
        }

		/* log config register value */
        DEBUG_MESSAGE("Config register: 0x%02x.", rxData[0]);

        /* read temperature registers */
        prologueData[0]     = 0;  /* address of 1st temperature register */
        xfr.pPrologue       = &prologueData[0];
        xfr.nPrologueSize   = 1;
        xfr.pData           = rxData;
        xfr.nDataSize       = 2;
        xfr.bReadNotWrite   = true;
        xfr.bRepeatStart    = true;

        /* blocking read */
        result = adi_i2c_ReadWrite(hDevice, &xfr, &hwErrors);
        if (result) {
            fault++;
            break;
        }

        /* translate temperature reading to human-readable form */
        uint16_t temp;
        float ctemp, ftemp;

        temp = (rxData[0] << 8) | (rxData[1]);
        ctemp = (temp >> 3) / 16.0;
        ftemp = ctemp * 9.0 / 5.0 + 32.0;

		/* log temperature reading */
        DEBUG_MESSAGE("Current temperature: %05.1f C.", ctemp);
        DEBUG_MESSAGE("Current temperature: %05.1f F.", ftemp);

        /* range check */
        if ((MAX_TEMP < ftemp) || (MIN_TEMP > ftemp)) {
            DEBUG_MESSAGE("ADI ADT7420 temperature measure out of range.");
            fault++;
            break;
		}


        /*
           Conclude with a general call reset command to the I2C Bus,
           to which only the connected ADT7420 temperature sensor is
           expected to respond.

           From the ADT7420 data sheet:

            "When a master issues a slave address consisting of seven 0s
            with the eighth bit (R/W bit) set to 0, this is known as the
            general call address.  The general call address is for
            addressing every device connected to the I2C bus.  The ADT7420
            acknowledges this address and reads in the following data byte.

            If the second byte is 0x06 [the pre-defined GC reset command],
            the ADT7420 is reset, completely uploading all default values.
            The ADT7420 does not respond to the I2C bus commands (do not
            acknowledge) while the default values upload for approximately
            200 �s.

            The ADT7420 does not acknowledge any other general call commands."

        */
        uint8_t gcCmd = 0x06;  /* pre-defined "reset" general call command */
        result = adi_i2c_IssueGeneralCall(hDevice, &gcCmd, 1, &hwErrors);
        if (result) {
            fault++;
            break;
        }

#endif /* TEMP */

        /* success */
        DEBUG_MESSAGE("poll = %d", poll);
        DEBUG_MESSAGE("fault = %d", fault);
        DEBUG_MESSAGE("Success... result = 0x%04x.", result);
        common_Pass();
        return SUCCESS;
    }

    /* failure */
    DEBUG_MESSAGE("poll = %d", poll);
    DEBUG_MESSAGE("fault = %d", fault);
    DEBUG_MESSAGE("result = 0x%04x.", result);
    common_Fail("Unsuccessful run of \"temperature_sensor\" example.");
    return FAILURE;
}
