#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

this test does not work as-is...

#if 0
#include "muska_cdef.h"
#else
#include "adi_processor.h"
// mapping to official Muska BSP macros
#define BITM_I2CF_I2CFMCON_MASEN  BITM_I2C_MCTL_MASEN
#define BITM_I2CF_I2CFMSTA_MRXREQ BITM_I2C_MSTAT_MRXREQ
#define BITM_I2CF_I2CFMSTA_MSTOP  BITM_I2C_MSTAT_MSTOP
#define BITP_GPIO_GP0CON_PIN4_CFG BITP_GPIO_CFG_PIN04
#define BITP_GPIO_GP0CON_PIN5_CFG BITP_GPIO_CFG_PIN05
#define BITP_I2CF_I2CFDIV_HIGH    BITP_I2C_DIV_HIGH
#define BITP_I2CF_I2CFDIV_LOW     BITP_I2C_DIV_LOW
#define pREG_GPIO_GP0CON          pREG_GPIO0_CFG
#define pREG_I2CF_I2CFADR1        pREG_I2C0_ADDR1
#define pREG_I2CF_I2CFDIV         pREG_I2C0_DIV
#define pREG_I2CF_I2CFMCON        pREG_I2C0_MCTL
#define pREG_I2CF_I2CFMRX         pREG_I2C0_MRX
#define pREG_I2CF_I2CFMRXCNT      pREG_I2C0_MRXCNT
#define pREG_I2CF_I2CFMSTA        pREG_I2C0_MSTAT
#define pREG_I2CF_I2CFMSTA        pREG_I2C0_MSTAT
#define pREG_I2CF_I2CFMTX         pREG_I2C0_MTX
#endif


#define PCLK_FREQ_HZ            12500000 // 12.5MHz Pclk
#define SCL_CLK_FREQ_HZ         100000 // 100Khz SCLK
#define CLK_DIV_COUNT           (PCLK_FREQ_HZ/SCL_CLK_FREQ_HZ)
#define HIGH_TIME               ((CLK_DIV_COUNT/2)-2)
#define LOW_TIME                ((CLK_DIV_COUNT/2)-1)

#define TEMP_SENSOR_I2C_ADDR    0x4C
#define IIC_1_8_BUS_SWITCH_ADDR 0x74
#define IIC_1_8_BUS_SWITCH_6TH_CHANNEL_SELECTION 0X40 // 6th channel
#define IIC_EEPROM_XM105_ADDR   0x50
#define READ                    1
#define WRITE                   0

inline void time_delay()
{
    uint32_t count = 0;
    while (count++ <= 0xFF);
}


void i2c_gpio_config()
{
    // Pin0.4 SCL(B) and Pin0.5 SDA(B) // Set the GPIO pin as IIC pins
    *pREG_GPIO_GP0CON =   (1 << BITP_GPIO_GP0CON_PIN4_CFG) | (1 << BITP_GPIO_GP0CON_PIN5_CFG) ;
    printf("GPIO Config Register = 0x%08x\n", *pREG_GPIO_GP0CON);
}

void i2c_master_enable()
{
    // Enable the Master
    *pREG_I2CF_I2CFMCON = BITM_I2CF_I2CFMCON_MASEN ;
    printf("Master Control Register = 0x%08x\n", *pREG_I2CF_I2CFMCON);

}

void i2c_sclk_set()
{
    // Set the SCLK clock
    *pREG_I2CF_I2CFDIV =  (HIGH_TIME << BITP_I2CF_I2CFDIV_HIGH) | (LOW_TIME << BITP_I2CF_I2CFDIV_LOW) ;
    printf(" Serial Clock Divisor register = 0x%08x\n", *pREG_I2CF_I2CFDIV);
}

void i2c_write(uint8_t addr, uint8_t* buf, uint32_t size)
{
    int i = 0;
    *pREG_I2CF_I2CFADR1 = (addr << 1) | WRITE;
    for(i = 0; i < size; i++) {
        *pREG_I2CF_I2CFMTX = buf[i];
       while (!( *pREG_I2CF_I2CFMSTA & BITM_I2CF_I2CFMSTA_MSTOP));
    }
}
void i2c_read(uint8_t addr, uint8_t* buf, uint32_t size)
{
    int i = 0;
    *pREG_I2CF_I2CFADR1 = (addr << 1) | READ;
    *pREG_I2CF_I2CFMRXCNT = size;
    for(i = 0; i < size; i++) {
        while(!(*pREG_I2CF_I2CFMSTA & BITM_I2CF_I2CFMSTA_MRXREQ));
        buf[i] = *pREG_I2CF_I2CFMRX;
    }
}


int main()
{

/* define this to tickle GPIO P0.4 & P0.5 as raw GPIO */
#define PINDANCE
#ifdef PINDANCE

#define PINS (1 << 4 | 1 << 5)

    /* reset entire Port0 mux register */
    pADI_GPIO0->CFG = 0;         // 0x40020000

    /* set pins P0.4 & P0.5 fpr ouput */
    pADI_GPIO0->OEN = PINS;      // 0x40020004

    /* toggle these pins forever */
    while (1) {
        pADI_GPIO0->TGL = PINS;  // 0x40020020
    }
#else

    uint8_t tx_buf[10];
    uint8_t rx_buf[10];
    int i;

    i2c_gpio_config();
    printf("Master Status Register: 0x%08x\n", *pREG_I2CF_I2CFMSTA);
    i2c_master_enable();
    printf("Master Status Register: 0x%08x\n", *pREG_I2CF_I2CFMSTA);
    i2c_sclk_set();
    printf("Master Status Register: 0x%08x\n", *pREG_I2CF_I2CFMSTA);;


 #if 0
    tx_buf[0] = IIC_1_8_BUS_SWITCH_6TH_CHANNEL_SELECTION;
    i2c_write(IIC_1_8_BUS_SWITCH_ADDR, tx_buf, 1);

    tx_buf[0] = 0x12;
    tx_buf[1] = 0x11;
    i2c_write(IIC_EEPROM_XM105_ADDR, tx_buf, 2);

    i2c_write(IIC_EEPROM_XM105_ADDR, tx_buf, 1);

    i2c_read(IIC_EEPROM_XM105_ADDR, rx_buf, 1);

    for(i = 0; i < 1; i++)
        printf("RX buf[%2d] = 0x%02" PRIx8 "\n", i, rx_buf[i]);
#else
    *pREG_I2CF_I2CFMTX = IIC_1_8_BUS_SWITCH_6TH_CHANNEL_SELECTION;// to select channel 6(FMC_LPC_IIC) of the I2C Bus topology on the ZC706 board
    *pREG_I2CF_I2CFADR1 = (IIC_1_8_BUS_SWITCH_ADDR << 1) | WRITE;
    while (!( *pREG_I2CF_I2CFMSTA & BITM_I2CF_I2CFMSTA_MSTOP));
    printf("Master Status Register: 0x%08x\n", *pREG_I2CF_I2CFMSTA);

    *pREG_I2CF_I2CFMTX = 0x20;
    *pREG_I2CF_I2CFMTX = 0x98;
    *pREG_I2CF_I2CFADR1 = (IIC_EEPROM_XM105_ADDR << 1) | WRITE;
    while (!( *pREG_I2CF_I2CFMSTA & BITM_I2CF_I2CFMSTA_MSTOP));
    printf("Master Status Register: 0x%08x\n", *pREG_I2CF_I2CFMSTA);

    *pREG_I2CF_I2CFMTX = 0x20;
    *pREG_I2CF_I2CFADR1 = (IIC_EEPROM_XM105_ADDR << 1) | WRITE;
    while (!( *pREG_I2CF_I2CFMSTA & BITM_I2CF_I2CFMSTA_MSTOP));
    printf("Master Status Register: 0x%08x\n", *pREG_I2CF_I2CFMSTA);

    i2c_read(IIC_EEPROM_XM105_ADDR, rx_buf, 1);
    printf("Rx data: 0x%08x\n", *rx_buf);

    *pREG_I2CF_I2CFMRXCNT = 1;
    *pREG_I2CF_I2CFADR1 = (IIC_EEPROM_XM105_ADDR << 1) | READ;
    while(!(  *pREG_I2CF_I2CFMSTA & BITM_I2CF_I2CFMSTA_MRXREQ));
    while (!( *pREG_I2CF_I2CFMSTA & BITM_I2CF_I2CFMSTA_MSTOP));
    printf("Rx data: 0x%08x\n", *pREG_I2CF_I2CFMRX);

#endif

#endif // PINDANCE
}

