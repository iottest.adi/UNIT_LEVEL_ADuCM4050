/*!****************************************************************************
 * @file    main.c
 * @brief   WDT device driver test
 * @details Test all functions in both reset and interrupt mode. In reset mode,
 *          stop kicking the dog to ensure that a reset actually happens.
******************************************************************************/

#include <stdint.h>
#include <stdbool.h>
#include <common.h>
#include <adi_processor.h>
#include <adi_wdt_config.h>
#include <drivers/wdt/adi_wdt.h>
#include <drivers/pwr/adi_pwr.h>

#ifndef FAILURE
#define FAILURE -1
#endif

/* Helper macro */
#define CHECK_ERROR(result, expected, message) \
do {\
	if ( (expected) != (result))\
	{\
        common_Fail(message); \
        return 1; \
	}\
} while (0);

#define CHECK_ERROR_ISR(result, expected, message) \
do {\
    if ( (expected) != (result))\
    {\
        common_Fail(message); \
    }\
} while (0);

/******************************************************************************************** 
                                    RESET MODE TESTING
*********************************************************************************************/
#if (ADI_WDT_CONTROL_TIMEOUT_MODE == 0u)   

/* Difference between read value and load value tolerable (this is in units of counts) */
#define ACCEPTABLE_MARGIN_OF_ERROR (15u)

/* Total number of kicks before we let a reset happen */
#define NUM_KICKS                  (10000u)

/* SysTick must be set to run faster than the WDT to allow kicks */
#define TICKS                      (26000000u / 10000u)

/* Count the number of kicks */
static uint32_t gNumDogs;

/* SysTick will be used to kick the dog */
void SysTick_Handler(void) {
    uint16_t goodDog;
    uint16_t error;
    
    /* IF(More kicks remain) */
    if (gNumDogs < NUM_KICKS) {

        /* Test the kick function */
        adi_wdt_Kick();
          
         /* Test the read function */
        adi_wdt_GetCount(&goodDog);
        
        /* Check the error and make sure it is less than the allowable amount */
        error = ADI_WDT_LOAD_VALUE - goodDog;
        if (error > ACCEPTABLE_MARGIN_OF_ERROR) {
            common_Fail("WDT load value too far from expected value.");
        }
        
        /* After a certain number of kicks, we will stop kicking to make sure a reset happens */
        gNumDogs++;
    } /* ENDIF */
}

/* Test code */
static int test_wdt_ResetMode(void) {
    ADI_WDT_RESULT eResult;

    /* For this test case, we will use SysTick to periodically kick the dog */
    SysTick_Config(TICKS);

    /* Test enabling the WDT in reset mode */
    eResult = adi_wdt_Enable(true, NULL);
    CHECK_ERROR(eResult, ADI_WDT_SUCCESS, "Error enabling the WDT.")

     /* Test that subsequent enables in reset mode fail */
    eResult = adi_wdt_Enable(false, NULL);
    CHECK_ERROR(eResult, ADI_WDT_FAILURE_LOCKED, "Error trying to disable the WDT.")

    /* If enabling worked, we should eventually reset */
    while(1u);
}
    
/******************************************************************************************** 
                                    INTERRUPT MODE TESTING
*********************************************************************************************/
#else 

/* Total number of interrupts before we end the test program */                                  
#define NUMBER_OF_WDT_INTERRUPTS_TO_WAIT (20u)

/* Number of WDT Interrupts */
volatile static uint8_t gInterrupts;

/* Callback Function */
static void WdtCallback(void *pCBParam, uint32_t Event, void *pArg) {
    gInterrupts++;
}

/* Test code */
static int test_wdt_InterruptMode(void) {
    ADI_WDT_RESULT eResult;
  
    /* Test enabling the WDT in interrupt mode */
    eResult = adi_wdt_Enable(true, WdtCallback);
    CHECK_ERROR(eResult, ADI_WDT_SUCCESS, "Error enabling the WDT.")  

    /* Test that subsequent enables in interrupt mode fail */
    eResult = adi_wdt_Enable(false, NULL);
    CHECK_ERROR(eResult, ADI_WDT_FAILURE_LOCKED, "Error trying to disable the WDT.")              

    /* If enabling worked, we should eventually get an interrupt */
    while(gInterrupts == 0u);

    return 0;
}

#endif /* (ADI_WDT_CONTROL_TIMEOUT_MODE == 0u) */

/******************************************************************************************** 
                                            MAIN
*********************************************************************************************/
int main(void) {  
    int nError; 
  
    common_Init();
      /* Initialize the power service */
    if(ADI_PWR_SUCCESS != adi_pwr_Init())
    {
      return FAILURE;
    }
  
    if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_HCLK,1))
    {
      return FAILURE;
    }
  
    if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_PCLK,1))
    {
      return FAILURE;
    }
    /* Test code */
#if (ADI_WDT_CONTROL_TIMEOUT_MODE == 0u)    
    /* IF (WDT reset occured) */
    if ((pADI_PMG0->RST_STAT & BITM_PMG_RST_STAT_WDRST) == BITM_PMG_RST_STAT_WDRST) {
        /* Clear the reset status register */
        pADI_PMG0->RST_STAT = pADI_PMG0->RST_STAT;
    /* ELSE(Run the test code to force a WDT reset) */
    } else {  
        nError = test_wdt_ResetMode();
        CHECK_ERROR(nError, 0, "WDT reset mode test failed.")
    } /* ENDIF */  
#else
    nError = test_wdt_InterruptMode();
    CHECK_ERROR(nError, 0, "WDT interrupt mode test failed.")
#endif

    common_Pass();
    return 0;
}
