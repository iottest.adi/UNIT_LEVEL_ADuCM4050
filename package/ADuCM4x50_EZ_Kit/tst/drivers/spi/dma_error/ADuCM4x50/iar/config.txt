CATEGORY = BSP,SPI

PROCESSOR = ADuCM4050

#delete any previous build files 
<CMD, del /Q/S "$testdir\Debug">
<CMD, del /Q/S "$testdir\Release">


###### Debug Config ######

#build the test project
<CMD, %IAR_EW_HOME%\\common\\bin\\iarbuild dma_error.ewp -build Debug>

#confirm build succeeded
<BUILDFIND, Total number of errors: 0, Total number of warnings: 0>
<ASSERT_FILEEXISTS, $testdir\Debug\Exe\dma_error.out>


###### Release Config ######

#build the test project
<CMD, %IAR_EW_HOME%\\common\\bin\\iarbuild dma_error.ewp -build Release>

#confirm build succeeded
<BUILDFIND, Total number of errors: 0, Total number of warnings: 0>
<ASSERT_FILEEXISTS, $testdir\Release\Exe\dma_error.out>


###### Run Debug ######

#load and run the test 
<CMD, TTH:TIMEOUT(120), "%CSPY_PATH%\cspy.bat" "$testdir\Debug\Exe\dma_error.out">

#look for test result printed in the terminal.
<BUILDFIND, All done!>


###### Run Release ######

#load and run the test 
<CMD, TTH:TIMEOUT(120), "%CSPY_PATH%\cspy.bat" "$testdir\Release\Exe\dma_error.out">

#look for test result printed in the terminal.
<BUILDFIND, All done!>
