/*********************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/

#include <common.h>
#include <drivers/pwr/adi_pwr.h>
#include <drivers/spi/adi_spi.h>

#define MAX_BUFFER_SIZE          (2048u)

#define TIMEOUT_DURATION_SECONDS (10u)

static uint8_t transmit[MAX_BUFFER_SIZE];
static uint8_t receive [MAX_BUFFER_SIZE];
static volatile uint32_t gnTimeout;
static volatile bool     gbRxError;
static volatile bool     gbTxError;

#define RX_DMA_MASK (ADI_SPI_HW_ERROR_RX_CHAN_DMA_BUS_FAULT | ADI_SPI_HW_ERROR_RX_CHAN_DMA_INVALID_DESCR | ADI_SPI_HW_ERROR_RX_CHAN_DMA_UNKNOWN_ERROR)
#define TX_DMA_MASK (ADI_SPI_HW_ERROR_TX_CHAN_DMA_BUS_FAULT | ADI_SPI_HW_ERROR_TX_CHAN_DMA_INVALID_DESCR | ADI_SPI_HW_ERROR_TX_CHAN_DMA_UNKNOWN_ERROR)

void SysTick_Handler(void)
{
    gnTimeout++;
}

static void DmaCallback(void *pCBParam, uint32_t Event, void *pArg)
{
    DEBUG_MESSAGE("SPI Hardware Error Code: %X\r\n", Event);
  
    if ((Event & RX_DMA_MASK) != 0u) {
        gbRxError = true;
    }
    
    /* Can't seem to force a DMA error on the TX channel, an underflow error always occurs instead */
    if (((Event & TX_DMA_MASK) != 0u) || ((Event & ADI_SPI_HW_ERROR_TX_UNDERFLOW) != 0u)){
        gbTxError = true;
    }    
}

int main(void)
{
    ADI_SPI_TRANSCEIVER transceive;
    ADI_SPI_RESULT      eSpiResult;
    ADI_PWR_RESULT      ePwrResult;
    ADI_SPI_HANDLE      hDevice;
    uint8_t             DeviceMemory[ADI_SPI_MEMORY_SIZE];
    
    common_Init();
    
    ePwrResult = adi_pwr_Init();
    DEBUG_RESULT("adi_pwr_Init failed.", ePwrResult, ADI_PWR_SUCCESS);
    ePwrResult = adi_pwr_SetClockDivider(ADI_CLOCK_HCLK, 1u);
    DEBUG_RESULT("adi_pwr_SetClockDivider (HCLK) failed.", ePwrResult, ADI_PWR_SUCCESS);
    ePwrResult = adi_pwr_SetClockDivider(ADI_CLOCK_PCLK, 1u);
    DEBUG_RESULT("adi_pwr_SetClockDivider (PCLK) failed.", ePwrResult, ADI_PWR_SUCCESS);

    /* Configure SysTick to timeout 4 times a second */
    SysTick_Config(6500000u);
     
    /* Fill buffers */
    for (uint16_t i = 0u; i < MAX_BUFFER_SIZE; i++) {
        transmit[i] = 0xAAu;
        receive [i] = 0xFFu;
    }

    eSpiResult = adi_spi_Open(0u, DeviceMemory, ADI_SPI_MEMORY_SIZE, &hDevice);
    DEBUG_RESULT("adi_spi_Open failed.", eSpiResult, ADI_SPI_SUCCESS);
    
    eSpiResult = adi_spi_RegisterCallback(hDevice, DmaCallback, NULL);
    DEBUG_RESULT("adi_spi_RegisterCallback failed.", eSpiResult, ADI_SPI_SUCCESS);
    
    eSpiResult = adi_spi_SetLoopback(hDevice, true);
    DEBUG_RESULT("adi_spi_SetLoopback failed.", eSpiResult, ADI_SPI_SUCCESS);

    eSpiResult = adi_spi_SetBitrate(hDevice, 1000000u);
    DEBUG_RESULT("adi_spi_SetBitrate failed.", eSpiResult, ADI_SPI_SUCCESS);

    eSpiResult = adi_spi_SetChipSelect(hDevice, ADI_SPI_CS0);
    DEBUG_RESULT("adi_spi_SetChipSelect failed.", eSpiResult, ADI_SPI_SUCCESS);    

    /* Create a bad DMA transfer - RX channel */
    transceive.TransmitterBytes = MAX_BUFFER_SIZE;
    transceive.ReceiverBytes    = MAX_BUFFER_SIZE;
    transceive.nTxIncrement     = 1u;
    transceive.nRxIncrement     = 1u;
    transceive.bDMA             = true;
    transceive.bRD_CTL          = false;
    transceive.pTransmitter     = transmit;
    transceive.pReceiver        = NULL;

    eSpiResult = adi_spi_MasterSubmitBuffer(hDevice, &transceive);
    DEBUG_RESULT("adi_spi_MasterSubmitBuffer failed.", eSpiResult, ADI_SPI_SUCCESS);
    
    gnTimeout = 0u;
    gbRxError = false;
    
    while((gbRxError == false) && (gnTimeout < 4u*TIMEOUT_DURATION_SECONDS));

    if (gnTimeout >= 4u*TIMEOUT_DURATION_SECONDS)
    {
        common_Fail("Timeout occured waiting for a RX interrupt.");
        return 1;
    }
    
    /* Create a bad DMA transfer - TX channel */
    transceive.TransmitterBytes = MAX_BUFFER_SIZE;
    transceive.ReceiverBytes    = MAX_BUFFER_SIZE;
    transceive.nTxIncrement     = 1u;
    transceive.nRxIncrement     = 1u;
    transceive.bDMA             = true;
    transceive.bRD_CTL          = false;
    transceive.pTransmitter     = NULL;
    transceive.pReceiver        = receive;
    
    eSpiResult = adi_spi_MasterSubmitBuffer(hDevice, &transceive);
    DEBUG_RESULT("adi_spi_MasterSubmitBuffer failed.", eSpiResult, ADI_SPI_SUCCESS);
            
    gnTimeout = 0u;
    gbTxError = false;
    
    while((gbTxError == false) && (gnTimeout < 4u*TIMEOUT_DURATION_SECONDS));   

    if (gnTimeout >= 4u*TIMEOUT_DURATION_SECONDS)
    {
        common_Fail("Timeout occured waiting for a TX interrupt.");
        return 1;
    } 
    
    eSpiResult = adi_spi_Close(hDevice);
    DEBUG_RESULT("adi_spi_Close failed.", eSpiResult, ADI_SPI_SUCCESS);
    
    common_Pass();
    return 0;
}
