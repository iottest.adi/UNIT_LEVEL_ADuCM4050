         Analog Devices, Inc. ADuCM4x50 Application test

Project Name: Readme_spi_aardvark

Description: This test demonstrates how to use the SPI driver to program an external, non-ADI,
===========  SPI EEPROM. The test transfers the content of one buffer to the eeprom 1 32 byte 
			 page at a time. The programmed data is then read back and compared to the original
			 data. 
			 
Overview:
=========
             The test opens a SPI device, configures the the device writes to and reads data
			 from the AT25080 eeprom device on a Total Phase Aardvark Activity board.
             The transmit buffer is filled with known values and submitted to the SPI device 
             for transmitting.	After programming, an empty buffer is also submitted to SPI device 
             for storing the received data. At the end of the test, the content of the 
             received buffer is verified against the content of the transmit buffer. 


User Configuration Macros:
==========================
             None.
                              

Hardware Setup:
===============  
             Etra HW setup is required for performing the test. The user must connect the 
             MISO, MOSI, CLK and CS pins of the SPI device to program the data externally. 
			 This can be done as follows:

             * Attach the ADZS-BRKOUT-EX3 to J1 on the ADuCM4050 Ez-KIT
			 * VIO to the Aardvark J5 +5V
			 * GND to the Aardvark J5 GND
			 * SPI0_CLK to the Aardvark J5 SCK
			 * SPI0_MISO to the Aardvark J5 MISO
			 * SPI0_MOSI to the Aardvark J5 M0SI
			 * SPI0_CS1 to the Aardvark J5 SS

External connections:
=====================
			Note that the spi_MasterSlave loopback example uses SPI0 and SPI1 for its test.
			Therefore, the signals are present on additional pins used by the slave.

             * Attach the ADZS-BRKOUT-EX3 to J1 on the ADuCM4050 Ez-KIT
			 * Connect P6.116 (VIO) to the Aardvark J5 +5V
			 * Connect P6.117 (GND) to the Aardvark J5 GND
			 * Connect P6.92  (SPI0_CLK) to the Aardvark J5 SCK
			 * Connect P6.91  (SPI0_MISO) to the Aardvark J5 MISO
			 * Connect P5.90  (SPI0_MOSI) to the Aardvark J5 M0SI
			 * Connect P5.38  (SPI0_CS1) to the Aardvark J5 SS

How to build and run:
=====================    
             Prepare hardware as explained in the Hardware Setup section.
             Build the project, load the executable to ADuCM4x50, and run it.
			
			NOTE: We have seen failures on some Aardvark boards. If the board fails, odd number
			pages are reported as 255 (0xFF). We've seen this pass after connecting a logic 
			analyzer to MISO and MOSI. The seems to be a loading issue on the pins.

Expected Result:
=================    
             Should see "All Done" after the execution is complete.


References:
===========
             ADuCM4x50 Hardware Reference Manual
