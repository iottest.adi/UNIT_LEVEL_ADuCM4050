/*********************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*!
* @file     spi_aardvark.h
*
* @brief    Primary header file for SPI Aardvark eeprom example.
*
* @details  Primary header file for SPI driver example which contains the
*           processor specific defines.
*
*/

#ifndef _SPI_LOOPBACK_H_
#define _SPI_LOOPBACK_H_


/* Choose a SPI device, need SPI0 */
#define SPI_DEVICE_NUM    0U

/* define opcodes for AT25080 SPI EEPROM */
#define  AT25080_WREN 0x06      /* Set write enable latch */
#define  AT25080_WRDI 0x04      /* Reset write enable latch */
#define  AT25080_RDSR 0x05      /* Read status register */
#define  AT25080_WRSR 0x01      /* Write status register */
#define  AT25080_Read 0x03      /* Read data from memory array */
#define  AT25080_Write 0x02     /* Write data to memory array */
#define  AT25080_SIZE (8 * 1024)/* 8k Bytes */

/** define size of data buffers, DMA max size is 255 */
#define HEADER_SIZE 3u      /* read command + 2 bytes for address */
#define BUFFERSIZE 32u      /* Maximum page size that can be written */
#define RXBUFFER (AT25080_SIZE + 3)  /* Read the complete eeprom, it's just a test */
#define PAD         24
#define SC           0 /* Some Constant used to make things a bit odd */
#define DATA_START   3 /* Offset after command and address bytes for the data */

/* Read Count bytes from Starting address */
bool EEPROM_Read(ADI_SPI_HANDLE hDevice, unsigned int uStartAddr, unsigned int uCount, unsigned char *dest);

/* Write Count bytes from Starting address */
bool EEPROM_Write(ADI_SPI_HANDLE hDevice, unsigned int uStartAddr, unsigned int uCount, unsigned char *dest);

/* Pin muxing */
extern int32_t adi_initpinmux(void);

#endif /* _SPI_LOOPBACK_H_ */
