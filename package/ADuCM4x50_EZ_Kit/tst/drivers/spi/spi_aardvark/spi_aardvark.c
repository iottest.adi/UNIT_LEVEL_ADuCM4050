/*!
 *****************************************************************************
 * @file:    spi_aardvark.c
 * @brief:   SPI Device driver example soruce file
 *-----------------------------------------------------------------------------
 *
Copyright (c) 2011-2016 Analog Devices, Inc.

All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
  - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  - Modified versions of the software must be conspicuously marked as such.
  - This software is licensed solely and exclusively for use with processors
    manufactured by or for Analog Devices, Inc.
  - This software may not be combined or merged with other code in any manner
    that would cause the software to become subject to terms and conditions
    which differ from those listed here.
  - Neither the name of Analog Devices, Inc. nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
  - The use of this software may or may not infringe the patent rights of one
    or more patent holders.  This license does not release you from the
    requirement that you obtain separate licenses from these patent holders
    to use this software.

THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT,
TITLE, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
NO EVENT SHALL ANALOG DEVICES, INC. OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF CLAIMS OF INTELLECTUAL
PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/

#include <stddef.h>		/* for 'NULL' */
#include <string.h>		/* for strlen */
#include <system_ADuCM4050.h>
#include "common.h"

#include <drivers/spi/adi_spi.h>
#include <drivers/pwr/adi_pwr.h>
#include <drivers/gpio/adi_gpio.h>
#include <drivers/dma/adi_dma.h>

#include "spi_aardvark.h"

/* allocate oversized buffers with guard data */

 /*! Overallocated transmit data buffer with guard data */
uint8_t overtx[BUFFERSIZE+PAD];

/*! Overallocated receive data buffer with guard data */
#pragma data_alignment= 4 /* IAR */
uint8_t overrx[RXBUFFER];

/* the "usable" buffers within the overallocated buffers housing the guard data */


uint8_t spidevicemem[ADI_SPI_MEMORY_SIZE];
uint8_t spidevicemem_xxx[ADI_SPI_MEMORY_SIZE];
/*!
 * @brief  Function 'main' for SPI test program
 *
 * @param  none
 * @return int (Zero for success, non-zero for failure).
 *
 * A simple application that sets up SPI device to test if the SPI controller and 
 * basic infastructure works on a device other than ADI.
 *
 */
/**
 * @brief  Interrupt handler for SysTick example program
 *
 * @return none
 *
 * SysTick Handler is called whenever SysTick interrupt occurs.
 * In this example, the handler increments the interrupt count
 */

#define NCLKS 0x7FFFFF

unsigned long ticks;

static unsigned int Count;

void SysTick_Handler(void)
{
    /* bump Count up to COUNT  */
    Count += 1;

}


unsigned long GetTicks( void )
{
  return Count*NCLKS + (0x7FFFFF - SysTick->VAL);
}

int main(void)
{
    ADI_SPI_HANDLE hDevice;
    ADI_SPI_RESULT eResult;
    unsigned int i = 0;
    
    /* Data to be written to eeprom */
    unsigned char src[0x80];
    
    for(i=0; i<0x80;++i)
    {
      src[i] = i;
      overrx[i]=0;
    }
   
    adi_initpinmux();

    /* Test system initialization */
    common_Init();

    /* Power initialization */
    if(ADI_PWR_SUCCESS != adi_pwr_Init())
    {
        DEBUG_MESSAGE("Failed to intialize the power service");
    }
    if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_HCLK,1))
    {
        DEBUG_MESSAGE("Failed to intialize the power service");
    }
    if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_PCLK,1))
    {
        DEBUG_MESSAGE("Failed to intialize the power service\n");
    }

    /* Initialize SysTick */
    SysTick_Config(NCLKS);

    /* Initialize SPI */
    eResult = adi_spi_Open(SPI_DEVICE_NUM,spidevicemem,ADI_SPI_MEMORY_SIZE,&hDevice);
    DEBUG_RESULT("Failed to init SPI driver",eResult,ADI_SPI_SUCCESS);

    /* throttle bitrate to something the controller can reach */
    eResult = adi_spi_SetBitrate(hDevice, 500000);
    DEBUG_RESULT("Failed to set Bitrate",eResult,ADI_SPI_SUCCESS);

    /* set the chip select */
    eResult = adi_spi_SetChipSelect(hDevice, ADI_SPI_CS1);
    DEBUG_RESULT("Failed to set the chip select",eResult,ADI_SPI_SUCCESS);
    
    EEPROM_Write(hDevice, 0, 0x80, &src[0]);

    EEPROM_Read(hDevice, 0, 0x80+DATA_START, overrx);

    /* reset to all FF's */
    for(i=0; i<0x80;++i)
    {
      src[i] = 0xff;
    }
    EEPROM_Write(hDevice, 0, 0x80, &src[0]);

    /* shut it down */
    eResult = adi_spi_Close(hDevice);
    DEBUG_RESULT("Failed to uninit SPI driver",eResult,ADI_SPI_SUCCESS);

    /* validate the original write */
    for(i=0;i<0x80;++i)
    {
      if(overrx[i+DATA_START] != i)
      {
        /* printf("Index %d = %d\n", i, overrx[i+DATA_START]); */
        eResult = ADI_SPI_FAILURE;
      }
    }
                
    if(eResult == ADI_SPI_SUCCESS)
    {
        common_Pass();
    }
    else
    {
        common_Fail("Failed to run the SPI eeprom test\n");
    }

}

/* Read Count bytes from Starting address */
bool EEPROM_Read(ADI_SPI_HANDLE hDevice, unsigned int uStartAddr, unsigned int uCount, unsigned char *dest)
{
  ADI_SPI_TRANSCEIVER transceive;

  if( uStartAddr + uCount >= AT25080_SIZE)
    return true;
  
  /* establish starting address to read */
  /* link transceive data size to the remaining count */
  transceive.TransmitterBytes = HEADER_SIZE;      /* read command + 2 bytes for address */
  /* link transceive data size to the remaining count */
  transceive.ReceiverBytes = uCount;

  overtx[0] = AT25080_Read;
  overtx[1] = (uint8_t)(uStartAddr>>8) & 0xff;
  overtx[2] = (uint8_t)(uStartAddr & 0xff);
  
  /* initialize data attributes */
  transceive.pTransmitter = overtx;
  transceive.pReceiver = dest;

  /* auto increment both buffers */
  transceive.nTxIncrement = 1;
  transceive.nRxIncrement = 1;
  
  transceive.bDMA = true;
  transceive.bRD_CTL = false;

  // submit the call
  if (ADI_SPI_SUCCESS != adi_spi_MasterReadWrite(hDevice, &transceive))
  {
    return true;
  }
  return false;
}

bool EEPROM_Write_One(ADI_SPI_HANDLE hDevice, unsigned char uCmd)
{
  ADI_SPI_RESULT result = ADI_SPI_SUCCESS;  /* assume the best */
  ADI_SPI_TRANSCEIVER transceive;
  overtx[0] = uCmd;
  
  transceive.TransmitterBytes = 1;
  /* link transceive data size to the remaining count */
  transceive.ReceiverBytes = 0;

  transceive.pTransmitter = overtx;
  transceive.pReceiver = NULL;

  /* auto increment Tx buffer */
  transceive.nTxIncrement = 1;
  transceive.nRxIncrement = 0;
  
  transceive.bDMA = false;
  transceive.bRD_CTL = false;

  // submit the call
  result = adi_spi_MasterReadWrite(hDevice, &transceive);
  return result;
}

bool EEPROM_Read_One(ADI_SPI_HANDLE hDevice, unsigned char uCmd, unsigned char *uVal )
{
  ADI_SPI_RESULT result = ADI_SPI_SUCCESS;  /* assume the best */
  ADI_SPI_TRANSCEIVER transceive;
  overtx[0] = uCmd;
  
  transceive.TransmitterBytes = 1;
  /* link transceive data size to the remaining count */
  transceive.ReceiverBytes = 1;

  transceive.pTransmitter = overtx;
  transceive.pReceiver = overrx;

  /* auto increment both buffers */
  transceive.nTxIncrement = 1;
  transceive.nRxIncrement = 1;
  
  transceive.bDMA = false;
  transceive.bRD_CTL = false; //useRdCtl;

  // submit the call
  result = adi_spi_MasterReadWrite(hDevice, &transceive);
  *uVal = overrx[0];
  return result;
}

/* Write Count bytes from Starting address */
bool EEPROM_Write(ADI_SPI_HANDLE hDevice, unsigned int uStartAddr, unsigned int uCount, unsigned char *src)
{
  ADI_SPI_RESULT result = ADI_SPI_SUCCESS;  /* assume the best */
  ADI_SPI_TRANSCEIVER transceive;
  unsigned char uChar = 0;
  unsigned char *uCurrent;
  uCurrent = src;
  
  unsigned int i = 0;
  
  if( uStartAddr + uCount >= AT25080_SIZE)
    return true;
  
  /* establish starting address to write */
  /* link transceive data size to the remaining count */
  transceive.TransmitterBytes = 3 + (uCount < 32 ? uCount : 32);      /* write command, 2 bytes for address, then the data */
  /* link transceive data size to the remaining count */
  transceive.ReceiverBytes = 0;

  
  /* We can write up to 32 bytes at a time. The lower 5 address bits will 
     automatically increment */
  do
  {
    i = 0;
    /* Set WREN latch, uses overtx[0] */
    EEPROM_Write_One(hDevice, AT25080_WREN);
  
    /* initialize data attributes */
    transceive.pTransmitter = overtx;
    transceive.pReceiver = NULL;

    /* auto increment both buffers */
    transceive.nTxIncrement = 1;
    transceive.nRxIncrement = 0;
  
    transceive.bDMA = true;
    transceive.bRD_CTL = false;

    overtx[0] = AT25080_Write;
    overtx[1] = (uint8_t)(uStartAddr>>8) & 0xff;
    overtx[2] = (uint8_t)(uStartAddr & 0xff);
 
    /* every 32 bytes written need to reload the page address */
    for(i=0; i < 32; ++i)
    {
      overtx[3+i] = *uCurrent++;
    }
  
    // submit the call
    result = adi_spi_MasterReadWrite(hDevice, &transceive);
    uCount -= 32;
    uStartAddr += 32;
    do{
      ++i;
      EEPROM_Read_One(hDevice, AT25080_RDSR, &uChar );
    }while(uChar & 1);
  
  }while(uCount);
  /* printf("Statread = %d\n", i); */
  
  /* Reset write enable latch */
  EEPROM_Write_One(hDevice, AT25080_WRDI);

  return result;
}

/*
** EOF
*/

/*@}*/
