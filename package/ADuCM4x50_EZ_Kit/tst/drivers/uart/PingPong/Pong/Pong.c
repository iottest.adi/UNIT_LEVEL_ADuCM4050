/*
 *****************************************************************************
 * @file:    uart_loop_back.c
 * @brief:   File which contain "main" for testing UART Device Driver.
 * @version: $Revision: 33569 $
 * @date:    $Date: 2016-08-16 07:04:39 -0500 (Tue, 16 Aug 2016) $
 *****************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*!
* @file      uart_loop_back.c
*
* @brief     This file contains an example that demonstrates loopback with
*            UART device driver APIs.
*
*/
#include <stdio.h>
#include <drivers/uart/adi_uart.h>
#include <drivers/pwr/adi_pwr.h>
#include "uart_loop_back.h"
#include "common.h"

extern void adi_initpinmux(void);

/* Handle for the UART device. */
static ADI_UART_HANDLE hDevice;

/* Memory for the UART driver. */
static uint8_t UartDeviceMembi[ADI_UART_MEMORY_SIZEBIDIR];
static uint8_t UartDeviceMemuni[ADI_UART_MEMORY_SIZEUNIDIR];


/* First Tx  Buffer. */
static char  nBufferTx0[SIZE_OF_BUFFER]="ABCDEFGHIJKLMNOPQRSTUVWXY";

/* Second Tx  Buffer. */
static char  nBufferTx1[SIZE_OF_BUFFER]="abcdefghijklmnopqrstuvwxy";

/* Third Tx  Buffer. */
static char  nBufferTx2[SIZE_OF_BUFFER]="0123456789101112131415161";

/* First Rx  Buffer. */
static uint8_t nBufferRx0[SIZE_OF_BUFFER];

/* Second Rx  Buffer. */
static uint8_t nBufferRx1[SIZE_OF_BUFFER];

/* Third Rx  Buffer. */
static uint8_t nBufferRx2[SIZE_OF_BUFFER];


static uint8_t nBufferRx3[SIZE_OF_BUFFER];
static uint8_t nBufferRx4[SIZE_OF_BUFFER];
static uint8_t nBufferRx5[SIZE_OF_BUFFER];
static uint8_t nBufferRx6[SIZE_OF_BUFFER];
static uint8_t nBufferRx7[SIZE_OF_BUFFER];
static uint8_t nBufferRx8[SIZE_OF_BUFFER];
static uint8_t nBufferRx9[SIZE_OF_BUFFER];
static uint8_t nBufferRx10[SIZE_OF_BUFFER];

static uint8_t nBufferRx11[9];
static uint8_t nBufferRx12[9];

/*********************************************************************
*
*   Function:   main
*
*********************************************************************/
int main(void)
{     
        
        /* Variable that keeps track of any errors found. */
        bool bResult = true;
        
        /* Variable to confirm the transmit shift register is empty before closing the device. */
        bool bTxComplete = false;
        
        /* Variable to catch any errors when there is not a callback registered. */
        static uint32_t pHwError;
        
	/* Variable to get the address of the processed buffer.*/
        void *pProcRxBuff0, *pProcRxBuff2;
	
        /* Variable to get the buffer size to compare the buffer contents at the end of this test. */
        uint32_t i;
        
        /* Pinmux initialization. */
        adi_initpinmux();

 /******************************** Test UART 1 ************************************/

        do {  
                        
              /* Power initialization. */
              if(ADI_PWR_SUCCESS != adi_pwr_Init())
              {
                  DEBUG_MESSAGE("Failed to intialize the power service.");
                  bResult = false;
                  break;
              }
              
              /* System clock initialization. */
              if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_HCLK, 1u))
              {
                  DEBUG_MESSAGE("Failed to intialize the system clock.");
                  bResult = false;
                  break;
              }
              
              /* Peripheral clock initialization. */
              if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_PCLK, 1u))
              {
                  DEBUG_MESSAGE("Failed to intialize the peripheral clock.");
                  bResult = false;
                  break;
              }
 /*******************************************Bidirectional read/write dma/interrupts roating ********************************************/            
             
              /* Open the UART device bidirectional. */
              if(ADI_UART_SUCCESS != adi_uart_Open(1, ADI_UART_DIR_BIDIRECTION, UartDeviceMembi, ADI_UART_MEMORY_SIZEBIDIR, &hDevice))
              {
                  DEBUG_MESSAGE("Failed to open the UART device.");
                  bResult = false;
                  break;
              }
              
              adi_uart_EnableFifo(hDevice, true);
              if(adi_uart_Write(hDevice, nBufferTx0, SIZE_OF_BUFFER, 0u,  &pHwError))
              {
                  DEBUG_MESSAGE("Failed to Read Buffer 0.");
                  bResult = false;
                  break;
              }
              
              if(adi_uart_Write(hDevice, nBufferTx1, SIZE_OF_BUFFER, 1u,  &pHwError))
              {
                  DEBUG_MESSAGE("Failed to Read Buffer 0.");
                  bResult = false;
                  break;
              }
              
              if(adi_uart_Read(hDevice, nBufferRx0, SIZE_OF_BUFFER, 1u,  &pHwError))
              {
                  DEBUG_MESSAGE("Failed to Read Buffer 0.");
                  bResult = false;
                  break;
              }
               
              if(adi_uart_Read(hDevice, nBufferRx1, SIZE_OF_BUFFER, 0u,  &pHwError))
              {
                  DEBUG_MESSAGE("Failed to Read Buffer 0.");
                  bResult = false;
                  break;
              }     
           
              if(adi_uart_Write(hDevice, nBufferTx2, SIZE_OF_BUFFER, 1u,  &pHwError))
              {
                  DEBUG_MESSAGE("Failed to Read Buffer 0.");
                  bResult = false;
                  break;
              }
              
              if(adi_uart_Read(hDevice, nBufferRx2, SIZE_OF_BUFFER, 0u,  &pHwError))
              {
                  DEBUG_MESSAGE("Failed to Read Buffer 0.");
                  bResult = false;
                  break;
              }
              
              /* Make sure the transmit shift register is empty before closing the device. */
              while(bTxComplete == false)
              {
                  if(adi_uart_IsTxComplete(hDevice, &bTxComplete) != ADI_UART_SUCCESS)
                  {
                      DEBUG_MESSAGE("Failed to check if the transmit holding register is empty.");
                      bResult = false;
                      break;
                  }
              }
              
              /* Close the device. */
              if((adi_uart_Close(hDevice)) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to close the device");
                  bResult = false;
              }              
              
              /* Make sure the data transfers all worked properly. */
              for(i = 0u; i < SIZE_OF_BUFFER; i++)
              {
                 if((nBufferTx1[i] != nBufferRx0[i]) || ( nBufferTx0[i] != nBufferRx1[i]) || ( nBufferTx2[i] != nBufferRx2[i]))
                 {
                    bResult = false;
                    break;
                 }
                 else
                 {
                   DEBUG_MESSAGE("Success!");
                   break;                   
                 }
              } 
              
              
/*******************************************RX read/submitrx dma/interrupts roating *******************************************/
              
              /* Open the UART device RX. */
              if(ADI_UART_SUCCESS != adi_uart_Open(1, ADI_UART_DIR_RECEIVE, UartDeviceMemuni, ADI_UART_MEMORY_SIZEUNIDIR, &hDevice))
              {
                  DEBUG_MESSAGE("Failed to open the UART device.");
                  bResult = false;
                  break;
              }
             
              adi_uart_EnableFifo(hDevice, true);

              /* Submit an empty buffer to the driver for receiving data using interrupt mode. */
              if(adi_uart_SubmitRxBuffer(hDevice, nBufferRx3, SIZE_OF_BUFFER, 0u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("AFailed to submit the Rx buffer 0 using interrupt mode.");
                  bResult = false;
                  break;
              }

              /* Submit an empty buffer to the driver for receiving data using interrupt mode. */
              if(adi_uart_SubmitRxBuffer(hDevice, nBufferRx4, SIZE_OF_BUFFER, 1u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("BFailed to submit the Rx buffer 1 using dma mode.");
                  bResult = false;
                  break;
              }
              
              if(adi_uart_GetRxBuffer(hDevice, &pProcRxBuff2, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 2.");
                  bResult = false;
                  break;
              }
              
                           
              if(adi_uart_GetRxBuffer(hDevice, &pProcRxBuff2, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 2.");
                  bResult = false;
                  break;
              }
              
              /* Submit an empty buffer to the driver for receiving data using interrupt mode. */
              if(adi_uart_SubmitRxBuffer(hDevice, nBufferRx5, SIZE_OF_BUFFER, 0u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 1 using dma mode.");
                  bResult = false;
                  break;
              }
              
              if(adi_uart_GetRxBuffer(hDevice, &pProcRxBuff2, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 2.");
                  bResult = false;
                  break;
              }
                            /* Submit an empty buffer to the driver for receiving data using interrupt mode. */
              if(adi_uart_SubmitRxBuffer(hDevice, nBufferRx6, SIZE_OF_BUFFER, 0u)!= ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 0 using interrupt mode.");
                  bResult = false;
                  break;
              }

              if(adi_uart_GetRxBuffer(hDevice, &pProcRxBuff2, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 2.");
                  bResult = false;
                  break;
              }
              
              /* Submit an empty buffer to the driver for receiving data using interrupt mode. */
              if(adi_uart_SubmitRxBuffer(hDevice, nBufferRx7, SIZE_OF_BUFFER, 1u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 1 using dma mode.");
                  bResult = false;
                  break;
              }
              
              
              if(adi_uart_GetRxBuffer(hDevice, &pProcRxBuff2, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 2.");
                  bResult = false;
                  break;
              }
              
              /* Submit an empty buffer to the driver for receiving data using interrupt mode. */
              if(adi_uart_SubmitRxBuffer(hDevice, nBufferRx8, SIZE_OF_BUFFER, 1u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 1 using dma mode.");
                  bResult = false;
                  break;
              }
              
              if(adi_uart_GetRxBuffer(hDevice, &pProcRxBuff2, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 2.");
                  bResult = false;
                  break;
              }
              
              
              /* Submit an empty buffer to the driver for receiving data using interrupt mode. */
              if(adi_uart_SubmitRxBuffer(hDevice, nBufferRx9, SIZE_OF_BUFFER, 1u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 1 using dma mode.");
                  bResult = false;
                  break;
              }
              
              if(adi_uart_GetRxBuffer(hDevice, &pProcRxBuff2, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 2.");
                  bResult = false;
                  break;
              }
              
              
              /* Submit an empty buffer to the driver for receiving data using interrupt mode. */
              if(adi_uart_SubmitRxBuffer(hDevice, nBufferRx10, SIZE_OF_BUFFER, 1u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 1 using dma mode.");
                  bResult = false;
                  break;
              }
              
              if(adi_uart_GetRxBuffer(hDevice, &pProcRxBuff2, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 2.");
                  bResult = false;
                  break;
              }
              
              /* Close the device. */
              if((adi_uart_Close(hDevice)) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to close the device");
                  bResult = false;
              }
              
              /* Make sure the data transfers all worked properly. */
              for(i = 0u; i < SIZE_OF_BUFFER; i++)
              {
                 if((nBufferTx0[i] != nBufferRx3[i]) || ( nBufferTx0[i] != nBufferRx4[i]) || ( nBufferTx0[i] != nBufferRx5[i]) || (nBufferTx0[i] != nBufferRx6[i]) || ( nBufferTx0[i] != nBufferRx7[i]) || ( nBufferTx0[i] != nBufferRx8[i]))
                 {
                    bResult = false;
                    break;
                 }
                 else
                 {
                   DEBUG_MESSAGE("Success 2 !");
                   break;                   
                 }
              }                
              
              
              
/******************************************* TX Write/submittx dma/interrupts rotating *******************************************/
              
               /* Open the UART device TX. */
              if(ADI_UART_SUCCESS != adi_uart_Open(1, ADI_UART_DIR_TRANSMIT, UartDeviceMemuni, ADI_UART_MEMORY_SIZEUNIDIR, &hDevice))
              {
                  DEBUG_MESSAGE("Failed to open the UART device.");
                  bResult = false;
                  break;
              }
                                    
              adi_uart_EnableFifo(hDevice, true);
     
              /* Submit an empty buffer to the driver for receiving data using interrupt mode. */
              if(adi_uart_SubmitTxBuffer(hDevice, nBufferTx0, SIZE_OF_BUFFER, 0u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 0 using interrupt mode.");
                  bResult = false;
                  break;
              }

              /* Submit an empty buffer to the driver for transmitting data using interrupt mode. */
              if(adi_uart_SubmitTxBuffer(hDevice, nBufferTx0, SIZE_OF_BUFFER, 0u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 1 using dma mode.");
                  bResult = false;
                  break;
              }
              if(adi_uart_GetTxBuffer(hDevice, &pProcRxBuff0, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 0.");
                  bResult = false;
                  break;
              }
                            
              if(adi_uart_GetTxBuffer(hDevice, &pProcRxBuff0, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 0.");
                  bResult = false;
                  break;
              }
              
              /* Submit an empty buffer to the driver for transmitting data using interrupt mode. */
              if(adi_uart_SubmitTxBuffer(hDevice, nBufferTx0, SIZE_OF_BUFFER, 1u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 1 using dma mode.");
                  bResult = false;
                  break;
              }
                            /* Submit an empty buffer to the driver for transmitting data using interrupt mode. */
              if(adi_uart_SubmitTxBuffer(hDevice, nBufferTx0, SIZE_OF_BUFFER, 1u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 0 using interrupt mode.");
                  bResult = false;
                  break;
              }
              
              if(adi_uart_GetTxBuffer(hDevice, &pProcRxBuff0, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 0.");
                  bResult = false;
                  break;
              }
              
              if(adi_uart_GetTxBuffer(hDevice, &pProcRxBuff0, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 0.");
                  bResult = false;
                  break;
              }
              
              /* Submit an empty buffer to the driver for transmitting data using interrupt mode. */
              if(adi_uart_SubmitTxBuffer(hDevice, nBufferTx0, SIZE_OF_BUFFER, 1u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 1 using dma mode.");
                  bResult = false;
                  break;
              }
              
              if(adi_uart_GetTxBuffer(hDevice, &pProcRxBuff0, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 0.");
                  bResult = false;
                  break;
              }
              
              /* Submit an empty buffer to the driver for transmitting data using interrupt mode. */
              if(adi_uart_SubmitTxBuffer(hDevice, nBufferTx0, SIZE_OF_BUFFER, 0u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 1 using dma mode.");
                  bResult = false;
                  break;
              }
 
              if(adi_uart_GetTxBuffer(hDevice, &pProcRxBuff0, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 0.");
                  bResult = false;
                  break;
              }
              
              /* Submit an empty buffer to the driver for transmitting data using interrupt mode. */
              if(adi_uart_SubmitTxBuffer(hDevice, nBufferTx0, SIZE_OF_BUFFER, 0u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 1 using dma mode.");
                  bResult = false;
                  break;
              }
              
              /* Submit an empty buffer to the driver for transmitting data using interrupt mode. */
              if(adi_uart_SubmitTxBuffer(hDevice, nBufferTx0, SIZE_OF_BUFFER, 1u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 1 using dma mode.");
                  bResult = false;
                  break;
              }
              
              if(adi_uart_GetTxBuffer(hDevice, &pProcRxBuff0, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 0.");
                  bResult = false;
                  break;
              }
              
              if(adi_uart_GetTxBuffer(hDevice, &pProcRxBuff0, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 0.");
                  bResult = false;
                  break;
              }
              bTxComplete = false;
              
              /* Make sure the transmit shift register is empty before closing the device. */
              while(bTxComplete == false)
              {
                  
                  if(adi_uart_IsTxComplete(hDevice, &bTxComplete) != ADI_UART_SUCCESS)
                  {
                      DEBUG_MESSAGE("Failed to check if the transmit holding register is empty.");
                      bResult = false;
                      break;
                  }
              }
              
              /* Close the device. */
              if((adi_uart_Close(hDevice)) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to close the device");
                  bResult = false;
                  break;
              }  
              
              
/*************************************************FINAL LOOP TEST*************************************************/             
                            
              /* Open the UART device bidirectional. */
              if(ADI_UART_SUCCESS != adi_uart_Open(1, ADI_UART_DIR_BIDIRECTION, UartDeviceMembi, ADI_UART_MEMORY_SIZEBIDIR, &hDevice))
              {
                  DEBUG_MESSAGE("Failed to open the UART device.");
                  bResult = false;
                  break;
              }
              
              while (1)
              {
              
              /* Submit an empty buffer to the driver for receiving data using interrupt mode. */
                  if(adi_uart_SubmitRxBuffer(hDevice, nBufferRx11, 9, 1u) != ADI_UART_SUCCESS)
                  {
                      DEBUG_MESSAGE("Failed to submit the Rx buffer 1 using dma mode.");
                      bResult = false;
                      break;
                  }
                                
                  if(adi_uart_GetRxBuffer(hDevice, &pProcRxBuff2, &pHwError) != ADI_UART_SUCCESS)
                  {
                      DEBUG_MESSAGE("Failed to get Rx buffer 2.");
                      bResult = false;
                      break;
                  }              
             
                  /* Submit an empty buffer to the driver for receiving data using interrupt mode. */
                  if(adi_uart_SubmitRxBuffer(hDevice, nBufferRx12, 9, 1u) != ADI_UART_SUCCESS)
                  {
                      DEBUG_MESSAGE("Failed to submit the Rx buffer 1 using dma mode.");
                      bResult = false;
                      break;
                  }
                                
                  if(adi_uart_GetRxBuffer(hDevice, &pProcRxBuff2, &pHwError) != ADI_UART_SUCCESS)
                  {
                      DEBUG_MESSAGE("Failed to get Rx buffer 2.");
                      bResult = false;
                      break;
                  } 
                  
                
              
              }              
   
        }while(0);      
            
        if(bResult == false)
        {
          return 1;
        }
        return(0);
}
