/*
 *****************************************************************************
 * @file:    uart_loop_back.h
 * @brief:   File which contain "main" for testing UART Device Driver.
 * @version: $Revision: 32842 $
 * @date:    $Date: 2015-11-28 13:38:51 -0500 (Sat, 28 Nov 2015) $
 *****************************************************************************

Copyright(c) 2011 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*!
* @file      uart_loop_back.h
*
* @brief     This is file is contain an loop back example with which using of
*            UART device driver APIs is demonstrated.
*
*/

#ifndef UART_LOOPBACK_H
#define UART_LOOPBACK_H

#include <stdio.h>

static void UARTCallback(void        *pAppHandle,
                         uint32_t     nEvent,
                         void        *pArg);

/* Memory required by the driver for bidirectional mode of operation. */
#define ADI_UART_MEMORY_SIZEBIDIR    (ADI_UART_BIDIR_MEMORY_SIZE)
/* Memory required by the driver for bidirectional mode of operation. */
#define ADI_UART_MEMORY_SIZEUNIDIR   (ADI_UART_UNIDIR_MEMORY_SIZE)

/* Size of the data buffers that will be processed. */
#define SIZE_OF_BUFFER  25u

/* UART device number. There are 2 devices, so this can be 0 or 1. */
#define UART_DEVICE_NUM 1u

#endif /* UART_LOOPBACK_H */
