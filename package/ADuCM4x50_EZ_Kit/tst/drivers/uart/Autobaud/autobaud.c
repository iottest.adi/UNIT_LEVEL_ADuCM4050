/*
 *****************************************************************************
 * @file:    autobaud.c
 * @brief:   File which contain "main" for testing UART Device Driver.
 *****************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*!
* @file      autobaud.c
*
* @brief     This is file contains an autobaud example which demonstrate how to configure 
*            a UART device to detect the baudrate.
*
*/

#include <stdio.h>
#include <drivers/uart/adi_uart.h>
#include <drivers/pwr/adi_pwr.h>
#include "autobaud.h"
#include "common.h"

#define UART_OPEN
#define UART_CLOSE
#define UART_SETCONFIG
#define UART_CONFGBAUD
#define UART_REGCALLBACK
#define UART_ENABLE_AUTOBAUD 
#define UART_SUBMITBFR
#define UART_ENABLELOOP
#define UART_ENABLEFIFO
#define UART_ENABLE_AUTOBAUD 
#define UART_FLUSHTXFIFO
#define UART_FLUSHRXFIFO
extern int32_t adi_initpinmux(void);

/* Handle for UART device. */
static ADI_UART_HANDLE hDevice,hDevice1;

/* Memory for UART driver. */
static uint8_t UartDeviceMem[ADI_UART_MEMORY_SIZE];
 #define SIZE_OF_BUFFER  1000u
static char  nBufferTx1[SIZE_OF_BUFFER]="ABCDEFSCVDGDTSF";
static char nBufferRx0[SIZE_OF_BUFFER];
   
/* Boolean to check if we entered the callback. */
static bool bCallback = false;
/*********************************************************************

    Function:       UARTCallback

    Description:    In the example, we've configured the autobaud to 
                    notify the API when it is done.

*********************************************************************/
static void UARTCallback(
    void        *pAppHandle,
    uint32_t     nEvent,
    void        *pArg
)
{
    switch (nEvent)
    {
        case ADI_UART_EVENT_AUTOBAUD_COMPLETE:
            bCallback = true;
            break;
    }
}
#if 0
int main( void )
{  
    /* Variable to store the baudrate. */
    static uint32_t nBaudrate;
    
    /* Variable to store any autobaud errors. */
    uint32_t nAutobaudError;
    

    
    /* Boolean to tell the driver if we will be using the callback or not. */
    bool bAutobaudCallback = true;

    /* Pinmux initialization. */
    adi_initpinmux();
    
    do
    {
        /* Power initialization. */
        if(ADI_PWR_SUCCESS != adi_pwr_Init())
        {
            DEBUG_MESSAGE("Failed to intialize the power service.");
            break;
        }
        
        /* System clock initialization. */
        if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_HCLK, 1u))
        {
            DEBUG_MESSAGE("Failed to intialize the system clock.");
            break;
        }
        
        /* Peripheral clock initialization. */
        if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_PCLK, 1u))
        {
            DEBUG_MESSAGE("Failed to intialize the peripheral clock.");
            break;
        }
        
        /* Open the UART device. */
        if(ADI_UART_SUCCESS != adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice))
        {
            DEBUG_MESSAGE("Failed to open the UART device.");
            break;
        }

        /* Register a callback to test callback mode. */
        if(adi_uart_RegisterCallback(hDevice, UARTCallback, hDevice) != ADI_UART_SUCCESS)
        {
            DEBUG_MESSAGE("Call back registration failed");
            break;
        }

        /* Enable autobaud. */
        if(adi_uart_EnableAutobaud(hDevice, true,bAutobaudCallback) != ADI_UART_SUCCESS)
        {
            DEBUG_MESSAGE("Failed to enable autobaud");
            break;
        }

        /* Wait until the baudrate has been detected. */          
        do
        {
            adi_uart_GetBaudRate(hDevice, &nBaudrate, &nAutobaudError);
            
        }while(nBaudrate == 0);

        /* Disable autobaud. */
        if(adi_uart_EnableAutobaud(hDevice, false, false) != ADI_UART_SUCCESS)
        {
            DEBUG_MESSAGE("Failed to disable autobaud");
            break;
        }            
        
        /* Close the device. */
        if(adi_uart_Close(hDevice)!= ADI_UART_SUCCESS)
        {
            DEBUG_MESSAGE("Failed to close the device");
        }  
    }while(0);
    
    /* Check to make sure we reached the callback, if we chose to use it. */
    if((bCallback == false) && (bAutobaudCallback == true))
    {
      DEBUG_MESSAGE("Failed to reach the autobaud success callback."); 
    }
    if ((nBaudrate > 9400u) && (nBaudrate < 9800u))
    {
      common_Pass();
    }
    else
      common_Fail("Failed to detect and accurate baud rate.");      

    return(0);
}
#endif

int main( void )
{  
    /* Variable to store the baudrate. */
   uint32_t nBaudrate=10;
    uint32_t        nHardwareError;
    /* Variable to store any autobaud errors. */
    uint32_t nAutobaudError=10u;
    bool bAutobaudCallback = true;
    bool bTxBufferComplete=false;
    bool      bResult,bEnable;
   static uint32_t pHwError;
   int fault=0;
   static char nChar=0x00;
   // uint8_t nChar=0x00;
    int Test_Res;
      adi_initpinmux();
      adi_pwr_Init();
      adi_pwr_SetClockDivider(ADI_CLOCK_HCLK, 1u);
      adi_pwr_SetClockDivider(ADI_CLOCK_PCLK, 1u);
#ifdef UART_OPEN
  /*****************************************************************/
  /*****************ADI_UART_SUCCESS*******************************/
  /***************************************************************/
  printf("************** UART TESTING *******************\n\n");   
  printf("==============UART_OPEN TESTING===============\n");
  Test_Res =adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
  if(Test_Res==ADI_UART_SUCCESS)
  {
    printf("PASS:adi_uart_Open & ADI_UART_SUCCESS \n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_Open & ADI_UART_SUCCESS \n");  
  }  
  adi_uart_Close(hDevice);
  /*****************************************************************/
  /*****************ADI_UART_INVALID_DEVICE_NUM********************/
  /***************************************************************/  
      Test_Res =adi_uart_Open(2, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
  if(Test_Res==ADI_UART_INVALID_DEVICE_NUM)
  {
    printf("PASS:adi_uart_Open & ADI_UART_INVALID_DEVICE_NUM \n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_Open & ADI_UART_INVALID_DEVICE_NUM \n");  
  } 
 
  adi_uart_Close(hDevice);
  /*****************************************************************/
  /***************** ADI_UART_INSUFFICIENT_MEMORY********************/
  /***************************************************************/  
Test_Res=adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_TRANSMIT, UartDeviceMem,0, &hDevice);
 if(Test_Res==ADI_UART_INSUFFICIENT_MEMORY)
  {
    printf("PASS:adi_uart_Open & ADI_UART_INSUFFICIENT_MEMORY \n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_Open & ADI_UART_INSUFFICIENT_MEMORY  \n");  
  }   
  
    adi_uart_Close(hDevice);
  /*****************************************************************/
  /***************** ADI_UART_DEVICE_IN_USE ********************/
  /***************************************************************/ 
  adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
Test_Res=adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
  if(Test_Res==ADI_UART_DEVICE_IN_USE )
  {
    printf("PASS:adi_uart_Open & ADI_UART_DEVICE_IN_USE \n\n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_Open & ADI_UART_DEVICE_IN_USE \n");  
  }  
#endif     
#ifdef UART_CLOSE  
  /*****************************************************************/
  /*****************ADI_UART_SUCCESS*******************************/
  /***************************************************************/
  printf("==============UART_CLOSE TESTING===============\n");
 adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
  Test_Res = adi_uart_Close(hDevice);
  if(Test_Res==ADI_UART_SUCCESS)
  {
    printf("PASS:adi_uart_Close & ADI_UART_SUCCESS \n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_Close & ADI_UART_SUCCESS \n");  
  }  
  /*****************************************************************/
  /*****************ADI_UART_INVALID_HANDLE*******************/
  /***************************************************************/  
    adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
    Test_Res = adi_uart_Close(hDevice1);
  if(Test_Res==ADI_UART_INVALID_HANDLE)
  {
    printf("PASS:adi_uart_Close & ADI_UART_INVALID_HANDLE \n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_Close & ADI_UART_INVALID_HANDLE \n");  
  } 
 /*****************************************************************/
  /***************** ADI_UART_DEVICE_IN_USE ********************/
  /***************************************************************/ 
  adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
adi_uart_EnableAutobaud(hDevice, true,bAutobaudCallback);
 Test_Res=adi_uart_Close(hDevice);

  if(Test_Res==ADI_UART_DEVICE_IN_USE )
  {
    printf("PASS:adi_uart_Close & ADI_UART_DEVICE_IN_USE \n\n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_Close & ADI_UART_DEVICE_IN_USE \n");  
  }  
 
#endif      
#ifdef UART_SETCONFIG
   /*****************************************************************/
  /*****************ADI_UART_SUCCESS*******************************/
  /***************************************************************/
  printf("==============UART_SetConfiguration TESTING===============\n");
  adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
 Test_Res=adi_uart_SetConfiguration(hDevice,0,1,5);
  if(Test_Res==ADI_UART_SUCCESS)
  {
    printf("PASS:adi_uart_SetConfiguration & ADI_UART_SUCCESS \n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_SetConfiguration & ADI_UART_SUCCESS \n");  
  }  
  adi_uart_Close(hDevice);
  
  /*****************************************************************/
  /***************** ADI_UART_DEVICE_IN_USE ********************/
  /***************************************************************/ 
  adi_uart_Open(UART_DEVICE_NUM,  ADI_UART_DIR_RECEIVE, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
adi_uart_EnableAutobaud(hDevice, true,bAutobaudCallback);
Test_Res=adi_uart_SetConfiguration(hDevice,0,1,5);
  if(Test_Res==ADI_UART_DEVICE_IN_USE )
  {
    printf("PASS:adi_uart_SetConfiguration & ADI_UART_DEVICE_IN_USE \n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_SetConfiguration & ADI_UART_DEVICE_IN_USE \n");  
  }  
    adi_uart_Close(hDevice);
  
  /*****************************************************************/
  /*****************ADI_UART_INVALID_HANDLE*******************/
  /***************************************************************/  
    adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
    Test_Res =adi_uart_SetConfiguration(hDevice1,0,1,5);
   if(Test_Res==ADI_UART_INVALID_HANDLE)
  {
    printf("PASS:adi_uart_SetConfiguration & ADI_UART_INVALID_HANDLE \n\n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_SetConfiguration & ADI_UART_INVALID_HANDLE \n");  
  }  
 adi_uart_Close(hDevice);
  
#endif  
#ifdef UART_CONFGBAUD
   /*****************************************************************/
  /*****************ADI_UART_SUCCESS*******************************/
  /***************************************************************/

  printf("==============UART_ConfigBaudRate TESTING===============\n");
  adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
   Test_Res=adi_uart_ConfigBaudRate(hDevice,4,1,1563,3);
  if(Test_Res==ADI_UART_SUCCESS)
  {
    printf("PASS:adi_uart_ConfigBaudRate & ADI_UART_SUCCESS \n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_ConfigBaudRate & ADI_UART_SUCCESS \n");  
  }  
  adi_uart_Close(hDevice);
  
  /*****************************************************************/
  /*****************ADI_UART_INVALID_HANDLE*******************/
  /***************************************************************/  
    adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
   Test_Res=adi_uart_ConfigBaudRate(hDevice1,4,1,1563,3);
   if(Test_Res==ADI_UART_INVALID_HANDLE)
  {
    printf("PASS:adi_uart_ConfigBaudRate& ADI_UART_INVALID_HANDLE \n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_ConfigBaudRate & ADI_UART_INVALID_HANDLE \n");  
  }  
 adi_uart_Close(hDevice);
 
 /*****************************************************************/
  /***************** ADI_UART_DEVICE_IN_USE ********************/
  /***************************************************************/ 
  adi_uart_Open(UART_DEVICE_NUM,  ADI_UART_DIR_RECEIVE, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
 Test_Res= adi_uart_Open(UART_DEVICE_NUM,  ADI_UART_DIR_RECEIVE, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);

adi_uart_ConfigBaudRate(hDevice,4,1,1563,3);
  if(Test_Res==ADI_UART_DEVICE_IN_USE )
  {
    printf("PASS:adi_uart_ConfigBaudRate & ADI_UART_DEVICE_IN_USE \n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_ConfigBaudRate & ADI_UART_DEVICE_IN_USE \n");  
  }  
    adi_uart_Close(hDevice);
    
  /*****************************************************************/
  /*****************  ADI_UART_INVALID_PARAMETER*******************************/
  /***************************************************************/
  adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
   Test_Res=adi_uart_ConfigBaudRate(hDevice,4,1000,1563,3);
  if(Test_Res==  ADI_UART_INVALID_PARAMETER)
  {
    printf("PASS:adi_uart_ConfigBaudRate & ADI_UART_INVALID_PARAMETER \n\n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_ConfigBaudRate &  ADI_UART_INVALID_PARAMETER \n");  
  }  
  adi_uart_Close(hDevice);
#endif
#ifdef UART_REGCALLBACK
   /*****************************************************************/
  /*****************ADI_UART_SUCCESS*******************************/
  /***************************************************************/
  printf("==============UART_RegisterCallback TESTING===============\n");
  adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
  Test_Res=adi_uart_RegisterCallback(hDevice, UARTCallback, hDevice);
  if(Test_Res==ADI_UART_SUCCESS)
  {
    printf("PASS:adi_uart_RegisterCallback & ADI_UART_SUCCESS \n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_RegisterCallback & ADI_UART_SUCCESS \n");  
  }  
  adi_uart_Close(hDevice);
  

  /*****************************************************************/
  /*******************ADI_UART_DEVICE_IN_USE*******************************/
  /***************************************************************/

  adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
adi_uart_EnableAutobaud(hDevice, true,bAutobaudCallback);
   Test_Res= adi_uart_RegisterCallback(hDevice, UARTCallback, hDevice);
  if(Test_Res==  ADI_UART_DEVICE_IN_USE)
  {
    printf("PASS:adi_uart_RegisterCallback & ADI_UART_DEVICE_IN_USE \n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_RegisterCallback & ADI_UART_DEVICE_IN_USE \n");  
  }  
  adi_uart_Close(hDevice);
  
/*****************************************************************/
  /*****************ADI_UART_INVALID_HANDLE*******************/
  /***************************************************************/  
    adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
   Test_Res=adi_uart_RegisterCallback(hDevice1, UARTCallback, hDevice);
   if(Test_Res==ADI_UART_INVALID_HANDLE)
  {
    printf("PASS:adi_uart_RegisterCallback & ADI_UART_INVALID_HANDLE \n\n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_RegisterCallback & ADI_UART_INVALID_HANDLE \n");  
  }  
 adi_uart_Close(hDevice);  
  
#endif
#ifdef UART_ENABLE_AUTOBAUD 
 /*****************************************************************/
  /*****************ADI_UART_SUCCESS*******************************/
  /***************************************************************/
  printf("==============UART_EnableAutobaud TESTING===============\n");
  adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
  Test_Res=adi_uart_EnableAutobaud(hDevice, true,bAutobaudCallback);
  if(Test_Res==ADI_UART_SUCCESS)
  {
    printf("PASS:adi_uart_EnableAutobaud & ADI_UART_SUCCESS \n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_EnableAutobaud & ADI_UART_SUCCESS \n");  
  }  
  adi_uart_Close(hDevice);
  
 /*****************************************************************/
  /*******************ADI_UART_DEVICE_IN_USE*******************************/
  /***************************************************************/

  adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
adi_uart_EnableAutobaud(hDevice, true,bAutobaudCallback);
   Test_Res= adi_uart_EnableAutobaud(hDevice, true,bAutobaudCallback);
  if(Test_Res==  ADI_UART_DEVICE_IN_USE)
  {
    printf("PASS:adi_uart_EnableAutobaud & ADI_UART_DEVICE_IN_USE \n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_EnableAutobaud  & ADI_UART_DEVICE_IN_USE \n");  
  }  
  adi_uart_Close(hDevice);
  
/*****************************************************************/
  /*****************ADI_UART_INVALID_HANDLE*******************/
  /***************************************************************/  
    adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
   Test_Res=adi_uart_EnableAutobaud(hDevice1, true,bAutobaudCallback);
   if(Test_Res==ADI_UART_INVALID_HANDLE)
  {
    printf("PASS:adi_uart_EnableAutobaud & ADI_UART_INVALID_HANDLE \n\n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_EnableAutobaud & ADI_UART_INVALID_HANDLE \n");  
  }  
 adi_uart_Close(hDevice);  
  
#endif 
#ifdef UART_GETBAUDRATE
/*****************************************************************/
/*****************ADI_UART_SUCCESS*******************************/
/***************************************************************/
  printf("==============UART_adi_uart_GetBaudRate TESTING===============\n");
  adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
  Test_Res=adi_uart_GetBaudRate(hDevice,&nBaudrate, &nAutobaudError);
  if(Test_Res==ADI_UART_SUCCESS)
  {
    printf("PASS:adi_uart_GetBaudRate & ADI_UART_SUCCESS \n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_GetBaudRate & ADI_UART_SUCCESS \n");  
  }  
  adi_uart_Close(hDevice);
 /*****************************************************************/
  /*****************ADI_UART_INVALID_HANDLE*******************/
  /***************************************************************/  
    adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
   Test_Res=adi_uart_GetBaudRate(hDevice1,&nBaudrate, &nAutobaudError)
   if(Test_Res==ADI_UART_INVALID_HANDLE)
  {
    printf("PASS:adi_uart_GetBaudRate & ADI_UART_INVALID_HANDLE \n\n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_GetBaudRate & ADI_UART_INVALID_HANDLE \n");  
  }  
 adi_uart_Close(hDevice);  

 /*****************************************************************/
  /*****************ADI_UART_INVALID_POINTER *******************/
  /***************************************************************/  
    adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
   Test_Res=adi_uart_GetBaudRate(hDevice,0, &nAutobaudError);
   if(Test_Res==ADI_UART_INVALID_HANDLE)
  {
    printf("PASS:adi_uart_GetBaudRate & ADI_UART_INVALID_HANDLE \n\n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_GetBaudRate & ADI_UART_INVALID_HANDLE \n");  
  }  
 adi_uart_Close(hDevice); 
#endif 
#ifdef UART_SUBMITBFR
  /*****************************************************************/
  /*****************ADI_UART_SUCCESS*******************************/
  /***************************************************************/
  printf("==============UART_SUBMITBFR TESTING===============\n");
  adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
 Test_Res =adi_uart_SubmitTxBuffer(hDevice, nBufferTx1, SIZE_OF_BUFFER, 0u);
  if(Test_Res==ADI_UART_SUCCESS)
  {
    printf("PASS:adi_uart_SubmitTxBuffer & ADI_UART_SUCCESS \n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_SubmitTxBuffer & ADI_UART_SUCCESS \n");  
  }  
  adi_uart_Close(hDevice);
 
  /*****************************************************************/
  /***************** ADI_UART_FAILED*****************************/
  /***************************************************************/
 
  adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
 Test_Res =adi_uart_SubmitTxBuffer(hDevice, nBufferTx1,NULL, 0u);
  if(Test_Res== ADI_UART_FAILED)
  {
    printf("PASS:adi_uart_SubmitTxBuffer &  ADI_UART_FAILED \n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_SubmitTxBuffer &  ADI_UART_FAILED \n");  
  }  
  adi_uart_Close(hDevice);
 
  /*****************************************************************/
  /*****************ADI_UART_INVALID_HANDLE*******************/
  /***************************************************************/  
 adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
 Test_Res=adi_uart_SubmitTxBuffer(hDevice1, nBufferTx1, SIZE_OF_BUFFER, 0u);
   if(Test_Res==ADI_UART_INVALID_HANDLE)
  {
    printf("PASS:adi_uart_SubmitTxBuffer & ADI_UART_INVALID_HANDLE \n\n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_SubmitTxBuffer & ADI_UART_INVALID_HANDLE \n");  
  }  
 adi_uart_Close(hDevice);  

 /*****************************************************************/
 /*******************ADI_UART_DEVICE_IN_USE***********************/
 /***************************************************************/
#if 0
  adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
adi_uart_EnableAutobaud(hDevice, true,bAutobaudCallback);
     adi_uart_SubmitTxBuffer(hDevice, nBufferTx1, SIZE_OF_BUFFER, 0u);
Test_Res=     adi_uart_SubmitTxBuffer(hDevice, nBufferTx1, SIZE_OF_BUFFER, 0u);
  if(Test_Res==  ADI_UART_DEVICE_IN_USE)
  {
    printf("PASS:adi_uart_SubmitTxBuffer & ADI_UART_DEVICE_IN_USE \n\n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_SubmitTxBuffer  & ADI_UART_DEVICE_IN_USE \n");  
  }  
  adi_uart_Close(hDevice);
 #endif  
#endif 
#ifdef UART_ENABLELOOP
 
  /*****************************************************************/
/*****************ADI_UART_SUCCESS*******************************/
/***************************************************************/
  printf("==============UART_adi_uart_EnableLoopBack TESTING===============\n");
  adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
  Test_Res= adi_uart_EnableLoopBack(hDevice,bEnable);
  if(Test_Res==ADI_UART_SUCCESS)
  {
    printf("PASS: adi_uart_EnableLoopBack & ADI_UART_SUCCESS \n");  
  } 
  else
  {   
    printf("FAIL: adi_uart_EnableLoopBack & ADI_UART_SUCCESS \n");  
  }  
  adi_uart_Close(hDevice);
 /*****************************************************************/
  /*****************ADI_UART_INVALID_HANDLE*******************/
  /***************************************************************/  
    adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
   Test_Res= adi_uart_EnableLoopBack(hDevice1,bEnable);
   if(Test_Res==ADI_UART_INVALID_HANDLE)
  {
    printf("PASS:adi_uart_EnableLoopBack & ADI_UART_INVALID_HANDLE \n\n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_EnableLoopBack  & ADI_UART_INVALID_HANDLE \n");  
  }  
 adi_uart_Close(hDevice);  

#endif  
  
#ifdef UART_ENABLEFIFO
 
  /*****************************************************************/
/*****************ADI_UART_SUCCESS*******************************/
/***************************************************************/
  printf("==============UART_adi_uart_EnableFifo TESTING===============\n");
  adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
  Test_Res= adi_uart_EnableFifo(hDevice,bEnable);
  if(Test_Res==ADI_UART_SUCCESS)
  {
    printf("PASS: adi_uart_EnableFifo & ADI_UART_SUCCESS \n");  
  } 
  else
  {   
    printf("FAIL: adi_uart_EnableLoopBack & ADI_UART_SUCCESS \n");  
  }  
  adi_uart_Close(hDevice);
 /*****************************************************************/
  /*****************ADI_UART_INVALID_HANDLE*******************/
  /***************************************************************/  
    adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
   Test_Res= adi_uart_EnableFifo(hDevice1,bEnable);
   if(Test_Res==ADI_UART_INVALID_HANDLE)
  {
    printf("PASS:adi_uart_EnableFifo & ADI_UART_INVALID_HANDLE \n\n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_EnableFifo  & ADI_UART_INVALID_HANDLE \n");  
  }  
 adi_uart_Close(hDevice);  

#endif  

   
#ifdef UART_FLUSHTXFIFO
 
  /*****************************************************************/
/*****************ADI_UART_SUCCESS*******************************/
/***************************************************************/
  printf("==============UART_adi_uart_FlushTxFifoTESTING===============\n");
  adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
  Test_Res=  adi_uart_FlushTxFifo(hDevice);
  if(Test_Res==ADI_UART_SUCCESS)
  {
    printf("PASS: adi_uart_FlushTxFifo & ADI_UART_SUCCESS \n");  
  } 
  else
  {   
    printf("FAIL: adi_uart_FlushTxFifo & ADI_UART_SUCCESS \n");  
  }  
  adi_uart_Close(hDevice);
 /*****************************************************************/
  /*****************ADI_UART_INVALID_HANDLE*******************/
  /***************************************************************/  
    adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
   Test_Res=  adi_uart_FlushTxFifo(hDevice1);
   if(Test_Res==ADI_UART_INVALID_HANDLE)
  {
    printf("PASS: adi_uart_FlushTxFifo & ADI_UART_INVALID_HANDLE \n\n");  
  } 
  else
  {   
    printf("FAIL: adi_uart_FlushTxFifo  & ADI_UART_INVALID_HANDLE \n");  
  }  
 adi_uart_Close(hDevice);  

#endif    
 
 #ifdef UART_FLUSHRXFIFO
 
  /*****************************************************************/
/*****************ADI_UART_SUCCESS*******************************/
/***************************************************************/
  printf("==============UART_adi_uart_FlushRxFifoTESTING===============\n");
  adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
  Test_Res=  adi_uart_FlushRxFifo(hDevice);
  if(Test_Res==ADI_UART_SUCCESS)
  {
    printf("PASS: adi_uart_FlushTxFifo & ADI_UART_SUCCESS \n");  
  } 
  else
  {   
    printf("FAIL:adi_uart_FlushTxFifo& ADI_UART_SUCCESS \n");  
  }  
  adi_uart_Close(hDevice);
 /*****************************************************************/
  /*****************ADI_UART_INVALID_HANDLE*******************/
  /***************************************************************/  
    adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice);
   Test_Res=  adi_uart_FlushTxFifo(hDevice1);
   if(Test_Res==ADI_UART_INVALID_HANDLE)
  {
    printf("PASS: adi_uart_FlushRxFifo & ADI_UART_INVALID_HANDLE \n\n");  
  } 
  else
  {   
    printf("FAIL: adi_uart_FlushRxFifo  & ADI_UART_INVALID_HANDLE \n");  
  }  
 adi_uart_Close(hDevice);  

#endif    
}
