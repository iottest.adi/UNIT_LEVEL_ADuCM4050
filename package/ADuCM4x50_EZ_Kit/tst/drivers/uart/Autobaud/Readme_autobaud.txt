              Analog Devices, Inc. ADuCM4x50 Application Example
            
Project Name: autobaud

Description:  Demonstrates how to use the UART driver for autobaud detection 

Overview:
=========
    This example shows how to use UART device driver for baudrate detection. 
    The example opens a UART device, configures the device for baudrate detection. 
    After receiving the key character (carrige return), it configure the UART device to the detected baudrate. 

User Configuration Macros:
==========================
    None. 

Hardware Setup:
===============
    ADuCM4x50 EX Kit configured with default settings.
 
External connections:
=====================
    Connect host terminal using USB to UART connector using USB cable. 

How to build and run:
=====================
    Build the project, load the executable to ADuCM4x50, and run it. 

    Open host utilities such as teraterm. 

    Hit the enter key (carrige return) to detect the baud rate. 

    Enter any character and it will be echoed on host terminal. Enter "Q" to end the example. 

Expected Result:
=====================
    The string 

    "If you can read this then the baudrate was successfully detected!
     The baudrate is: XXXX
     Note: This is using integer precision so it could vary slightly from the actual baudrate." 

    should be printed to the terminal. From here you can continue to echo back characters.
    
    Any errors will be printed to terminal IO.
 
References:
===========
    ADuCM4x50 Hardware Reference Manual
