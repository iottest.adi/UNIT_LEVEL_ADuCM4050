/*
 *****************************************************************************
 * @file:    uart_loop_back.h
 * @brief:   File which contain "main" for testing UART Device Driver.
 *****************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*!
* @file      autobaud.h
*
* @brief     This is file is include file for contain an autobaud example.
*
*/
#include <stdio.h>


/* Memory required by the driver for DMA mode of operation */
#define ADI_UART_MEMORY_SIZE    (ADI_UART_BIDIR_MEMORY_SIZE)

/* UART device number. There are 2 devices so that can either be a 0 or 1. */
#define UART_DEVICE_NUM 0
