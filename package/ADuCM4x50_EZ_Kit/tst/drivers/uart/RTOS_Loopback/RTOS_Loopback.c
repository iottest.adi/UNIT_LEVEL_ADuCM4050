/*********************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*!
 * @file      Loopback.c
 * @brief     This file contains an example that demonstrates loopback with
 *            UART device driver APIs using Micrium's UcosIII RTOS.
 */

/*=============  I N C L U D E S   =============*/

#include "os.h"
#include <stdio.h>
#include <stdlib.h>
#include <drivers/uart/adi_uart.h>
#include <drivers/pwr/adi_pwr.h>
#include <drivers/dma/adi_dma.h>
#include "RTOS_Loopback.h"
#include "common.h"


/*=============  D E F I N E S   =============*/

#define TASK_LOOPBACK_STK_SIZE   (200u)
#define TASK_LOOPBACK_PRIO        (10u)

/*=============  R T O S D A T A   =============*/

static OS_TCB g_Task_Loopback_TCB;

static CPU_STK g_Task_Loopback_Stack  [TASK_LOOPBACK_STK_SIZE];


/*=============  V A R I A B L E S   =============*/ 

/* Handle for the UART device. */
static ADI_UART_HANDLE hDevice;

/* Memory for the UART driver. */
static uint8_t UartDeviceMem[ADI_UART_MEMORY_SIZE];

/* First Tx  Buffer. */
static char  nBufferTx0[SIZE_OF_BUFFER]="ABCDEFGHIJKLMNOPQRSTUVWXYZ";

/* First Rx  Buffer. */
static uint8_t nBufferRx0[SIZE_OF_BUFFER];

/* Second Tx  Buffer. */
static char  nBufferTx1[SIZE_OF_BUFFER]="abcdefghijklmnopqrstuvwxyz";

/* Second Rx  Buffer. */
static uint8_t nBufferRx1[SIZE_OF_BUFFER];

/*********************************************************************

    Function:       LoopbackTask

    Description:    This is the main task in the example. It sets up 
                    the UART device, it submits Rx and Tx buffers
                    for loopback and then checks the output. 

*********************************************************************/
void LoopbackTask(void* arg)
{
    /* Variable used to check if the Rx buffer has been filled. */
    bool bRxBufferComplete = false;
    
    /* Variable that keeps track of any errors found. */
    bool bResult = true;
    
    /* Variable to confirm the transmit shift register is empty before closing the device. */
    bool bTxComplete = false;
    
    /* Variable to catch any errors when there is not a callback registered. */
    uint32_t pHwError;
    
    /* Variable to get the address of the processed buffer.*/
    void *pProcTxBuff0, *pProcRxBuff0, *pProcTxBuff1, *pProcRxBuff1;
    
    /* Variable to get the buffer size to compare the buffer contents at the end of this test. */
    uint32_t nCounter = 0u;
    
    /* Timeout nCounter for waiting on a receive buffer to be filled. */
    uint32_t nTimeout = 0u;
    
    /* Pinmux initialization. */
    adi_initpinmux();
    
    do{            

          /* Open the bidirectional UART device. */
          if(adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice) != ADI_UART_SUCCESS)
          {
              DEBUG_MESSAGE("Failed to open the UART device.");
              bResult = false;
              break;
          }
        
          /* Submit an empty buffer to the driver for receiving data using interrupt mode. */
          if(adi_uart_SubmitRxBuffer(hDevice, nBufferRx0, SIZE_OF_BUFFER, 0u) != ADI_UART_SUCCESS)
          {
              DEBUG_MESSAGE("Failed to submit the Rx buffer 0 using interrupt mode.");
              bResult = false;
              break;
          }

          /* Submit an empty buffer to the driver for receiving data using interrupt mode. */
          if(adi_uart_SubmitRxBuffer(hDevice, nBufferRx1, SIZE_OF_BUFFER, 0u) != ADI_UART_SUCCESS)
          {
              DEBUG_MESSAGE("Failed to submit the Rx buffer 1 using dma mode.");
              bResult = false;
              break;
          }
                     
          /* Submit a filled buffer to the driver using interrupt mode. This data will be what fills an empty Rx buffer. */
          if(adi_uart_SubmitTxBuffer(hDevice, nBufferTx0, SIZE_OF_BUFFER, 0u) != ADI_UART_SUCCESS)
          {
              DEBUG_MESSAGE("Failed to submit Tx buffer 0 using dma mode.");
              bResult = false;
              break;
          }          
          
          /* Submit a filled buffer to the driver using interrupt mode. This data will be what
             fills an empty Rx buffer. 
          */
          if(adi_uart_SubmitTxBuffer(hDevice, nBufferTx1, SIZE_OF_BUFFER, 0u) != ADI_UART_SUCCESS)
          {
              DEBUG_MESSAGE("Failed to submit Tx buffer 1 using interrupt mode.");
              bResult = false;
              break;
          }              
          
          /* Return the buffer back to the API. "pProcTxBuff0" should contain the address
             of "nBufferTx0".
          */
          if(adi_uart_GetTxBuffer(hDevice, &pProcTxBuff0, &pHwError) != ADI_UART_SUCCESS)
          {
              DEBUG_MESSAGE("Failed to get Tx buffer 0.");
              bResult = false;
              break;
          }
          
          /* Wait here until the processing of the first Rx buffer has completed or a timeout occured. */
          while((bRxBufferComplete == false) && (nTimeout != UART_GET_BUFFER_TIMEOUT))
          {   

              if(adi_uart_IsRxBufferAvailable(hDevice, &bRxBufferComplete) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to check if the Rx buffer 0 is available.");
                  bResult = false;
                  break;
              }
              nTimeout++;
          }
          
          /* Make sure there was not a timeout. */     
          if(nTimeout == UART_GET_BUFFER_TIMEOUT)
          { 
              DEBUG_MESSAGE("Timeout. Check loopback connection.");
              bResult = false;
              break;
          }
          else
          {
            nTimeout = 0u;
          }
          
          /* Return the buffer back to the API. "pProcRxBuff0" should contain the address
             of "nBufferRx0" and content of size "nBufferRx0". (i.e SIZE_OF_BUFFER)
          */    
          if(adi_uart_GetRxBuffer(hDevice, &pProcRxBuff0, &pHwError) != ADI_UART_SUCCESS)
          {
              DEBUG_MESSAGE("Failed to get Rx buffer 0.");
              bResult = false;
              break;
          }
          
          /* Wait here until the processing of the second Rx buffer has completed or a timeout occured. */
          while((bRxBufferComplete == false) && (nTimeout != UART_GET_BUFFER_TIMEOUT))
          {   

              if(adi_uart_IsRxBufferAvailable(hDevice, &bRxBufferComplete) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to check if the Rx buffer 1 is available.");
                  bResult = false;
                  break;
              }
              nTimeout++;
          }
          
          /* Make sure there was not a timeout. */     
          if(nTimeout == UART_GET_BUFFER_TIMEOUT)
          { 
              DEBUG_MESSAGE("Timeout. Check loopback connection.");
              bResult = false;
              break;
          }
          else
          {
            nTimeout = 0u;
          }
          
          /* Return the buffer back to the API. "pProcRxBuff1" should contain the address
             of "nBufferRx1" and content of size "nBufferRx1". (i.e SIZE_OF_BUFFER)
          */    
          if(adi_uart_GetRxBuffer(hDevice, &pProcRxBuff1, &pHwError) != ADI_UART_SUCCESS)
          {
              DEBUG_MESSAGE("Failed to get Rx buffer 1.");
              bResult = false;
              break;
          }
          
          /* Return the buffer back to the API. "pProcTxBuff1" should contain the address
             of "nBufferTx1".
          */
          if(adi_uart_GetTxBuffer(hDevice, &pProcTxBuff1, &pHwError) != ADI_UART_SUCCESS)
          {
              DEBUG_MESSAGE("Failed to get Tx buffer 1.");
              bResult = false;
              break;
          }
          
          /* Make sure the transmit shift register is empty before closing the device. */
          while(bTxComplete == false)
          {
              if(adi_uart_IsTxComplete(hDevice, &bTxComplete) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to check if the transmit holding register is empty.");
                  bResult = false;
                  break;
              }
          }
          
          /* Close the device. */
          if((adi_uart_Close(hDevice)) != ADI_UART_SUCCESS)
          {
              DEBUG_MESSAGE("Failed to close the device");
              bResult = false;
          }
    }while(0);
        
    if(bResult == true)
    {
        /* Make sure the data transfers all worked properly. */
        for(nCounter = 0u; nCounter < SIZE_OF_BUFFER; nCounter++)
        {
           if((nBufferTx0[nCounter] != nBufferRx0[nCounter]) || ( nBufferTx1[nCounter] != nBufferRx1[nCounter]))
           {
              bResult = false;
              DEBUG_MESSAGE("\nDetected a mismatch in Rx and Tx buffers.");
              break;
           }
        }  
    }
  
    if(bResult == true)
    {
        common_Pass();
        exit(0);
    }
    else
    {   
      common_Fail("UART loop back example failed.");
      exit(1);
    }
}

/*********************************************************************
*
*   Function:   main
*
*********************************************************************/
int main (void)
{
    adi_initpinmux();

    /* Initialize power service */
    if((adi_pwr_Init())!= ADI_PWR_SUCCESS )
    {
        DEBUG_MESSAGE("Power-init  failed");
        return(0);
    }
    /* Set core clock divider to "1" which sets it to 26Mhz*/
    if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_HCLK,1))
    {
        DEBUG_MESSAGE("Failed to intialize the power service\n");
        return(0);
    }
    /* Set peripheral clock  divider to "1" which sets it to 26Mhz*/    
    if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_PCLK,1))
    {
        DEBUG_MESSAGE("Failed to intialize the power service\n");
        return(0);
    }
    
    OS_ERR OSRetVal;

    /* Initialize the RTOS */
    OSInit(&OSRetVal);
    if (OSRetVal != OS_ERR_NONE)
    {
        printf("Failed to initialize the RTOS \n");
        return(0);
    }

    /* Create the Start-up Task*/
    OSTaskCreate (&g_Task_Loopback_TCB, /* address of TCB */
        "Loopback Task",                /* Task name */
        LoopbackTask,                   /* Task "Run" function */
        NULL,                           /* arg for the "Run" function*/
        TASK_LOOPBACK_PRIO,             /* priority */
        g_Task_Loopback_Stack,          /* base of the stack */
        TASK_LOOPBACK_STK_SIZE - 1u ,   /* limit for stack growth */
        TASK_LOOPBACK_STK_SIZE,         /* stack size in CPU_STK */
        0u,                             /* number of messages allowed */
        (OS_TICK)  0u,                  /* time_quanta */
        NULL,                           /* extension pointer */
        (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
        &OSRetVal);

    if (OSRetVal != OS_ERR_NONE)
    {
        printf("Failed to create startup task\n");
        return(0);
    }

    /* Start OS */
    OSStart(&OSRetVal);

    if (OSRetVal != OS_ERR_NONE)
    {
        printf("Failed to start RTOS\n");
    }
}
               
/*****/

