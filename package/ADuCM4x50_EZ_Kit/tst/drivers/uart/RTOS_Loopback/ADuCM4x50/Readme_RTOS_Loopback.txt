            Analog Devices, Inc. ADuCM4x50 Application

Description:  RTOS_Loopback

Overview:
====================
    This example shows how to use UART device for transmitting/receiving data. 
    It opens a UART device and configures the device to perform loopback using an RTOS task. 
    A data buffer is filled with known values and submitted to the UART device for 
    transmitting. Similarly, an empty buffer is submitted to UART device 
    for storing the received data. At the end of the example, content of received 
    buffers are verified aganist the content of transmit buffers. This example runs
    in both DMA as well as PIO mode.

    The UART driver supports both nonblocking and blocking calls, which are 
    demonstrated in this example. A nonblocking call is turned into a blocking
    call with "adi_uart_GetTxBuffer" and "adi_uart_GetRxBuffer".
    
    Please note that the external hardware connection for this loopback example 
    is not the same as testing the internal loopback hardware on the device.

User Configuration Macros:
==========================
    ADI_UART_CFG_DIVN
    ADI_UART_CFG_DIVM
    ADI_UART_CFG_DIVC
    ADI_UART_CFG_OSR

Hardware Setup:
===============
   ADuCM4050 EZ Kit configured using default hardware settings with the following jumper settings.
   For UART 0:
        J6 - Connect jumper between pins 1 and 2 (first two pins). 
   For UART 1:
        P7 - Connect jumper between pins 2 and 3 (middle two pins). 
 
   Note: By default the example is set to use UART 1 in the software. This means that the variable  
   UART_DEVICE_NUM in uart_loop_back.h is set to "1". To use UART 0 in the hardware, UART_DEVICE_NUM 
   would need to be changed to "0" in the software.  
   
External connections:
=====================
    None. 
    
How to build and run:
=====================
    Set the baud rate using the fractional divide macros listed in 'adi_uart_config.h' file. 
    Please use the utility "UartDivCalculator.exe" provided along with the BSP package 
    to generate the optimum values used to configure the device for different baud rates. 

    Prepare hardware as explained in the Hardware Setup section.

    Build the project, load the executable to ADuCM4050, and run it. 

    Look for the debug information displayed on terminal IO. 

Expected Result:
=====================
    Upon successful completion the program should output:
    
    "All done!"
        
References:
===========
    ADuCM4x50 Hardware Reference Manual



