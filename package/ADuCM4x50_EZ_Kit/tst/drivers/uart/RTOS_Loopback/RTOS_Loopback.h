
/*****************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*!
* @file      RTOS_Loopback.h
*
* @brief     Primary header file for loopback example using 
*            UART device driver APIs and Micrium's UcosIII RTOS..
*
*/

#ifndef UART_LOOPBACK_H
#define UART_LOOPBACK_H

#include <stdio.h>

/* Pin Muxing. */
extern void adi_initpinmux();

/* UART Callback. */
static void UARTCallback(void        *pAppHandle,
                         uint32_t     nEvent,
                         void        *pArg);

/* Memory required by the driver for bidirectional mode of operation. */
#define ADI_UART_MEMORY_SIZE    (ADI_UART_BIDIR_MEMORY_SIZE)

/* Size of the data buffers that will be processed. */
#define SIZE_OF_BUFFER  26u

/* UART device number. There are 2 devices, so this can be 0 or 1. */
#define UART_DEVICE_NUM 1u

/* Timeout value for receiving data. */
#define UART_GET_BUFFER_TIMEOUT 1000000u

#endif /* UART_LOOPBACK_H */