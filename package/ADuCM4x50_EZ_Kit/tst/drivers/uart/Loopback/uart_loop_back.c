/*
 *****************************************************************************
 * @file:    uart_loop_back.c
 * @brief:   File which contain "main" for testing UART Device Driver.
 *****************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*!
* @file      uart_loop_back.c
*
* @brief     This file contains an example that demonstrates loopback with
*            UART device driver APIs.
*
*/
#include <stdio.h>
#include <drivers/uart/adi_uart.h>
#include <drivers/pwr/adi_pwr.h>
#include "uart_loop_back.h"
#include "common.h"

extern void adi_initpinmux(void);

/* Handle for the UART device. */
static ADI_UART_HANDLE hDevice;

/* Memory for the UART driver. */
static uint8_t UartDeviceMem[ADI_UART_MEMORY_SIZE];


/* First Tx  Buffer. */
static char  nBufferTx0[SIZE_OF_BUFFER]="ABCDEFGHIJKLMNOPQRSTUVWXYZ";

/* Second Tx  Buffer. */
static char  nBufferTx1[SIZE_OF_BUFFER]="abcdefghijklmnopqrstuvwxyz";

/* Third Tx  Buffer. */
static char  nBufferTx2[SIZE_OF_BUFFER]="01234567891011121314151617";

#if 0
/* First Rx  Buffer. */
static uint8_t nBufferRx0[SIZE_OF_BUFFER];

/* Second Rx  Buffer. */
static uint8_t nBufferRx1[SIZE_OF_BUFFER];

/* Third Rx  Buffer. */
static uint8_t nBufferRx2[SIZE_OF_BUFFER];
#endif

/* Fourth Rx  Buffer. */
static uint8_t nBufferRx3[SIZE_OF_BUFFER];

/* Fifth Rx  Buffer. */
static uint8_t nBufferRx4[SIZE_OF_BUFFER];

/* Sixth Rx  Buffer. */
static uint8_t nBufferRx5[SIZE_OF_BUFFER];

/* Variables to count the number of times a buffer was processed using the UART callback. */
static uint32_t nRxCallbackCounter = 0u;
static uint32_t nTxCallbackCounter = 0u;

/*********************************************************************

    Function:       UARTCallback

    Description:    In the example, we've configured the inbound buffer
                    to generate a callback when it is full and an outboud
                    buffer to generate an callback once it is empty. 

*********************************************************************/
static void UARTCallback(
    void        *pAppHandle,
    uint32_t     nEvent,
    void        *pArg)
{
    /* CASEOF (ADI_UART_EVENT) */
    switch (nEvent)
    {
        /* CASE (Tx buffer processed) */
        case ADI_UART_EVENT_TX_BUFFER_PROCESSED:
             nTxCallbackCounter++;
             break;
        /* CASE (Rx buffer processed) */
        case ADI_UART_EVENT_RX_BUFFER_PROCESSED:
             nRxCallbackCounter++;
             break;
        default:
             break;
    }
}

/*********************************************************************
*
*   Function:   main
*
*********************************************************************/
int main(void)
{     
	/* Variable used to check if the Rx buffer has been filled. */
	bool bRxBufferComplete = false;
        
        /* Variable that keeps track of any errors found. */
        bool bResult = true;
        
        /* Variable to confirm the transmit shift register is empty before closing the device. */
        bool bTxComplete = false;
        
        /* Variable to catch any errors when there is not a callback registered. */
        static uint32_t pHwError;
        
	/* Variable to get the address of the processed buffer.*/
        void *pProcTxBuff0, *pProcRxBuff0, *pProcTxBuff1, *pProcRxBuff1;
	
        /* Variable to get the buffer size to compare the buffer contents at the end of this test. */
        uint32_t i;
        
        /* Pinmux initialization. */
        adi_initpinmux();
#if 0
/******************************** Test UART 0 ************************************/       
        do{            
              /* Power initialization. */
              if(ADI_PWR_SUCCESS != adi_pwr_Init())
              {
                  DEBUG_MESSAGE("Failed to intialize the power service.");
                  bResult = false;
                  break;
              }
              
              /* System clock initialization. */
              if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_HCLK, 1u))
              {
                  DEBUG_MESSAGE("Failed to intialize the system clock.");
                  bResult = false;
                  break;
              }
              
              /* Peripheral clock initialization. */
              if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_PCLK, 1u))
              {
                  DEBUG_MESSAGE("Failed to intialize the peripheral clock.");
                  bResult = false;
                  break;
              }

              /* Open the UART device bidirectional. */
              if(ADI_UART_SUCCESS != adi_uart_Open(0, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice))
              {
                  DEBUG_MESSAGE("Failed to open the UART device.");
                  bResult = false;
                  break;
              }
            
              /* Submit an empty buffer to the driver for receiving data using interrupt mode. */
              if(adi_uart_SubmitRxBuffer(hDevice, nBufferRx0, SIZE_OF_BUFFER, 0u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 0 using interrupt mode.");
                  bResult = false;
                  break;
              }

              /* Submit an empty buffer to the driver for receiving data using interrupt mode. */
              if(adi_uart_SubmitRxBuffer(hDevice, nBufferRx1, SIZE_OF_BUFFER, 0u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 1 using dma mode.");
                  bResult = false;
                  break;
              }
                         
              /* Submit a filled buffer to the driver using interrupt mode. This data will be what fills an empty Rx buffer. */
              if(adi_uart_SubmitTxBuffer(hDevice, nBufferTx0, SIZE_OF_BUFFER, 0u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit Tx buffer 0 using dma mode.");
                  bResult = false;
                  break;
              }          
              
              /* Submit a filled buffer to the driver using interrupt mode. This data will be what
                 fills an empty Rx buffer. 
              */
              if(adi_uart_SubmitTxBuffer(hDevice, nBufferTx1, SIZE_OF_BUFFER, 0u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit Tx buffer 1 using interrupt mode.");
                  bResult = false;
                  break;
              }              
             
              /* Wait here until the processing of the first buffer has completed. */
              while(bRxBufferComplete == false)
              {
                  if(ADI_UART_SUCCESS != adi_uart_IsRxBufferAvailable(hDevice, &bRxBufferComplete))
                  {
                      DEBUG_MESSAGE("Failed to check if the Rx buffer 0 is available.");
                      bResult = false;
                      break;
                  }
              }
              
              /* Return the buffer back to the API. "pProcTxBuff0" should contain the address
                 of "nBufferTx0".
              */
              if(adi_uart_GetTxBuffer(hDevice, &pProcTxBuff0, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Tx buffer 0.");
                  bResult = false;
                  break;
              }
              
               /* Return the buffer back to the API. "pProcRxBuff0" should contain the address
                  of "nBufferRx0" and content of size "nBufferRx0". (i.e 26)
               */    
              if(adi_uart_GetRxBuffer(hDevice, &pProcRxBuff0, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 0.");
                  bResult = false;
                  break;
              }
              
               /* Return the buffer back to the API. "pProcRxBuff1" should contain the address
                  of "nBufferRx1" and content of size "nBufferRx1". (i.e 26)
               */    
              if(adi_uart_GetRxBuffer(hDevice, &pProcRxBuff1, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 1.");
                  bResult = false;
                  break;
              }
              
               /* Return the buffer back to the API. "pProcTxBuff1" should contain the address
                  of "nBufferTx1".
               */
              if(adi_uart_GetTxBuffer(hDevice, &pProcTxBuff1, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Tx buffer 1.");
                  bResult = false;
                  break;
              }
              
              /* Register a callback. */
              if(adi_uart_RegisterCallback(hDevice, UARTCallback, NULL) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to register the UART callback.");
                  bResult = false;
                  break;
              }
              
              /* Submit an empty buffer for receiving data using DMA mode. */
              if(adi_uart_SubmitRxBuffer(hDevice, nBufferRx2, SIZE_OF_BUFFER, 1u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 2.");
                  bResult = false;
                  break;
              }
       

              /* Submit a filled buffer using DMA mode. This data will be what fills an empty Rx buffer. */
              if(adi_uart_SubmitTxBuffer(hDevice, nBufferTx2, SIZE_OF_BUFFER, 1u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit Tx buffer 2.");
                  bResult = false;
                  break;
              }
        
               /* Return the buffer back to the API. "pProcTxBuff2" should contain the address
                  of "nBufferTx2".
               */
              if(adi_uart_GetTxBuffer(hDevice, &pProcTxBuff2, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Tx buffer 2.");
                  bResult = false;
                  break;
              }
              
               /* Return the buffer back to the API. "pProcRxBuff2" should contain the address
                  of "nBufferRx2" and content should be size of "nBufferRx2". (i.e 26)
               */    
              if(adi_uart_GetRxBuffer(hDevice, &pProcRxBuff2, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 2.");
                  bResult = false;
                  break;
              }
              
              /* Make sure the transmit shift register is empty before closing the device. */
              while(bTxComplete == false)
              {
                  if(adi_uart_IsTxComplete(hDevice, &bTxComplete) != ADI_UART_SUCCESS)
                  {
                      DEBUG_MESSAGE("Failed to check if the transmit holding register is empty.");
                      bResult = false;
                      break;
                  }
              }
              
              /* Close the device. */
              if((adi_uart_Close(hDevice)) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to close the device");
                  bResult = false;
              }
        }while(0);
             
        /* Make sure the callback processed all of the expected buffers. */
        if (nTxCallbackCounter != nRxCallbackCounter)
        {
            bResult = false;
        }
        
        /* Make sure the data transfers all worked properly. */
        for(i = 0u; i < SIZE_OF_BUFFER; i++)
        {
           if((nBufferTx0[i] != nBufferRx0[i]) || ( nBufferTx1[i] != nBufferRx1[i]) || ( nBufferTx2[i] != nBufferRx2[i]))
           {
              bResult = false;
              break;
           }
        }  
       
        if(bResult == true)
        {
            DEBUG_MESSAGE("UART0 loop back  test completed successfully. \n");
        }
        else
        {
            DEBUG_MESSAGE("Failed UART 0: Difference in RX and TX buffer ");
            common_Fail("UART 0 Failed");
            return 0;
        }
#endif  
 /******************************** Test UART 1 ************************************/
        nRxCallbackCounter = 0u;
        nTxCallbackCounter = 0u;
        do {  
                        
              /* Power initialization. */
              if(ADI_PWR_SUCCESS != adi_pwr_Init())
              {
                  DEBUG_MESSAGE("Failed to intialize the power service.");
                  bResult = false;
                  break;
              }
              
              /* System clock initialization. */
              if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_HCLK, 1u))
              {
                  DEBUG_MESSAGE("Failed to intialize the system clock.");
                  bResult = false;
                  break;
              }
              
              /* Peripheral clock initialization. */
              if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_PCLK, 1u))
              {
                  DEBUG_MESSAGE("Failed to intialize the peripheral clock.");
                  bResult = false;
                  break;
              }
              
              /* Open the UART device bidirectional. */
              if(ADI_UART_SUCCESS != adi_uart_Open(1, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice))
              {
                  DEBUG_MESSAGE("Failed to open the UART device.");
                  bResult = false;
                  break;
              }
            
              /* Submit an empty buffer to the driver for receiving data using interrupt mode. */
              if(adi_uart_SubmitRxBuffer(hDevice, nBufferRx3, SIZE_OF_BUFFER, 0u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 0 using interrupt mode.");
                  bResult = false;
                  break;
              }

              /* Submit an empty buffer to the driver for receiving data using interrupt mode. */
              if(adi_uart_SubmitRxBuffer(hDevice, nBufferRx4, SIZE_OF_BUFFER, 0u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 1 using dma mode.");
                  bResult = false;
                  break;
              }
                         
              /* Submit a filled buffer to the driver using interrupt mode. This data will be what fills an empty Rx buffer. */
              if(adi_uart_SubmitTxBuffer(hDevice, nBufferTx0, SIZE_OF_BUFFER, 0u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit Tx buffer 0 using dma mode.");
                  bResult = false;
                  break;
              }          
              
              /* Submit a filled buffer to the driver using interrupt mode. This data will be what
                 fills an empty Rx buffer. 
              */
              if(adi_uart_SubmitTxBuffer(hDevice, nBufferTx1, SIZE_OF_BUFFER, 0u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit Tx buffer 1 using interrupt mode.");
                  bResult = false;
                  break;
              }              
             
              /* Wait here until the processing of the first buffer has completed. */
              while(bRxBufferComplete == false)
              {
                  if(ADI_UART_SUCCESS != adi_uart_IsRxBufferAvailable(hDevice, &bRxBufferComplete))
                  {
                      DEBUG_MESSAGE("Failed to check if the Rx buffer 0 is available.");
                      bResult = false;
                      break;
                  }
              }
              
              /* Return the buffer back to the API. "pProcTxBuff0" should contain the address
                 of "nBufferTx0".
              */
              if(adi_uart_GetTxBuffer(hDevice, &pProcTxBuff0, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Tx buffer 0.");
                  bResult = false;
                  break;
              }
              
               /* Return the buffer back to the API. "pProcRxBuff0" should contain the address
                  of "nBufferRx0" and content of size "nBufferRx0". (i.e 26)
               */    
              if(adi_uart_GetRxBuffer(hDevice, &pProcRxBuff0, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 0.");
                  bResult = false;
                  break;
              }
              
               /* Return the buffer back to the API. "pProcRxBuff1" should contain the address
                  of "nBufferRx1" and content of size "nBufferRx1". (i.e 26)
               */    
              if(adi_uart_GetRxBuffer(hDevice, &pProcRxBuff1, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 1.");
                  bResult = false;
                  break;
              }
              
               /* Return the buffer back to the API. "pProcTxBuff1" should contain the address
                  of "nBufferTx1".
               */
              if(adi_uart_GetTxBuffer(hDevice, &pProcTxBuff1, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Tx buffer 1.");
                  bResult = false;
                  break;
              }
              
              /* Register a callback. */
              if(adi_uart_RegisterCallback(hDevice, UARTCallback, NULL) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to register the UART callback.");
                  bResult = false;
                  break;
              }
              
              /* Submit an empty buffer for receiving data using DMA mode. */
              if(adi_uart_SubmitRxBuffer(hDevice, nBufferRx5, SIZE_OF_BUFFER, 1u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 2.");
                  bResult = false;
                  break;
              }
       

              /* Submit a filled buffer using DMA mode. This data will be what fills an empty Rx buffer. */
              if(adi_uart_SubmitTxBuffer(hDevice, nBufferTx2, SIZE_OF_BUFFER, 1u) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit Tx buffer 2.");
                  bResult = false;
                  break;
              }
              
              /* Make sure the transmit shift register is empty before closing the device. */
              while(bTxComplete == false)
              {
                  if(adi_uart_IsTxComplete(hDevice, &bTxComplete) != ADI_UART_SUCCESS)
                  {
                      DEBUG_MESSAGE("Failed to check if the transmit holding register is empty.");
                      bResult = false;
                      break;
                  }
              }
              
              /* Close the device. */
              if((adi_uart_Close(hDevice)) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to close the device");
                  bResult = false;
              }
        }while(0);
             
        /* Make sure the callback processed all of the expected buffers. */
        if (nTxCallbackCounter != nRxCallbackCounter)
        {
            bResult = false;
        }
        
        /* Make sure the data transfers all worked properly. */
        for(i = 0u; i < SIZE_OF_BUFFER; i++)
        {
           if((nBufferTx0[i] != nBufferRx3[i]) || ( nBufferTx1[i] != nBufferRx4[i]) || ( nBufferTx2[i] != nBufferRx5[i]))
           {
              bResult = false;
              break;
           }
        }  
       
        if(bResult == true)
        {
            DEBUG_MESSAGE("UART1 loop back  test completed successfully. \n");
            common_Pass();
        }
        else
        {
            DEBUG_MESSAGE("Failed UART 1: Difference in RX and TX buffer ");
            common_Fail("UART 1 Failed");
        }              
            
        return(0);
}
