            Analog Devices, Inc. ADuCM4x50 Application Example

Project Name: uart_loopback

Description:  Demonstrates how to use the UART driver. 
              The example transfers the content of one buffer to another by using the
              loopback feature of the device. The example also demonstrates how to enable DMA
              operation and register a callback. 
 

Overview:
=========
    This example shows how to use UART device for transmitting/receiving data. 
    It opens a UART device and configures the device to perform loopback. 
    A data buffer is filled with known values and submitted to the UART device for 
    transmitting. Similarly, an empty buffer is submitted to UART device 
    for storing the received data. At the end of the example, content of received 
    buffers are verified aganist the content of transmit buffers. This example runs
    in both DMA as well as callback mode. A counter in the callback is checked to
    make sure it was entered the correct number of times. 

    The UART driver supports both nonblocking and blocking calls, which are 
    demonstrated in this example. A nonblocking call is turned into a blocking
    call with "adi_uart_GetTxBuffer" and "adi_uart_GetRxBuffer".
    
    Please note that the external hardware connection for this loopback example 
    is not the same as testing the internal loopback hardware on the device.


User Configuration Macros:
==========================
    ADI_UART_CFG_DIVN
    ADI_UART_CFG_DIVM
    ADI_UART_CFG_DIVC
    ADI_UART_CFG_OSR

Hardware Setup:
===============
   ADuCM4050 EZ Kit configured with default settings with following jumper settings.
   By default the example is set to use UART 1.
   For UART 0:
        J6 - Connect jumper between pins 1 and 2 (first two pins). 
   For UART 1:
        P7 - Connect jumper between pins 2 and 3 (middle two pins). 
 
External connections:
=====================
    None. Please do not connect USB UART connector using USB cable 

How to build and run:
=====================
    Set the baud rate using the fractional divide macros listed in 'adi_uart_config.h' file. 
    Please use the utility "UartDivCalculator.exe" provided along with the BSP package 
    to generate the optimum values used to configure the device for different baud rates. 

    Prepare hardware as explained in the Hardware Setup section.

    Build the project, load the executable to ADuCM4050, and run it. 

    Look for the debug information displayed on terminal IO. 

Expected Result:
=====================
    Upon successful completion the program should output:
    "All done! UART loop back  example completed successfully."
    "Callback mode, nonblocking mode, interrupt mode and dma mode were all tested successfully."
        
References:
===========
    ADuCM4x50 Hardware Reference Manual
