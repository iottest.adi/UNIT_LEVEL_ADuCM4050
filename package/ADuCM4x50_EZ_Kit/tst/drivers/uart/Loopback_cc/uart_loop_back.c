/*
 *****************************************************************************
 * @file:    uart_loop_back.c
 * @brief:   File which contain "main" for testing UART Device Driver.
 *****************************************************************************

Copyright(c) 2017 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*!
* @file      uart_loop_back.c
*
* @brief     This file contains an example that demonstrates loopback with
*            UART device driver APIs.
*
*/
#include <stdio.h>
#include <drivers/uart/adi_uart.h>
#include <drivers/pwr/adi_pwr.h>
#include "uart_loop_back.h"
#include "common.h"
#include <adi_cyclecount.h>

extern void adi_initpinmux(void);

/* Handle for the UART device. */
static ADI_UART_HANDLE hDevice;

/* Memory for the UART driver. */
static uint8_t UartDeviceMem[ADI_UART_MEMORY_SIZE];


/* First Tx  Buffer. */
static char  nBufferTx0[SIZE_OF_BUFFER]="ABCDEFGHIJKLMNOPQRSTUVWXYZ";

/* Second Tx  Buffer. */
static char  nBufferTx1[SIZE_OF_BUFFER]="abcdefghijklmnopqrstuvwxyz";

/* First Rx  Buffer. */
static uint8_t nBufferRx0[SIZE_OF_BUFFER];

/* Second Rx  Buffer. */
static uint8_t nBufferRx1[SIZE_OF_BUFFER];

/* Variables to count the number of times a buffer was processed using the UART callback. */
static uint32_t nRxCallbackCounter = 0u;
static uint32_t nTxCallbackCounter = 0u;

uint32_t cycleCountId_uart_Rx = 0u;
uint32_t cycleCountId_uart_Tx = 0u;

static inline void start_counting(void);
static inline void stop_counting(uint32_t cycleCountId);

/*********************************************************************

    Function:       UARTCallback

    Description:    In the example, we've configured the inbound buffer
                    to generate a callback when it is full and an outboud
                    buffer to generate an callback once it is empty.

*********************************************************************/
static void UARTCallback(
    void        *pAppHandle,
    uint32_t     nEvent,
    void        *pArg)
{
    /* CASEOF (ADI_UART_EVENT) */
    switch (nEvent)
    {
        /* CASE (Tx buffer processed) */
        case ADI_UART_EVENT_TX_BUFFER_PROCESSED:
             nTxCallbackCounter++;
             break;
        /* CASE (Rx buffer processed) */
        case ADI_UART_EVENT_RX_BUFFER_PROCESSED:
             nRxCallbackCounter++;
             break;
        default:
             break;
    }
}

/*********************************************************************
*
*   Function:   main
*
*********************************************************************/
int main(void)
{
        /* Variable used to check if the Rx buffer has been filled. */
        bool bRxBufferComplete = false;

        /* Variable that keeps track of any errors found. */
        bool bResult = true;

        /* Variable to confirm the transmit shift register is empty before closing the device. */
        bool bTxComplete = false;

        /* Variable to catch any errors when there is not a callback registered. */
        static uint32_t pHwError;

        /* Variable to get the address of the processed buffer.*/
        void *pProcTxBuff0, *pProcRxBuff0;

        /* Variable to get the buffer size to compare the buffer contents at the end of this test. */
        uint32_t i;

        /* Pinmux initialization. */
        adi_initpinmux();

#if defined(ADI_CYCLECOUNT_ENABLED) && (ADI_CYCLECOUNT_ENABLED == 1u)
        ADI_CYCLECOUNT_INITIALIZE();                                            /* Initialize cycle counting */

        /* Obtain cycle counting IDs for core-driven UART Rx operations  */
        ADI_CYCLECOUNT_RESULT eCyclecountResult = adi_cyclecount_addEntity("UART Rx", &cycleCountId_uart_Rx);
        DEBUG_RESULT("Failed to add cycle counting ID for cycleCountId_uart_Rx", eCyclecountResult, ADI_CYCLECOUNT_SUCCESS);

        /* Obtain cycle counting IDs for core-driven UART Tx operations  */
        eCyclecountResult = adi_cyclecount_addEntity("UART Tx", &cycleCountId_uart_Tx);
        DEBUG_RESULT("Failed to add cycle counting ID for cycleCountId_uart_Tx", eCyclecountResult, ADI_CYCLECOUNT_SUCCESS);
#endif

 /******************************** Test UART 1 ************************************/
        nRxCallbackCounter = 0u;
        nTxCallbackCounter = 0u;
        do {
              ADI_UART_RESULT res;

              /* Power initialization. */
              if(ADI_PWR_SUCCESS != adi_pwr_Init())
              {
                  DEBUG_MESSAGE("Failed to intialize the power service.");
                  bResult = false;
                  break;
              }

              /* System clock initialization. */
              if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_HCLK, 1u))
              {
                  DEBUG_MESSAGE("Failed to intialize the system clock.");
                  bResult = false;
                  break;
              }

              /* Peripheral clock initialization. */
              if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_PCLK, 1u))
              {
                  DEBUG_MESSAGE("Failed to intialize the peripheral clock.");
                  bResult = false;
                  break;
              }

              /* Open the UART device bidirectional. */
              if(ADI_UART_SUCCESS != adi_uart_Open(1, ADI_UART_DIR_BIDIRECTION, UartDeviceMem, ADI_UART_MEMORY_SIZE, &hDevice))
              {
                  DEBUG_MESSAGE("Failed to open the UART device.");
                  bResult = false;
                  break;
              }

              /* Submit an empty buffer to the driver for receiving data using interrupt mode. */
              start_counting();
              res = adi_uart_SubmitRxBuffer(hDevice, nBufferRx0, SIZE_OF_BUFFER, 0u);
              stop_counting(cycleCountId_uart_Rx);
              if(res != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 0 using interrupt mode.");
                  bResult = false;
                  break;
              }

              /* Submit a filled buffer to the driver using interrupt mode. This data will be what fills an empty Rx buffer. */
              start_counting();
              res = adi_uart_SubmitTxBuffer(hDevice, nBufferTx0, SIZE_OF_BUFFER, 0u);
              stop_counting(cycleCountId_uart_Tx);
              if(res != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit Tx buffer 0 using dma mode.");
                  bResult = false;
                  break;
              }

              /* Wait here until the processing of the first buffer has completed. */
              while(bRxBufferComplete == false)
              {
                  if(ADI_UART_SUCCESS != adi_uart_IsRxBufferAvailable(hDevice, &bRxBufferComplete))
                  {
                      DEBUG_MESSAGE("Failed to check if the Rx buffer 0 is available.");
                      bResult = false;
                      break;
                  }
              }

              /* Return the buffer back to the API. "pProcTxBuff0" should contain the address
                 of "nBufferTx0".
              */
              if(adi_uart_GetTxBuffer(hDevice, &pProcTxBuff0, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Tx buffer 0.");
                  bResult = false;
                  break;
              }

               /* Return the buffer back to the API. "pProcRxBuff0" should contain the address
                  of "nBufferRx0" and content of size "nBufferRx0". (i.e 26)
               */
              if(adi_uart_GetRxBuffer(hDevice, &pProcRxBuff0, &pHwError) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to get Rx buffer 0.");
                  bResult = false;
                  break;
              }

              /* Register a callback. */
              if(adi_uart_RegisterCallback(hDevice, UARTCallback, NULL) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to register the UART callback.");
                  bResult = false;
                  break;
              }

              /* Submit an empty buffer for receiving data using DMA mode. */
              start_counting();
              res = adi_uart_SubmitRxBuffer(hDevice, nBufferRx1, SIZE_OF_BUFFER, 1u);
              stop_counting(cycleCountId_uart_Rx);
              if(res != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit the Rx buffer 1.");
                  bResult = false;
                  break;
              }


              /* Submit a filled buffer using DMA mode. This data will be what fills an empty Rx buffer. */
              start_counting();
              res = adi_uart_SubmitTxBuffer(hDevice, nBufferTx1, SIZE_OF_BUFFER, 1u);
              stop_counting(cycleCountId_uart_Tx);
              if(res != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to submit Tx buffer 1.");
                  bResult = false;
                  break;
              }

              /* Make sure the transmit shift register is empty before closing the device. */
              while(bTxComplete == false)
              {
                  if(adi_uart_IsTxComplete(hDevice, &bTxComplete) != ADI_UART_SUCCESS)
                  {
                      DEBUG_MESSAGE("Failed to check if the transmit holding register is empty.");
                      bResult = false;
                      break;
                  }
              }

              /* Close the device. */
              if((adi_uart_Close(hDevice)) != ADI_UART_SUCCESS)
              {
                  DEBUG_MESSAGE("Failed to close the device");
                  bResult = false;
              }
        }while(0);

        /* Make sure the callback processed all of the expected buffers. */
        if (nTxCallbackCounter != nRxCallbackCounter)
        {
            bResult = false;
        }

        /* Make sure the data transfers all worked properly. */
        for(i = 0u; i < SIZE_OF_BUFFER; i++)
        {
           if((nBufferTx0[i] != nBufferRx0[i]) || ( nBufferTx1[i] != nBufferRx1[i]))
           {
              bResult = false;
              break;
           }
        }

        if(bResult == true)
        {
            DEBUG_MESSAGE("UART1 loop back  test completed successfully. \n");
            common_Pass();
        }
        else
        {
            DEBUG_MESSAGE("Failed UART 1: Difference in RX and TX buffer ");
            common_Fail("UART 1 Failed");
        }

        /* Print the cycle counts obtained */
        ADI_CYCLECOUNT_REPORT();

        return(0);
}

static inline void start_counting(void)
{
#if defined(ADI_CYCLECOUNT_ENABLED) && (ADI_CYCLECOUNT_ENABLED == 1u)
    ADI_CYCLECOUNT_RESULT ccRes = adi_cyclecount_start();
    DEBUG_RESULT("Failed to execute adi_cyclecount_start",ccRes,ADI_CYCLECOUNT_SUCCESS);
#endif
}

static inline void stop_counting(uint32_t cycleCountId)
{
#if defined(ADI_CYCLECOUNT_ENABLED) && (ADI_CYCLECOUNT_ENABLED==1u)
    ADI_CYCLECOUNT_STORE(cycleCountId);
    ADI_CYCLECOUNT_RESULT ccRes = adi_cyclecount_stop();
    DEBUG_RESULT("Failed to execute adi_cyclecount_stop",ccRes,ADI_CYCLECOUNT_SUCCESS);
#endif
}
