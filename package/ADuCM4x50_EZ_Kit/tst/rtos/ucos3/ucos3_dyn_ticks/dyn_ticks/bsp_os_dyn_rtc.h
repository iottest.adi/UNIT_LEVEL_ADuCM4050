/*
*********************************************************************************************************
*                                     MICIRUM BOARD SUPPORT PACKAGE
*
*                          (c) Copyright 2003-2015; Micrium, Inc.; Weston, FL
*
*               All rights reserved.  Protected by international copyright laws.
*               Knowledge of the source code may NOT be used to develop a similar product.
*               Please help us continue to provide the Embedded community with the finest
*               software available.  Your honesty is greatly appreciated.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                       BOARD SUPPORT PACKAGE
*                                          uCOS-III LAYER
*                                         Dynamic Tick (RTC)
*
* Filename      : bsp_os_dyn_rtc.h
* Version       : V1.00
* Programmer(s) : JBL
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                                 MODULE
*
* Note(s) : (1) This header file is protected from multiple pre-processor inclusion through use of the
*               BSP present pre-processor macro definition.
*********************************************************************************************************
*/

#ifndef  BSP_OS_DYN_RTC_PRESENT
#define  BSP_OS_DYN_RTC_PRESENT


/*
*********************************************************************************************************
*                                              INCLUDE FILES
*********************************************************************************************************
*/

#include <os.h>
#include <drivers/rtc/adi_rtc.h>
#include <adi_rtc_config.h>

/*
*********************************************************************************************************
*                                              DEFINES
*********************************************************************************************************
*/

/*
 * It is possible in some situation for the kernel to ask for an unlimited tick.
 * In that case we set a long delay instead of disabling the tick in order not to lose the kernel timebase.
 */
#define BSP_OS_RTC_MAX_DELAY 100000

/* RTC is set to run at 32 KHz */
#define RTC_TICK_FREQ (32768ull / (1 << RTC1_CFG_PRESCALE))

/* Verify the RTC can meet the required resolution of the operating system */
#if (RTC_TICK_FREQ < OS_CFG_TICK_RATE_HZ)
    #error RTC is running to slow to meet the desired tick resolution
#endif

/*
*********************************************************************************************************
*                                                 EXTERNS
*********************************************************************************************************
*/

#ifdef   BSP_OS_MODULE
#define  BSP_OS_EXT
#else
#define  BSP_OS_EXT  extern
#endif

/* Need to extern this global so it can be used in cpu_bsp.c */
extern ADI_RTC_HANDLE  hRTCDevice;

/*
*********************************************************************************************************
*                                            FUNCTION PROTOTYPES
*********************************************************************************************************
*/

void         BSP_OS_RTC_TickInit   (void);

#endif /* BSP_OS_DYN_RTC_PRESENT */
