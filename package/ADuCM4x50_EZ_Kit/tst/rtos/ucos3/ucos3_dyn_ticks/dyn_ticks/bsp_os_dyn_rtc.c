/*
*********************************************************************************************************
*                                     MICIRUM BOARD SUPPORT PACKAGE
*
*                          (c) Copyright 2003-2015; Micrium, Inc.; Weston, FL
*
*               All rights reserved.  Protected by international copyright laws.
*               Knowledge of the source code may NOT be used to develop a similar product.
*               Please help us continue to provide the Embedded community with the finest
*               software available.  Your honesty is greatly appreciated.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                       BOARD SUPPORT PACKAGE
*                                          uCOS-III LAYER
*                                         Dynamic Tick (RTC)
*
* Filename      : bsp_os_dyn_rtc.c
* Version       : V1.00
* Programmer(s) : JBL
*********************************************************************************************************
*/


/*
*********************************************************************************************************
*                                             INCLUDE FILES
*********************************************************************************************************
*/

#define  BSP_OS_MODULE
#include <os_cfg_app.h>
#include <adi_processor.h>
#include <stdlib.h>
#include "bsp_os_dyn_rtc.h"
#include <adi_rtc_config.h>
#include <common.h>

#if (OS_CFG_DYN_TICK_EN == DEF_ENABLED)

/*
*********************************************************************************************************
*                                       LOCAL GLOBAL VARIABLES
*********************************************************************************************************
*/

/* Total Number of Elapsed Ticks */
 static CPU_INT64U BSP_OS_LastTick;

/* Current Number of Ticks To Wait */
 static CPU_INT32U BSP_OS_TicksToGo;

/* RTC Device Memory */
static uint8_t aRtcDevMem1[ADI_RTC_MEMORY_SIZE];

/* RTC Device Handle */
ADI_RTC_HANDLE  hRTCDevice;

/*
*********************************************************************************************************
*                                            FUNCTION PROTOTYPES
*********************************************************************************************************
*/

static void  RtcCallback(void *pCBParam, uint32_t nEvent, void *EventArg);

/*
*********************************************************************************************************
*********************************************************************************************************
**                                       uC/OS-III Low Power Tick
*********************************************************************************************************
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                         BSP_OS_RTC_TickInit()
*
* Description : Initialize the Low-Power Timer to enable dynamic ticking.
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Caller(s)   : BSP_Tick_Init.
*
*               This function is an INTERNAL uC/OS-III function & MUST NOT be called by application
*               function(s).
*
* Note(s)     : none.
*********************************************************************************************************
*/

void  BSP_OS_RTC_TickInit (void)
{
    /* Check if RTC has been initialized */
    if (hRTCDevice == (ADI_RTC_HANDLE) 0) {

    	/* Initialize it */

    	ADI_RTC_RESULT eResult;

	    /* Open RTC 1 timer, once opened it will be configured using the static
	       configuration settings in the file adi_rtc_config.h
	    */
	    eResult = adi_rtc_Open(1, aRtcDevMem1, ADI_RTC_MEMORY_SIZE, &hRTCDevice);
        if(ADI_RTC_SUCCESS != eResult)
        {
            DEBUG_MESSAGE("Error opening RTC driver");
            while (1) {};
        }

         /* Set Prescaler */
        eResult = adi_rtc_SetPreScale(hRTCDevice, RTC1_CFG_PRESCALE);
        if(ADI_RTC_SUCCESS != eResult)
        {
            DEBUG_MESSAGE("Failed to set the prescaler");
            while (1) {};
        }
	    /* Register the callback for alarms */
	    eResult = adi_rtc_RegisterCallback(hRTCDevice,RtcCallback,hRTCDevice);
        if(ADI_RTC_SUCCESS != eResult)
        {
            DEBUG_MESSAGE("Failed to register the callback function to the device");
            while (1) {};
        }

	    /* Enable RTC1 */
	    eResult = adi_rtc_Enable(hRTCDevice,true);
        if(ADI_RTC_SUCCESS != eResult)
        {
            DEBUG_MESSAGE("Failed to enable the device");
            while (1) {};
        }

	    /* Set the output compare value to generate the beacon pulse every 1 second */
	    eResult = adi_rtc_SetSensorStrobeValue(hRTCDevice, ADI_RTC_SS_CHANNEL_1, RTC1_CFG_SS1_AUTO_RELOAD_VALUE);
        if(ADI_RTC_SUCCESS != eResult)
        {
            DEBUG_MESSAGE("Failed to enable the device");
            while (1) {};
        }

    } else {

      /* If already initialized, fall through and do nothing */
    }

    return;
}


/*
*********************************************************************************************************
*                                            BSP_OS_TickGet()
*
* Description : Get the OS Tick Counter as if it were running continuously.
*
* Argument(s) : none.
*
* Return(s)   : The effective OS Tick Counter.
*
* Caller(s)   : OS_TaskBlock, OS_TickListInsertDly and OSTimeGet.
*
*               This function is an INTERNAL uC/OS-III function & MUST NOT be called by application
*               function(s).
*
* Note(s)     : none.
*********************************************************************************************************
*/

OS_TICK  BSP_OS_TickGet (void)
{
    ADI_RTC_RESULT eResult;
    OS_TICK        tick;
    CPU_INT32U     rtc_val;
    CPU_INT64U     tmp_val;

    CPU_SR_ALLOC();

    CPU_CRITICAL_ENTER();

    /* Get the current RTC count and convert it into ticks */
    eResult = adi_rtc_GetCount(hRTCDevice, (uint32_t *)&rtc_val);
    if(ADI_RTC_SUCCESS != eResult)
    {
        DEBUG_MESSAGE("Failed to get the RTC count value");
        while (1) {};
    }
    /* Convert from counts to ticks, avoiding 32-bit integer overflow */
    tmp_val = (((CPU_INT64U)rtc_val) * ((CPU_INT64U)OS_CFG_TICK_RATE_HZ)) / ((CPU_INT64U)RTC_TICK_FREQ);

    /* Convert back to 32-bit */
    tick = (OS_TICK) tmp_val;

    CPU_CRITICAL_EXIT();

    return tick;
}


/*
*********************************************************************************************************
*                                          BSP_OS_TickNextSet()
*
* Description : Set the number of OS Ticks to wait before calling OSTimeTick.
*
* Argument(s) : ticks       number of OS Ticks to wait.
*
* Return(s)   : Number of effective OS Ticks until next OSTimeTick.
*
* Caller(s)   : OS_TickTask and OS_TickListInsert.
*
*               This function is an INTERNAL uC/OS-III function & MUST NOT be called by application
*               function(s).
*
* Note(s)     : none.
*********************************************************************************************************
*/

OS_TICK  BSP_OS_TickNextSet (OS_TICK  ticks)
{
    ADI_RTC_RESULT eResult;
    CPU_INT64U     alarm_val;
    CPU_INT64U     delay_val;
    uint32_t       rtc_val;

    /* Check if kernel has asked for infinite tick (0xFFFFFFFF) */
    if (ticks == (OS_TICK)-1) {

        /* Can't set infinite alarm, just set it at some maximum value */
        ticks = BSP_OS_RTC_MAX_DELAY;
    }

    /* Update global value which holds current number of ticks to wait */
    BSP_OS_TicksToGo = ticks;

    /* Convert from ticks to counts, avoiding 32-bit integer overflow */
    delay_val = (((CPU_INT64U)ticks) * ((CPU_INT64U)RTC_TICK_FREQ)) / ((CPU_INT64U)OS_CFG_TICK_RATE_HZ);

    /* Compute absolute time for alarm */
    alarm_val = ((BSP_OS_LastTick + ((CPU_INT64U)ticks)) * ((CPU_INT64U)RTC_TICK_FREQ)) / ((CPU_INT64U)OS_CFG_TICK_RATE_HZ);

    /* Set alarm based on absolute time (RTC is free running) */
    eResult = adi_rtc_SetAlarm(hRTCDevice, (uint32_t)alarm_val);
    if(ADI_RTC_SUCCESS != eResult)
    {
        DEBUG_MESSAGE("Failed to set the RTC Alarm value");
        while (1) {};
    }

    /* Readback the count, this will be used to check if we already missed the alarm */
    eResult = adi_rtc_GetCount(hRTCDevice, &rtc_val);
    if(ADI_RTC_SUCCESS != eResult)
    {
        DEBUG_MESSAGE("Failed to get the RTC count value");
        while (1) {};
    }

    /* If yes, signal interrupt in case we missed the alarm. */
    if ((alarm_val - ((CPU_INT64U) rtc_val)) > delay_val) {
        NVIC_SetPendingIRQ(RTC1_EVT_IRQn);

        /* The RTC service does not call the callback if status bits are not set.
           Call the callback function as if it were called from the service */
        RtcCallback(hRTCDevice, ADI_RTC_ALARM_INT, NULL);
    }

    return ticks;
}


/*
*********************************************************************************************************
*                                        RtcCallback()
*
* Description : Handle RTC callback.
*
* Argument(s) : none.
*
* Return(s)   : none.
*
* Caller(s)   : This is callback .
*
* Note(s)     : none.
*********************************************************************************************************
*/
static void  RtcCallback(void *pCBParam, uint32_t nEvent, void *EventArg)
{

    /* Check if the callback is called due to the expiration of the alarm */
    if((ADI_RTC_ALARM_INT & nEvent)==ADI_RTC_ALARM_INT) {
        OSTimeDynTick(BSP_OS_TicksToGo);
        BSP_OS_LastTick += BSP_OS_TicksToGo;
    }

}

#endif /* (OS_CFG_DYN_TICK_EN == DEF_ENABLED) */

