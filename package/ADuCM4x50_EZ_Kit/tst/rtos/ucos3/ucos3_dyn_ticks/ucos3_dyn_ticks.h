/*********************************************************************************
Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you
agree to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/

#ifndef UCOS3_DYN_TICKS_H__
#define UCOS3_DYN_TICKS_H__

#include <os.h>

/*=============  M A C R O    D E F I N I T I O N S   =============*/
#define DYN_TICKS_TEST_PRIOR    5u
#define DYN_TICKS_TEST_STACK_SZ 500u

/*=============  G L O B A L  V A R I A B L E S       =============*/
extern CPU_STK dyn_ticks_test_stk[DYN_TICKS_TEST_STACK_SZ];
extern OS_TCB  dyn_ticks_test_TaskTCB;

/*=============  E X T E R N A L  F U N C T I O N S   =============*/
extern void dyn_ticks_test_Task(void* arg);

#endif /* UCOS3_DYN_TICKS_H__ */
