/*
 *******************************************************************************
 * @file:    dyn_ticks_test.c
 *
 * @brief:   Tests if dynamic ticks are working.
 *
 * @details  Tests if dynamic ticks are working by calling OSTimeDly which will
 *           eventually call BSP_OS_TickNextSet. The delay is verified by calling
 *           OSTimeGet before and after to determine if the delay function was
 *           accurate. This process repeats forever. By adjusting the "test step"
 *           the delay value will count down to 0 and then reset to the initial
 *           value.
*******************************************************************************

 Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

 This software is proprietary and confidential.  By using this software you agree
 to the terms of the associated Analog Devices License Agreement.

 ******************************************************************************/

/*=============  I N C L U D E S   =============*/

#include <common.h>
#include <os.h>
#include "os_app_hooks.h"
#include <stdio.h>
#include "ucos3_dyn_ticks.h"
#include <drivers/rtc/adi_rtc.h>
#include <drivers/pwr/adi_pwr.h>
#if (OS_CFG_DYN_TICK_EN == DEF_ENABLED)
#include "dyn_ticks/bsp_os_dyn_rtc.h"
#endif

/*=============  M A C R O    D E F I N I T I O N S   =============*/
#define TASK_STARTUP_STK_SIZE   (200u)
#define TASK_STARTUP_PRIO        (10u)

/*=============  G L O B A L  V A R I A B L E S       =============*/

/*=============  R T O S D A T A   =============*/
static OS_TCB g_Task_Startup_TCB;

static CPU_STK g_Task_Startup_Stack  [TASK_STARTUP_STK_SIZE];

#define FAILURE_INFO

#ifdef FAILURE_INFO
#include <stdio.h>
#include <string.h>
#define  CHAR_BUF_SZ 50
#endif

/* Task Memory */
CPU_STK  dyn_ticks_test_stk[DYN_TICKS_TEST_STACK_SZ];
OS_TCB   dyn_ticks_test_TaskTCB;

/* User Defined Test Parameters */
#define  TEST_PERIOD 2u       /* Starting and maximum value for delay */
#define  TEST_STEP   0u       /* Decrement value for delay */
#define  TEST_MARGIN 0u       /* Margin of error for delay */
#define  TEST_LENGTH 1000u    /* Number of iterations to test */

/*=============  E X T E R N A L  F U N C T I O N S   =============*/
extern int32_t adi_initpinmux(void);

/*=============  L O C A L       F U N C T I O N S   =============*/
void  dyn_ticks_test_Task(void* arg){
    OS_ERR   OSRetVal;
    OS_TICK  delay;
    OS_TICK  preTick;
    OS_TICK  postTick;
    uint32_t counter;
    uint32_t nError = 0u;

#ifdef FAILURE_INFO
    char buf[CHAR_BUF_SZ];
    CPU_TS_TMR readRTC;
#endif

    /* Init */
    delay   = TEST_PERIOD;
    counter = 0u;

    /* WHILE (More iterations to go) */
    while (counter < TEST_LENGTH) {

        /* Get current time */
        preTick = OSTimeGet(&OSRetVal);

        if (OSRetVal != OS_ERR_NONE)
        {
            DEBUG_MESSAGE("OSTimeGet Error");
            nError++;
            break;
        }

        /* Insert delay */
        //OSTimeDly(delay, OS_OPT_TIME_DLY, &OSRetVal);
        OSTimeDly(TEST_PERIOD, OS_OPT_TIME_DLY, &OSRetVal);

        if (OSRetVal != OS_ERR_NONE)
        {
            DEBUG_MESSAGE("OSTimeDly Error");
            nError++;
            break;
        }

        /* Get current time */
        postTick = OSTimeGet(&OSRetVal);
        //printf("SONIA pre %d\n", preTick);
        //printf("SONIA post %d\n", postTick);
        if (OSRetVal != OS_ERR_NONE)
        {
            DEBUG_MESSAGE("OSTimeGet Error");
            nError++;
            break;
        }

        /* IF (test passed) */
        if (((postTick - preTick) >= (delay-TEST_MARGIN)) && ((postTick - preTick) <= (delay+TEST_MARGIN))) {
            /* If it worked, do nothing */
        } else {
#ifdef FAILURE_INFO
            /* Read the RTC for debug info */
            readRTC = CPU_TS_TmrRd();
            /* Dump debug info */
            sprintf(buf, "Dynamic tick test failed.\r\nDelay: %d\r\nPreTick: %d\r\nPostTick %d\r\nRTC Val: %d\r\n", delay, preTick, postTick, readRTC);
            DEBUG_MESSAGE(buf);
            memset(buf, 0, CHAR_BUF_SZ);
#endif
            break;
        } /* ENDIF */

        /* Adjust delay value based on user defined parameters */
        delay -= TEST_STEP;

        /* IF (delay reaches zero or delay with error margin reaches zero) */
        if ((delay <= 0) || ((delay-TEST_MARGIN) <= 0)) {
          /* Reset delay */
          delay = TEST_PERIOD;
        } /* ENDIF */

        counter++;
    } /* ENDWHILE */

    /* IF(The test passed) */
    if (counter == TEST_LENGTH) {
        common_Pass();
     //   DEBUG_MESSAGE("Dynamic Ticks Test Passed")
    } else {
        common_Fail("Dynamic Ticks Test Failed");
   //     DEBUG_MESSAGE("Dynamic Ticks Test Failed")
    } /* ENDIF */

    /* Halt the entire system and exit debugger, conclude test */

    /* Supress warning about using exit(). This is a test, exit is required for
    * automatic testing.
    */
    #pragma diag_suppress=Pm108
    exit(0);
    #pragma diag_default=Pm108
}

void StartUpTask(void* arg)
{
    OS_ERR OSRetVal;

    /* Create rest of the tasks and signals */
    OSTaskCreate (&dyn_ticks_test_TaskTCB, /* address of TCB */
        "Dynamic Ticks Task",               /* Task name */
        dyn_ticks_test_Task,                   /* Task "Run" function */
        NULL,                          /* arg for the "Run" function*/
        DYN_TICKS_TEST_PRIOR,             /* priority */
        dyn_ticks_test_stk,          /* base of the stack */
        DYN_TICKS_TEST_STACK_SZ - 1u ,   /* limit for stack growth */
        DYN_TICKS_TEST_STACK_SZ,         /* stack size in CPU_STK */
        0u,                            /* number of messages allowed */
        (OS_TICK)  0u,                 /* time_quanta */
        NULL,                          /* extension pointer */
        (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
        &OSRetVal);

    if (OSRetVal != OS_ERR_NONE)
    {
        DEBUG_MESSAGE("Failure dynamic tick test task");
        common_Fail("Dynamic Ticks Test Failed");
    }


    OSTaskDel (0, &OSRetVal );

}

/*********************************************************************
*
*   Function:   main
*
*********************************************************************/
int main (void)
{
    OS_ERR OSRetVal;

#if (CPU_CFG_TS_32_EN == DEF_ENABLED || CPU_CFG_TS_64_EN == DEF_ENABLED)
  CPU_Init();
#endif

#if (OS_CFG_DYN_TICK_EN == DEF_ENABLED)
    BSP_OS_RTC_TickInit();
#endif

    /* Initialize the RTOS */
    OSInit(&OSRetVal);
    if (OSRetVal != OS_ERR_NONE)
    {
        printf("Failed to initialize the RTOS \n");
        return(0);
    }

    App_OS_SetAllHooks();

    /* Power Driver initialization if required */
    if(ADI_PWR_SUCCESS != adi_pwr_Init())
    {
        DEBUG_MESSAGE("Failure initializing the power driver");
        return (1);
    }

    /* example framework initialization */
    common_Init();

    /* Create the Start-up Task*/
    OSTaskCreate (&g_Task_Startup_TCB, /* address of TCB */
        "Start-up Task",               /* Task name */
        StartUpTask,                   /* Task "Run" function */
        NULL,                          /* arg for the "Run" function*/
        TASK_STARTUP_PRIO,             /* priority */
        g_Task_Startup_Stack,          /* base of the stack */
        TASK_STARTUP_STK_SIZE - 1u ,   /* limit for stack growth */
        TASK_STARTUP_STK_SIZE,         /* stack size in CPU_STK */
        0u,                            /* number of messages allowed */
        (OS_TICK)  0u,                 /* time_quanta */
        NULL,                          /* extension pointer */
        (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
        &OSRetVal);

    if (OSRetVal != OS_ERR_NONE)
    {
        DEBUG_MESSAGE("Failure creating start-up task");
        return (1);
    }

    /* Start OS */
    OSStart(&OSRetVal);

    if (OSRetVal != OS_ERR_NONE)
    {
        DEBUG_MESSAGE("Failure starting the RTOS ");
        return (1);
    }

    common_Fail("Application should not return to main after RTOS start");
    return (0);
}