/*********************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you
agree to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*!
 * @brief     Brief example information
 * @details   More example information
 *
 */
/*=============  I N C L U D E S   =============*/

#include <common.h>
#include <os.h>
#include <stdio.h>
#include "example.h"
#include <drivers/pwr/adi_pwr.h>

#include <drivers/beep/adi_beep.h>
#include <drivers/spi/adi_spi.h>
#include <drivers/uart/adi_uart.h>
#include <drivers/i2c/adi_i2c.h>
#include <drivers/flash/adi_flash.h>
#include <drivers/crypto/adi_crypto.h>
#include <drivers/rtc/adi_rtc.h>
#include <drivers/crc/adi_crc.h>
#include <drivers/adc/adi_adc.h>
#include <drivers/adxl363/adi_adxl363.h>
#include <drivers/rng/adi_rng.h>
#include <drivers/sport/adi_sport.h>
#if defined (__ADUCM4150__)
#include <drivers/icc/adi_icc_cm.h>
#endif

#if ADI_CFG_RTOS != ADI_CFG_RTOS_MICRIUM_III
#error "Incorrect RTOS detected"
#endif

/*=============  M A C R O    D E F I N I T I O N S   =============*/
#define TASK_STARTUP_STK_SIZE   (200u)
#define TASK_STARTUP_PRIO        (10u)

/*=============  G L O B A L  V A R I A B L E S       =============*/
/* Handle and memory for BEEP driver */
static ADI_BEEP_HANDLE  hBeepDevice;
static uint8_t BeepDeviceMem[ADI_BEEP_MEMORY_SIZE];

/* Handle and memory for SPI driver */
static ADI_SPI_HANDLE  hSPIDevice;
static uint8_t SPIDeviceMem[ADI_SPI_MEMORY_SIZE];

/* Handle and memory for UART driver */
static ADI_UART_HANDLE hUARTBiDevice;
static uint8_t UARTBiDeviceMem[ADI_UART_BIDIR_MEMORY_SIZE];

static ADI_UART_HANDLE hUARTUniDevice;
static uint8_t UARTUniDeviceMem[ADI_UART_UNIDIR_MEMORY_SIZE];

/* Handle and memory for I2C driver */
static ADI_I2C_HANDLE  hI2CDevice;
static uint8_t I2CDeviceMem[ADI_I2C_MEMORY_SIZE];

/* Handle and memory for Flash driver */
static ADI_FEE_HANDLE  hFeeDevice;
static uint8_t FeeDeviceMem[ADI_FEE_MEMORY_SIZE];

/* Handle and memory for Crypto driver */
static ADI_CRYPTO_HANDLE  hCryptoDevice;
static uint8_t CryptoDeviceMem[ADI_CRYPTO_MEMORY_SIZE];

#if 0
/* Handle and memory for RTC driver */
static ADI_RTC_HANDLE  hRTCDevice;
static uint8_t RTCDeviceMem[ADI_RTC_MEMORY_SIZE];
#endif

/* Handle and memory for CRC driver */
static ADI_CRC_HANDLE  hCRCDevice;
static uint8_t CRCDeviceMem[ADI_CRC_MEMORY_SIZE];

/* Handle and memory for ADC driver */
static ADI_ADC_HANDLE  hADCDevice;
static uint8_t ADCDeviceMem[ADI_ADC_MEMORY_SIZE];

/* Handle and memory for ADXL363 driver */
static ADI_ADXL363_HANDLE  hADXL363Device;
static uint8_t ADXL363DeviceMem[ADI_ADXL363_MEMORY_SIZE];

/* Handle and memory for RNG driver */
static ADI_RNG_HANDLE  hRNGDevice;
static uint8_t RNGDeviceMem[ADI_RNG_MEMORY_SIZE];

/* Handle and memory for SPORT driver */
static ADI_SPORT_HANDLE  hSPORTDevice;
static uint8_t SPORTDeviceMem[ADI_SPORT_MEMORY_SIZE];

#if defined (__ADUCM4150__)
/* Handle and memory for ICC driver */
static ADI_ICC_HANDLE  hICCDevice;
static uint8_t ICCDeviceMem[ADI_ICC_MEM_SIZE];
#endif

/*=============  R T O S D A T A   =============*/
static OS_TCB g_Task_Startup_TCB;

static CPU_STK g_Task_Startup_Stack  [TASK_STARTUP_STK_SIZE];


/*=============  E X T E R N A L  F U N C T I O N S   =============*/

/*=============  L O C A L       F U N C T I O N S   =============*/
void StartUpTask(void* arg)
{
  uint8_t failure_detected = 0u;

    /* Create rest of the tasks and signals */
    /* The location where the example passes or fails is example-specific */
    while (1)
    {

	  if(ADI_BEEP_SUCCESS != adi_beep_Open(ADI_BEEP_DEVID_0,
                                           BeepDeviceMem,
                                           ADI_BEEP_MEMORY_SIZE,
                                           &hBeepDevice)) {
          DEBUG_MESSAGE("Failure to open beep driver");
          failure_detected++;
      } else {
          if(ADI_BEEP_SUCCESS != adi_beep_Close(hBeepDevice)) {
              DEBUG_MESSAGE("Failure to close beep driver");
              failure_detected++;
          }
      }

      /* Example body here or in other tasks */
      if(ADI_SPI_SUCCESS != adi_spi_Open(1,SPIDeviceMem,ADI_SPI_MEMORY_SIZE,&hSPIDevice)) {
          DEBUG_MESSAGE("Failure to open spi driver");
          failure_detected++;
      } else {
          if(ADI_SPI_SUCCESS != adi_spi_Close(hSPIDevice)) {
              DEBUG_MESSAGE("Failure to close spi driver");
              failure_detected++;
          }
      }

      if (ADI_I2C_SUCCESS != adi_i2c_Open(0, I2CDeviceMem, ADI_I2C_MEMORY_SIZE, &hI2CDevice)){
          DEBUG_MESSAGE("Failure to open i2c driver");
          failure_detected++;
      } else {

        if(ADI_I2C_SUCCESS != adi_i2c_Close(hI2CDevice)) {
            DEBUG_MESSAGE("Failure to open/close i2c driver");
            failure_detected++;
        }
      }

      if (ADI_FEE_SUCCESS != adi_fee_Open(0, FeeDeviceMem, ADI_FEE_MEMORY_SIZE, &hFeeDevice)){
          DEBUG_MESSAGE("Failure to open Flash driver");
          failure_detected++;
      } else {

        if(ADI_FEE_SUCCESS != adi_fee_Close(hFeeDevice)) {
            DEBUG_MESSAGE("Failure to open/close Flash driver");
            failure_detected++;
        }
      }

      if (ADI_CRYPTO_SUCCESS != adi_crypto_Open(0, CryptoDeviceMem, ADI_CRYPTO_MEMORY_SIZE, &hCryptoDevice)){
          DEBUG_MESSAGE("Failure to open Flash driver");
          failure_detected++;
      } else {

        if(ADI_CRYPTO_SUCCESS != adi_crypto_Close(hCryptoDevice)) {
            DEBUG_MESSAGE("Failure to open/close Flash driver");
            failure_detected++;
        }
      }

      if (ADI_UART_SUCCESS != adi_uart_Open(1, ADI_UART_DIR_BIDIRECTION, UARTBiDeviceMem, ADI_UART_BIDIR_MEMORY_SIZE, &hUARTBiDevice)){
          DEBUG_MESSAGE("Failure to open uart driver bidirectional ");
          failure_detected++;
      } else {

        if(ADI_UART_SUCCESS != adi_uart_Close(hUARTBiDevice)) {
            DEBUG_MESSAGE("Failure to close uart driver bidirectional");
            failure_detected++;
        }
      }

      if (ADI_UART_SUCCESS != adi_uart_Open(1, ADI_UART_DIR_TRANSMIT, UARTUniDeviceMem, ADI_UART_UNIDIR_MEMORY_SIZE, &hUARTUniDevice)){
          DEBUG_MESSAGE("Failure to open uart driver unidirectional");
          failure_detected++;
      } else {

        if(ADI_UART_SUCCESS != adi_uart_Close(hUARTUniDevice)) {
            DEBUG_MESSAGE("Failure to close uart driver unidirectional");
            failure_detected++;
        }
      }

#if 0
      /* for now we are not doing the RTC since it has its own RTOS test and
       * the configuration seems to be wrong so it hangs in open
       */
      if (ADI_RTC_SUCCESS != adi_rtc_Open(0, RTCDeviceMem, ADI_RTC_MEMORY_SIZE, &hRTCDevice)){
          DEBUG_MESSAGE("Failure to open rtc driver");
          failure_detected++;
      } else {

        if(ADI_RTC_SUCCESS != adi_rtc_Close(hRTCDevice)) {
            DEBUG_MESSAGE("Failure to close rtc driver");
            failure_detected++;
        }
      }
#endif

      if (ADI_CRC_SUCCESS != adi_crc_Open(0, CRCDeviceMem, ADI_CRC_MEMORY_SIZE, &hCRCDevice)){
          DEBUG_MESSAGE("Failure to open crc driver");
          failure_detected++;
      } else {

        if(ADI_CRC_SUCCESS != adi_crc_Close(hCRCDevice)) {
            DEBUG_MESSAGE("Failure to close crc driver");
            failure_detected++;
        }
      }

      if (ADI_ADC_SUCCESS != adi_adc_Open(0, ADCDeviceMem, ADI_ADC_MEMORY_SIZE, &hADCDevice)){
          DEBUG_MESSAGE("Failure to open adc driver");
          failure_detected++;
      } else {

        if(ADI_ADC_SUCCESS != adi_adc_Close(hADCDevice)) {
            DEBUG_MESSAGE("Failure to close adc driver");
            failure_detected++;
        }
      }

      if (ADI_ADXL363_SUCCESS != adi_adxl363_Open(ADXL363DeviceMem, ADI_ADXL363_MEMORY_SIZE, &hADXL363Device)){
          DEBUG_MESSAGE("Failure to open adxl363 driver");
          failure_detected++;
      } else {

        if(ADI_ADXL363_SUCCESS != adi_adxl363_Close(hADXL363Device)) {
          DEBUG_MESSAGE("Failure to close adxl363 driver");
          failure_detected++;
         }
      }

      if (ADI_RNG_SUCCESS != adi_rng_Open(0, RNGDeviceMem, ADI_RNG_MEMORY_SIZE, &hRNGDevice)){
          DEBUG_MESSAGE("Failure to open rng driver");
          failure_detected++;
      } else {

        if(ADI_RNG_SUCCESS != adi_rng_Close(hRNGDevice)) {
            DEBUG_MESSAGE("Failure to close rng driver");
            failure_detected++;
        }
      }

      if (ADI_SPORT_SUCCESS != adi_sport_Open(0, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, SPORTDeviceMem, ADI_SPORT_MEMORY_SIZE, &hSPORTDevice)){
          DEBUG_MESSAGE("Failure to open sport driver");
          failure_detected++;
      } else {

        if(ADI_SPORT_SUCCESS != adi_sport_Close(hSPORTDevice)) {
            DEBUG_MESSAGE("Failure to close sport driver");
            failure_detected++;
        }
      }

#if defined (__ADUCM4150__)
      if (ADI_ICC_SUCCESS != adi_icc_Open(0, ICCDeviceMem, ADI_ICC_MEM_SIZE, &hICCDevice)){
          DEBUG_MESSAGE("Failure to open icc driver");
          failure_detected++;
      } else {

        if(ADI_ICC_SUCCESS != adi_icc_Close(hICCDevice)) {
            DEBUG_MESSAGE("Failure to close icc driver");
            failure_detected++;
        }
      }
#endif
      /* if success */
      if (0 == failure_detected){
        common_Pass();
      } else {
        common_Fail("Failures opening drivers");
      }
      exit(0);

      /* if there are no other actions this task should perform it can
       * be destroyed
       */
    }
}
/*********************************************************************
*
*   Function:   main
*
*********************************************************************/
int main (void)
{
    OS_ERR OSRetVal;

#if 0
    /* pinmux init */
    adi_initpinmux();
#endif

    /* Initialize the RTOS */
    OSInit(&OSRetVal);
    if (OSRetVal != OS_ERR_NONE)
    {
        printf("Failed to initialize the RTOS \n");
        return(0);
    }

    /* Power Driver initialization if required */
    if(ADI_PWR_SUCCESS != adi_pwr_Init())
    {
        DEBUG_MESSAGE("Failure initializing the power driver");
        return (1);
    }
		
    if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_HCLK, 1u))
    {
        DEBUG_MESSAGE("Failure initializing the power driver");
        return (1);
    }		
		
    if(ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_PCLK, 1u))
    {
        DEBUG_MESSAGE("Failure initializing the power driver");
        return (1);
		}

    /* example framework initialization */
    common_Init();

    /* Create the Start-up Task*/
    OSTaskCreate (&g_Task_Startup_TCB, /* address of TCB */
        "Start-up Task",               /* Task name */
        StartUpTask,                   /* Task "Run" function */
        NULL,                          /* arg for the "Run" function*/
        TASK_STARTUP_PRIO,             /* priority */
        g_Task_Startup_Stack,          /* base of the stack */
        TASK_STARTUP_STK_SIZE - 1u ,   /* limit for stack growth */
        TASK_STARTUP_STK_SIZE,         /* stack size in CPU_STK */
        0u,                            /* number of messages allowed */
        (OS_TICK)  0u,                 /* time_quanta */
        NULL,                          /* extension pointer */
        (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
        &OSRetVal);

    if (OSRetVal != OS_ERR_NONE)
    {
        DEBUG_MESSAGE("Failure creating start-up task");
        return (1);
    }

    /* Start OS */
    OSStart(&OSRetVal);

    if (OSRetVal != OS_ERR_NONE)
    {
        DEBUG_MESSAGE("Failure starting the RTOS ");
        return (1);
    }

    common_Fail("Application should not return to main after RTOS start");
    return (0);
}
