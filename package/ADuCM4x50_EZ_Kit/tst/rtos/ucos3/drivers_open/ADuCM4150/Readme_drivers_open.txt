         Analog Devices, Inc. ADuCM4x50 Application Example

Project Name: drivers_open

Description:
=========
Open and close drivers in ucos3 context

Overview:
=========
This test opens and closes the drivers in the ucos3 context and therefore checks
that the driver size macros are defined correctly for each driver.

This test needs to be edited every time that there is a new driver that requires
the RTOS.

The tests currently has:
	* UART bidirectional and unidirectional
	* SPI
	* I2C
	* Flash (FEE)
	* Crypto


User Configuration Macros:
==========================
adi_global_cfg.h has been replaced to say we use ucos3. Other configuration
files required by the RTOS are also in the config folder.

Hardware Setup:
===============
None

External connections:
=====================
None

How to build and run:
=====================

Expected Result:
=================
Prints All Done. If there are errors there should be a message indicating which
driver(s) failed.

References:
===========
    ADuCM4x50 Hardware Reference Manual
