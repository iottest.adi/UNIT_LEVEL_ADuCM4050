#include <drivers/gpio/adi_gpio.h>
#include <common.h>
#include <stdio.h>

/*********************************************************UNIT TEST***********************************************************/ 


#ifndef UNIT_TEST_H
#define UNIT_TEST_H

typedef enum
{
  
  ADI_UNIT_SUCCESS = 0u,
  ADI_UNIT_FAILURE = 1u
    
} ADI_UNIT_RESULT;


#define UNIT_TEST(func, dbg) \
do \
{ \
    if ((func) != (dbg)) \
    { \
        DEBUG_MESSAGE("Unit test fail at line %d of %s\r\n", __LINE__, __FILE__); \
        return (ADI_UNIT_FAILURE); \
    } \
} while(0u);

#endif /* UNIT_TEST_H */


/***************************************************************UNIT_TEST_H****************************************************************************/

  
/* UART device number. There are 2 devices so that can either be a 0 or 1. */
#define UART_DEVICE_NUM 0u
/* Include the driver under test */
#include <drivers/uart/adi_uart.h>
/* Power driver is needed to get the clocks at expected values */
#include <drivers/pwr/adi_pwr.h>

/* Timeout value for receiving data. */
#define UART_GET_BUFFER_TIMEOUT 1000000u

/* Memory for the UART driver. */
static uint8_t UartBiDirDeviceMem[ADI_UART_BIDIR_MEMORY_SIZE]; 
static uint8_t UartRxDeviceMem[ADI_UART_UNIDIR_MEMORY_SIZE]; 
static uint8_t UartTxDeviceMem[ADI_UART_UNIDIR_MEMORY_SIZE]; 

/* Top-level unit testing for the driver */
ADI_UNIT_RESULT uart_unit_testing(void)
{   
    /* Handle for the UART device. */
    static ADI_UART_HANDLE hDevice;
    
    uint32_t pHwError;
    uint32_t pBaudRate;
    uint8_t nBufferRx[5u];   
    void *pProcBuff;
    uint32_t i = 0u;
    
    /* Variable to confirm the transmit shift register is empty before closing the device. */
    bool bTxComplete = false;
    
    /* Timeout nCounter for waiting on a receive buffer to be filled. */
    uint32_t nTimeout = 0u;
          
    /* The power test may have modified the clocks, restore to 26 MHz */
    UNIT_TEST(adi_pwr_Init(), ADI_PWR_SUCCESS)
    UNIT_TEST(adi_pwr_SetClockDivider(ADI_CLOCK_HCLK, 1u), ADI_PWR_SUCCESS)
    UNIT_TEST(adi_pwr_SetClockDivider(ADI_CLOCK_PCLK, 1u), ADI_PWR_SUCCESS)   
    
    /* adi_uart_Open: Fail */
    UNIT_TEST(adi_uart_Open(5, ADI_UART_DIR_BIDIRECTION, UartBiDirDeviceMem, ADI_UART_BIDIR_MEMORY_SIZE, &hDevice), ADI_UART_INVALID_DEVICE_NUM)   
    UNIT_TEST(adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartBiDirDeviceMem, 3u, &hDevice), ADI_UART_INSUFFICIENT_MEMORY) 
     
    /* adi_uart_Open: Pass */  
    UNIT_TEST(adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartBiDirDeviceMem, ADI_UART_BIDIR_MEMORY_SIZE, &hDevice), ADI_UART_SUCCESS)
        
    /* adi_uart_Open: Fail */
    UNIT_TEST(adi_uart_Open(UART_DEVICE_NUM, ADI_UART_DIR_BIDIRECTION, UartBiDirDeviceMem, ADI_UART_BIDIR_MEMORY_SIZE, &hDevice), ADI_UART_DEVICE_IN_USE)    
    
    /* adi_uart_Close: Fail */
    UNIT_TEST(adi_uart_Close(NULL), ADI_UART_INVALID_HANDLE) 
    
    /* adi_uart_SubmitRxBuffer: Fail */ 
    UNIT_TEST(adi_uart_SubmitRxBuffer(UART_DEVICE_NUM, nBufferRx, 5u, 0u), ADI_UART_INVALID_HANDLE)
    UNIT_TEST(adi_uart_SubmitRxBuffer(hDevice, NULL, 5u, 0u), ADI_UART_INVALID_POINTER)  
    UNIT_TEST(adi_uart_SubmitRxBuffer(hDevice, nBufferRx, 0u, 0u), ADI_UART_FAILED)  
      
      /* adi_uart_SubmitTxBuffer: Pass (Note: Cant do the same with RX because we dont have data to fill it.)*/     
    UNIT_TEST(adi_uart_SubmitTxBuffer(hDevice, "ABCDE", 5u, 0u), ADI_UART_SUCCESS) 
    UNIT_TEST(adi_uart_SubmitTxBuffer(hDevice, "ABCDE", 5u, 0u), ADI_UART_SUCCESS)  
    
    /* adi_uart_SubmitTxBuffer: Fail */ 
    UNIT_TEST(adi_uart_SubmitTxBuffer(hDevice, "ABCDE", 5u, 0u), ADI_UART_OPERATION_NOT_ALLOWED)     
    UNIT_TEST(adi_uart_SubmitTxBuffer(0, "ABCDE", 5u, 0u), ADI_UART_INVALID_HANDLE)
    UNIT_TEST(adi_uart_SubmitTxBuffer(hDevice, NULL, 5u, 0u), ADI_UART_INVALID_POINTER)  
    UNIT_TEST(adi_uart_SubmitTxBuffer(hDevice, "ABCDE", 0u, 0u), ADI_UART_FAILED) 
    
    /* ADI_UART_DEVICE_IN_USE when buffers are in action. */
    UNIT_TEST(adi_uart_EnableAutobaud(hDevice, true, false), ADI_UART_DEVICE_IN_USE)       
    UNIT_TEST(adi_uart_Close(hDevice), ADI_UART_DEVICE_IN_USE)    
    UNIT_TEST(adi_uart_ConfigBaudRate(hDevice, 24, 3, 1078, 3), ADI_UART_DEVICE_IN_USE)
    
    
    /* Get the Tx buffers in action */    
    UNIT_TEST(adi_uart_GetTxBuffer(hDevice, &pProcBuff, &pHwError), ADI_UART_SUCCESS)   
    
    /* Cannot mix Blocking and Non Blocking mode. (Note: cannot reproduce for Rx because we do not have data to finish the transaction.)*/
    UNIT_TEST(adi_uart_Write(hDevice, "ABCDE", 5u, 0u, &pHwError), ADI_UART_INVALID_DATA_TRANSFER_MODE) 
      
    UNIT_TEST(adi_uart_GetTxBuffer(hDevice, &pProcBuff, &pHwError), ADI_UART_SUCCESS)    
  
    /* adi_uart_EnableAutobaud: Pass */ 
    UNIT_TEST(adi_uart_EnableAutobaud(hDevice, true, false), ADI_UART_SUCCESS)       

    /*ADI_UART_DEVICE_IN_USE when Autobaud is in progress. */
    UNIT_TEST(adi_uart_SubmitTxBuffer(hDevice, "ABCDE", 5u, 0u), ADI_UART_DEVICE_IN_USE)      
    UNIT_TEST(adi_uart_SubmitRxBuffer(hDevice, nBufferRx, 5u, 0u), ADI_UART_DEVICE_IN_USE) 
    UNIT_TEST(adi_uart_Write(hDevice, "ABCDE", 5u, 0u, &pHwError), ADI_UART_DEVICE_IN_USE) 
    UNIT_TEST(adi_uart_Read(hDevice, nBufferRx, 5u, 0u, &pHwError), ADI_UART_DEVICE_IN_USE) 
    UNIT_TEST(adi_uart_SetConfiguration(hDevice, ADI_UART_NO_PARITY, ADI_UART_ONE_STOPBIT, ADI_UART_WORDLEN_5BITS), ADI_UART_DEVICE_IN_USE)   
    UNIT_TEST(adi_uart_RegisterCallback(hDevice, NULL, NULL), ADI_UART_DEVICE_IN_USE) 
    /* adi_uart_EnableAutobaud */ 
    UNIT_TEST(adi_uart_EnableAutobaud(hDevice, false, false), ADI_UART_SUCCESS)  
    UNIT_TEST(adi_uart_EnableAutobaud(0, false, false), ADI_UART_INVALID_HANDLE)  
      
    /* adi_uart_IsTxComplete */
    while((bTxComplete == false) && (nTimeout != UART_GET_BUFFER_TIMEOUT))
    {
        UNIT_TEST(adi_uart_IsTxComplete(hDevice, &bTxComplete), ADI_UART_SUCCESS)

        nTimeout++;
    }
    UNIT_TEST(adi_uart_IsTxComplete(0, &bTxComplete), ADI_UART_INVALID_HANDLE)     
      
    /* Make sure there was not a timeout. */     
    if(nTimeout == UART_GET_BUFFER_TIMEOUT)
    {
        return ADI_UNIT_FAILURE;
    }
    else
    {
      nTimeout = 0u;
    }
    
    /* adi_uart_GetBuffer: Fail */  
    UNIT_TEST(adi_uart_GetTxBuffer(0, &pProcBuff, &pHwError), ADI_UART_INVALID_HANDLE)        
    UNIT_TEST(adi_uart_GetRxBuffer(0, &pProcBuff, &pHwError), ADI_UART_INVALID_HANDLE)   
    UNIT_TEST(adi_uart_GetTxBuffer(hDevice, &pProcBuff, &pHwError), ADI_UART_BUFFER_NOT_SUBMITTED)        
    UNIT_TEST(adi_uart_GetRxBuffer(hDevice, &pProcBuff, &pHwError), ADI_UART_BUFFER_NOT_SUBMITTED)  
      
      
    for(i = 0u; i <= 1; i++)
    {
      /* adi_uart_EnableLoopBack */
      UNIT_TEST(adi_uart_EnableLoopBack(hDevice, (bool)i), ADI_UART_SUCCESS)       
      UNIT_TEST(adi_uart_EnableLoopBack(0, (bool)i), ADI_UART_INVALID_HANDLE)   
      
      /* adi_uart_EnableFifo */     
      UNIT_TEST(adi_uart_EnableFifo(hDevice, (bool)i), ADI_UART_SUCCESS)       
      UNIT_TEST(adi_uart_EnableFifo(0, (bool)i), ADI_UART_INVALID_HANDLE)   
      
      /* adi_uart_ForceTxBreak */  
      UNIT_TEST(adi_uart_ForceTxBreak(hDevice, (bool)i), ADI_UART_SUCCESS)       
      UNIT_TEST(adi_uart_ForceTxBreak(0, (bool)i), ADI_UART_INVALID_HANDLE)   
    }
    
    UNIT_TEST(adi_uart_ForceTxBreak(hDevice, false), ADI_UART_SUCCESS) 
      
    /* adi_uart_GetBaudRate */  
    UNIT_TEST(adi_uart_GetBaudRate(hDevice, &pBaudRate, &pHwError), ADI_UART_SUCCESS)   
    UNIT_TEST(adi_uart_GetBaudRate(0, &pBaudRate, &pHwError), ADI_UART_INVALID_HANDLE)   
    UNIT_TEST(adi_uart_GetBaudRate(hDevice, NULL, &pHwError), ADI_UART_INVALID_POINTER)   
     
    /* adi_uart_SetRxFifoTriggerLevel */  
    for(i = 0u; i <= 3; i++)
    {
      UNIT_TEST(adi_uart_SetRxFifoTriggerLevel(hDevice, (ADI_UART_TRIG_LEVEL) i), ADI_UART_SUCCESS)    
      UNIT_TEST(adi_uart_SetRxFifoTriggerLevel(0, (ADI_UART_TRIG_LEVEL) i), ADI_UART_INVALID_HANDLE)  
    }
    
    /* adi_uart_SetConfiguration */  
    UNIT_TEST(adi_uart_SetConfiguration(0, ADI_UART_NO_PARITY, ADI_UART_ONE_STOPBIT, ADI_UART_WORDLEN_5BITS), ADI_UART_INVALID_HANDLE)   
    UNIT_TEST(adi_uart_SetConfiguration(hDevice, ADI_UART_NO_PARITY, ADI_UART_ONE_STOPBIT, ADI_UART_WORDLEN_5BITS), ADI_UART_SUCCESS)   
 
    /* adi_uart_ConfigBaudRate */ 
    UNIT_TEST(adi_uart_ConfigBaudRate(0, 24, 3, 1078, 3), ADI_UART_INVALID_HANDLE)
    UNIT_TEST(adi_uart_ConfigBaudRate(hDevice, 24, 0, 1078, 3), ADI_UART_INVALID_PARAMETER)  
    UNIT_TEST(adi_uart_ConfigBaudRate(hDevice, 24, 4, 1078, 3), ADI_UART_INVALID_PARAMETER)
    UNIT_TEST(adi_uart_ConfigBaudRate(hDevice, 24, 3, 2048, 3), ADI_UART_INVALID_PARAMETER)
    UNIT_TEST(adi_uart_ConfigBaudRate(hDevice, 24, 3, 1078, 4), ADI_UART_INVALID_PARAMETER)
    UNIT_TEST(adi_uart_ConfigBaudRate(hDevice, 24, 3, 1078, 3), ADI_UART_SUCCESS)
      
    /* Channel control functions */ 
    UNIT_TEST(adi_uart_FlushTxFifo(0), ADI_UART_INVALID_HANDLE)  
    UNIT_TEST(adi_uart_FlushTxFifo(hDevice), ADI_UART_SUCCESS)    
    UNIT_TEST(adi_uart_FlushRxFifo(0), ADI_UART_INVALID_HANDLE)  
    UNIT_TEST(adi_uart_FlushRxFifo(hDevice), ADI_UART_SUCCESS)
    UNIT_TEST(adi_uart_FlushRxChannel(0), ADI_UART_INVALID_HANDLE)  
    UNIT_TEST(adi_uart_FlushRxChannel(hDevice), ADI_UART_SUCCESS)    
    UNIT_TEST(adi_uart_FlushTxChannel(0), ADI_UART_INVALID_HANDLE)  
    UNIT_TEST(adi_uart_FlushTxChannel(hDevice), ADI_UART_SUCCESS)
           
    /* Register Callback */ 
    UNIT_TEST(adi_uart_RegisterCallback(hDevice, NULL, NULL), ADI_UART_SUCCESS)
    UNIT_TEST(adi_uart_RegisterCallback(0, NULL, NULL), ADI_UART_INVALID_HANDLE)
      
    /* adi_uart_Close: Pass */
    UNIT_TEST(adi_uart_Close(hDevice), ADI_UART_SUCCESS) 
        
    /* adi_uart_Open: Pass */  
    UNIT_TEST(adi_uart_Open(0, ADI_UART_DIR_RECEIVE, UartRxDeviceMem, ADI_UART_UNIDIR_MEMORY_SIZE, &hDevice), ADI_UART_SUCCESS)       
      
    /* ADI_UART_OPERATION_NOT_ALLOWED when transmitting in Rx only device.  */
    UNIT_TEST(adi_uart_SubmitTxBuffer(hDevice, "ABCDE", 5u, 0u), ADI_UART_OPERATION_NOT_ALLOWED) 
    UNIT_TEST(adi_uart_Write(hDevice, "ABCDE", 5u, 0u, &pHwError), ADI_UART_OPERATION_NOT_ALLOWED) 
      
    /* adi_uart_Close: Pass */
    UNIT_TEST(adi_uart_Close(hDevice), ADI_UART_SUCCESS) 
      
    /* adi_uart_Open: Pass */  
    UNIT_TEST(adi_uart_Open(0, ADI_UART_DIR_TRANSMIT, UartTxDeviceMem, ADI_UART_UNIDIR_MEMORY_SIZE, &hDevice), ADI_UART_SUCCESS)       
      
    /* ADI_UART_OPERATION_NOT_ALLOWED when transmitting in Rx only device.  */
    UNIT_TEST(adi_uart_SubmitRxBuffer(hDevice, nBufferRx, 5u, 0u), ADI_UART_OPERATION_NOT_ALLOWED) 
    UNIT_TEST(adi_uart_Read(hDevice, nBufferRx, 5u, 0u, &pHwError), ADI_UART_OPERATION_NOT_ALLOWED)     
      
    /* adi_uart_Close: Pass */
    UNIT_TEST(adi_uart_Close(hDevice), ADI_UART_SUCCESS) 
    return ADI_UNIT_SUCCESS;
}