#include <drivers/gpio/adi_gpio.h>
#include <common.h>
#include <stdio.h>
/*********************************************************UNIT TEST***********************************************************/ 


#ifndef UNIT_TEST_H
#define UNIT_TEST_H

typedef enum
{
  
  ADI_UNIT_SUCCESS = 0u,
  ADI_UNIT_FAILURE = 1u
    
} ADI_UNIT_RESULT;


#define UNIT_TEST(func, dbg) \
do \
{ \
    if ((func) != (dbg)) \
    { \
        DEBUG_MESSAGE("Unit test fail at line %d of %s\r\n", __LINE__, __FILE__); \
        return (ADI_UNIT_FAILURE); \
    } \
} while(0u);

#endif /* UNIT_TEST_H */

/************************************************************ UNIT_TEST_H *****************************************************/


/*

Copyright (c) 2011-2016 Analog Devices, Inc.

All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
  - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  - Modified versions of the software must be conspicuously marked as such.
  - This software is licensed solely and exclusively for use with processors
    manufactured by or for Analog Devices, Inc.
  - This software may not be combined or merged with other code in any manner
    that would cause the software to become subject to terms and conditions
    which differ from those listed here.
  - Neither the name of Analog Devices, Inc. nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
  - The use of this software may or may not infringe the patent rights of one
    or more patent holders.  This license does not release you from the
    requirement that you obtain separate licenses from these patent holders
    to use this software.

THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT,
TITLE, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
NO EVENT SHALL ANALOG DEVICES, INC. OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF CLAIMS OF INTELLECTUAL
PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/
#if 1
#include <stddef.h>		/* for 'NULL' */
#include <string.h>		/* for strlen */
#endif

#include <system_ADuCM3029.h>
#include "common.h"

#include <drivers/crc/adi_crc.h>
#include <drivers/pwr/adi_pwr.h>
#include <drivers/gpio/adi_gpio.h>
#include <drivers/dma/adi_dma.h>
#include <adi_cyclecount.h>
#define CRC_OPEN
#define CRC_CLOSE
#define CRC_REGISTERCALLBACK
#define CRC_SETBITMIRRORR
#define CRC_SETBYTEMIRRORR
#define CRC_SETLSBFIRST
#define CRC_ENABLEWORDSWAP
#define CRC_SETPOLYNOMIALVAL 
#define CRC_SETSEEDVAL
#define CRC_ISINPROGRESS
#define CRC_GETFINALVALUE
#define CRC_GETGETCURRENT_VALUE
#define CRC_COMPUTE
#if defined(ADI_CYCLECOUNT_ENABLED) && (ADI_CYCLECOUNT_ENABLED==1u)
static uint32_t cycleCountId_crc = 0u;                  /* Cycle counting ID for application specified API for which cycle counts are to be obtained */
#endif

#if ADI_CRC_CFG_ENABLE_DMA_SUPPORT==0
#define CC_NAME         "CORE-CRC"
#elif ADI_CFG_ENABLE_CALLBACK_SUPPORT==1
#define CC_NAME         "DMA-CRC-CB"
#else
#define CC_NAME         "DMA-CRC"
#endif

/*=============  D A T A  =============*/

static ADI_CRC_HANDLE   hCrcDev;                        /*!< CRC Device Handle */
static uint8_t          CrcDevMem[ADI_CRC_MEMORY_SIZE]; /*!< Memory to handle CRC Device */

#if ADI_CFG_ENABLE_CALLBACK_SUPPORT==1
static volatile bool bCRCBusy = true;
static volatile uint32_t cbEvent = 0u;
#else
static bool bCRCProgress = true;
#endif

/*=============  L O C A L    F U N C T I O N S  =============*/

#if ADI_CFG_ENABLE_CALLBACK_SUPPORT==1
/* callback function to be used by the interrupt handler associated with
 * the software DMA channel driving the CRC in CRC DMA driven operations */
static void CrcCallback(void *AppHandle, uint32_t Event, void *pArg);
#endif
static ADI_CRC_HANDLE   hCrcDev;                        /*!< CRC Device Handle */
static uint8_t          CrcDevMem[ADI_CRC_MEMORY_SIZE]; /*!< Memory to handle CRC Device */

/*=============  C O D E  =============*/

#if ADI_CFG_ENABLE_CALLBACK_SUPPORT==1
/*
 *  Callback from CRC Driver in DMA driven operations
 *
 * Parameters
 *  - [in]  pCBParam    Callback parameter supplied by application
 *  - [in]  Event       Callback event
 *  - [in]  pArg        Callback argument
 *
 * Returns  None
 *
 */
static void CrcCallback(void *pCBParam, uint32_t Event, void *pArg)
{
    cbEvent = Event;    /* record the nature of the event that occurred in the callback function */
    bCRCBusy = false;   /* CRC is entering IDLE state */
}
#endif

ADI_UNIT_RESULT crc_unit_testing(void)
{ 

  
  int CRC_DEV_NUM;
 ADI_CALLBACK  CrcCallback;
  
 uint8_t * StartAddr;
 uint8_t crc_data_buf;
    const bool bLsbFirst = true;  /* used to set CRC LSB/MSB first calculation */
    static bool bCRCProgress = true;
    uint32_t CRC32_POLYNOMIAL_LE, CRC32_POLYNOMIAL_BE;
    const uint32_t crc_polynomial = (bLsbFirst ? CRC32_POLYNOMIAL_LE: CRC32_POLYNOMIAL_BE);
    const uint32_t NumBytes = sizeof(crc_data_buf) / sizeof(uint8_t);   /* number of bytes in the data buffer to be processed by the CRC engine */
                                        /* no partial byte */
    uint32_t computedCRCValue;
    common_Init();
    ADI_CYCLECOUNT_INITIALIZE();                                        /* Initialize cycle counting */
    adi_pwr_Init(); 
    adi_pwr_SetClockDivider(ADI_CLOCK_HCLK,1);
    adi_pwr_SetClockDivider(ADI_CLOCK_PCLK,1);

#ifdef CRC_OPEN

  /*****************ADI_CRC_SUCCESSS*******************************/
 
UNIT_TEST(adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev),ADI_CRC_SUCCESS)
adi_crc_Close(hCrcDev);
  /***************************************************************************/
  /*****************ADI_CRC_BAD_DEVICE_NUMBER*******************************/
  /**************************************************************************/
 UNIT_TEST(adi_crc_Open (1, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev),ADI_CRC_BAD_DEVICE_NUMBER)
 adi_crc_Close(hCrcDev);

  /*****************ADI_CRC_IN_USE *******************************/
  /**************************************************************************/
 adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev);
 UNIT_TEST(adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev),ADI_CRC_IN_USE )
  adi_crc_Close(hCrcDev);
  /***************************************************************************/
  /*****************ADI_CRC_INSUFFICIENT_MEMORY *******************************/
  /**************************************************************************/
 UNIT_TEST(adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0],10, &hCrcDev),ADI_CRC_INSUFFICIENT_MEMORY)
  adi_crc_Close(hCrcDev);
#endif
 
 
 #ifdef CRC_CLOSE 

  /***************************************************************************/
  /*****************ADI_CRC_SUCCESS*******************************/
  /**************************************************************************/
adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev);
 UNIT_TEST(adi_crc_Close(hCrcDev),ADI_CRC_SUCCESS)
  
  
/***************************************************************************/
/**************************ADI_CRC_FAILURE*********************************/
/**************************************************************************/
      /* Need To Develop Logic */
  
  #endif
   
   
   
#ifdef CRC_SETBITMIRRORR

  /**************************ADI_CRC_SUCCESS*********************************/
  /**************************************************************************/
adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0],ADI_CRC_MEMORY_SIZE, &hCrcDev);
 UNIT_TEST(adi_crc_SetBitMirroring(hCrcDev,true),ADI_CRC_SUCCESS)
 adi_crc_Close(hCrcDev);
 /***************************************************************************/
  /**************************ADI_CRC_BAD_HANDLE******************************/
  /**************************************************************************/
  adi_crc_Open (1, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev);
UNIT_TEST(adi_crc_SetBitMirroring(hCrcDev,true),ADI_CRC_BAD_HANDLE)
 adi_crc_Close(hCrcDev);
  
  /***************************************************************************/
  /*********************ADI_CRC_FN_NOT_PERMITTED*****************************/
  /**************************************************************************/
                /* Need to develop */
 #endif 

#ifdef CRC_SETBYTEMIRRORR
 

  /****************************ADI_CRC_SUCCESSS*******************************/
  /**************************************************************************/
adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0],ADI_CRC_MEMORY_SIZE, &hCrcDev);
UNIT_TEST(adi_crc_SetByteMirroring(hCrcDev,true),ADI_CRC_SUCCESS)
adi_crc_Close(hCrcDev);
  
 /***************************************************************************/
  /**************************ADI_CRC_BAD_HANDLE******************************/
  /**************************************************************************/
  adi_crc_Open (1, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev);
  UNIT_TEST(adi_crc_SetByteMirroring(hCrcDev,true),ADI_CRC_BAD_HANDLE)
  adi_crc_Close(hCrcDev);
  
  /***************************************************************************/
  /*********************ADI_CRC_FN_NOT_PERMITTED*****************************/
  /**************************************************************************/
                      /* Need To Develop Logic */
  
 #endif 
#ifdef CRC_SETLSBFIRST

  /***************************ADI_CRC_SUCCESSS*******************************/
  /**************************************************************************/
adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0],ADI_CRC_MEMORY_SIZE, &hCrcDev);
 UNIT_TEST(adi_crc_SetLSBFirst(hCrcDev,true),ADI_CRC_SUCCESS)
 adi_crc_Close(hCrcDev);
  
 /***************************************************************************/
  /**************************ADI_CRC_BAD_HANDLE******************************/
  /**************************************************************************/
  adi_crc_Open (1, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev);
  UNIT_TEST(adi_crc_SetLSBFirst(hCrcDev,true),ADI_CRC_BAD_HANDLE)
  adi_crc_Close(hCrcDev);
  
  /***************************************************************************/
  /*********************ADI_CRC_FN_NOT_PERMITTED*****************************/
  /**************************************************************************/
                      /* Need To Develop Logic */
#endif  
             
#ifdef CRC_ENABLEWORDSWAP

  /**************************ADI_CRC_SUCCESSS*******************************/
  /**************************************************************************/
adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0],ADI_CRC_MEMORY_SIZE, &hCrcDev);
 UNIT_TEST(adi_crc_EnableWordSwap(hCrcDev,true),ADI_CRC_SUCCESS)
    adi_crc_Close(hCrcDev);
  
 /***************************************************************************/
  /**************************ADI_CRC_BAD_HANDLE******************************/
  /**************************************************************************/
  adi_crc_Open (1, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev);
   UNIT_TEST(adi_crc_EnableWordSwap(hCrcDev,true),ADI_CRC_BAD_HANDLE)
  adi_crc_Close(hCrcDev);
  
  /***************************************************************************/
  /*****************ADI_CRC_FN_NOT_PERMITTED*****************************/
  /**************************************************************************/
              /* Need To Develop Logic */
 #endif 
#ifdef CRC_REGISTERCALLBACK

  /*****************ADI_CRC_SUCCESSS*******************************/
  /***************************************************************/
  adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev);
 UNIT_TEST(adi_crc_RegisterCallback(hCrcDev, CrcCallback, hCrcDev),ADI_CRC_SUCCESS)
  adi_crc_Close(hCrcDev);
  
  /***************************************************************************/
  /*************************ADI_CRC_BAD_HANDLE*******************************/
  /**************************************************************************/
  adi_crc_Open (1, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev);
   void *const pCBParam;
UNIT_TEST(adi_crc_RegisterCallback(hCrcDev, CrcCallback,pCBParam),ADI_CRC_BAD_HANDLE)
   adi_crc_Close(hCrcDev);
#endif   

#ifdef CRC_SETSEEDVAL
uint32_t  CRC32_SEED_VALUE;

  /*****************ADI_CRC_SUCCESSS*******************************/
  /***************************************************************/
 adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev);
 UNIT_TEST(adi_crc_SetCrcSeedVal (hCrcDev, CRC32_SEED_VALUE),ADI_CRC_SUCCESS)
adi_crc_Close(hCrcDev);
  
  /***************************************************************************/
  /**************************ADI_CRC_BAD_HANDLE******************************/
  /**************************************************************************/
  adi_crc_Open (1, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev); 
 UNIT_TEST(adi_crc_SetCrcSeedVal (hCrcDev, CRC32_SEED_VALUE),ADI_CRC_BAD_HANDLE)
 
  adi_crc_Close(hCrcDev);
  
  /***************************************************************************/
  /*****************ADI_CRC_FN_NOT_PERMITTED*****************************/
  /**************************************************************************/
                /* Need To Develop Logic */
#endif
#ifdef CRC_SETPOLYNOMIALVAL 

  /*****************ADI_CRC_SUCCESSS*******************************/
  /***************************************************************/
 adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev);
 UNIT_TEST(adi_crc_SetPolynomialVal(hCrcDev, crc_polynomial),ADI_CRC_SUCCESS)
 adi_crc_Close(hCrcDev);
  
  /***************************************************************************/
  /*****************ADI_CRC_BAD_HANDLE******************************/
  /**************************************************************************/
  adi_crc_Open (1, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev);
 UNIT_TEST(adi_crc_SetPolynomialVal(hCrcDev, crc_polynomial),ADI_CRC_BAD_HANDLE)
  adi_crc_Close(hCrcDev);
  
  /***************************************************************************/
  /*****************ADI_CRC_FN_NOT_PERMITTED*****************************/
  /**************************************************************************/
        /* Need To Develop Logic */
#endif
#ifdef CRC_ISINPROGRESS  

  /***************************ADI_CRC_SUCCESSS*******************************/
  /**************************************************************************/
adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev);
UNIT_TEST(adi_crc_IsCrcInProgress(hCrcDev, &bCRCProgress),ADI_CRC_SUCCESS)
adi_crc_Close(hCrcDev);
  
   /***************************************************************************/
  /**************************ADI_CRC_BAD_HANDLE******************************/
  /**************************************************************************/
  adi_crc_Open (1, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev);
UNIT_TEST(adi_crc_IsCrcInProgress(hCrcDev, &bCRCProgress),ADI_CRC_BAD_HANDLE)
adi_crc_Close(hCrcDev);
#endif

#ifdef CRC_GETFINALVALUE

  /*****************ADI_CRC_SUCCESSS*******************************/
  /**************************************************************************/
adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev);
UNIT_TEST(adi_crc_GetFinalCrcVal(hCrcDev, &computedCRCValue),ADI_CRC_SUCCESS)
  adi_crc_Close(hCrcDev);
  
   /***************************************************************************/
  /***************************ADI_CRC_BAD_HANDLE******************************/
  /**************************************************************************/
  adi_crc_Open (1, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev);
UNIT_TEST(adi_crc_GetFinalCrcVal(hCrcDev, &computedCRCValue),ADI_CRC_BAD_HANDLE)
 adi_crc_Close(hCrcDev);
  
#endif

#ifdef CRC_GETGETCURRENT_VALUE
  uint32_t pCurrentCrcVal;
  /*****************ADI_CRC_SUCCESSS*******************************/
  /**************************************************************************/
adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev);
UNIT_TEST(adi_crc_GetCurrentCrcVal(hCrcDev, &pCurrentCrcVal),ADI_CRC_SUCCESS)
  adi_crc_Close(hCrcDev);
 
  /*****************ADI_CRC_BAD_HANDLE******************************/
  /**************************************************************************/
  adi_crc_Open (1, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev);
UNIT_TEST(adi_crc_GetCurrentCrcVal(hCrcDev,&pCurrentCrcVal),ADI_CRC_BAD_HANDLE)
 adi_crc_Close(hCrcDev);
#endif

#ifdef CRC_COMPUTE

 uint32_t NumBits;
/***************************************************************************/
/****************************ADI_CRC_SUCCESS*******************************/
/**************************************************************************/
adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0], ADI_CRC_MEMORY_SIZE, &hCrcDev);
UNIT_TEST(adi_crc_Compute (hCrcDev, StartAddr, NumBytes, NumBits),ADI_CRC_SUCCESS)
 adi_crc_Close(hCrcDev);
  

/*****************ADI_CRC_INVALID_PARAMETER*******************************/
/**************************************************************************/
adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0],ADI_CRC_MEMORY_SIZE, &hCrcDev);
UNIT_TEST(adi_crc_Compute(hCrcDev, StartAddr,10,10000),ADI_CRC_INVALID_PARAMETER)
 adi_crc_Close(hCrcDev);
 

/***************************ADI_CRC_BAD_HANDLE*******************************/
/**************************************************************************/
adi_crc_Open (CRC_DEV_NUM, &CrcDevMem[0],10u, &hCrcDev);
UNIT_TEST(adi_crc_Compute(hCrcDev, StartAddr, NumBytes, NumBits),ADI_CRC_BAD_HANDLE)
 adi_crc_Close(hCrcDev);
/***************************************************************************/
/*****************ADI_CRC_FN_NOT_SUPPORTED ******************************/
/**************************************************************************/
                  /* Need To Develop Logic */  
  
/***************************************************************************/
/*****************ADI_CRC_FN_NOT_PERMITTED******************************/
/**************************************************************************/
                  /* Need To Develop Logic */
  
  /***************************************************************************/
/*****************ADI_CRC_INVALID_DMA_CHANNEL*******************************/
/**************************************************************************/
                  /* Need To Develop Logic */
  
#endif  
return ADI_UNIT_SUCCESS;  
}
           