#include <drivers/gpio/adi_gpio.h>
#include <common.h>
#include <stdio.h>
/*********************************************************UNIT TEST***********************************************************/ 


#ifndef UNIT_TEST_H
#define UNIT_TEST_H

typedef enum
{
  
  ADI_UNIT_SUCCESS = 0u,
  ADI_UNIT_FAILURE = 1u
    
} ADI_UNIT_RESULT;


#define UNIT_TEST(func, dbg) \
do \
{ \
    if ((func) != (dbg)) \
    { \
        DEBUG_MESSAGE("Unit test fail at line %d of %s\r\n", __LINE__, __FILE__); \
        return (ADI_UNIT_FAILURE); \
    } \
} while(0u);

#endif /* UNIT_TEST_H */

/************************************************************ UNIT_TEST_H *****************************************************/


/*!****************************************************************************
 * @file    spi_unit_testing.c
 * @brief   Unit tests for SPI driver
 * @details Checks inputs and outputs of all SPI APIs
******************************************************************************/

/* Include the unit test framework */
//#include <unit_testing.h>

/* Include the driver under test */
#include <drivers/spi/adi_spi.h>

/* Power driver is needed to get the clocks at expected values */
#include <drivers/pwr/adi_pwr.h>

ADI_UNIT_RESULT spi_master_testing(ADI_SPI_HANDLE hDevice, bool bDMA, bool bRD_CTL)
{
    uint32_t nHWErrors;
    bool     bComplete;
    uint8_t test_tx[4u] = {0xAA, 0xBB, 0xCC, 0xDD};
    uint8_t test_rx[4u] = {0x00, 0x00, 0x00, 0x00};

    ADI_SPI_TRANSCEIVER transceive;
    transceive.TransmitterBytes = 4u;
    transceive.ReceiverBytes    = 4u;
    transceive.nTxIncrement     = 1u;
    transceive.nRxIncrement     = 1u;
    transceive.bDMA             = bDMA;
    transceive.bRD_CTL          = bRD_CTL;
    transceive.pTransmitter     = test_tx;
    transceive.pReceiver        = test_rx;

    /* Move to master mode with loopback */
    UNIT_TEST(adi_spi_SetMasterMode(hDevice, true), ADI_SPI_SUCCESS)
    UNIT_TEST(adi_spi_SetLoopback(hDevice, true), ADI_SPI_SUCCESS)

    /* Blocking mode */
    UNIT_TEST(adi_spi_MasterReadWrite(hDevice, &transceive), ADI_SPI_SUCCESS)

    /* Non-blocking mode */
    UNIT_TEST(adi_spi_MasterSubmitBuffer(hDevice, &transceive), ADI_SPI_SUCCESS)
    UNIT_TEST(adi_spi_GetBuffer(hDevice, &nHWErrors), ADI_SPI_SUCCESS)
    UNIT_TEST(nHWErrors, ADI_SPI_HW_ERROR_NONE)
    UNIT_TEST(adi_spi_isBufferAvailable(hDevice, &bComplete), ADI_SPI_SUCCESS)
    UNIT_TEST(bComplete, true)     

    return ADI_UNIT_SUCCESS;
}

/* Top-level unit testing for the driver */
ADI_UNIT_RESULT spi_unit_testing(void)
{   
    ADI_SPI_HANDLE hDevice;
    uint8_t        DeviceMemory[ADI_SPI_MEMORY_SIZE];

    /* The power test may have modified the clocks, restore to 26 MHz */
    UNIT_TEST(adi_pwr_Init(), ADI_PWR_SUCCESS)
    UNIT_TEST(adi_pwr_SetClockDivider(ADI_CLOCK_HCLK, 1u), ADI_PWR_SUCCESS)
    UNIT_TEST(adi_pwr_SetClockDivider(ADI_CLOCK_PCLK, 1u), ADI_PWR_SUCCESS)    

   /* Test all SPI devices */
   for (uint8_t i = 0u; i < 3u; i++) {

       /* adi_spi_Open */
       {
           /* Bad inputs - no way to stimulate ADI_SPI_SEMAPHORE_FAILED without stub code which isn't supported now */
           UNIT_TEST(adi_spi_Open(3u, DeviceMemory, ADI_SPI_MEMORY_SIZE, &hDevice), ADI_SPI_INVALID_DEVICE_NUM)
           UNIT_TEST(adi_spi_Open(i, DeviceMemory, ADI_SPI_MEMORY_SIZE + 1u, &hDevice), ADI_SPI_INVALID_PARAM)

           /* Good inputs */
           UNIT_TEST(adi_spi_Open(i, DeviceMemory, ADI_SPI_MEMORY_SIZE, &hDevice), ADI_SPI_SUCCESS)
       }

         /* adi_spi_RegisterCallback */
       {
           /* Bad inputs */
          // UNIT_TEST(adi_spi_RegisterCallback(NULL, NULL, NULL), ADI_SPI_INVALID_HANDLE)

           /* Good inputs */
           UNIT_TEST(adi_spi_RegisterCallback(hDevice, NULL, NULL), ADI_SPI_SUCCESS)
       }
       
         /* adi_spi_SetIrqmode */
       {
          /* Bad inputs */
          //UNIT_TEST(adi_spi_SetIrqmode(NULL, 0u), ADI_SPI_INVALID_HANDLE)
         // UNIT_TEST(adi_spi_SetIrqmode(hDevice, 8u), ADI_SPI_INVALID_PARAM)

          /* Good inputs */
          UNIT_TEST(adi_spi_SetIrqmode(hDevice, 0u), ADI_SPI_SUCCESS)
       }
       
        /* adi_spi_SetContinuousMode */
       {
           /* Bad inputs */
           //UNIT_TEST(adi_spi_SetContinuousMode(NULL, false), ADI_SPI_INVALID_HANDLE)

           /* Good inputs */
           UNIT_TEST(adi_spi_SetContinuousMode(hDevice, true), ADI_SPI_SUCCESS)
           UNIT_TEST(adi_spi_SetContinuousMode(hDevice, false), ADI_SPI_SUCCESS)
       }
       
       /* adi_spi_SetLoopback */
       {
           /* Bad inputs */
          // UNIT_TEST(adi_spi_SetLoopback(NULL, false), ADI_SPI_INVALID_HANDLE)

           /* Good inputs */
           UNIT_TEST(adi_spi_SetLoopback(hDevice, true), ADI_SPI_SUCCESS)
           UNIT_TEST(adi_spi_SetLoopback(hDevice, false), ADI_SPI_SUCCESS)      
       }
      
       /* adi_spi_SetLoopback */
       {
           /* Bad inputs */
           //UNIT_TEST(adi_spi_SetMasterMode(NULL, false), ADI_SPI_INVALID_HANDLE)

           /* Good inputs */
           UNIT_TEST(adi_spi_SetMasterMode(hDevice, true), ADI_SPI_SUCCESS)
           UNIT_TEST(adi_spi_SetMasterMode(hDevice, false), ADI_SPI_SUCCESS)       
       }

       /* adi_spi_SetReceiveOverflow */
       {
           /* Bad inputs */
           //UNIT_TEST(adi_spi_SetReceiveOverflow(NULL, false), ADI_SPI_INVALID_HANDLE)

           /* Good inputs */
           UNIT_TEST(adi_spi_SetReceiveOverflow(hDevice, true), ADI_SPI_SUCCESS)
           UNIT_TEST(adi_spi_SetReceiveOverflow(hDevice, false), ADI_SPI_SUCCESS)       
       }

       /* adi_spi_SetTransmitUnderflow */
       {
           /* Bad inputs */
          // UNIT_TEST(adi_spi_SetTransmitUnderflow(NULL, false), ADI_SPI_INVALID_HANDLE)

           /* Good inputs */
           UNIT_TEST(adi_spi_SetTransmitUnderflow(hDevice, true), ADI_SPI_SUCCESS)
           UNIT_TEST(adi_spi_SetTransmitUnderflow(hDevice, false), ADI_SPI_SUCCESS)       
       }

       /* adi_spi_SetBitrate */
       {
           /* Bad inputs - assuming PCLK is operating at 26 MHz */
          // UNIT_TEST(adi_spi_SetBitrate(NULL, 65000000u), ADI_SPI_INVALID_HANDLE)
           UNIT_TEST(adi_spi_SetBitrate(hDevice, 14000000u), ADI_SPI_BAD_SYS_CLOCK)
          // UNIT_TEST(adi_spi_SetBitrate(hDevice, 100u), ADI_SPI_INVALID_PARAM)

           /* Good inputs */
           UNIT_TEST(adi_spi_SetBitrate(hDevice, 2500000), ADI_SPI_SUCCESS)
       }

       /* adi_spi_GetBitrate */
       {
           uint32_t nBitrate;

           /* Bad inputs - no way to stimulate ADI_SPI_FAILURE without stub code which isn't supported now */
         // UNIT_TEST(adi_spi_GetBitrate(NULL, &nBitrate), ADI_SPI_INVALID_HANDLE)

           /* Good inputs */
           UNIT_TEST(adi_spi_GetBitrate(hDevice, &nBitrate), ADI_SPI_SUCCESS)
       }

       /* adi_spi_SetChipSelect */
       {
           /* Bad inputs */
          // UNIT_TEST(adi_spi_SetChipSelect(NULL, ADI_SPI_CS_NONE), ADI_SPI_INVALID_HANDLE)

           /* Good inputs */
           UNIT_TEST(adi_spi_SetChipSelect(hDevice, ADI_SPI_CS_NONE), ADI_SPI_SUCCESS)
           UNIT_TEST(adi_spi_SetChipSelect(hDevice, ADI_SPI_CS0), ADI_SPI_SUCCESS)
           UNIT_TEST(adi_spi_SetChipSelect(hDevice, ADI_SPI_CS1), ADI_SPI_SUCCESS)
           UNIT_TEST(adi_spi_SetChipSelect(hDevice, ADI_SPI_CS2), ADI_SPI_SUCCESS)
           UNIT_TEST(adi_spi_SetChipSelect(hDevice, ADI_SPI_CS3), ADI_SPI_SUCCESS)
       }  
       
       
        /* adi_spi_MasterReadWrite, adi_spi_MasterSubmitBuffer, adi_spi_GetBuffer, and adi_spi_isBufferAvailable */
       {
           ADI_SPI_TRANSCEIVER transceive;
           uint32_t            nHWErrors;
           bool                bComplete;

           /* Bad handle - no way to stimulate ADI_SPI_PEND_FAILED/ADI_SPI_SEMAPHORE_FAILED without stub code which isn't supported now */
          // UNIT_TEST(adi_spi_MasterReadWrite   (NULL, &transceive), ADI_SPI_INVALID_HANDLE)
         //  UNIT_TEST(adi_spi_MasterSubmitBuffer(NULL, &transceive), ADI_SPI_INVALID_HANDLE)
         //  UNIT_TEST(adi_spi_GetBuffer         (NULL, &nHWErrors), ADI_SPI_INVALID_HANDLE)
         //  UNIT_TEST(adi_spi_isBufferAvailable (NULL, &bComplete), ADI_SPI_INVALID_HANDLE)            

           /* Bad pointers */
           transceive.pTransmitter = NULL;
           transceive.pReceiver    = NULL;             
           UNIT_TEST(adi_spi_MasterReadWrite   (hDevice, &transceive), ADI_SPI_INVALID_POINTER)            
           UNIT_TEST(adi_spi_MasterSubmitBuffer(hDevice, &transceive), ADI_SPI_INVALID_POINTER)

           /* Bad read control */
           uint8_t notNullPointer;
           transceive.pTransmitter = &notNullPointer;
           transceive.pReceiver    = &notNullPointer;
           transceive.bRD_CTL = true;
           transceive.TransmitterBytes = 17u;             
         //  UNIT_TEST(adi_spi_MasterReadWrite   (hDevice, &transceive), ADI_SPI_INVALID_PARAM)            
         //  UNIT_TEST(adi_spi_MasterSubmitBuffer(hDevice, &transceive), ADI_SPI_INVALID_PARAM)

           /* Good inputs - sweep through with full truth table for full code coverage */
           UNIT_TEST(spi_master_testing(hDevice, false, false), ADI_UNIT_SUCCESS)
           UNIT_TEST(spi_master_testing(hDevice, true, false), ADI_UNIT_SUCCESS)
           UNIT_TEST(spi_master_testing(hDevice, false, true), ADI_UNIT_SUCCESS)
           UNIT_TEST(spi_master_testing(hDevice, true, true), ADI_UNIT_SUCCESS)            
       } 
       
            /* adi_spi_SlaveReadWrite and adi_spi_SlaveSubmitBuffer */
       {
           ADI_SPI_TRANSCEIVER transceive;
           uint8_t             notNullPointer;

           /* Move the SPI controller into slave mode */
           UNIT_TEST(adi_spi_SetMasterMode(hDevice, false), ADI_SPI_SUCCESS)

           /* Bad handle - no way to stimulate ADI_SPI_PEND_FAILED without stub code which isn't supported now */
         //  UNIT_TEST(adi_spi_SlaveReadWrite   (NULL, &transceive), ADI_SPI_INVALID_HANDLE)
         //  UNIT_TEST(adi_spi_SlaveSubmitBuffer(NULL, &transceive), ADI_SPI_INVALID_HANDLE)

           /* Bad pointers */
           transceive.pTransmitter = NULL;
           transceive.pReceiver    = NULL;             
           UNIT_TEST(adi_spi_SlaveReadWrite  (hDevice,  &transceive), ADI_SPI_INVALID_POINTER)                         
           UNIT_TEST(adi_spi_SlaveSubmitBuffer(hDevice, &transceive), ADI_SPI_INVALID_POINTER)

           /* Bad number of bytes */
           transceive.pTransmitter     = &notNullPointer;
           transceive.pReceiver        = &notNullPointer;               
           transceive.TransmitterBytes = 0u;
           transceive.ReceiverBytes    = 0u;          
           UNIT_TEST(adi_spi_SlaveReadWrite   (hDevice, &transceive), ADI_SPI_INVALID_PARAM)                         
           UNIT_TEST(adi_spi_SlaveSubmitBuffer(hDevice, &transceive), ADI_SPI_INVALID_PARAM) 

           /* Bad byte counter and pointer combo, for TX */
           transceive.TransmitterBytes = 4u;
           transceive.pTransmitter     = NULL;
           UNIT_TEST(adi_spi_SlaveReadWrite   (hDevice, &transceive), ADI_SPI_INVALID_PARAM)                         
           UNIT_TEST(adi_spi_SlaveSubmitBuffer(hDevice, &transceive), ADI_SPI_INVALID_PARAM) 
           transceive.TransmitterBytes = 0u;
           transceive.pTransmitter = &notNullPointer;
           UNIT_TEST(adi_spi_SlaveReadWrite   (hDevice, &transceive), ADI_SPI_INVALID_PARAM)                         
           UNIT_TEST(adi_spi_SlaveSubmitBuffer(hDevice, &transceive), ADI_SPI_INVALID_PARAM)

           /* Bad byte counter and pointer combo, for RX */
           transceive.ReceiverBytes = 4u;
           transceive.pReceiver     = NULL;
           UNIT_TEST(adi_spi_SlaveReadWrite   (hDevice, &transceive), ADI_SPI_INVALID_PARAM)                         
           UNIT_TEST(adi_spi_SlaveSubmitBuffer(hDevice, &transceive), ADI_SPI_INVALID_PARAM) 
           transceive.ReceiverBytes = 0u;
           transceive.pReceiver = &notNullPointer;
           UNIT_TEST(adi_spi_SlaveReadWrite   (hDevice, &transceive), ADI_SPI_INVALID_PARAM)                  
           UNIT_TEST(adi_spi_SlaveSubmitBuffer(hDevice, &transceive), ADI_SPI_INVALID_PARAM)

           /* Bad DMA alignment */
           transceive.bDMA             = true;
           transceive.ReceiverBytes    = 4u;
           transceive.pReceiver        = &notNullPointer;
           transceive.TransmitterBytes = 4u;
           transceive.pTransmitter     = (void *) (0x20000001UL);
         //  UNIT_TEST(adi_spi_SlaveReadWrite   (hDevice, &transceive), ADI_SPI_INVALID_PARAM)                  
         //  UNIT_TEST(adi_spi_SlaveSubmitBuffer(hDevice, &transceive), ADI_SPI_INVALID_PARAM)         

           /* Can't do a good input test for the slave mode since the loopback doesn't work in slave mode, it will get stuck waiting for a master */
        }

       /* adi_spi_Close */
       {
           /* Bad inputs - no way to stimulate ADI_SPI_SEMAPHORE_FAILED without stub code which isn't supported now */
          // UNIT_TEST(adi_spi_Close(NULL), ADI_SPI_INVALID_HANDLE)

           /* Good inputs */
           UNIT_TEST(adi_spi_Close(hDevice), ADI_SPI_SUCCESS)
       }         


   }

   return ADI_UNIT_SUCCESS;
}
