#include <drivers/gpio/adi_gpio.h>
#include <common.h>
#include <stdio.h>
/*********************************************************UNIT TEST***********************************************************/ 


#ifndef UNIT_TEST_H
#define UNIT_TEST_H

typedef enum
{
  
  ADI_UNIT_SUCCESS = 0u,
  ADI_UNIT_FAILURE = 1u
    
} ADI_UNIT_RESULT;


#define UNIT_TEST(func, dbg) \
do \
{ \
    if ((func) != (dbg)) \
    { \
        DEBUG_MESSAGE("Unit test fail at line %d of %s\r\n", __LINE__, __FILE__); \
        return (ADI_UNIT_FAILURE); \
    } \
} while(0u);

#endif /* UNIT_TEST_H */

/************************************************************ UNIT_TEST_H *****************************************************/

static uint8_t gpioMemory[ADI_GPIO_MEMORY_SIZE];

ADI_UNIT_RESULT gpio_unit_testing(void)
{
    ADI_GPIO_PORT i = (ADI_GPIO_PORT) 0;
    uint32_t j = (1u << 0); 
    uint16_t pval;
    
   /* adi_gpio_Init: Pass */
    UNIT_TEST(adi_gpio_Init(gpioMemory, ADI_GPIO_MEMORY_SIZE), ADI_GPIO_SUCCESS)
      
    /* adi_gpio_UnInit: Pass */
    UNIT_TEST(adi_gpio_UnInit(), ADI_GPIO_SUCCESS)
      
   /* adi_gpio_Init: Fail */
    UNIT_TEST(adi_gpio_Init(NULL, ADI_GPIO_MEMORY_SIZE), ADI_GPIO_NULL_PARAMETER)
    UNIT_TEST(adi_gpio_Init(gpioMemory, NULL), ADI_GPIO_INVALID_MEMORY_SIZE)

    /* adi_gpio_RegisterCallback: Fail */
   //UNIT_TEST(adi_gpio_RegisterCallback(ADI_GPIO_INTA_IRQ, NULL, NULL), ADI_GPIO_NOT_INITIALIZED)

    /* adi_gpio_Init: Pass */
    UNIT_TEST(adi_gpio_Init(gpioMemory, ADI_GPIO_MEMORY_SIZE), ADI_GPIO_SUCCESS)
    
    /* adi_gpio_RegisterCallback: Pass */
    UNIT_TEST(adi_gpio_RegisterCallback(ADI_GPIO_INTA_IRQ, NULL, NULL), ADI_GPIO_SUCCESS)
    UNIT_TEST(adi_gpio_RegisterCallback(ADI_GPIO_INTB_IRQ, NULL, NULL), ADI_GPIO_SUCCESS)
     
#ifdef 0
    for(i = (ADI_GPIO_PORT) 0; i < ADI_GPIO_NUM_PORTS; i++)
    {
        for(j = (1 << 0); j <= (1 << 15); j = (j << 1))
        {          
            if((i == ADI_GPIO_PORT2) && ((j == (1 << 12)) | (j == (1 << 13)) | (j == (1 << 14)) | (j == (1 << 15))))
            {
                /* adi_gpio_SetGroupInterruptPins: Fail */
               UNIT_TEST(adi_gpio_SetGroupInterruptPins(i, ADI_GPIO_INTA_IRQ, (ADI_GPIO_DATA)j), ADI_GPIO_INVALID_PINS)
               UNIT_TEST(adi_gpio_SetGroupInterruptPins(i, ADI_GPIO_INTB_IRQ, (ADI_GPIO_DATA)j), ADI_GPIO_INVALID_PINS)
                  
                /* adi_gpio_SetGroupInterruptPolarity: Fail */
               UNIT_TEST(adi_gpio_SetGroupInterruptPolarity(i, (ADI_GPIO_DATA)j), ADI_GPIO_INVALID_PINS)
               UNIT_TEST(adi_gpio_SetGroupInterruptPolarity(i, (ADI_GPIO_DATA)j), ADI_GPIO_INVALID_PINS)
             
                /* adi_gpio_OutputEnable: Fail */
               UNIT_TEST(adi_gpio_OutputEnable(i, (ADI_GPIO_DATA)j, true), ADI_GPIO_INVALID_PINS)
               UNIT_TEST(adi_gpio_OutputEnable(i, (ADI_GPIO_DATA)j, true), ADI_GPIO_INVALID_PINS)
               UNIT_TEST(adi_gpio_OutputEnable(i, (ADI_GPIO_DATA)j, false), ADI_GPIO_INVALID_PINS)
               UNIT_TEST(adi_gpio_OutputEnable(i, (ADI_GPIO_DATA)j, false), ADI_GPIO_INVALID_PINS)
                  
                /* adi_gpio_OutputEnable: Fail */
               UNIT_TEST(adi_gpio_InputEnable(i, (ADI_GPIO_DATA)j, true), ADI_GPIO_INVALID_PINS)
               UNIT_TEST(adi_gpio_InputEnable(i, (ADI_GPIO_DATA)j, true), ADI_GPIO_INVALID_PINS)                  
               UNIT_TEST(adi_gpio_InputEnable(i, (ADI_GPIO_DATA)j, false), ADI_GPIO_INVALID_PINS)
                UNIT_TEST(adi_gpio_InputEnable(i, (ADI_GPIO_DATA)j, false), ADI_GPIO_INVALID_PINS)  
                  
                /* adi_gpio_PullUpEnable: Fail */
                UNIT_TEST(adi_gpio_PullUpEnable(i, (ADI_GPIO_DATA)j, true), ADI_GPIO_INVALID_PINS)
                UNIT_TEST(adi_gpio_PullUpEnable(i, (ADI_GPIO_DATA)j, true), ADI_GPIO_INVALID_PINS) 
                UNIT_TEST(adi_gpio_PullUpEnable(i, (ADI_GPIO_DATA)j, false), ADI_GPIO_INVALID_PINS)
                UNIT_TEST(adi_gpio_PullUpEnable(i, (ADI_GPIO_DATA)j, false), ADI_GPIO_INVALID_PINS)  
                  
                /* adi_gpio_SetHigh: Fail */
                UNIT_TEST(adi_gpio_SetHigh(i, (ADI_GPIO_DATA)j), ADI_GPIO_INVALID_PINS)
                UNIT_TEST(adi_gpio_SetHigh(i, (ADI_GPIO_DATA)j), ADI_GPIO_INVALID_PINS)

                /* adi_gpio_SetLow: Fail */
                UNIT_TEST(adi_gpio_SetLow(i, (ADI_GPIO_DATA)j), ADI_GPIO_INVALID_PINS)
                UNIT_TEST(adi_gpio_SetLow(i, (ADI_GPIO_DATA)j), ADI_GPIO_INVALID_PINS)  
                 
                /* adi_gpio_Toggle: Fail */
                UNIT_TEST(adi_gpio_Toggle(i, (ADI_GPIO_DATA)j), ADI_GPIO_INVALID_PINS)
                UNIT_TEST(adi_gpio_Toggle(i, (ADI_GPIO_DATA)j), ADI_GPIO_INVALID_PINS)

                /* adi_gpio_SetData: Fail */
                UNIT_TEST(adi_gpio_SetData(i, (ADI_GPIO_DATA)j), ADI_GPIO_INVALID_PINS)
                UNIT_TEST(adi_gpio_SetData(i, (ADI_GPIO_DATA)j), ADI_GPIO_INVALID_PINS)                    
                
                /* adi_gpio_GetData: Fail */
                UNIT_TEST(adi_gpio_GetData(i, (ADI_GPIO_DATA)j, &pval), ADI_GPIO_INVALID_PINS)
                UNIT_TEST(adi_gpio_GetData(i, (ADI_GPIO_DATA)j, &pval), ADI_GPIO_INVALID_PINS)        
            }
            else
            {
                /* adi_gpio_SetGroupInterruptPins: Pass */
                UNIT_TEST(adi_gpio_SetGroupInterruptPins(i, ADI_GPIO_INTA_IRQ, (ADI_GPIO_DATA)j), ADI_GPIO_SUCCESS)
                UNIT_TEST(adi_gpio_SetGroupInterruptPins(i, ADI_GPIO_INTB_IRQ, (ADI_GPIO_DATA)j), ADI_GPIO_SUCCESS)
                  
                /* adi_gpio_SetGroupInterruptPolarity: Pass */
                UNIT_TEST(adi_gpio_SetGroupInterruptPolarity(i, (ADI_GPIO_DATA)j), ADI_GPIO_SUCCESS)
                UNIT_TEST(adi_gpio_SetGroupInterruptPolarity(i, (ADI_GPIO_DATA)j), ADI_GPIO_SUCCESS)
                
                /* adi_gpio_OutputEnable: Pass */
                UNIT_TEST(adi_gpio_OutputEnable(i, (ADI_GPIO_DATA)j, true), ADI_GPIO_SUCCESS)
                UNIT_TEST(adi_gpio_OutputEnable(i, (ADI_GPIO_DATA)j, true), ADI_GPIO_SUCCESS)
                UNIT_TEST(adi_gpio_OutputEnable(i, (ADI_GPIO_DATA)j, false), ADI_GPIO_SUCCESS)
                UNIT_TEST(adi_gpio_OutputEnable(i, (ADI_GPIO_DATA)j, false), ADI_GPIO_SUCCESS)
                  
                /* adi_gpio_InputEnable: Pass */
                UNIT_TEST(adi_gpio_InputEnable(i, (ADI_GPIO_DATA)j, true), ADI_GPIO_SUCCESS)
                UNIT_TEST(adi_gpio_InputEnable(i, (ADI_GPIO_DATA)j, true), ADI_GPIO_SUCCESS)
                UNIT_TEST(adi_gpio_InputEnable(i, (ADI_GPIO_DATA)j, false), ADI_GPIO_SUCCESS)
                UNIT_TEST(adi_gpio_InputEnable(i, (ADI_GPIO_DATA)j, false), ADI_GPIO_SUCCESS)
                  
                /* adi_gpio_PullUpEnable: Pass */
                UNIT_TEST(adi_gpio_PullUpEnable(i, (ADI_GPIO_DATA)j, true), ADI_GPIO_SUCCESS)
                UNIT_TEST(adi_gpio_PullUpEnable(i, (ADI_GPIO_DATA)j, true), ADI_GPIO_SUCCESS) 
                UNIT_TEST(adi_gpio_PullUpEnable(i, (ADI_GPIO_DATA)j, false), ADI_GPIO_SUCCESS)
                UNIT_TEST(adi_gpio_PullUpEnable(i, (ADI_GPIO_DATA)j, false), ADI_GPIO_SUCCESS)  
 
                /* adi_gpio_SetHigh: Pass */
                UNIT_TEST(adi_gpio_SetHigh(i, (ADI_GPIO_DATA)j), ADI_GPIO_SUCCESS)
                UNIT_TEST(adi_gpio_SetHigh(i, (ADI_GPIO_DATA)j), ADI_GPIO_SUCCESS)

                /* adi_gpio_SetLow: Pass */
                UNIT_TEST(adi_gpio_SetLow(i, (ADI_GPIO_DATA)j), ADI_GPIO_SUCCESS)
                UNIT_TEST(adi_gpio_SetLow(i, (ADI_GPIO_DATA)j), ADI_GPIO_SUCCESS)  
       
                /* adi_gpio_Toggle: Pass */
                UNIT_TEST(adi_gpio_Toggle(i, (ADI_GPIO_DATA)j), ADI_GPIO_SUCCESS)
                UNIT_TEST(adi_gpio_Toggle(i, (ADI_GPIO_DATA)j), ADI_GPIO_SUCCESS)

                /* adi_gpio_SetData: Pass */
                UNIT_TEST(adi_gpio_SetData(i, (ADI_GPIO_DATA)j), ADI_GPIO_SUCCESS)
                UNIT_TEST(adi_gpio_SetData(i, (ADI_GPIO_DATA)j), ADI_GPIO_SUCCESS)                    
                
                /* adi_gpio_GetData: Pass */
                UNIT_TEST(adi_gpio_GetData(i, (ADI_GPIO_DATA)j, &pval), ADI_GPIO_SUCCESS)
                UNIT_TEST(adi_gpio_GetData(i, (ADI_GPIO_DATA)j, &pval), ADI_GPIO_SUCCESS)                      
            }
        }
    } 
#endif    
    
    
  /* adi_gpio_UnInit: Pass */
    UNIT_TEST(adi_gpio_UnInit(), ADI_GPIO_SUCCESS)
 
    for(i = (ADI_GPIO_PORT) 0; i < ADI_GPIO_NUM_PORTS; i++)
    {
        for(j = (1 << 0); j <= (1 << 15); j = (j << 1))
        {          
            /* adi_gpio_SetGroupInterruptPins: Fail */
            UNIT_TEST(adi_gpio_SetGroupInterruptPins(i, ADI_GPIO_INTA_IRQ, (ADI_GPIO_DATA)j), ADI_GPIO_NOT_INITIALIZED)
            UNIT_TEST(adi_gpio_SetGroupInterruptPins(i, ADI_GPIO_INTB_IRQ, (ADI_GPIO_DATA)j), ADI_GPIO_NOT_INITIALIZED)
              
            /* adi_gpio_SetGroupInterruptPolarity: Fail */
            UNIT_TEST(adi_gpio_SetGroupInterruptPolarity(i, (ADI_GPIO_DATA)j), ADI_GPIO_NOT_INITIALIZED)
            UNIT_TEST(adi_gpio_SetGroupInterruptPolarity(i, (ADI_GPIO_DATA)j), ADI_GPIO_NOT_INITIALIZED)
            
            /* adi_gpio_OutputEnable: Fail */
            UNIT_TEST(adi_gpio_OutputEnable(i, (ADI_GPIO_DATA)j, true), ADI_GPIO_NOT_INITIALIZED)
            UNIT_TEST(adi_gpio_OutputEnable(i, (ADI_GPIO_DATA)j, true), ADI_GPIO_NOT_INITIALIZED)
            UNIT_TEST(adi_gpio_OutputEnable(i, (ADI_GPIO_DATA)j, false), ADI_GPIO_NOT_INITIALIZED)
            UNIT_TEST(adi_gpio_OutputEnable(i, (ADI_GPIO_DATA)j, false), ADI_GPIO_NOT_INITIALIZED)
              
            /* adi_gpio_InputEnable: Fail */
            UNIT_TEST(adi_gpio_InputEnable(i, (ADI_GPIO_DATA)j, true), ADI_GPIO_NOT_INITIALIZED)
            UNIT_TEST(adi_gpio_InputEnable(i, (ADI_GPIO_DATA)j, true), ADI_GPIO_NOT_INITIALIZED)
            UNIT_TEST(adi_gpio_InputEnable(i, (ADI_GPIO_DATA)j, false), ADI_GPIO_NOT_INITIALIZED)
            UNIT_TEST(adi_gpio_InputEnable(i, (ADI_GPIO_DATA)j, false), ADI_GPIO_NOT_INITIALIZED)
              
            /* adi_gpio_PullUpEnable: Fail */
            UNIT_TEST(adi_gpio_PullUpEnable(i, (ADI_GPIO_DATA)j, true), ADI_GPIO_NOT_INITIALIZED)
            UNIT_TEST(adi_gpio_PullUpEnable(i, (ADI_GPIO_DATA)j, true), ADI_GPIO_NOT_INITIALIZED) 
            UNIT_TEST(adi_gpio_PullUpEnable(i, (ADI_GPIO_DATA)j, false), ADI_GPIO_NOT_INITIALIZED)
            UNIT_TEST(adi_gpio_PullUpEnable(i, (ADI_GPIO_DATA)j, false), ADI_GPIO_NOT_INITIALIZED)  

            /* adi_gpio_SetHigh: Fail */
            UNIT_TEST(adi_gpio_SetHigh(i, (ADI_GPIO_DATA)j), ADI_GPIO_NOT_INITIALIZED)
            UNIT_TEST(adi_gpio_SetHigh(i, (ADI_GPIO_DATA)j), ADI_GPIO_NOT_INITIALIZED)

            /* adi_gpio_SetLow: Fail */
            UNIT_TEST(adi_gpio_SetLow(i, (ADI_GPIO_DATA)j), ADI_GPIO_NOT_INITIALIZED)
            UNIT_TEST(adi_gpio_SetLow(i, (ADI_GPIO_DATA)j), ADI_GPIO_NOT_INITIALIZED)  
   
            /* adi_gpio_Toggle: Fail */
            UNIT_TEST(adi_gpio_Toggle(i, (ADI_GPIO_DATA)j), ADI_GPIO_NOT_INITIALIZED)
            UNIT_TEST(adi_gpio_Toggle(i, (ADI_GPIO_DATA)j), ADI_GPIO_NOT_INITIALIZED)

            /* adi_gpio_SetData: Fail */
            UNIT_TEST(adi_gpio_SetData(i, (ADI_GPIO_DATA)j), ADI_GPIO_NOT_INITIALIZED)
            UNIT_TEST(adi_gpio_SetData(i, (ADI_GPIO_DATA)j), ADI_GPIO_NOT_INITIALIZED)                    
            
            /* adi_gpio_SetData: Fail */
            UNIT_TEST(adi_gpio_GetData(i, (ADI_GPIO_DATA)j, &pval), ADI_GPIO_NOT_INITIALIZED)
            UNIT_TEST(adi_gpio_GetData(i, (ADI_GPIO_DATA)j, &pval), ADI_GPIO_NOT_INITIALIZED)                      
        }
    }
          
    
  return ADI_UNIT_SUCCESS;
  
}
  

