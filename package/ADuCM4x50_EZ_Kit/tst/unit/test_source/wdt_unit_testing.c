#include <drivers/gpio/adi_gpio.h>
#include <common.h>
#include <stdio.h>
/*********************************************************UNIT TEST***********************************************************/ 


#ifndef UNIT_TEST_H
#define UNIT_TEST_H

typedef enum
{
  
  ADI_UNIT_SUCCESS = 0u,
  ADI_UNIT_FAILURE = 1u
    
} ADI_UNIT_RESULT;


#define UNIT_TEST(func, dbg) \
do \
{ \
    if ((func) != (dbg)) \
    { \
        DEBUG_MESSAGE("Unit test fail at line %d of %s\r\n", __LINE__, __FILE__); \
        return (ADI_UNIT_FAILURE); \
    } \
} while(0u);

#endif /* UNIT_TEST_H */

/************************************************************ UNIT_TEST_H *****************************************************/

/*!****************************************************************************
 * @file    wdt_unit_testing.c
 * @brief   Unit tests for WDT driver
 * @details Checks inputs and outputs of all WDT APIs
******************************************************************************/

/* Include the unit test framework */
//#include <unit_testing.h>

/* Include the driver under test */
#include <drivers/wdt/adi_wdt.h>

/* Top-level unit testing for the driver */
ADI_UNIT_RESULT wdt_unit_testing(void)
{   
    /* adi_wdt_Enable */
    {
        UNIT_TEST(adi_wdt_Enable(false, NULL), ADI_WDT_SUCCESS)
        UNIT_TEST(adi_wdt_Enable(true , NULL), ADI_WDT_SUCCESS)
        UNIT_TEST(adi_wdt_Enable(false, NULL), ADI_WDT_FAILURE_LOCKED)
    }

    /* adi_wdt_Kick */
    {
        /* Void function, can't check return value */
        adi_wdt_Kick();
    }

    /* adi_wdt_GetCount */
    {
        /* Void function, can't check return value */
        uint16_t nCurCount;
        adi_wdt_GetCount(&nCurCount);
    }

    return ADI_UNIT_SUCCESS;
}
