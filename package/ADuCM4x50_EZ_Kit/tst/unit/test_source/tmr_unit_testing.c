#include <drivers/gpio/adi_gpio.h>
#include <common.h>
#include <stdio.h>
/*********************************************************UNIT TEST***********************************************************/ 


#ifndef UNIT_TEST_H
#define UNIT_TEST_H

typedef enum
{
  
  ADI_UNIT_SUCCESS = 0u,
  ADI_UNIT_FAILURE = 1u
    
} ADI_UNIT_RESULT;


#define UNIT_TEST(func, dbg) \
do \
{ \
    if ((func) != (dbg)) \
    { \
        DEBUG_MESSAGE("Unit test fail at line %d of %s\r\n", __LINE__, __FILE__); \
        return (ADI_UNIT_FAILURE); \
    } \
} while(0u);

#endif /* UNIT_TEST_H */

/************************************************************ UNIT_TEST_H *****************************************************/


/*!****************************************************************************
 * @file    tmr_unit_testing.c
 * @brief   Unit tests for TMR driver
 * @details Checks inputs and outputs of all TMR APIs
 * @note    The only untested path in the APIs are places where the internal
 *          function WaitForStatusBit is called. Forcing a failure on one 
 *          of these function calls would require stubbing since the return
 *          value is hardware dependent. There is currently no method for 
 *          stubbing within this unit test framework.
******************************************************************************/

/* Include the unit test framework */
//#include <unit_testing.h>

/* Include the driver under test */
#include <drivers/tmr/adi_tmr.h>

/* Top-level unit testing for the driver */
ADI_UNIT_RESULT tmr_unit_testing(void)
{   
    /* Run the entire unit test for each timer */
    for (ADI_TMR_DEVICE i = ADI_TMR_DEVICE_GP0; i < ADI_TMR_DEVICE_NUM; i++) {

        /* adi_tmr_Init */
        {   
            /* Bad input testing */
            UNIT_TEST(adi_tmr_Init(ADI_TMR_DEVICE_NUM, NULL, NULL, true), ADI_TMR_BAD_DEVICE_NUM)

            /* Good input testing */
            UNIT_TEST(adi_tmr_Init(i, NULL, NULL, true),  ADI_TMR_SUCCESS)
            UNIT_TEST(adi_tmr_Init(i, NULL, NULL, false), ADI_TMR_SUCCESS)

            /* Try enabling the timer, and then calling the API again */
            UNIT_TEST(adi_tmr_Enable(i, true),  ADI_TMR_SUCCESS)
            UNIT_TEST(adi_tmr_Init  (i, NULL, NULL, true), ADI_TMR_OPERATION_NOT_ALLOWED)
            UNIT_TEST(adi_tmr_Enable(i, false), ADI_TMR_SUCCESS)
        }

        /* adi_tmr_ConfigTimer */
        {
            ADI_TMR_CONFIG timerConfig;
            timerConfig.eClockSource = ADI_TMR_CLOCK_HFOSC;
            timerConfig.ePrescaler = ADI_TMR_PRESCALER_16;
            timerConfig.nLoad = 0xF000;
            timerConfig.nAsyncLoad = 0xFF00;
            timerConfig.bSyncBypass = true;
            timerConfig.bCountingUp = true; 
            timerConfig.bReloading = true;
            timerConfig.bPeriodic = false;

            /* Bad input testing */
            UNIT_TEST(adi_tmr_ConfigTimer(ADI_TMR_DEVICE_NUM, timerConfig), ADI_TMR_BAD_DEVICE_NUM)
            UNIT_TEST(adi_tmr_ConfigTimer(i, timerConfig), ADI_TMR_BAD_RELOAD_CONFIGURATION)

            /* Good input testing */
            timerConfig.bPeriodic = true;
            UNIT_TEST(adi_tmr_ConfigTimer(i, timerConfig), ADI_TMR_SUCCESS)
            timerConfig.bSyncBypass = false;
            UNIT_TEST(adi_tmr_ConfigTimer(i, timerConfig), ADI_TMR_SUCCESS)
            timerConfig.bReloading = false;
            UNIT_TEST(adi_tmr_ConfigTimer(i, timerConfig), ADI_TMR_SUCCESS)
            timerConfig.bCountingUp = false;
            UNIT_TEST(adi_tmr_ConfigTimer(i, timerConfig), ADI_TMR_SUCCESS)

            /* Try enabling the timer, and then calling the API again */
            UNIT_TEST(adi_tmr_Enable     (i, true),  ADI_TMR_SUCCESS)
            UNIT_TEST(adi_tmr_ConfigTimer(i, timerConfig), ADI_TMR_OPERATION_NOT_ALLOWED)
            UNIT_TEST(adi_tmr_Enable     (i, false), ADI_TMR_SUCCESS)            

        }

        /* adi_tmr_ConfigEvent */
        {
            ADI_TMR_EVENT_CONFIG timerConfig;
            timerConfig.bEnable = true;
            timerConfig.bPrescaleReset = true;

            /* Bad input testing */
            UNIT_TEST(adi_tmr_ConfigEvent(ADI_TMR_DEVICE_NUM, timerConfig), ADI_TMR_BAD_DEVICE_NUM)
           /* for(uint8_t j = 0u; j < 255u; j++) {
                timerConfig.nEventID = j;
                if (j > 15u) {
                    UNIT_TEST(adi_tmr_ConfigEvent(i, timerConfig), ADI_TMR_BAD_EVENT_ID)
                } else {
                    UNIT_TEST(adi_tmr_ConfigEvent(i, timerConfig), ADI_TMR_SUCCESS)
                } 
            }

            /* Good input testing */
            timerConfig.nEventID = 15u;
            timerConfig.bEnable = false;
            UNIT_TEST(adi_tmr_ConfigEvent(i, timerConfig), ADI_TMR_SUCCESS)
            timerConfig.bPrescaleReset = false;
            UNIT_TEST(adi_tmr_ConfigEvent(i, timerConfig), ADI_TMR_SUCCESS)

            /* Try enabling the timer, and then calling the API again */
            UNIT_TEST(adi_tmr_Enable     (i, true),  ADI_TMR_SUCCESS)
            UNIT_TEST(adi_tmr_ConfigEvent(i, timerConfig), ADI_TMR_OPERATION_NOT_ALLOWED)
            UNIT_TEST(adi_tmr_Enable     (i, false), ADI_TMR_SUCCESS)            

        }

        /* adi_tmr_ConfigPwm */
      /*  {
            ADI_TMR_PWM_CONFIG timerConfig;
            timerConfig.nMatchValue = 0xF000;
            timerConfig.bMatch = true;
            timerConfig.bIdleHigh = true;

            /* Bad input testing */
          //  UNIT_TEST(adi_tmr_ConfigPwm(ADI_TMR_DEVICE_NUM, timerConfig), ADI_TMR_BAD_DEVICE_NUM)

            /* Good input testing */
         /*   UNIT_TEST(adi_tmr_ConfigPwm(i, timerConfig), ADI_TMR_SUCCESS)
            timerConfig.bMatch = false;
            UNIT_TEST(adi_tmr_ConfigPwm(i, timerConfig), ADI_TMR_SUCCESS)
            timerConfig.bIdleHigh = false;
            UNIT_TEST(adi_tmr_ConfigPwm(i, timerConfig), ADI_TMR_SUCCESS)

            /* Try enabling the timer, and then calling the API again */
          /*  UNIT_TEST(adi_tmr_Enable     (i, true),  ADI_TMR_SUCCESS)
            UNIT_TEST(adi_tmr_ConfigPwm(i, timerConfig), ADI_TMR_OPERATION_NOT_ALLOWED)
            UNIT_TEST(adi_tmr_Enable     (i, false), ADI_TMR_SUCCESS)            

        }

        /* adi_tmr_Enable */
        {
            /* Bad input testing */
            UNIT_TEST(adi_tmr_Enable(ADI_TMR_DEVICE_NUM, true), ADI_TMR_BAD_DEVICE_NUM)

            /* Good input testing */
            UNIT_TEST(adi_tmr_Enable(i, true), ADI_TMR_SUCCESS)
            UNIT_TEST(adi_tmr_Enable(i, false), ADI_TMR_SUCCESS)
 
        }

        /* adi_tmr_GetCurrentCount */
        {
            /* Bad input testing */
            UNIT_TEST(adi_tmr_GetCurrentCount(ADI_TMR_DEVICE_NUM, NULL), ADI_TMR_BAD_DEVICE_NUM)
            UNIT_TEST(adi_tmr_GetCurrentCount(i, NULL), ADI_TMR_NULL_POINTER)

            /* Good input testing */
            uint16_t pCount;
            UNIT_TEST(adi_tmr_GetCurrentCount(i, &pCount), ADI_TMR_SUCCESS)

        }

         /* adi_tmr_GetCaptureCount */
        {
            /* Bad input testing */
            UNIT_TEST(adi_tmr_GetCaptureCount(ADI_TMR_DEVICE_NUM, NULL), ADI_TMR_BAD_DEVICE_NUM)
            UNIT_TEST(adi_tmr_GetCaptureCount(i, NULL), ADI_TMR_NULL_POINTER)

            /* Good input testing */
            uint16_t pCount;
            UNIT_TEST(adi_tmr_GetCaptureCount(i, &pCount), ADI_TMR_SUCCESS)

        }

        /* adi_tmr_Reload */
        {
            /* Bad input testing */
            UNIT_TEST(adi_tmr_Reload(ADI_TMR_DEVICE_NUM), ADI_TMR_BAD_DEVICE_NUM)
            ADI_TMR_CONFIG timerConfig;
            timerConfig.bReloading = false;
            timerConfig.bPeriodic = true;
            UNIT_TEST(adi_tmr_ConfigTimer(i, timerConfig), ADI_TMR_SUCCESS)
            UNIT_TEST(adi_tmr_Reload(i), ADI_TMR_RELOAD_DISABLED)
            timerConfig.bReloading = false;
            timerConfig.bPeriodic = false;
            UNIT_TEST(adi_tmr_ConfigTimer(i, timerConfig), ADI_TMR_SUCCESS)
            UNIT_TEST(adi_tmr_Reload(i), ADI_TMR_RELOAD_DISABLED)     
              
            /* The case of bPeriodic = false and bReloading = true is not valid since configTimer will throw an error */

            /* Good input testing */
            timerConfig.bReloading = true;
            timerConfig.bPeriodic = true;
            UNIT_TEST(adi_tmr_ConfigTimer(i, timerConfig), ADI_TMR_SUCCESS)
            UNIT_TEST(adi_tmr_Reload(i), ADI_TMR_SUCCESS)   
 
        }

    }

    return ADI_UNIT_SUCCESS;
}
