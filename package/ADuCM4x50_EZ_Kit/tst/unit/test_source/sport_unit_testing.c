#include <drivers/gpio/adi_gpio.h>
#include <common.h>
#include <stdio.h>
/*********************************************************UNIT TEST***********************************************************/ 


#ifndef UNIT_TEST_H
#define UNIT_TEST_H

typedef enum
{
  
  ADI_UNIT_SUCCESS = 0u,
  ADI_UNIT_FAILURE = 1u
    
} ADI_UNIT_RESULT;


#define UNIT_TEST(func, dbg) \
do \
{ \
    if ((func) != (dbg)) \
    { \
        DEBUG_MESSAGE("Unit test fail at line %d of %s\r\n", __LINE__, __FILE__); \
        return (ADI_UNIT_FAILURE); \
    } \
} while(0u);

#endif /* UNIT_TEST_H */

/************************************************************ UNIT_TEST_H *****************************************************/


/*******************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*******************************************************************************/
/**
 * @file:       sport_loopback_dma.c
 * 
 * @brief       This file contains tests for the SPORT device driver executing
 *              DMA driven requests.
 */
#include <stddef.h>             /* for 'NULL' */
#include <system_ADuCM3029.h>
#include <common.h>

#include <drivers/general/adi_drivers_general.h>
#include <drivers/sport/adi_sport.h>
#include <drivers/pwr/adi_pwr.h>



#define MAX_DATA_TRANSFER       (BUFFER_SIZE - 4u)

#define MAX_ITER        (3u)
#define TEST_ITER               (4u)
#define MAX_CB_DATA             (3u)

#define SPORT_OPEN
#define SPORT_CLOSE
#define SPORT_REGISTER_CALLBACK
#define SPORT_CONFIGCLOCK
#define  SPORT_CONFIGDATA
#define SPORT_CONFIGFRAMESYNC
#define SPORT_MULTIPLXPORTSIGNAL
#define SPORT_CONFIGTMRMODE
#define SPORT_SUBMITBFR
#define SPORT_ISBFRAVLBLE
#define SPORT_GETBFR
extern uint32_t sport_SysDataErr;

extern int32_t adi_initpinmux(void);

extern uint32_t test_sport_dma_without_callback(void);
extern uint32_t test_sport_dma_with_callback(void);

ADI_ALIGNED_PRAGMA(4)
static unsigned char sportDriverTx[ADI_SPORT_MEMORY_SIZE] ADI_ALIGNED_ATTRIBUTE(4);     /* Memory required by the device for TX operation */

ADI_ALIGNED_PRAGMA(4)
static unsigned char sportDriverRx[ADI_SPORT_MEMORY_SIZE] ADI_ALIGNED_ATTRIBUTE(4);     /* Memory required by the device for RX operation */

ADI_ALIGNED_PRAGMA(4)
ADI_SPORT_HANDLE hSportRx ADI_ALIGNED_ATTRIBUTE(4);                                     /* Handle for Rx channel */

ADI_ALIGNED_PRAGMA(4)
ADI_SPORT_HANDLE hSportTx ADI_ALIGNED_ATTRIBUTE(4);                                     /* Handle for Tx channel */
 ADI_CALLBACK        const pfCallback;


ADI_UNIT_RESULT sport_unit_testing(void)
{ 

 ADI_SPORT_HANDLE  hSportRx1;
 int SPORT_DEVICE_ID;
  const bool bUseIntlClock,bRisingEdge,bGatedClk;
  const uint16_t              nFsDivisor;
         
  const uint16_t  nClockRatio; 

 ADI_SPORT_PACKING_MODE    ePackMode;
  bool bBufferComplete=false;
  //const bool     bUseOtherFS,bUseOtherClk;
  const uint8_t           nFSDuration;
 const uint8_t           nWidth;

  void *pBuffer;
   uint32_t         const nNumBytes;
  
         /* Variable to get the buffer address */
 
    
    uint32_t memSize = (uint32_t)(ADI_SPORT_MEMORY_SIZE);

   // adi_initpinmux();
    common_Init();
    adi_pwr_Init();
    adi_pwr_SetClockDivider(ADI_CLOCK_HCLK,1);
    adi_pwr_SetClockDivider(ADI_CLOCK_PCLK,1);
    adi_pwr_EnableClock(ADI_CLOCK_GATE_PCLK, true);
   

#ifdef SPORT_OPEN


/*****************ADI_SPORT_SUCCESSS*******************************/

   UNIT_TEST(adi_sport_Open(SPORT_DEVICE_ID, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx),ADI_SPORT_SUCCESS)
  

/*****************ADI_SPORT_DEVICE_IN_USE ******************************/
adi_sport_Open(SPORT_DEVICE_ID, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx);
 UNIT_TEST(adi_sport_Open(SPORT_DEVICE_ID, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx),ADI_SPORT_DEVICE_IN_USE )
  
/***************************************************************************/
/*****************ADI_SPORT_INVALID_DEVICE_NUM  ***************************/
/**************************************************************************/
 UNIT_TEST(adi_sport_Open(10u, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize,&hSportRx),ADI_SPORT_INVALID_DEVICE_NUM)
   adi_sport_Close(hSportRx);
/***************************************************************************/
/****************ADI_SPORT_INVALID_NULL_POINTER***************************/
/**************************************************************************/
 UNIT_TEST(adi_sport_Open(SPORT_DEVICE_ID, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, NULL),ADI_SPORT_INVALID_NULL_POINTER)
 adi_sport_Close(hSportRx);
#endif
 

#ifdef SPORT_REGISTER_CALLBACK
  
/*************************ADI_SPORT_SUCCESSS*******************************/
 adi_sport_Open(SPORT_DEVICE_ID, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx);
 UNIT_TEST(adi_sport_RegisterCallback(hSportRx, pfCallback, hSportRx),ADI_SPORT_SUCCESS)
 adi_sport_Close(hSportRx);
/**********************ADI_SPORT_INVALID_HANDLE ****************************/
/* adi_sport_Open(10u, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx);
   UNIT_TEST(adi_sport_RegisterCallback(hSportRx, pfCallback, hSportRx),ADI_SPORT_INVALID_HANDLE )
  adi_sport_Close(hSportRx);
 */
#endif
 
 
 #ifdef SPORT_CONFIGCLOCK
/*************************ADI_SPORT_SUCCESSS*******************************/

adi_sport_Open(SPORT_DEVICE_ID, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx);
 UNIT_TEST(adi_sport_ConfigClock(hSportRx,nClockRatio,true,true,true),ADI_SPORT_SUCCESS);
   adi_sport_Close(hSportRx);
/*****************ADI_SPORT_INVALID_HANDLE*******************************/
 adi_sport_Open(5u, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx);
 UNIT_TEST(adi_sport_ConfigClock(hSportRx,nClockRatio,true,true,true),ADI_SPORT_INVALID_HANDLE)

/*************************ADI_SPORT_OPERATION_NOT_ALLOWED ******************/

              /* Need To Develop Logic */
#endif
   
 #ifdef SPORT_CONFIGDATA

/***************************************************************************/
/*************************ADI_SPORT_SUCCESSS*******************************/
/**************************************************************************/
adi_sport_Open(SPORT_DEVICE_ID, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx);
UNIT_TEST(adi_sport_ConfigData(hSportRx,3,ADI_SPORT_8BIT_PACKING, true),ADI_SPORT_SUCCESS)                             
 adi_sport_Close(hSportRx);
  
 /***************************************************************************/
/*****************ADI_SPORT_INVALID_HANDLE*******************************/
/**************************************************************************/
adi_sport_Open(10u, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx);
 UNIT_TEST(adi_sport_ConfigData(hSportRx,3,ADI_SPORT_8BIT_PACKING, true),ADI_SPORT_INVALID_HANDLE)   
 adi_sport_Close(hSportRx);

  
/*****************ADI_SPORT_INVALID_WORD_LENGTH ******************************/
 adi_sport_Open(SPORT_DEVICE_ID, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx);
  UNIT_TEST(adi_sport_ConfigData(hSportRx,33,ADI_SPORT_8BIT_PACKING, true),ADI_SPORT_INVALID_WORD_LENGTH)   
  adi_sport_Close(hSportRx);
  
 
  /***************************************************************************/
/*************************ADI_SPORT_OPERATION_NOT_ALLOWED ******************************/
/**************************************************************************/
          /* Need To Develop Logic */

#endif

#ifdef SPORT_CONFIGFRAMESYNC
/***************************************************************************/
/*************************ADI_SPORT_SUCCESSS*******************************/
/**************************************************************************/
 adi_sport_Open(SPORT_DEVICE_ID, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx);
 UNIT_TEST(adi_sport_ConfigFrameSync(hSportRx,10,true,true,true,true,true,true),ADI_SPORT_SUCCESS)
 adi_sport_Close(hSportRx);

/***************************************************************************/
/********************ADI_SPORT_INVALID_HANDLE*******************************/
/**************************************************************************/
adi_sport_Open(5u, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx);
UNIT_TEST(adi_sport_ConfigFrameSync(hSportRx,nFsDivisor,true,true,true,true,true,true),ADI_SPORT_INVALID_HANDLE)
adi_sport_Close(hSportRx);

  
/***************************************************************************/
/*************************ADI_SPORT_OPERATION_NOT_ALLOWED ******************************/
/**************************************************************************/
                /* Need To Develop Logic */
#endif

#ifdef SPORT_MULTIPLXPORTSIGNAL
 
/*************************ADI_SPORT_SUCCESSS*******************************/
/**************************************************************************/
adi_sport_Open(SPORT_DEVICE_ID, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx);
 UNIT_TEST(adi_sport_MultiplexSportSignal(hSportRx,true,true),ADI_SPORT_SUCCESS)
adi_sport_Close(hSportRx);

/***************************************************************************/
/*****************ADI_SPORT_INVALID_HANDLE*******************************/
/**************************************************************************/
adi_sport_Open(5u, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx);
UNIT_TEST(adi_sport_MultiplexSportSignal(hSportRx,true,true),ADI_SPORT_INVALID_HANDLE)
adi_sport_Close(hSportRx);

  
/***************************************************************************/
/*************************ADI_SPORT_OPERATION_NOT_ALLOWED ******************************/
/**************************************************************************/
                  /* Need To Develop Logic */
#endif


#ifdef SPORT_CONFIGTMRMODE

/*************************ADI_SPORT_SUCCESSS*******************************/
/**************************************************************************/
adi_sport_Open(SPORT_DEVICE_ID, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx);
UNIT_TEST(adi_sport_ConfigTimerMode(hSportRx,nFSDuration, nWidth,true),ADI_SPORT_SUCCESS)
  adi_sport_Close(hSportRx);


/*****************ADI_SPORT_INVALID_HANDLE*******************************/
/**************************************************************************/
adi_sport_Open(5u, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx);
UNIT_TEST(adi_sport_ConfigTimerMode(hSportRx,nFSDuration, nWidth,true),ADI_SPORT_INVALID_HANDLE)
adi_sport_Close(hSportRx);

/*************************ADI_SPORT_OPERATION_NOT_ALLOWED ******************************/
/**************************************************************************/
       /* Need To Develop Logic */

#endif
#ifdef SPORT_SUBMITBFR
/*************************ADI_SPORT_SUCCESSS*******************************/
/**************************************************************************/
adi_sport_Open(SPORT_DEVICE_ID, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx);
UNIT_TEST(adi_sport_SubmitBuffer(hSportRx,&pBuffer,nNumBytes,true),ADI_SPORT_SUCCESS)
adi_sport_Close(hSportRx);
  

/**************************ADI_SPORT_INVALID_HANDLE ************************/
/**************************************************************************/
  adi_sport_Open(4u, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverTx, memSize, &hSportRx);
  UNIT_TEST(adi_sport_SubmitBuffer(hSportRx,&pBuffer,nNumBytes,true),ADI_SPORT_INVALID_HANDLE)
  adi_sport_Close(hSportRx);
 

/*****************ADI_SPORT_INVALID_PARAMETER *****************************/

 adi_sport_Open(SPORT_DEVICE_ID, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx);
  UNIT_TEST(adi_sport_SubmitBuffer(hSportRx,&pBuffer,10000,true),ADI_SPORT_INVALID_PARAMETER )
  adi_sport_Close(hSportRx);

/************************* ADI_SPORT_BUFFERS_NOT_SUBMITTED ******************/
/**************************************************************************/
            /* Need To Develop Logic */
   
#endif 
  
  #ifdef SPORT_ISBFRAVLBLE

/***************************ADI_SPORT_SUCCESS*******************************/
/**************************************************************************/
adi_sport_Open(SPORT_DEVICE_ID, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx);
  adi_sport_SubmitBuffer(hSportRx,&pBuffer,nNumBytes,true);
  UNIT_TEST(adi_sport_IsBufferAvailable(hSportRx,&bBufferComplete),ADI_SPORT_SUCCESS)
  adi_sport_Close(hSportRx);
  
 
    

/***************************ADI_SPORT_INVALID_HANDLE  **********************/

 adi_sport_Open(10u, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx);
 adi_sport_SubmitBuffer(hSportRx,&pBuffer,nNumBytes,true);
  UNIT_TEST(adi_sport_IsBufferAvailable(hSportRx,&bBufferComplete),ADI_SPORT_INVALID_HANDLE )
  adi_sport_Close(hSportRx);

/***************************ADI_SPORT_PERIPHERAL_ERROR  *******************************/

 adi_sport_Open(SPORT_DEVICE_ID, ADI_HALF_SPORT_A, ADI_SPORT_DIR_RX, sportDriverRx, memSize, &hSportRx);
 adi_sport_SubmitBuffer(hSportRx,&pBuffer,nNumBytes,true);
 UNIT_TEST(adi_sport_IsBufferAvailable(hSportRx,&bBufferComplete),ADI_SPORT_SUCCESS)
  adi_sport_Close(hSportRx); 
  

/***************************ADI_SPORT_OPERATION_NOT_ALLOWED  ****************/

        /* Need To Develop Logic */
 #endif 
 
 /*****************************SPORT_GETBFR ***********************************/
   /* Need To Develop Logic */
 
 
  return ADI_UNIT_SUCCESS;  
  
}  