#include <drivers/gpio/adi_gpio.h>
#include <common.h>
#include <stdio.h>
/*********************************************************UNIT TEST***********************************************************/ 


#ifndef UNIT_TEST_H
#define UNIT_TEST_H

typedef enum
{
  
  ADI_UNIT_SUCCESS = 0u,
  ADI_UNIT_FAILURE = 1u
    
} ADI_UNIT_RESULT;


#define UNIT_TEST(func, dbg) \
do \
{ \
    if ((func) != (dbg)) \
    { \
        DEBUG_MESSAGE("Unit test fail at line %d of %s\r\n", __LINE__, __FILE__); \
        return (ADI_UNIT_FAILURE); \
    } \
} while(0u);

#endif /* UNIT_TEST_H */

/************************************************************ UNIT_TEST_H *****************************************************/

/*!****************************************************************************
 * @file    xint_unit_testing.c
 * @brief   Unit tests for XINT driver
 * @details Checks inputs and outputs of all XINT APIs
******************************************************************************/

/* Include the unit test framework */
//#include <unit_testing.h>

/* Include the driver under test */
#include <drivers/xint/adi_xint.h>


static uint8_t xintMemory[ADI_XINT_MEMORY_SIZE];


/* Top-level unit testing for the driver */
ADI_UNIT_RESULT xint_unit_testing(void)
{    
    ADI_XINT_EVENT i;
   // ADI_XINT_IRQ_MODE j;
    
    
    /* adi_gpio_Init: Pass */
    UNIT_TEST(adi_xint_Init(xintMemory, ADI_XINT_MEMORY_SIZE), ADI_XINT_SUCCESS)
      
    /* adi_gpio_Init: Fail */
   // UNIT_TEST(adi_xint_Init(xintMemory, ADI_XINT_MEMORY_SIZE), ADI_XINT_ALREADY_INITIALIZED)  
    UNIT_TEST(adi_xint_Init(NULL, ADI_XINT_MEMORY_SIZE), ADI_XINT_NULL_PARAMETER)  
    UNIT_TEST(adi_xint_Init(xintMemory, NULL), ADI_XINT_INVALID_MEMORY_SIZE)  
     
    /* adi_gpio_UnInit: Pass */  
    UNIT_TEST(adi_xint_UnInit(), ADI_XINT_SUCCESS)  

    /* adi_gpio_UnInit: Fail */  
    UNIT_TEST(adi_xint_UnInit(), ADI_XINT_NOT_INITIALIZED)  

    /* adi_gpio_Init: Pass */
    UNIT_TEST(adi_xint_Init(xintMemory, ADI_XINT_MEMORY_SIZE), ADI_XINT_SUCCESS)
      
    for(i = (ADI_XINT_EVENT)0u; i < ADI_XINT_EVENT_MAX; i++)
    {    
        /* Skip logic low mode to avoid real interrupt. */
       /// for(j = (ADI_XINT_IRQ_MODE)0u; j < ADI_XINT_IRQ_MODE_MAX-1; j++)
        //{
            /* adi_xint_EnableIRQ: Pass */
         //   UNIT_TEST(adi_xint_EnableIRQ(i, j), ADI_XINT_SUCCESS)
       // }
            /* adi_xint_DisableIRQ: Pass */
            UNIT_TEST(adi_xint_DisableIRQ(i), ADI_XINT_SUCCESS)   
            
            /* adi_xint_RegisterCallback: Pass */  
            UNIT_TEST(adi_xint_RegisterCallback(i, NULL, NULL), ADI_XINT_SUCCESS)   
    }
    
    /* adi_gpio_UnInit: Pass */  
    UNIT_TEST(adi_xint_UnInit(), ADI_XINT_SUCCESS) 
      
    for(i = (ADI_XINT_EVENT)0u; i < ADI_XINT_EVENT_MAX; i++)
    {    
       // for(j = (ADI_XINT_IRQ_MODE)0u; j < ADI_XINT_IRQ_MODE_MAX; j++)
        //{
            /* adi_xint_EnableIRQ: Fail */
          //  UNIT_TEST(adi_xint_EnableIRQ(i, j), ADI_XINT_NOT_INITIALIZED)
       // }
            /* adi_xint_DisableIRQ: Fail */
            UNIT_TEST(adi_xint_DisableIRQ(i), ADI_XINT_NOT_INITIALIZED)   
            
            /* adi_xint_RegisterCallback: Fail */  
            UNIT_TEST(adi_xint_RegisterCallback(i, NULL, NULL), ADI_XINT_NOT_INITIALIZED)   
    }
      
  return ADI_UNIT_SUCCESS;
}