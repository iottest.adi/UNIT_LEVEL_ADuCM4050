#include <drivers/gpio/adi_gpio.h>
#include <common.h>
#include <stdio.h>
/*********************************************************UNIT TEST***********************************************************/ 


#ifndef UNIT_TEST_H
#define UNIT_TEST_H

typedef enum
{
  
  ADI_UNIT_SUCCESS = 0u,
  ADI_UNIT_FAILURE = 1u
    
} ADI_UNIT_RESULT;


#define UNIT_TEST(func, dbg) \
do \
{ \
    if ((func) != (dbg)) \
    { \
        DEBUG_MESSAGE("Unit test fail at line %d of %s\r\n", __LINE__, __FILE__); \
        return (ADI_UNIT_FAILURE); \
    } \
} while(0u);

#endif /* UNIT_TEST_H */

/************************************************************ UNIT_TEST_H *****************************************************/




/*!****************************************************************************
 * @file    i2c_unit_testing.c
 * @brief   Unit tests for SPI driver
 * @details Checks inputs and outputs of all SPI APIs
******************************************************************************/

/* Include the driver under test */
#include <drivers/i2c/adi_i2c.h>

/* Power driver is needed to get the clocks at expected values */
#include <drivers/pwr/adi_pwr.h>

/* Set max data size to 8 bytes */
#define DATASIZE 8

 /* data array statics */
uint8_t txData[DATASIZE];
uint8_t rxData[DATASIZE];

/* Top-level unit testing for the driver */
ADI_UNIT_RESULT i2c_unit_testing(void)
{   
    ADI_I2C_HANDLE hDevice;
    uint8_t        DeviceMemory[ADI_I2C_MEMORY_SIZE];
    
    /* transaction structure */
    ADI_I2C_TRANSACTION xfr;
    
    bool pComplete = false;
    uint32_t pHwErrors;

    /* The power test may have modified the clocks, restore to 26 MHz */
    UNIT_TEST(adi_pwr_Init(), ADI_PWR_SUCCESS)
    UNIT_TEST(adi_pwr_SetClockDivider(ADI_CLOCK_HCLK, 1u), ADI_PWR_SUCCESS)
    UNIT_TEST(adi_pwr_SetClockDivider(ADI_CLOCK_PCLK, 1u), ADI_PWR_SUCCESS)    
     
    /* adi_i2c_Open */
    UNIT_TEST(adi_i2c_Open(1, &DeviceMemory, ADI_I2C_MEMORY_SIZE, &hDevice), ADI_I2C_BAD_DEVICE_NUMBER)
    UNIT_TEST(adi_i2c_Open(0, &DeviceMemory, 2, &hDevice), ADI_I2C_INSUFFICIENT_MEMORY)
    UNIT_TEST(adi_i2c_Open(0, &DeviceMemory, ADI_I2C_MEMORY_SIZE, &hDevice), ADI_I2C_SUCCESS)       
    UNIT_TEST(adi_i2c_Open(0, &DeviceMemory, ADI_I2C_MEMORY_SIZE, &hDevice), ADI_I2C_DEVICE_IN_USE)
     
    /* adi_i2c_Close */
    UNIT_TEST(adi_i2c_Close(0), ADI_I2C_BAD_DEVICE_HANDLE)     
    UNIT_TEST(adi_i2c_Close(hDevice), ADI_I2C_SUCCESS)   
     
    UNIT_TEST(adi_i2c_Open(0, &DeviceMemory, ADI_I2C_MEMORY_SIZE, &hDevice), ADI_I2C_SUCCESS)  
      
    xfr.pData           = 0;
    xfr.nDataSize       = 0; 
    
    /* adi_i2c_SubmitBuffer: Fail */
    UNIT_TEST(adi_i2c_SubmitBuffer(hDevice, &xfr), ADI_I2C_INVALID_PARAMETER)   
    UNIT_TEST(adi_i2c_SubmitBuffer(0, &xfr), ADI_I2C_BAD_DEVICE_HANDLE) 
    
    /* adi_i2c_GetBuffer: Fail */
    UNIT_TEST(adi_i2c_GetBuffer(0, &pHwErrors), ADI_I2C_BAD_DEVICE_HANDLE)     
    UNIT_TEST(adi_i2c_GetBuffer(hDevice, &pHwErrors), ADI_I2C_INVALID_SUBMIT_API)      

    /* adi_i2c_Reset */
    UNIT_TEST(adi_i2c_Reset(0), ADI_I2C_BAD_DEVICE_HANDLE) 
    UNIT_TEST(adi_i2c_Reset(hDevice), ADI_I2C_SUCCESS) 
    
    /* adi_i2c_SetBitRate */
    UNIT_TEST(adi_i2c_SetBitRate(0, 4000), ADI_I2C_BAD_DEVICE_HANDLE) 
    UNIT_TEST(adi_i2c_SetBitRate(hDevice, 4000), ADI_I2C_BAD_BITRATE) 
    UNIT_TEST(adi_i2c_SetBitRate(hDevice, 400000), ADI_I2C_SUCCESS)  
    
    /* adi_i2c_SetSlaveAddress */  
    UNIT_TEST(adi_i2c_SetSlaveAddress(0, 0x50), ADI_I2C_BAD_DEVICE_HANDLE) 
    UNIT_TEST(adi_i2c_SetSlaveAddress(hDevice, 0xFFFF), ADI_I2C_INVALID_SLAVE_ADDRESS) 
    UNIT_TEST(adi_i2c_SetSlaveAddress(hDevice, 0x50), ADI_I2C_SUCCESS)  
      
    /* adi_i2c_IssueGeneralCall */    
    UNIT_TEST(adi_i2c_IssueGeneralCall(0, "ABCDE", 5u, &pHwErrors), ADI_I2C_BAD_DEVICE_HANDLE) 
      
    xfr.pData           = rxData;
    xfr.nDataSize       = 8;
    
    /* adi_i2c_SubmitBuffer */  
    UNIT_TEST(adi_i2c_SubmitBuffer(hDevice, &xfr), ADI_I2C_SUCCESS) 
    
    /* adi_i2c_IsBufferAvailable */  
    UNIT_TEST(adi_i2c_IsBufferAvailable(0, &pComplete), ADI_I2C_BAD_DEVICE_HANDLE) 
    UNIT_TEST(adi_i2c_IsBufferAvailable(hDevice, &pComplete), ADI_I2C_SUCCESS) 
    
    /* ADI_I2C_DEVICE_IN_USE when a buffer is active */
    UNIT_TEST(adi_i2c_SubmitBuffer(hDevice, &xfr), ADI_I2C_DEVICE_IN_USE)
    UNIT_TEST(adi_i2c_SetBitRate(hDevice, 4000), ADI_I2C_DEVICE_IN_USE) 
    UNIT_TEST(adi_i2c_SetSlaveAddress(hDevice, 0x50), ADI_I2C_DEVICE_IN_USE)  
    UNIT_TEST(adi_i2c_IssueGeneralCall(hDevice, "ABCDE", 5u, &pHwErrors), ADI_I2C_DEVICE_IN_USE) 

 
      return ADI_UNIT_SUCCESS;
}