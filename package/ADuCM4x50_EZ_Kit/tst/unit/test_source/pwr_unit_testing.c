#include <drivers/gpio/adi_gpio.h>
#include <common.h>
#include <stdio.h>
/*********************************************************UNIT TEST***********************************************************/ 


#ifndef UNIT_TEST_H
#define UNIT_TEST_H

typedef enum
{
  
  ADI_UNIT_SUCCESS = 0u,
  ADI_UNIT_FAILURE = 1u
    
} ADI_UNIT_RESULT;


#define UNIT_TEST(func, dbg) \
do \
{ \
    if ((func) != (dbg)) \
    { \
        DEBUG_MESSAGE("Unit test fail at line %d of %s\r\n", __LINE__, __FILE__); \
        return (ADI_UNIT_FAILURE); \
    } \
} while(0u);

#endif /* UNIT_TEST_H */

/************************************************************ UNIT_TEST_H *****************************************************/


/*!****************************************************************************
 * @file    pwr_unit_testing.c
 * @brief   Unit tests for PWR driver
 * @details Checks inputs and outputs of all PWR APIs
******************************************************************************/

/* Include the unit test framework */
//#include <unit_testing.h>

/* Include the driver under test */
#include <drivers/pwr/adi_pwr.h>

/* Test code */
static void TestCallback(void *pCBParam, uint32_t nEvent, void * pEventData)
{
  
}

/* Top-level unit testing for the driver */
ADI_UNIT_RESULT pwr_unit_testing(void)
{   
    /* adi_pwr_Init */
    UNIT_TEST(adi_pwr_Init(), ADI_PWR_SUCCESS)
      
    /* adi_pwr_RegisterCallback */
    {
        UNIT_TEST(adi_pwr_RegisterCallback(NULL, NULL), ADI_PWR_NULL_POINTER)
        UNIT_TEST(adi_pwr_RegisterCallback(TestCallback, NULL), ADI_PWR_SUCCESS)
    }
      
    /* adi_pwr_UpdateCoreClock */
    UNIT_TEST(adi_pwr_UpdateCoreClock(), ADI_PWR_SUCCESS)

    /* adi_pwr_SetExtClkFreq and adi_pwr_GetExtClkFreq*/
    {
        uint32_t ExtClk;
        UNIT_TEST(adi_pwr_GetExtClkFreq(NULL    ), ADI_PWR_FAILURE)
        UNIT_TEST(adi_pwr_GetExtClkFreq(&ExtClk ), ADI_PWR_FAILURE)
        UNIT_TEST(adi_pwr_SetExtClkFreq(26000000), ADI_PWR_SUCCESS)
        UNIT_TEST(adi_pwr_GetExtClkFreq(&ExtClk ), ADI_PWR_SUCCESS)
        UNIT_TEST(ExtClk, 26000000)
        UNIT_TEST(adi_pwr_SetExtClkFreq(26000001), ADI_PWR_INVALID_CLOCK_SPEED)
    }
    
     /* adi_pwr_EnableClockInterrupt */

   {
        /* Sweep through API with input parameters */
     //  UNIT_TEST(adi_pwr_EnableClockInterrupt(ADI_PWR_LFXTAL_CLOCK_MON_IEN,   true),     ADI_PWR_SUCCESS)
      // UNIT_TEST(adi_pwr_EnableClockInterrupt(ADI_PWR_LFXTAL_STATUS_IEN,      true),     ADI_PWR_SUCCESS)
      // UNIT_TEST(adi_pwr_EnableClockInterrupt(ADI_PWR_HFXTAL_STATUS_IEN,      true),     ADI_PWR_SUCCESS)
      // UNIT_TEST(adi_pwr_EnableClockInterrupt(ADI_PWR_PLL_STATUS_IEN,         true),     ADI_PWR_SUCCESS)
      // UNIT_TEST(adi_pwr_EnableClockInterrupt(ADI_PWR_LFXTAL_CLOCK_MON_IEN,   false),    ADI_PWR_SUCCESS)
      // UNIT_TEST(adi_pwr_EnableClockInterrupt(ADI_PWR_LFXTAL_STATUS_IEN,      false),    ADI_PWR_SUCCESS)
      // UNIT_TEST(adi_pwr_EnableClockInterrupt(ADI_PWR_HFXTAL_STATUS_IEN,      false),    ADI_PWR_SUCCESS)
       //UNIT_TEST(adi_pwr_EnableClockInterrupt(ADI_PWR_PLL_STATUS_IEN,         false),    ADI_PWR_SUCCESS)

        /* Test a bad input parameter */
       // #ifdef __ICCARM__
        //#pragma diag_suppress=Pe188
        //#endif
        //UNIT_TEST(adi_pwr_EnableClockInterrupt((ADI_PWR_POWER_MODE) 100u, false), ADI_PWR_FAILURE)
       // #ifdef __ICCARM__
       // #pragma diag_default=Pe188
       // #endif 
    } 
    

  /* adi_pwr_SetVoltageRange */
    {
        UNIT_TEST(adi_pwr_SetVoltageRange(ADI_PWR_BAT_VOLTAGE_RANGE_SAFE   ), ADI_PWR_SUCCESS)
        UNIT_TEST(adi_pwr_SetVoltageRange(ADI_PWR_VOLTAGE_RANGE_2_2_TO_2_75), ADI_PWR_SUCCESS)
        UNIT_TEST(adi_pwr_SetVoltageRange(ADI_PWR_VOLTAGE_RANGE_1_6_TO_2_2 ), ADI_PWR_SUCCESS)
    }

    /* adi_pwr_EnablePMGInterrupt */
    {
        /* Test with the voltage not in safe range */
        UNIT_TEST(adi_pwr_EnablePMGInterrupt(ADI_PWR_LOW_BATTERY_VOLTAGE_IEN,   true),  ADI_PWR_FAILURE)
        UNIT_TEST(adi_pwr_EnablePMGInterrupt(ADI_PWR_UNDER_VOLATAGE_IEN,        true),  ADI_PWR_FAILURE)
        UNIT_TEST(adi_pwr_EnablePMGInterrupt(ADI_PWR_OVER_VOLATAGE_IEN,         true),  ADI_PWR_FAILURE)
        UNIT_TEST(adi_pwr_EnablePMGInterrupt(ADI_PWR_BATTERY_VOLTAGE_RANGE_IEN, true),  ADI_PWR_SUCCESS)
        UNIT_TEST(adi_pwr_EnablePMGInterrupt(ADI_PWR_LOW_BATTERY_VOLTAGE_IEN,   false), ADI_PWR_FAILURE)
        UNIT_TEST(adi_pwr_EnablePMGInterrupt(ADI_PWR_UNDER_VOLATAGE_IEN,        false), ADI_PWR_FAILURE)
        UNIT_TEST(adi_pwr_EnablePMGInterrupt(ADI_PWR_OVER_VOLATAGE_IEN,         false), ADI_PWR_FAILURE)
        UNIT_TEST(adi_pwr_EnablePMGInterrupt(ADI_PWR_BATTERY_VOLTAGE_RANGE_IEN, false), ADI_PWR_SUCCESS)
    
        /* Adjust voltage */
        UNIT_TEST(adi_pwr_SetVoltageRange(ADI_PWR_BAT_VOLTAGE_RANGE_SAFE), ADI_PWR_SUCCESS)
    
        /* Test with the voltage in safe range */
        UNIT_TEST(adi_pwr_EnablePMGInterrupt(ADI_PWR_LOW_BATTERY_VOLTAGE_IEN,   true ), ADI_PWR_FAILURE)
        UNIT_TEST(adi_pwr_EnablePMGInterrupt(ADI_PWR_UNDER_VOLATAGE_IEN,        true ), ADI_PWR_FAILURE)
        UNIT_TEST(adi_pwr_EnablePMGInterrupt(ADI_PWR_OVER_VOLATAGE_IEN,         true ), ADI_PWR_FAILURE)
        UNIT_TEST(adi_pwr_EnablePMGInterrupt(ADI_PWR_BATTERY_VOLTAGE_RANGE_IEN, true ), ADI_PWR_FAILURE)
        UNIT_TEST(adi_pwr_EnablePMGInterrupt(ADI_PWR_LOW_BATTERY_VOLTAGE_IEN,   false), ADI_PWR_FAILURE)
        UNIT_TEST(adi_pwr_EnablePMGInterrupt(ADI_PWR_UNDER_VOLATAGE_IEN,        false), ADI_PWR_FAILURE)
        UNIT_TEST(adi_pwr_EnablePMGInterrupt(ADI_PWR_OVER_VOLATAGE_IEN,         false), ADI_PWR_FAILURE)
        UNIT_TEST(adi_pwr_EnablePMGInterrupt(ADI_PWR_BATTERY_VOLTAGE_RANGE_IEN, false), ADI_PWR_FAILURE)
    }

    /* adi_pwr_EnableHPBuck */
    {
        UNIT_TEST(adi_pwr_EnableHPBuck(true ), ADI_PWR_SUCCESS)
        UNIT_TEST(adi_pwr_EnableHPBuck(false), ADI_PWR_SUCCESS)
    }

    /* adi_pwr_GetWakeUpStatus */
    {
        ADI_PWR_WAKEUP_STATUS eStatus = ADI_PWR_INT_EXT1;
        UNIT_TEST(adi_pwr_GetWakeUpStatus(&eStatus), ADI_PWR_SUCCESS)
        UNIT_TEST(eStatus, ADI_PWR_INT_EXT0)
    }
/* adi_pwr_SetClockDivider and adi_pwr_GetClockFrequency*/ 
    {
        uint32_t ClockFreq;
        
        /* Start by setting up PCLK and HCLK to the default values of 26 MHz */
        UNIT_TEST(adi_pwr_SetClockDivider(ADI_CLOCK_HCLK, 1u), ADI_PWR_SUCCESS)
        UNIT_TEST(adi_pwr_SetClockDivider(ADI_CLOCK_PCLK, 1u), ADI_PWR_SUCCESS)
        
        /* Verify default values */
        UNIT_TEST(adi_pwr_GetClockFrequency(ADI_CLOCK_PCLK, &ClockFreq), ADI_PWR_SUCCESS)
        UNIT_TEST(ClockFreq, 26000000)
        UNIT_TEST(adi_pwr_GetClockFrequency(ADI_CLOCK_HCLK, &ClockFreq), ADI_PWR_SUCCESS)
        UNIT_TEST(ClockFreq, 26000000)
        UNIT_TEST(adi_pwr_GetClockFrequency(ADI_CLOCK_ACLK, &ClockFreq), ADI_PWR_INVALID_CLOCK_ID)

        /* All divider values should pass for PCLK, since HCLK is larger */
        for (uint8_t i = 1u; i <= 32u; i++) {
            UNIT_TEST(adi_pwr_SetClockDivider(ADI_CLOCK_PCLK, i), ADI_PWR_SUCCESS)    
        }

        /* Bad divisor setting */
        UNIT_TEST(adi_pwr_SetClockDivider(ADI_CLOCK_PCLK, 33u), ADI_PWR_INVALID_CLOCK_DIVIDER)

        /* Only some HCLK values should pass, since PCLK must be a multiple of HCLK */
        for (uint8_t i = 1u; i <= 32u; i++) {
            if (((ClockFreq / i) % (ClockFreq / 32u)) != 0u) {
                UNIT_TEST(adi_pwr_SetClockDivider(ADI_CLOCK_HCLK, i), ADI_PWR_INVALID_CLOCK_RATIO)    
            } else {
                UNIT_TEST(adi_pwr_SetClockDivider(ADI_CLOCK_HCLK, i), ADI_PWR_SUCCESS)
            }
        }

        /* Bad divisor setting */
        UNIT_TEST(adi_pwr_SetClockDivider(ADI_CLOCK_HCLK, 33u), ADI_PWR_INVALID_CLOCK_DIVIDER)

        /* Set HCLK back to the default value */
        UNIT_TEST(adi_pwr_SetClockDivider(ADI_CLOCK_HCLK, 1u), ADI_PWR_SUCCESS)
        UNIT_TEST(adi_pwr_GetClockFrequency(ADI_CLOCK_HCLK, &ClockFreq), ADI_PWR_SUCCESS)

        /* All divider values should pass for ACLK, since HCLK is larger */
        for (uint16_t i = 1u; i <= 511u; i++) {
            UNIT_TEST(adi_pwr_SetClockDivider(ADI_CLOCK_ACLK, i), ADI_PWR_SUCCESS)  
        }

        /* Bad divisor setting */
        UNIT_TEST(adi_pwr_SetClockDivider(ADI_CLOCK_ACLK, 512u), ADI_PWR_INVALID_CLOCK_DIVIDER)

        /* Bad clock ID */
        #ifdef __ICCARM__
        #pragma diag_suppress=Pe188
        #endif
        UNIT_TEST(adi_pwr_SetClockDivider((ADI_CLOCK_ID) ((uint32_t) ADI_CLOCK_ACLK + 1), 1u), ADI_PWR_INVALID_CLOCK_ID)
        #ifdef __ICCARM__
        #pragma diag_default=Pe188
        #endif
    }
 /* adi_pwr_EnableClockSource */
    {
        /* Enable and then disable the external oscillators, and then renable for future use */
        UNIT_TEST(adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_HFXTAL, true), ADI_PWR_SUCCESS)
        UNIT_TEST(adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_HFXTAL, false), ADI_PWR_SUCCESS)
        UNIT_TEST(adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_LFXTAL, true), ADI_PWR_SUCCESS)
        UNIT_TEST(adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_LFXTAL, false), ADI_PWR_SUCCESS)
        UNIT_TEST(adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_HFXTAL, true), ADI_PWR_SUCCESS)
        UNIT_TEST(adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_LFXTAL, true), ADI_PWR_SUCCESS)

        /* Switch clocks to run on the external oscillators */
        UNIT_TEST(adi_pwr_SetLFClockMux  (ADI_CLOCK_MUX_LFCLK_LFXTAL          ), ADI_PWR_SUCCESS)
        UNIT_TEST(adi_pwr_SetRootClockMux(ADI_CLOCK_MUX_ROOT_HFXTAL           ), ADI_PWR_SUCCESS)
        UNIT_TEST(adi_pwr_SetRefClockMux (ADI_CLOCK_MUX_REF_HFXTAL_26MHZ_CLK  ), ADI_PWR_SUCCESS)
        UNIT_TEST(adi_pwr_SetPLLClockMux (ADI_CLOCK_MUX_SPLL_HFXTAL           ), ADI_PWR_SUCCESS)

        /* Disable and then enable the external oscillators, but note that you can't mess with the LFOSC */
        UNIT_TEST(adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_HFOSC, false), ADI_PWR_SUCCESS)
        UNIT_TEST(adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_LFOSC, false), ADI_PWR_INVALID_PARAM)
        UNIT_TEST(adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_HFOSC, true),  ADI_PWR_SUCCESS)
        UNIT_TEST(adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_LFOSC, true),  ADI_PWR_INVALID_PARAM)

        /* Disable the PLL and then enable it */
        UNIT_TEST(adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_SPLL, false),  ADI_PWR_SUCCESS)
        UNIT_TEST(adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_SPLL, true),   ADI_PWR_SUCCESS)

        /* Disabling the GPIO clock should fail since we don't have it enabled in static config by default */
        UNIT_TEST(adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_GPIO, true),  ADI_PWR_INVALID_PARAM)        
    }
 /* adi_pwr_EnableLFXTALBypass */
    {
        /* Since the bypass API disables the LFXTAL, make sure we aren't using it first */
        UNIT_TEST(adi_pwr_SetLFClockMux(ADI_CLOCK_MUX_LFCLK_LFOSC), ADI_PWR_SUCCESS)
          
        /* This should fail, since no external clock is hooked up and it can't become stable */
       //UNIT_TEST(adi_pwr_EnableLFXTALBypass(true ), ADI_PWR_FAILURE)
          
         /* This should pass, LFXTAL is disabled from the previous step, so the clock is unstable */
      //  UNIT_TEST(adi_pwr_EnableLFXTALBypass(false), ADI_PWR_SUCCESS)
          
         /* Reenable LFXTAL for the future tests */
         UNIT_TEST(adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_LFXTAL, true), ADI_PWR_SUCCESS)
    }

   /* adi_pwr_GetClockStatus */ 
    {
        ADI_CLOCK_SOURCE_STATUS eStatus;
        for (ADI_CLOCK_SOURCE_ID i = ADI_CLOCK_SOURCE_HFXTAL; i <= ADI_CLOCK_SOURCE_GPIO; i++) {
            /* Test with NULL pointer */
            UNIT_TEST(adi_pwr_GetClockStatus(i, NULL    ), ADI_PWR_NULL_POINTER)

            /* Test with valid pointer */
            UNIT_TEST(adi_pwr_GetClockStatus(i, &eStatus), ADI_PWR_SUCCESS)

            /* All internal and external oscillators should be enabled from previous step */
            if ((i == ADI_CLOCK_SOURCE_HFOSC) || (i == ADI_CLOCK_SOURCE_LFOSC) || (i == ADI_CLOCK_SOURCE_HFXTAL) || (i == ADI_CLOCK_SOURCE_LFXTAL)) {
                UNIT_TEST(eStatus, ADI_CLOCK_SOURCE_ENABLED_STABLE)
            /* Can't check the GPIO or PLL status */
            } else {
                UNIT_TEST(eStatus, ADI_CLOCK_SOURCE_ID_NOT_VALID)
            }
            
        }   
    }    
    
    /* adi_pwr_SetLFClockMux */ 
    {
        for (ADI_CLOCK_MUX_ID i = ADI_CLOCK_MUX_SPLL_HFOSC; i <= ADI_CLOCK_MUX_ROOT_GPIO; i++) {
            if ((i == ADI_CLOCK_MUX_LFCLK_LFOSC) || (i == ADI_CLOCK_MUX_LFCLK_LFXTAL)) {
                UNIT_TEST(adi_pwr_SetLFClockMux(i), ADI_PWR_SUCCESS)
            } else {
                UNIT_TEST(adi_pwr_SetLFClockMux(i), ADI_PWR_INVALID_CLOCK_ID)
            }
        }
    }
    
     /* adi_pwr_SetRefClockMux */ 
    {
        for (ADI_CLOCK_MUX_ID i = ADI_CLOCK_MUX_SPLL_HFOSC; i <= ADI_CLOCK_MUX_ROOT_GPIO; i++) {
            if ((i == ADI_CLOCK_MUX_REF_HFXTAL_26MHZ_CLK) || (i == ADI_CLOCK_MUX_REF_HFXTAL_16MHZ_CLK) ||
                (i == ADI_CLOCK_MUX_REF_HFOSC_CLK)) {
                UNIT_TEST(adi_pwr_SetRefClockMux(i), ADI_PWR_SUCCESS)
            } else {
                UNIT_TEST(adi_pwr_SetRefClockMux(i), ADI_PWR_INVALID_CLOCK_ID)
            }
        }
    }
    
     /* adi_pwr_SetPLLClockMux */ 
    {
        for (ADI_CLOCK_MUX_ID i = ADI_CLOCK_MUX_SPLL_HFOSC; i <= ADI_CLOCK_MUX_ROOT_GPIO; i++) {
            if ((i == ADI_CLOCK_MUX_SPLL_HFXTAL) || (i == ADI_CLOCK_MUX_SPLL_HFOSC)) {
                UNIT_TEST(adi_pwr_SetPLLClockMux(i), ADI_PWR_SUCCESS)
            } 
            
           // else {
             //   UNIT_TEST(adi_pwr_SetPLLClockMux(i), ADI_PWR_INVALID_CLOCK_ID)
           // }
        }   
    } 

    /* adi_pwr_SetRootClockMux */ 
    {
         /* @TODO: Untested feature */
    }

    /* adi_pwr_SetPll */
    {
        /* Start by disabling the PLL */
        UNIT_TEST(adi_pwr_EnableClockSource(ADI_CLOCK_SOURCE_SPLL, false), ADI_PWR_SUCCESS)

        /* @TODO: Untested feature */

    }

    /* adi_pwr_EnterLowPowerMode */
    {
        /* Switch the root clock to HFOSC before entering hibernate */
        UNIT_TEST(adi_pwr_SetRootClockMux(ADI_CLOCK_MUX_ROOT_HFOSC), ADI_PWR_SUCCESS)

        /* @TODO: Untested feature */

    }    

    /* adi_pwr_ExitLowPowerMode */
    {
        /* @TODO: Untested feature */
    }     
    return ADI_UNIT_SUCCESS;
}
