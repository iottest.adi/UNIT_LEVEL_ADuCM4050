#include <drivers/gpio/adi_gpio.h>
#include <common.h>
#include <stdio.h>
/*********************************************************UNIT TEST***********************************************************/ 


#ifndef UNIT_TEST_H
#define UNIT_TEST_H

typedef enum
{
  
  ADI_UNIT_SUCCESS = 0u,
  ADI_UNIT_FAILURE = 1u
    
} ADI_UNIT_RESULT;


#define UNIT_TEST(func, dbg) \
do \
{ \
    if ((func) != (dbg)) \
    { \
        DEBUG_MESSAGE("Unit test fail at line %d of %s\r\n", __LINE__, __FILE__); \
        return (ADI_UNIT_FAILURE); \
    } \
} while(0u);

#endif /* UNIT_TEST_H */

/************************************************************ UNIT_TEST_H *****************************************************/


/*


/*********************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*!
 * @file      rng_test.c
 * @brief     Test to demonstrate RNG driver to generate random numbers
 *
 * @details
 *            This is the primary source file for the RNG test
 *
 */

/*=============  I N C L U D E S   =============*/

/* RNG test include */

/* Managed drivers and/or services include */
#include <drivers/rng/adi_rng.h>
#include <drivers/pwr/adi_pwr.h>
#include <common.h>



/*=============  D A T A  =============*/

/* RNG Device Handle */
static ADI_RNG_HANDLE     hDevice;
static ADI_RNG_HANDLE     Device_1;
static ADI_RNG_HANDLE     Device_2;
static ADI_RNG_HANDLE     Device_3;
static ADI_RNG_HANDLE     Device_4;
static ADI_RNG_HANDLE     Device_5;
static ADI_RNG_HANDLE     Device_6;
static ADI_RNG_HANDLE     Device_7;
static ADI_RNG_HANDLE     Device_8;
static ADI_RNG_HANDLE     Device_9;
static ADI_RNG_HANDLE     Device_10;
static ADI_RNG_HANDLE     Device_11;


/**********************************************Micro for testing  ***************************************************************/
#define adi_rng_Open1
#define adi_rng_Close1
#define adi_rng_Enable1
#define adi_rng_EnableBuffering1
#define adi_rng_SetSampleLen1
#define adi_rng_GetRdyStatus1
#define adi_rng_GetStuckStatus1
#define adi_rng_GetRngData1
#define adi_rng_GetOscCount1
#define adi_rng_GetOscDiff1
//#define adi_rng_RegisterCallback1

#define adi_rng_GetSampleLen1


//#define ADI_RNG_INVALID_PARAM  (1u)
//#define ADI_RNG_BAD_DEVICE_NUM (1u)



void Ms_delay(int time)
{
  int i,j;
  for(i=0;i<time;i++)
  {
    for(j=0;i<20;j++)
    {
    }
  }
}


/* Memory to handle CRC Device */
static uint8_t            RngDevMem[ADI_RNG_MEMORY_SIZE];
static uint8_t            MyDevMem[ADI_RNG_MEMORY_SIZE];

/* Data buffers for Random numbers */
static uint32_t           RNBuff[1000] = {0};

static volatile uint32_t  nNumRNGen = 0u;

static volatile uint32_t failure_detected = 0u;
static volatile bool stuck_status_detected = false;

/*=============  L O C A L    F U N C T I O N S  =============*/

void  run_all_configurations(void);

/*=============  C O D E  =============*/

/* IF (Callback mode enabled) */

/*
 *  Callback from RNG Driver
 *
 * Parameters
 *  - [in]  pCBParam    Callback parameter supplied by application
 *  - [in]  Event       Callback event
 *  - [in]  pArg        Callback argument
 *
 * Returns  None
 *
 */
#if 1
static void rngCallback(void *pCBParam, uint32_t Event, void *pArg)
{
    ADI_RNG_RESULT eResult;
    if (Event == ADI_RNG_EVENT_READY)
    {
      uint32_t nRandomNum;
      eResult = adi_rng_GetRngData(hDevice, &nRandomNum);
      if (ADI_RNG_SUCCESS != eResult)
      {
        /* A failure has been detected. Since we cannot print this from the ISR
         * we set a variable to indicate the problem.
         */
        failure_detected++ ;
        return;
      }
      /* Make sure that we do not overflow the array allocated */
      if (nNumRNGen < 1000) {
        RNBuff[nNumRNGen++] = nRandomNum;
      }
    }
    else if (Event == ADI_RNG_EVENT_STUCK)
    {
        stuck_status_detected = true;
    }
    else
    {
        /* Unknown event */
        failure_detected =true;
    }
}
#endif

ADI_UNIT_RESULT rng_unit_testing(void)
{ 
  
  ADI_RNG_RESULT test;
   int RNG_DEV_NUM;
   bool status  = 0;
 //  bool status1 = 0;
   uint32_t  data ;
   uint32_t  nIndex = 3u;
   uint8_t pOscDiff;
   uint16_t pLenPrescaler ;
   uint16_t  pLenReload;
   int a =5;
   
   common_Init();
   adi_pwr_Init();
   adi_pwr_SetClockDivider(ADI_CLOCK_HCLK,1);
   adi_pwr_SetClockDivider(ADI_CLOCK_PCLK,1);
   

#ifdef adi_rng_Open1
   
/*****************************************************************************************************************************/
/********************************************** adi_rng_open & ADI_RNG_INVALID_PARAM *****************************************/
/*****************************************************************************************************************************/

   UNIT_TEST(adi_rng_Open(RNG_DEV_NUM,RngDevMem,1,&hDevice),ADI_RNG_INVALID_PARAM)
    adi_rng_Close(hDevice);
 /*****************************************************************************************************************************/
/********************************************** adi_rng_open & ADI_RNG_ALREADY_INITIALIZED *****************************************/
/*****************************************************************************************************************************/

#if 0 
  adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&hDevice);
 UNIT_TEST(adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&hDevice),ADI_RNG_ALREADY_INITIALIZED)
  adi_rng_Close(hDevice);   
#endif 
    
       
 /*****************************************************************************************************************************/
/********************************************** adi_rng_open & ADI_RNG_BAD_DEVICE_NUM  *****************************************/
/*****************************************************************************************************************************/
   UNIT_TEST(adi_rng_Open(1 ,RngDevMem,sizeof(RngDevMem),&hDevice),ADI_RNG_BAD_DEVICE_NUM)
    adi_rng_Close(hDevice);  
/*****************************************************************************************************************************/
/********************************************** adi_rng_open & ADI_RNG_SUCCESS ***********************************************/
/*****************************************************************************************************************************/
   UNIT_TEST(adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&hDevice), ADI_RNG_SUCCESS)
   adi_rng_Close(hDevice);
   
#endif 
   
 #ifdef adi_rng_Enable1
   
/*****************************************************************************************************************************/
/********************************************** adi_rng_Enable  &  ADI_RNG_NOT_INITIALIZED   *****************************************/
/*****************************************************************************************************************************/
  
   
 /*****************************************************************************************************************************/
/********************************************** adi_rng_Enable  & ADI_RNG_BAD_DEV_HANDLE *****************************************/
/*****************************************************************************************************************************/  
   adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_1);
   UNIT_TEST(adi_rng_Enable(NULL,true),ADI_RNG_BAD_DEV_HANDLE)
   adi_rng_Close(Device_1);
   
   
/*****************************************************************************************************************************/
/********************************************** adi_rng_Enable  & ADI_RNG_SUCCESS   ******************************************/
/*****************************************************************************************************************************/
   
 adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_1);
UNIT_TEST(adi_rng_Enable(Device_1,true),ADI_RNG_SUCCESS )
adi_rng_Close(Device_1);
   
   

#endif 

#ifdef adi_rng_EnableBuffering1

 
/*****************************************************************************************************************************/
/********************************************** adi_rng_EnableBuffering &  ADI_RNG_NOT_INITIALIZED ****************************/
/*****************************************************************************************************************************/
    
    
/*****************************************************************************************************************************/
/********************************************** adi_rng_EnableBuffering &  ADI_RNG_BAD_DEV_HANDLE  ****************************/
/*****************************************************************************************************************************/
    adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_3);
    
    UNIT_TEST(adi_rng_EnableBuffering(NULL,true), ADI_RNG_BAD_DEV_HANDLE)
  
     adi_rng_Close(Device_3);
/*****************************************************************************************************************************/
/********************************************** adi_rng_EnableBuffering &  ADI_RNG_SUCCESS *****************************************/
/*****************************************************************************************************************************/
    adi_rng_Open(RNG_DEV_NUM,MyDevMem,sizeof(RngDevMem),&Device_3);
   UNIT_TEST(adi_rng_EnableBuffering(Device_3,true),ADI_RNG_SUCCESS )
    adi_rng_Close(Device_3);
#endif     

#ifdef adi_rng_SetSampleLen1
uint16_t  RNG_DEV_LEN_PRESCALER,RNG_DEV_LEN_RELOAD;
 /*****************************************************************************************************************************/
/********************************************** adi_rng_SetSampleLen & ADI_RNG_NOT_INITIALIZED ***************************************/
/*****************************************************************************************************************************/
   
    
    
 /*****************************************************************************************************************************/
/********************************************** adi_rng_SetSampleLen & ADI_RNG_BAD_DEV_HANDLE  ********************************/
/*****************************************************************************************************************************/  
    adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_4);
    UNIT_TEST(adi_rng_SetSampleLen(NULL, RNG_DEV_LEN_PRESCALER,RNG_DEV_LEN_RELOAD),ADI_RNG_BAD_DEV_HANDLE)
    adi_rng_Close(Device_4);
    


/*****************************************************************************************************************************/
/********************************************** adi_rng_SetSampleLen & ADI_RNG_SUCCESS **************************************/
/*****************************************************************************************************************************/
   /* adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_4);
     UNIT_TEST(adi_rng_SetSampleLen(Device_4, RNG_DEV_LEN_PRESCALER,RNG_DEV_LEN_RELOAD), ADI_RNG_SUCCESS)
    adi_rng_Close(Device_4);
    */
#endif 


#ifdef adi_rng_GetSampleLen1
 
/*****************************************************************************************************************************/
/********************************************** adi_rng_GetSampleLen & ADI_RNG_NOT_INITIALIZED ***************************************/
/*****************************************************************************************************************************/
  
    
    
 /*****************************************************************************************************************************/
/********************************************** adi_rng_GetSampleLen & ADI_RNG_BAD_DEV_HANDLE  ********************************/
/*****************************************************************************************************************************/  
    adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_5);
    adi_rng_SetSampleLen(Device_5, RNG_DEV_LEN_PRESCALER,RNG_DEV_LEN_RELOAD);
    UNIT_TEST(adi_rng_GetSampleLen(NULL,&pLenPrescaler,&pLenReload), ADI_RNG_BAD_DEV_HANDLE)
    adi_rng_Close(Device_5);
    
/*****************************************************************************************************************************/
/************************************** adi_rng_getSampleLen & ADI_RNG_SUCCESS ***********************************************/
/*****************************************************************************************************************************/
   adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_5);
   adi_rng_SetSampleLen(Device_5, RNG_DEV_LEN_PRESCALER,RNG_DEV_LEN_RELOAD);
  UNIT_TEST(adi_rng_GetSampleLen(Device_5,&pLenPrescaler,&pLenReload),ADI_RNG_SUCCESS)
    adi_rng_Close(Device_5);
     
#endif 
  
 #ifdef  adi_rng_GetRdyStatus1

/*****************************************************************************************************************************/
/********************************************** adi_rng_GetRdyStatus & ADI_RNG_NOT_INITIALIZED  ***********************************************/
/*****************************************************************************************************************************/
   
    
    
/*****************************************************************************************************************************/
/********************************************** adi_rng_GetRdyStatus &ADI_RNG_BAD_DEV_HANDLE ***********************************************/
/*****************************************************************************************************************************/
   adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_6);
   adi_rng_SetSampleLen(Device_6, RNG_DEV_LEN_PRESCALER,RNG_DEV_LEN_RELOAD);
  UNIT_TEST(adi_rng_GetRdyStatus(NULL,&status),ADI_RNG_BAD_DEV_HANDLE)
    adi_rng_Close(Device_6);
/*****************************************************************************************************************************/
/********************************************** adi_rng_GetRdyStatus &ADI_RNG_INVALID_PARAM  *********************************/
/*****************************************************************************************************************************/    
    adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_6);
    adi_rng_SetSampleLen(Device_6, RNG_DEV_LEN_PRESCALER,RNG_DEV_LEN_RELOAD);
   UNIT_TEST(adi_rng_GetRdyStatus(Device_6,NULL),ADI_RNG_INVALID_PARAM)
     adi_rng_Close(Device_6);
/*****************************************************************************************************************************/
/********************************************** adi_rng_GetRdyStatus & ADI_RNG_SUCCESS ***********************************************/
/*****************************************************************************************************************************/
   adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_6);
   adi_rng_SetSampleLen(Device_6, RNG_DEV_LEN_PRESCALER,RNG_DEV_LEN_RELOAD);
UNIT_TEST(adi_rng_GetRdyStatus(Device_6,&status),ADI_RNG_SUCCESS)
 adi_rng_Close(Device_6);
    
    
#endif 

#ifdef  adi_rng_GetStuckStatus1
    

/*****************************************************************************************************************************/
/********************************************** adi_rng_GetStuckStatus & ADI_RNG_NOT_INITIALIZED  ***********************************************/
/*****************************************************************************************************************************/
   
/*****************************************************************************************************************************/
/********************************************** adi_rng_GetStuckStatus &ADI_RNG_BAD_DEV_HANDLE *******************************/
/*****************************************************************************************************************************/
    adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_7);
    adi_rng_SetSampleLen(Device_7, RNG_DEV_LEN_PRESCALER,RNG_DEV_LEN_RELOAD);
    UNIT_TEST( adi_rng_GetRdyStatus(NULL,&status),ADI_RNG_BAD_DEV_HANDLE)
    adi_rng_Close(Device_7);
/*****************************************************************************************************************************/
/********************************************** adi_rng_GetRdyStatus &ADI_RNG_INVALID_PARAM  *********************************/
/*****************************************************************************************************************************/    
   adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_7);
   adi_rng_SetSampleLen(Device_7, RNG_DEV_LEN_PRESCALER,RNG_DEV_LEN_RELOAD);
   UNIT_TEST(adi_rng_GetStuckStatus(Device_7,NULL),ADI_RNG_INVALID_PARAM) 
    adi_rng_Close(Device_7);
 /*****************************************************************************************************************************/
/********************************************** adi_rng_GetStuckStatus & ADI_RNG_SUCCESS ***********************************************/
/*****************************************************************************************************************************/
   adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_7);
  UNIT_TEST(adi_rng_GetStuckStatus(Device_7,&status),ADI_RNG_SUCCESS)
  adi_rng_Close(Device_7);
   
    
#endif

#ifdef adi_rng_RegisterCallback1
 
 /*****************************************************************************************************************************/
/**********************************************  adi_rng_RegisterCallback&ADI_RNG_NOT_INITIALIZED  ****************************/
/*****************************************************************************************************************************/
   
/*****************************************************************************************************************************/
/**********************************************  adi_rng_RegisterCallback & ADI_RNG_BAD_DEV_HANDLE  ************************************/
/*****************************************************************************************************************************/
    adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),Device_2);
    UNIT_TEST(adi_rng_RegisterCallback(NULL, rngCallback,Device_2),ADI_RNG_BAD_DEV_HANDLE)
    adi_rng_Close(Device_2);
       
 /*****************************************************************************************************************************/
/**********************************************  adi_rng_RegisterCallback & ADI_RNG_SUCCESS ***********************************************/
/*****************************************************************************************************************************/
  adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_2);
  UNIT_TEST(adi_rng_RegisterCallback(Device_2, rngCallback,Device_2),ADI_RNG_SUCCESS)
  adi_rng_Close(Device_2);
    
#endif 
    
#ifdef adi_rng_GetRngData1
  

/*****************************************************************************************************************************/
/********************************************** adi_rng_GetRngData & ADI_RNG_NOT_INITIALIZED  ***********************************************/
/*****************************************************************************************************************************/
    
    
/*****************************************************************************************************************************/
/********************************************** adi_rng_GetRngData&ADI_RNG_BAD_DEV_HANDLE ***********************************************/
/*****************************************************************************************************************************/
   adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_8);
   adi_rng_SetSampleLen(Device_8, RNG_DEV_LEN_PRESCALER,RNG_DEV_LEN_RELOAD);
   UNIT_TEST(adi_rng_GetRngData(NULL,&data),ADI_RNG_BAD_DEV_HANDLE)
    adi_rng_Close(Device_8);
    
/*****************************************************************************************************************************/
/********************************************** adi_rng_GetRngData &ADI_RNG_INVALID_PARAM  *********************************/
/*****************************************************************************************************************************/    
   adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_8);
   adi_rng_SetSampleLen(Device_8, RNG_DEV_LEN_PRESCALER,RNG_DEV_LEN_RELOAD);
    UNIT_TEST(adi_rng_GetRngData(Device_8,NULL), ADI_RNG_INVALID_PARAM)
    adi_rng_Close(Device_8);
    
 /*****************************************************************************************************************************/
/**********************************************  adi_rng_GetRngData & ADI_RNG_SUCCESS *****************************************/
/*****************************************************************************************************************************/
# if 0
 adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_8);
 adi_rng_SetSampleLen(Device_8, RNG_DEV_LEN_PRESCALER,RNG_DEV_LEN_RELOAD);

UNIT_TEST(adi_rng_GetRngData(Device_8,&data), ADI_RNG_SUCCESS )
   
#endif  
#endif 

#ifdef adi_rng_GetOscCount1
 
/*****************************************************************************************************************************/
/********************************************** adi_rng_GetOscCount & ADI_RNG_NOT_INITIALIZED  ***********************************************/
/*****************************************************************************************************************************/
    
   
   
/*****************************************************************************************************************************/
/********************************************** adi_rng_GetOscCount &ADI_RNG_BAD_DEV_HANDLE **********************************/
/*****************************************************************************************************************************/
   adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_9);

UNIT_TEST(adi_rng_GetOscCount(NULL,&data),ADI_RNG_BAD_DEV_HANDLE)
    adi_rng_Close(Device_9);
/*****************************************************************************************************************************/
/********************************************** adi_rng_GetOscCount &ADI_RNG_INVALID_PARAM  *********************************/
/*****************************************************************************************************************************/    
    adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_9);
 UNIT_TEST(adi_rng_GetRngData(Device_9,NULL),ADI_RNG_INVALID_PARAM)
    adi_rng_Close(Device_9);
/**********************************************************************************************************************************/
/********************************************** adi_rng_GetOscCount & ADI_RNG_SUCCESS *******************************************/
/********************************************************************************************************************************/
#if 0   
    adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_9);
  UNIT_TEST(adi_rng_GetOscCount(Device_9,&data),ADI_RNG_SUCCESS)
    adi_rng_Close(Device_9);
#endif 
    
   
#endif  

#ifdef adi_rng_GetOscDiff1
  
/*****************************************************************************************************************************/
/**********************************************  adi_rng_GetOscDiff& ADI_RNG_NOT_INITIALIZED  ***********************************************/
/*****************************************************************************************************************************/
     
    
    
/*****************************************************************************************************************************/
/**********************************************adi_rng_GetOscDiff & ADI_RNG_BAD_DEV_HANDLE *********************************/
/*****************************************************************************************************************************/
   adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_10);
  UNIT_TEST(adi_rng_GetOscDiff(NULL, nIndex,&pOscDiff),ADI_RNG_BAD_DEV_HANDLE)
    adi_rng_Close(Device_10);
    
/*****************************************************************************************************************************/
/********************************************** adi_rng_GetOscCount &ADI_RNG_INVALID_PARAM  *********************************/
/*****************************************************************************************************************************/    
    adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_10);
   UNIT_TEST(adi_rng_GetOscDiff(Device_10, nIndex,NULL), ADI_RNG_INVALID_PARAM )
    adi_rng_Close(Device_10);
 /*****************************************************************************************************************************/
/**********************************************  adi_rng_GetOscDiff& ADI_RNG_SUCCESS ***********************************************/
/*****************************************************************************************************************************/
#if 0
    adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_10);
  UNIT_TEST(adi_rng_GetOscDiff(Device_10, nIndex,&pOscDiff),ADI_RNG_SUCCESS)
    adi_rng_Close(Device_10);
#endif
 
   
#endif    

#ifdef adi_rng_Close1
 
 /*****************************************************************************************************************************/
/**********************************************  adi_rng_Close &ADI_RNG_NOT_INITIALIZED  ***********************************************/
/*****************************************************************************************************************************/
    
/*****************************************************************************************************************************/
/**********************************************  adi_rng_Close & ADI_RNG_BAD_DEV_HANDLE  ************************************/
/*****************************************************************************************************************************/
    adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_11);
  UNIT_TEST(adi_rng_Close(NULL),ADI_RNG_BAD_DEV_HANDLE)

    adi_rng_Close(Device_11);
/*****************************************************************************************************************************/
/**********************************************  adi_rng_Close & ADI_RNG_SUCCESS ***********************************************/
/*****************************************************************************************************************************/
   adi_rng_Open(RNG_DEV_NUM,RngDevMem,sizeof(RngDevMem),&Device_11);
    UNIT_TEST(adi_rng_Close(Device_11),ADI_RNG_SUCCESS)
    
   adi_rng_Close(Device_11);
    
  
#endif



return ADI_UNIT_SUCCESS;  
}