#include <drivers/gpio/adi_gpio.h>
#include <common.h>
#include <stdio.h>
/*********************************************************UNIT TEST***********************************************************/ 


#ifndef UNIT_TEST_H
#define UNIT_TEST_H

typedef enum
{
  
  ADI_UNIT_SUCCESS = 0u,
  ADI_UNIT_FAILURE = 1u
    
} ADI_UNIT_RESULT;


#define UNIT_TEST(func, dbg) \
do \
{ \
    if ((func) != (dbg)) \
    { \
        DEBUG_MESSAGE("Unit test fail at line %d of %s\r\n", __LINE__, __FILE__); \
        return (ADI_UNIT_FAILURE); \
    } \
} while(0u);

#endif /* UNIT_TEST_H */

/************************************************************ UNIT_TEST_H *****************************************************/



/*********************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*!
* @file      adc_tst.c
* @brief     Test parts of the ADC driver.
*
*/

/*=============  I N C L U D E S   =============*/

/* ADC example include */

/* Managed drivers and/or services include */
#include <drivers/adc/adi_adc.h>
#include "common.h"
#include "drivers/pwr/adi_pwr.h"
#include <assert.h>
#include <stdio.h>
/*=============  D A T A  =============*/

#if defined ( __ICCARM__ )
    #define ALIGN4 _Pragma("data_alignment=4")
#elif defined (__CC_ARM)
    #define ALIGN4 __align(4)
#else
    #pragma message("WARNING: NO ALIGHMENT DEFINED FOR MEMORY")
#endif

/* ADC Handle */
ADI_ADC_HANDLE hDevice, hDevice_1; 

ADI_ADC_HANDLE hDevice_close , hDevice_close_1;

ADI_ADC_HANDLE hDevice_pwr_up, hDevice_pwr_up_1;

ADI_ADC_HANDLE hDevice_reg_call_back, hDevice_reg_call_back_1;

ADI_ADC_HANDLE hDevice_enable_sub_sys , hDevice_enable_sub_sys_1;

ADI_ADC_HANDLE hDevice_is_readdy, hDevice_is_readdy_1;

ADI_ADC_HANDLE hDevice_set_vol, hDevice_set_vol_1;

ADI_ADC_HANDLE hDevice_sink_enable, hDevice_sink_enable_1;

ADI_ADC_HANDLE hDevice_start_calib, hDevice_start_calib_1;

ADI_ADC_HANDLE hDevice_done_calib, hDevice_done_calib_1;

ADI_ADC_HANDLE hDevice_acq_time, hDevice_acq_time_1;

ADI_ADC_HANDLE hDevice_set_dly, hDevice_set_dly_1;

ADI_ADC_HANDLE hDevice_set_res, hDevice_set_res_1;

ADI_ADC_HANDLE hDevice_get_batt_vol , hDevice_get_batt_vol_1;

ADI_ADC_HANDLE hDevice_enable_avg , hDevice_enable_avg_1;

ADI_ADC_HANDLE hDevice_set_low , hDevice_set_low_1;

ADI_ADC_HANDLE hDevice_set_hig , hDevice_set_hig_1;

ADI_ADC_HANDLE hDevice_set_hys , hDevice_set_hys_1;

ADI_ADC_HANDLE hDevice_num_cyc , hDevice_num_cyc_1;

ADI_ADC_HANDLE hDevice_dig_comp , hDevice_dig_comp_1 , hDevice_dig_comp_2 ;

ADI_ADC_HANDLE hDevice_submit_buf, hDevice_submit_buf_1 ;

ADI_ADC_HANDLE hDevice_get_buf, hDevice_get_buf_1 ;

ADI_ADC_HANDLE hDevice_enable, hDevice_enable_1 ;

ADI_ADC_HANDLE hDevice_read_cha, hDevice_read_cha_1;
/* Memory Required for adc driver */
ALIGN4 static uint8_t DeviceMemory[ADI_ADC_MEMORY_SIZE];

/* ADC Data Buffer */
ALIGN4 static uint16_t ADC_DataBuffer[1000] = {0};



/* Example configurations */
#define CHANNEL_TO_USE        (ADI_ADC_CHANNEL_0)
#define ADC_WAIT_FOR_LIMIT    (1)
#define ADC_GET_TEMP          (1)
#define ADC_GET_BATTERY       (1)

#define READ_CHANNEL_SAMPLES  (8)
# define ADC_NUM_SAMPLES 1000
volatile bool bLimitCrossed_Hi = false;
volatile bool bLimitCrossed_Lo = false;

/*=============  E X T E R N A L    F U N C T I O N S  =============*/
int32_t adi_initpinmux(void);

/*=============  L O C A L    F U N C T I O N S  =============*/

static void usleep(uint32_t usec);

#if ADC_WAIT_FOR_LIMIT  == 1
static void WaitForLimit(ADI_ADC_HANDLE hDevice);
#endif

/*=============  C O D E  =============*/

/* Callback from the device */
static void ADCCallback(void *pCBParam, uint32_t Event, void *pArg)
{
    switch (Event)
    {
    case ADI_ADC_EVENT_HIGH_LIMIT_CROSSED:
        bLimitCrossed_Hi = true;
        break;
    case ADI_ADC_EVENT_LOW_LIMIT_CROSSED:
        bLimitCrossed_Lo = true;
        break;

    default:
        break;
    }
}

/* Set up and wait for a comparitor setting to trigger.
 *
 * Only tests one channel
 */

//static void WaitForLimit(ADI_ADC_HANDLE hDevice)
//{
//    ADI_ADC_RESULT  eResult = ADI_ADC_SUCCESS;
//    
//    /* Configure high limit for an ADC channel when it's used as a digital comparator. */
//    eResult = adi_adc_SetLowLimit (hDevice, CHANNEL_TO_USE, true, 1000);
//    DEBUG_RESULT("Failed to set the acquisition time ", eResult, ADI_ADC_SUCCESS);	
//
//    /* Configure hysteresis for an ADC channel when it's used as a digital comparator. */
//    eResult = adi_adc_SetHysteresis(hDevice, CHANNEL_TO_USE, true, 0);
//    DEBUG_RESULT("Failed to set adi_adc_SetHysteresis ", eResult, ADI_ADC_SUCCESS);
//
//    /* Configure number of monitor cycles for an ADC channel when it's used as a digital comparator. */
//    eResult = adi_adc_SetNumMonitorCycles(hDevice, CHANNEL_TO_USE, 0);
//    DEBUG_RESULT("Failed to set adi_adc_SetNumMonitorCycles ", eResult, ADI_ADC_SUCCESS);
//
//    /*  Enable/Disable digital comparator for the given channel(s) */
//  //  eResult = adi_adc_EnableDigitalComparator (hDevice, true);  
//   // DEBUG_RESULT("Failed to set adi_adc_EnableDigitalComparator ", eResult, ADI_ADC_SUCCESS);
//
//    while(!bLimitCrossed_Lo);
//    common_Perf("Limit crossed!\n");
//    
//   // eResult = adi_adc_EnableDigitalComparator (hDevice,false);  
//   // DEBUG_RESULT("Failed to set adi_adc_EnableDigitalComparator ", eResult, ADI_ADC_SUCCESS);  
//}
//
//
///* Simple test to get the current battery voltage. 
// * Assumes the battery voltage is around 3.15V
// *
// */
//static void GetBatteryVoltage(ADI_ADC_HANDLE hDevice)
//{
//    ADI_ADC_RESULT  eResult = ADI_ADC_SUCCESS;
//    uint32_t  voltage_read = 0u;
//    
//    /* The "safe" range of battery voltages is between 2.8V and 3.6V, use this
//     * as a passsing range. */
//    uint32_t  voltage_known_low  = (2u << 16u) + (uint32_t)(0.80 * 0xFFFFu);
//    uint32_t  voltage_known_hi   = (3u << 16u) + (uint32_t)(0.60 * 0xFFFFu);
//    
//    eResult = adi_adc_GetBatteryVoltage(hDevice, (uint32_t)((2u << 16u) + (1u << 8u)), &voltage_read);
//    //DEBUG_RESULT("Failed to get voltage ", eResult, ADI_ADC_SUCCESS);	/*commented by patil*/
//    
//    /* We expect the voltage to be around 3.15V, let's assume
//     * around 10% variance. */
//    if(voltage_read > voltage_known_low) {
//      if(voltage_read < voltage_known_hi) {
//         common_Perf("Voltage OK");
//      }
//      else {
//         common_Fail("Measured battery voltage too high!");
//      }
//    }
//    else {
//       common_Fail("Measured battery voltage too low!");
//      }
//}


/* Simple test to get the current temperature of the chip. 
 * Assumes a fairly stable, room-temperature value (centigrade).
 *
 */
//static void GetChipTemperature(ADI_ADC_HANDLE hDevice)
//{
//    ADI_ADC_RESULT  eResult = ADI_ADC_SUCCESS;
//    int32_t  temp_read = 0;
//    int32_t  temp_known_high = 30; /* degrees c */
//    int32_t  temp_known_low  = 15; /* degrees c */
//    
//    eResult = adi_adc_EnableTemperatureSensor(hDevice, true);
//    DEBUG_RESULT("Failed to get temperature ", eResult, ADI_ADC_SUCCESS);	
//    
//    eResult = adi_adc_GetTemperature (hDevice, (uint32_t)((2u<< 16u) + 5), &temp_read);
//    DEBUG_RESULT("Failed to get temperature ", eResult, ADI_ADC_SUCCESS);	
//    
//    if(temp_read > temp_known_low) {
//      if(temp_read < temp_known_high) {
//         common_Perf("Temp just right");
//      }
//      else {
//        common_Perf("Temp ok!");  
//      }
//    }
//    else {
//        common_Fail("Temperature too low!");
//    }
//    
//    eResult = adi_adc_EnableTemperatureSensor(hDevice, false);
//    DEBUG_RESULT("Failed to get temperature ", eResult, ADI_ADC_SUCCESS);	
//}

    
void SetUpADC(ADI_ADC_HANDLE hDevice)
{
    ADI_ADC_RESULT  eResult = ADI_ADC_SUCCESS;
    bool  bCalibrationDone = false;	
    
    /* Power up ADC */
    eResult = adi_adc_PowerUp (hDevice, true);
    DEBUG_RESULT("Failed to power up ADC", eResult, ADI_ADC_SUCCESS);

    /* Set ADC reference */
    eResult = adi_adc_SetVrefSource (hDevice, ADI_ADC_VREF_SRC_INT_2_50_V);
    DEBUG_RESULT("Failed to set ADC reference", eResult, ADI_ADC_SUCCESS);

    /* Enable ADC sub system */
    eResult = adi_adc_EnableADCSubSystem (hDevice, true);
    DEBUG_RESULT("Failed to enable ADC sub system", eResult, ADI_ADC_SUCCESS);

    /* Wait for 5.0ms */
    usleep (5000);
    
    /* Start calibration */
    eResult = adi_adc_StartCalibration (hDevice);
    DEBUG_RESULT("Failed to start calibration", eResult, ADI_ADC_SUCCESS);

    /* Wait until calibration is done */
    while (!bCalibrationDone)
    {
        eResult = adi_adc_IsCalibrationDone (hDevice, &bCalibrationDone);
        DEBUG_RESULT("Failed to get the calibration status", eResult, ADI_ADC_SUCCESS);
    }	  
  
}

/* Simply read a given channel - an unconnected EZ-kit will have
 * a slightly wandering signal around 0x0, so hard to test for any accuracy.
 * Simply read, and if returned successfully, then return successfully. */
void ReadChannels(ADI_ADC_HANDLE hDevice)
{
  ADI_ADC_RESULT  eResult = ADI_ADC_SUCCESS;
  uint32_t data[READ_CHANNEL_SAMPLES];
  
  eResult = adi_adc_ReadChannels (hDevice, CHANNEL_TO_USE, READ_CHANNEL_SAMPLES*2, (void *)&data, sizeof(data));
  DEBUG_RESULT("Failed to Read Channels ", eResult, ADI_ADC_SUCCESS);
    
  common_Perf("Read channels done ");     
}


/* Sample the ADC */
void SampleData(ADI_ADC_HANDLE hDevice)
{
    ADI_ADC_BUFFER Buffer;
    ADI_ADC_RESULT  eResult = ADI_ADC_SUCCESS;
        
    /* Set the delay time */
    eResult = adi_adc_SetDelayTime ( hDevice, 10);
    DEBUG_RESULT("Failed to set the Delay time ", eResult, ADI_ADC_SUCCESS);

    /* Set the acquisition time. (Application need to change it based on the impedence) */
    eResult = adi_adc_SetAcquisitionTime ( hDevice, 10);
    DEBUG_RESULT("Failed to set the acquisition time ", eResult, ADI_ADC_SUCCESS);	

    /* Unregister callback */
    eResult = adi_adc_RegisterCallback (hDevice, NULL, NULL);
    DEBUG_RESULT("Failed to unregister callback ", eResult, ADI_ADC_SUCCESS);
    
    /* Populate the buffer structure */
    Buffer.nBuffSize = sizeof(ADC_DataBuffer);
    Buffer.nChannels = CHANNEL_TO_USE;
    Buffer.nNumConversionPasses = ADC_NUM_SAMPLES/2;
    Buffer.pDataBuffer = ADC_DataBuffer;
    
    /* Submit the buffer to the driver */
    eResult = adi_adc_SubmitBuffer (hDevice, &Buffer);
    DEBUG_RESULT("Failed to submit buffer ", eResult, ADI_ADC_SUCCESS);
        
    /* Enable the ADC */
    eResult = adi_adc_Enable (hDevice, true);
    DEBUG_RESULT("Failed to enable the ADC for sampling ", eResult, ADI_ADC_SUCCESS);

    ADI_ADC_BUFFER* pAdcBuffer;
    eResult = adi_adc_GetBuffer (hDevice, &pAdcBuffer);
   // printf (" eResult = %d \n", eResult);
    DEBUG_RESULT("Failed to Get Buffer ", eResult, ADI_ADC_SUCCESS);
    
    
}

/*TETSING API's */
#if 1
#define ADC_OPEN
#define ADC_CLOSE
#define ADC_PWR_UP
#define REG_CALL_BACK
#define ENABLE_SUB_SYS
#define IS_READY
#define SET_VOL_REF 
#define SINK_ENABLE
#define START_CALI
#define DONE_CALI
#define SET_ACQ_TIME
#define SET_DLY
#define SET_RESOL
#define SET_LOW_LIMIT  
#define SET_HIGH_LIMIT 
#define SET_HYSTETE
#define NUM_MON_CYC
//#define SUBMIT_BUFF
#define ENABLE
//#define GET_BUFF
//#define TEMP_CHA 
#endif

//#define READ_CHANNEL
/*need to check */

//#define ENABLE_AVG
#define ENABLE_DIG_COMP  


#define INVAILD_DEVICE 4
#define INVALID_MEMORY 25
#define ADI_ADC_BAD_VREF_SRC_VBAT -84
#define INVAILD_PARMETER 45
#define CHANNEL_BAD 5
/*********************************************************************
*
*   Function:   main
*
*********************************************************************/
ADI_UNIT_RESULT adc_unit_testing(void)
{
    ADI_ADC_RESULT  eResult;
    int ADC_DEV_NUM;
    common_Init();

    if (adi_pwr_Init()!= ADI_PWR_SUCCESS)
    {
        DEBUG_MESSAGE("\n Failed to intialize the power service \n");
    }
    if (ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_HCLK,1))
    {
        DEBUG_MESSAGE("Failed to intialize the power service\n");
    }
    if (ADI_PWR_SUCCESS != adi_pwr_SetClockDivider(ADI_CLOCK_PCLK,1))
    {
        DEBUG_MESSAGE("Failed to intialize the power service\n");
    }
      
    
    /* Configure the Pin Mux for ADC */
    //adi_initpinmux();
 
#ifdef ADC_OPEN
     
 ALIGN4 static uint8_t InvalidDeviceMemory[INVALID_MEMORY];    
 /******************************************************************************/
/********************adi_adc_Opent & ADI_ADC_INSUFFICIENT_MEMORY **************/
/******************************************************************************/
    UNIT_TEST(adi_adc_Open(ADC_DEV_NUM, InvalidDeviceMemory, sizeof(InvalidDeviceMemory), &hDevice),ADI_ADC_INSUFFICIENT_MEMORY)
    adi_adc_Close (hDevice);
    
    
/******************************************************************************/
/********************adi_adc_Opent & ADI_ADC_INVALID_DEVICE_NUM****************/
/******************************************************************************/
    UNIT_TEST(adi_adc_Open(INVAILD_DEVICE, DeviceMemory, sizeof(DeviceMemory), &hDevice), ADI_ADC_INVALID_DEVICE_NUM)
    adi_adc_Close (hDevice);
  
/******************************************************************************/
/********************adi_adc_Opent & ADI_ADC_SUCCESS***************************/
/******************************************************************************/
 
  UNIT_TEST(adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice),ADI_ADC_SUCCESS )
  adi_adc_Close (hDevice); 
   
/******************************************************************************/
/********************adi_adc_Opent & ADI_ADC_IN_USE****************************/
/******************************************************************************/
  adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice);
   UNIT_TEST(adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice),ADI_ADC_IN_USE);
  adi_adc_Close (hDevice);
#endif 

#ifdef ADC_CLOSE
 
/******************************************************************************/
/********************adi_adc_Close & ADI_ADC_SUCCESS***************************/
/******************************************************************************/
   adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice_close);
   UNIT_TEST(adi_adc_Close(hDevice_close),ADI_ADC_SUCCESS)
   
/******************************************************************************/
/********************adi_adc_Close & ADI_ADC_INVALID_DEVICE_HANDLE ************/
/******************************************************************************/  
  UNIT_TEST(adi_adc_Close (hDevice_close_1),ADI_ADC_INVALID_DEVICE_HANDLE)

#endif     
   
#ifdef ADC_PWR_UP

 
adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice_pwr_up);

/******************************************************************************/
/********************adi_adc_PowerUp & #ADI_ADC_INVALID_DEVICE_HANDLE**********/
/******************************************************************************/    
UNIT_TEST(adi_adc_PowerUp(hDevice_pwr_up_1 , true),ADI_ADC_INVALID_DEVICE_HANDLE)
         
/******************************************************************************/
/********************adi_adc_PowerUp & ADI_ADC_BAD_SYS_CLOCK ******************/
/******************************************************************************/      
  /* need to devlop the logic */ 
   
/******************************************************************************/
/********************adi_adc_PowerUp & ADI_ADC_SUCCESS*************************/
/******************************************************************************/   
 UNIT_TEST(adi_adc_PowerUp(hDevice_pwr_up, true),ADI_ADC_SUCCESS)
 adi_adc_Close (hDevice_pwr_up_1);
 adi_adc_Close (hDevice_pwr_up);  
    
#endif    

#ifdef REG_CALL_BACK
  

/******************************************************************************/
/********************adi_adc_RegisterCallback & ADI_ADC_SUCCESS****************/
/******************************************************************************/  
    //printf (" ************************************************ \n ");
    adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice_reg_call_back);
    /* Register Callback */
   UNIT_TEST( adi_adc_RegisterCallback (hDevice_reg_call_back, ADCCallback, NULL), ADI_ADC_SUCCESS)

/******************************************************************************/
/********************adi_adc_RegisterCallback & ADI_ADC_INVALID_SEQUENCE*******/
/******************************************************************************/ 

    /*need to devlop the logic */


/******************************************************************************/
/*************adi_adc_RegisterCallback & ADI_ADC_INVALID_DEVICE_HANDLE*********/
/******************************************************************************/    

    /* Register Callback */
   UNIT_TEST(adi_adc_RegisterCallback (hDevice_reg_call_back_1, ADCCallback, NULL),ADI_ADC_INVALID_DEVICE_HANDLE)
   adi_adc_Close (hDevice_reg_call_back);
   
#endif    
    
#ifdef ENABLE_SUB_SYS
   
 
/******************************************************************************/
/*************adi_adc_EnableADCSubSystem & ADI_ADC_INVALID_SEQUENCE************/
/******************************************************************************/    
     
    adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice_enable_sub_sys);
     UNIT_TEST(adi_adc_EnableADCSubSystem (hDevice_enable_sub_sys, true), ADI_ADC_INVALID_SEQUENCE)

/******************************************************************************/
/*************adi_adc_EnableADCSubSystem & ADI_ADC_INVALID_DEVICE_HANDLE ******/
/******************************************************************************/        
UNIT_TEST(adi_adc_EnableADCSubSystem (hDevice_enable_sub_sys_1, true),ADI_ADC_INVALID_DEVICE_HANDLE)
/******************************************************************************/
/*************adi_adc_EnableADCSubSystem & ADI_ADC_SUCCESS *******************/
/******************************************************************************/  
       
       /* Power up ADC */
    adi_adc_PowerUp (hDevice_enable_sub_sys, true);
    

    /* Set ADC reference */
adi_adc_SetVrefSource (hDevice_enable_sub_sys, ADI_ADC_VREF_SRC_INT_2_50_V);
  
    /* Enable ADC sub system */
UNIT_TEST(adi_adc_EnableADCSubSystem (hDevice_enable_sub_sys, true),ADI_ADC_SUCCESS)
   
     
 adi_adc_Close(hDevice_enable_sub_sys);
 //printf (" adi_adc_Close : -> %d\n",  eResult);

#endif     

#ifdef IS_READY
 
bool  *temp_1;
 
adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice_is_readdy);
/******************************************************************************/
/*************adi_adc_IsReady & ADI_ADC_INVALID_SEQUENCE***********************/
/******************************************************************************/
  //UNIT_TEST(adi_adc_IsReady (hDevice_is_readdy, temp_1),ADI_ADC_INVALID_SEQUENCE)
   
     adi_adc_Close(hDevice_is_readdy);
/******************************************************************************/
/*************adi_adc_IsReady & ADI_ADC_SUCCESS *******************************/
/******************************************************************************/ 
   /* Power up ADC */
adi_adc_PowerUp (hDevice_is_readdy, true);


    /* Set ADC reference */
adi_adc_SetVrefSource (hDevice_is_readdy, ADI_ADC_VREF_SRC_INT_2_50_V);
    

    /* Enable ADC sub system */
 adi_adc_EnableADCSubSystem (hDevice_is_readdy, true);


    //UNIT_TEST( adi_adc_IsReady (hDevice_is_readdy, temp_1), ADI_ADC_SUCCESS) 
    
   adi_adc_Close(hDevice_is_readdy);
/******************************************************************************/
/*************adi_adc_IsReady & ADI_ADC_INVALID_DEVICE_HANDLE******************/
/******************************************************************************/
 UNIT_TEST(adi_adc_IsReady (hDevice_is_readdy_1, temp_1),ADI_ADC_INVALID_DEVICE_HANDLE)
   adi_adc_Close(hDevice_is_readdy);
   
#endif      

#ifdef SET_VOL_REF   
adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice_set_vol);  

/* Power up ADC */
    eResult = adi_adc_PowerUp (hDevice_set_vol, true);
    DEBUG_RESULT("Failed to power up ADC", eResult, ADI_ADC_SUCCESS); 
 
/******************************************************************************/
/*************adi_adc_SetVrefSource  & ADI_ADC_INVALID_PARAMETER***************/
/******************************************************************************/   
   
  UNIT_TEST(adi_adc_SetVrefSource (hDevice_set_vol, ADI_ADC_BAD_VREF_SRC_VBAT),ADI_ADC_INVALID_PARAMETER)
  
/******************************************************************************/
/*************adi_adc_SetVrefSource  & ADI_ADC_INVALID_DEVICE_HANDLE***************/
/******************************************************************************/   
   
  UNIT_TEST( adi_adc_SetVrefSource (hDevice_set_vol_1, ADI_ADC_VREF_SRC_INT_2_50_V), ADI_ADC_INVALID_DEVICE_HANDLE)
   
/******************************************************************************/
/*************adi_adc_SetVrefSource  & ADI_ADC_SUCCESS*************************/
/******************************************************************************/   
   
   UNIT_TEST(adi_adc_SetVrefSource (hDevice_set_vol, ADI_ADC_VREF_SRC_INT_2_50_V),ADI_ADC_SUCCESS)
   adi_adc_Close(hDevice_set_vol);
#endif    
   
#ifdef SINK_ENABLE
   
 adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice_sink_enable);  

/* Power up ADC */
    eResult = adi_adc_PowerUp (hDevice_sink_enable, true);
   
    eResult = adi_adc_SetVrefSource (hDevice_set_vol, ADI_ADC_VREF_SRC_INT_2_50_V);
/******************************************************************************/
/*************adi_adc_SinkEnable & ADI_ADC_SUCCESS*************************/
/******************************************************************************/      
  UNIT_TEST(adi_adc_SinkEnable (hDevice_sink_enable , true ),ADI_ADC_SUCCESS)
/******************************************************************************/
/*************adi_adc_SinkEnable  & ADI_ADC_INVALID_DEVICE_HANDLE***********/
/******************************************************************************/           
   UNIT_TEST(adi_adc_SinkEnable (hDevice_sink_enable_1 , true ), ADI_ADC_INVALID_DEVICE_HANDLE)
 adi_adc_Close(hDevice_sink_enable);
#endif   

#ifdef START_CALI

    eResult = adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice_start_calib); 

/* Power up ADC */
    eResult = adi_adc_PowerUp (hDevice_start_calib, true);
    DEBUG_RESULT("Failed to power up ADC", eResult, ADI_ADC_SUCCESS);

    /* Set ADC reference */
    eResult = adi_adc_SetVrefSource (hDevice_start_calib, ADI_ADC_VREF_SRC_INT_2_50_V);
    DEBUG_RESULT("Failed to set ADC reference", eResult, ADI_ADC_SUCCESS);

    /* Enable ADC sub system */
    eResult = adi_adc_EnableADCSubSystem (hDevice_start_calib, true);
    DEBUG_RESULT("Failed to enable ADC sub system", eResult, ADI_ADC_SUCCESS);

    /* Wait for 5.0ms */
    usleep (5000);

 /*****************************************************************************/
/*************adi_adc_StartCalibration & ADI_ADC_SUCCESS***********************/
/******************************************************************************/    
    
    
    UNIT_TEST(adi_adc_StartCalibration (hDevice_start_calib),ADI_ADC_SUCCESS)
      
      
 /******************************************************************************/
/*************adi_adc_SinkEnable  & ADI_ADC_INVALID_DEVICE_HANDLE***********/
/******************************************************************************/           
        
    UNIT_TEST(adi_adc_StartCalibration (hDevice_start_calib_1),ADI_ADC_INVALID_DEVICE_HANDLE)
    adi_adc_Close(hDevice_start_calib);   

#endif     

#ifdef DONE_CALI

 bool CalibrationStatus=0; 
   eResult = adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice_done_calib);

    //hDevice_done_calib
/* Power up ADC */
    eResult = adi_adc_PowerUp (hDevice_done_calib, true);
    DEBUG_RESULT("Failed to power up ADC", eResult, ADI_ADC_SUCCESS);

    /* Set ADC reference */
    eResult = adi_adc_SetVrefSource (hDevice_done_calib, ADI_ADC_VREF_SRC_INT_2_50_V);
    DEBUG_RESULT("Failed to set ADC reference", eResult, ADI_ADC_SUCCESS);

    /* Enable ADC sub system */
    eResult = adi_adc_EnableADCSubSystem (hDevice_done_calib, true);
    DEBUG_RESULT("Failed to enable ADC sub system", eResult, ADI_ADC_SUCCESS);

    /* Wait for 5.0ms */
    usleep (5000);
    
    /* Start calibration */
    eResult = adi_adc_StartCalibration (hDevice_done_calib);
    DEBUG_RESULT("Failed to start calibration", eResult, ADI_ADC_SUCCESS);

    
     
 /*****************************************************************************/
/*************adi_adc_StartCalibration & ADI_ADC_SUCCESS***********************/
/******************************************************************************/    
    
    
  UNIT_TEST(adi_adc_IsCalibrationDone (hDevice_done_calib, & CalibrationStatus),ADI_ADC_SUCCESS)
      
/*****************************************************************************/
/*************adi_adc_StartCalibration & ADI_ADC_INVALID_DEVICE_HANDLE *******/
/******************************************************************************/ 
   UNIT_TEST(adi_adc_IsCalibrationDone (hDevice_done_calib_1, & CalibrationStatus),ADI_ADC_INVALID_DEVICE_HANDLE)
     
/*****************************************************************************/
/*************adi_adc_StartCalibration & ADI_ADC_NULL_POINTER ****************/
/******************************************************************************/     
   UNIT_TEST(adi_adc_IsCalibrationDone (hDevice_done_calib, NULL), ADI_ADC_NULL_POINTER)
   adi_adc_Close(hDevice_done_calib);  
#endif       
   
#ifdef SET_ACQ_TIME
 
  eResult = adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice_acq_time);
   /* Set the delay time */
  eResult = adi_adc_SetDelayTime ( hDevice, 10);
  DEBUG_RESULT("Failed to set the Delay time ", eResult, ADI_ADC_SUCCESS);

  eResult = adi_adc_SetAcquisitionTime ( hDevice_acq_time, 10);
 
/*****************************************************************************/
/*************adi_adc_SetAcquisitionTime & ADI_ADC_SUCCESS***********************/
/******************************************************************************/      
    
  UNIT_TEST(adi_adc_SetAcquisitionTime ( hDevice_acq_time, 10), ADI_ADC_SUCCESS)

/*****************************************************************************/
/*************adi_adc_SetAcquisitionTime & ADI_ADC_INVALID_DEVICE_HANDLE*******/
/******************************************************************************/      
    
   UNIT_TEST(adi_adc_SetAcquisitionTime ( hDevice_acq_time_1, 10),ADI_ADC_INVALID_DEVICE_HANDLE)
     
 /*****************************************************************************/
/*************adi_adc_SetAcquisitionTime & ADI_ADC_INVALID_PARAMETER *******/
/******************************************************************************/      
    
   UNIT_TEST(adi_adc_SetAcquisitionTime ( hDevice_acq_time, -32),ADI_ADC_INVALID_PARAMETER)
   
 adi_adc_Close(hDevice_acq_time);   
#endif    

#ifdef SET_DLY   
    
 

 eResult = adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice_set_dly);
    
/******************************************************************************/
/***********adi_adc_SetDelayTime & ADI_ADC_SUCCESS*****************************/
/******************************************************************************/      
    
  UNIT_TEST(adi_adc_SetDelayTime (hDevice_set_dly, 10), ADI_ADC_SUCCESS)
/******************************************************************************/
/***********adi_adc_SetDelayTime & ADI_ADC_INVALID_DEVICE_HANDLE***************/
/******************************************************************************/      
   UNIT_TEST(adi_adc_SetDelayTime (hDevice_set_dly_1, 10),ADI_ADC_INVALID_DEVICE_HANDLE)
     
/******************************************************************************/
/***********adi_adc_SetDelayTime & ADI_ADC_INVALID_PARAMETER ***************/
/******************************************************************************/      
    UNIT_TEST(adi_adc_SetDelayTime (hDevice_set_dly, -9),ADI_ADC_INVALID_PARAMETER)
 adi_adc_Close(hDevice_set_dly);

#endif 

#ifdef SET_RESOL 
 
 eResult = adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice_set_res);
 
/******************************************************************************/
/***********adi_adc_SetDelayTime & ADI_ADC_SUCCESS*****************************/
/******************************************************************************/      
    
  UNIT_TEST(adi_adc_SetResolution (hDevice_set_res , ADI_ADC_RESOLUTION_13_BIT), ADI_ADC_SUCCESS)
     

/******************************************************************************/
/***********adi_adc_SetDelayTime & ADI_ADC_SUCCESS*****************************/
/******************************************************************************/      
    
   UNIT_TEST(adi_adc_SetResolution (hDevice_set_res_1 , ADI_ADC_RESOLUTION_13_BIT),ADI_ADC_INVALID_DEVICE_HANDLE )
      
  
/******************************************************************************/
/***********adi_adc_SetDelayTime & ADI_ADC_INVALID_PARAMETER ******************/
/******************************************************************************/      
    
    UNIT_TEST( adi_adc_SetResolution (hDevice_set_res, INVAILD_PARMETER),ADI_ADC_INVALID_PARAMETER)
adi_adc_Close(hDevice_set_res);
#endif    

#ifdef ENABLE_AVG
/* need to develop logic */
#endif     

#ifdef SET_LOW_LIMIT
 
eResult = adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice_set_low);
/******************************************************************************/
/***********adi_adc_SetLowLimit & ADI_ADC_SUCCESS*****************************/
/******************************************************************************/      
    
     UNIT_TEST( adi_adc_SetLowLimit (hDevice_set_low, CHANNEL_TO_USE, true, 1000),ADI_ADC_SUCCESS)
     
 
 /******************************************************************************/
/***********adi_adc_SetLowLimit & ADI_ADC_INVALID_DEVICE_HANDLE*****************/
/******************************************************************************/      
    
    UNIT_TEST( adi_adc_SetLowLimit (hDevice_set_low_1, CHANNEL_TO_USE, true, 1000),ADI_ADC_INVALID_DEVICE_HANDLE)
         
    
  /******************************************************************************/
/***********adi_adc_SetLowLimit & ADI_ADC_INVALID_PARAMETER *****************/
/******************************************************************************/      
    
 UNIT_TEST( adi_adc_SetLowLimit (hDevice_set_low, CHANNEL_TO_USE, true, 10000),ADI_ADC_INVALID_PARAMETER )
adi_adc_Close(hDevice_set_low);
#endif 
  
#ifdef SET_HIGH_LIMIT 
eResult = adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice_set_hig);
/******************************************************************************/
/***********adi_adc_SetHighLimit & ADI_ADC_SUCCESS*****************************/
/******************************************************************************/      
    
 UNIT_TEST( adi_adc_SetHighLimit (hDevice_set_hig, CHANNEL_TO_USE, true, 1000), ADI_ADC_SUCCESS)
 /******************************************************************************/
/***********adi_adc_SetHighLimit & ADI_ADC_INVALID_DEVICE_HANDLE*****************/
/******************************************************************************/      
    
 UNIT_TEST( adi_adc_SetHighLimit (hDevice_set_hig_1, CHANNEL_TO_USE, true, 1000),ADI_ADC_INVALID_DEVICE_HANDLE)
/******************************************************************************/
/***********adi_adc_SetHighLimit & ADI_ADC_INVALID_PARAMETER *****************/
/******************************************************************************/      
 UNIT_TEST( adi_adc_SetHighLimit (hDevice_set_hig, CHANNEL_TO_USE, true, 10000),ADI_ADC_INVALID_PARAMETER)  
adi_adc_Close(hDevice_set_hig);
#endif   

#ifdef SET_HYSTETE

eResult = adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice_set_hys);

/******************************************************************************/
/***********adi_adc_SetHysteresis & ADI_ADC_SUCCESS*****************************/
/******************************************************************************/      
    
  UNIT_TEST( adi_adc_SetHysteresis(hDevice_set_hys, CHANNEL_TO_USE, true, 0), ADI_ADC_SUCCESS)
      
 /******************************************************************************/
/***********adi_adc_SetHysteresis & ADI_ADC_INVALID_DEVICE_HANDLE*****************/
/******************************************************************************/      
    
  UNIT_TEST( adi_adc_SetHysteresis(hDevice_set_hys_1, CHANNEL_TO_USE, true, 0), ADI_ADC_INVALID_DEVICE_HANDLE)
/******************************************************************************/
/***********adi_adc_SetHysteresis & ADI_ADC_INVALID_PARAMETER *****************/
/******************************************************************************/      
    
 UNIT_TEST(adi_adc_SetHysteresis(hDevice_set_hys, CHANNEL_BAD, false, 0X0FFF), ADI_ADC_INVALID_PARAMETER)
      
adi_adc_Close(hDevice_set_hys);
#endif   

#ifdef NUM_MON_CYC

 eResult = adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice_num_cyc);
 
/******************************************************************************/
/***********SetNumMonitorCycles  & ADI_ADC_SUCCESS*****************************/
/******************************************************************************/      
    
  UNIT_TEST(adi_adc_SetNumMonitorCycles (hDevice_num_cyc, CHANNEL_TO_USE, 0),ADI_ADC_SUCCESS)
 
 /******************************************************************************/
/***********SetNumMonitorCycles & ADI_ADC_INVALID_DEVICE_HANDLE*****************/
/******************************************************************************/      
    
  UNIT_TEST(adi_adc_SetNumMonitorCycles (hDevice_num_cyc_1, CHANNEL_TO_USE, 0),ADI_ADC_INVALID_DEVICE_HANDLE)
      

/******************************************************************************/
/***********SetNumMonitorCycles  & ADI_ADC_INVALID_PARAMETER *****************/
/******************************************************************************/      
    
  UNIT_TEST( adi_adc_SetNumMonitorCycles (hDevice_num_cyc, CHANNEL_TO_USE, 10000), ADI_ADC_INVALID_PARAMETER )
     
adi_adc_Close(hDevice_num_cyc);

#endif

#ifdef ENABLE_DIG_COMP

 eResult = adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice_dig_comp);

   eResult = adi_adc_RegisterCallback (hDevice_dig_comp, ADCCallback, NULL);
   DEBUG_RESULT("Failed to register callback", eResult, ADI_ADC_SUCCESS);

    /* Configure hysteresis for an ADC channel when it's used as a digital comparator. */
    eResult = adi_adc_SetHysteresis(hDevice_dig_comp, CHANNEL_TO_USE, true, 0);
    DEBUG_RESULT("Failed to set adi_adc_SetHysteresis ", eResult, ADI_ADC_SUCCESS);

    /* Configure number of monitor cycles for an ADC channel when it's used as a digital comparator. */
   eResult = adi_adc_SetNumMonitorCycles(hDevice_dig_comp, CHANNEL_TO_USE, 0);
   DEBUG_RESULT("Failed to set adi_adc_SetNumMonitorCycles ", eResult, ADI_ADC_SUCCESS);

   
/******************************************************************************/
/***********adi_adc_EnableDigitalComparatos  & ADI_ADC_INVALID_STATE***********/
/******************************************************************************/    
    UNIT_TEST(adi_adc_EnableDigitalComparator (hDevice_dig_comp, true), ADI_ADC_INVALID_STATE)
  
     adi_adc_Close(hDevice_dig_comp) ; 
/******************************************************************************/
/***********adi_adc_EnableDigitalComparatos  & ADI_ADC_INVALID_PARAMETER ******/
/******************************************************************************/  

   eResult = adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice_dig_comp_1);    
        
    eResult = adi_adc_SetLowLimit (hDevice_dig_comp_1, CHANNEL_TO_USE, true, 1000);
    DEBUG_RESULT("Failed to set the acquisition time ", eResult, ADI_ADC_SUCCESS);	

    /* Configure hysteresis for an ADC channel when it's used as a digital comparator. */
    eResult = adi_adc_SetHysteresis(hDevice_dig_comp_1, CHANNEL_TO_USE, true, 0);
    DEBUG_RESULT("Failed to set adi_adc_SetHysteresis ", eResult, ADI_ADC_SUCCESS);

    /* Configure number of monitor cycles for an ADC channel when it's used as a digital comparator. */
   eResult = adi_adc_SetNumMonitorCycles(hDevice_dig_comp_1, CHANNEL_TO_USE, 0);
    DEBUG_RESULT("Failed to set adi_adc_SetNumMonitorCycles ", eResult, ADI_ADC_SUCCESS);
     UNIT_TEST(adi_adc_EnableDigitalComparator (hDevice_dig_comp_1, true),ADI_ADC_INVALID_OPERATION)
   

 /******************************************************************************/
/***********adi_adc_EnableDigitalComparatos  & ADI_ADC_INVALID_DEVICE_HANDLE*****/
/******************************************************************************/ 


  UNIT_TEST(adi_adc_EnableDigitalComparator (hDevice_dig_comp_2, true), ADI_ADC_INVALID_DEVICE_HANDLE)
      
 
#endif 

#ifdef SUBMIT_BUFF

 
 eResult = adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice_submit_buf);    


 /*********************************************************/ 
    ADI_ADC_BUFFER Buffer;
   
     
    /* Set the delay time */
    eResult = adi_adc_SetDelayTime ( hDevice_submit_buf, 10);
    DEBUG_RESULT("Failed to set the Delay time ", eResult, ADI_ADC_SUCCESS);

    /* Set the acquisition time. (Application need to change it based on the impedence) */
    eResult = adi_adc_SetAcquisitionTime ( hDevice_submit_buf, 10);
    DEBUG_RESULT("Failed to set the acquisition time ", eResult, ADI_ADC_SUCCESS);	

    /* Unregister callback */
    eResult = adi_adc_RegisterCallback (hDevice_submit_buf, NULL, NULL);
    DEBUG_RESULT("Failed to unregister callback ", eResult, ADI_ADC_SUCCESS);
    
   
/******************************************************************************/
/******************SubmitBuffer  & AADI_ADC_INVALID_BUFFER***************/
/******************************************************************************/ 
    Buffer.nBuffSize = sizeof(ADC_DataBuffer);
    Buffer.nChannels = 0u;
    Buffer.nNumConversionPasses = 1000/2;
    Buffer.pDataBuffer = ADC_DataBuffer;
  UNIT_TEST( adi_adc_SubmitBuffer (hDevice_submit_buf, &Buffer),ADI_ADC_INVALID_BUFFER)
 
/******************************************************************************/
/******************SubmitBuffer  & ADI_ADC_INVALID_DEVICE_HANDLE***************/
/******************************************************************************/      
   UNIT_TEST(adi_adc_SubmitBuffer (hDevice_submit_buf_1, &Buffer), ADI_ADC_INVALID_DEVICE_HANDLE)

/******************************************************************************/
/******************SubmitBuffer  & ADI_ADC_SUCCESS*****************************/
/******************************************************************************/     
 /* Populate the buffer structure */
    Buffer.nBuffSize = sizeof(ADC_DataBuffer);
    Buffer.nChannels = CHANNEL_TO_USE;
    Buffer.nNumConversionPasses = ADC_NUM_SAMPLES/2;
    Buffer.pDataBuffer = ADC_DataBuffer;       
UNIT_TEST(adi_adc_SubmitBuffer (hDevice_submit_buf, &Buffer), ADI_ADC_SUCCESS)
    
 
  /******************************************************************************/
/******************SubmitBuffer  & NULLPOINTER***********************************/
/******************************************************************************/  
    
  adi_adc_Close(hDevice_submit_buf); 
#endif  

#ifdef GET_BUFF  
  


for (int i = 0 ; i < 2 ; i++)
{
eResult = adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice);
DEBUG_RESULT("Failed to open ADC device",eResult, ADI_ADC_SUCCESS);

    /* Register Callback */
    eResult = adi_adc_RegisterCallback (hDevice, ADCCallback, NULL);
    DEBUG_RESULT("Failed to register callback", eResult, ADI_ADC_SUCCESS);

    /* Go through the initial setup of the ADC */
    SetUpADC(hDevice);
    
#if ADC_WAIT_FOR_LIMIT  == 1
    WaitForLimit(hDevice);
#endif
    
    /* A test to see if the chip is at, or around, ambient office temperature */
#if ADC_GET_TEMP == 1
    GetChipTemperature(hDevice);
#endif
    
    /* The battery voltage should be fairly constant. */
#if ADC_GET_BATTERY == 1 
    GetBatteryVoltage(hDevice);
#endif
    
     ADI_ADC_BUFFER Buffer;
   // ADI_ADC_RESULT  eResult = ADI_ADC_SUCCESS;
        
    /* Set the delay time */
    eResult = adi_adc_SetDelayTime ( hDevice, 10);
    DEBUG_RESULT("Failed to set the Delay time ", eResult, ADI_ADC_SUCCESS);

    /* Set the acquisition time. (Application need to change it based on the impedence) */
    eResult = adi_adc_SetAcquisitionTime ( hDevice, 10);
    DEBUG_RESULT("Failed to set the acquisition time ", eResult, ADI_ADC_SUCCESS);	

    /* Unregister callback */
    eResult = adi_adc_RegisterCallback (hDevice, NULL, NULL);
    DEBUG_RESULT("Failed to unregister callback ", eResult, ADI_ADC_SUCCESS);
    
    /* Populate the buffer structure */
    Buffer.nBuffSize = sizeof(ADC_DataBuffer);
    Buffer.nChannels = CHANNEL_TO_USE;
    Buffer.nNumConversionPasses = ADC_NUM_SAMPLES/2;
    Buffer.pDataBuffer = ADC_DataBuffer;
    
    /* Submit the buffer to the driver */
    eResult = adi_adc_SubmitBuffer (hDevice, &Buffer);
    DEBUG_RESULT("Failed to submit buffer ", eResult, ADI_ADC_SUCCESS);
        
    /* Enable the ADC */
   if(i != 0)
   {
    eResult = adi_adc_Enable (hDevice, true);
    DEBUG_RESULT("Failed to enable the ADC for sampling ", eResult, ADI_ADC_SUCCESS);
   }
    
  //if( i==1)
   //  {
    // ADI_ADC_BUFFER** pAdcBuffer = 0;
     //eResult = adi_adc_GetBuffer (hDevice, pAdcBuffer);
     
   //}
   //else
   //{
     ADI_ADC_BUFFER* pAdcBuffer ;
     eResult = adi_adc_GetBuffer (hDevice, &pAdcBuffer);
   //}
  

    adi_adc_Close (hDevice);
   
}   
#endif     

#ifdef ENABLE    

 
 eResult = adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice_enable);  

 
 /******************************************************************************/
/***********adi_adc_Enable  & ADI_ADC_SUCCESS*********************************/
/******************************************************************************/      
    
//UNIT_TEST( adi_adc_Enable (hDevice_enable, false),ADI_ADC_SUCCESS)
      
   
/******************************************************************************/
/***********adi_adc_Enable  & ADI_ADC_INVALID_DEVICE_HANDLE *********************/
/******************************************************************************/      
//UNIT_TEST(adi_adc_Enable (hDevice_enable_1, false),ADI_ADC_INVALID_DEVICE_HANDLE) 
      
  
adi_adc_Close(hDevice_enable);

#endif
 
#ifdef READ_CHANNEL

/* need to develop logic */
#endif 

#ifdef TEMP_CHA  
  /* need to develop logic */

#if ADC_WAIT_FOR_LIMIT  == 1
    WaitForLimit(hDevice);
#endif
    
   // ADI_ADC_RESULT  eResult = ADI_ADC_SUCCESS;
    int32_t  temp_read = 0;
    int32_t  temp_known_high = 30; /* degrees c */
    int32_t  temp_known_low  = 15; /* degrees c */
    
 /******************************************************************************/
/***********adi_adc_EnableTemperatureSensonr & ADI_ADC_SUCCESS*********************************/
/******************************************************************************/  
   UNIT_TEST( adi_adc_EnableTemperatureSensor(hDevice, true),ADI_ADC_SUCCESS)
    DEBUG_RESULT("Failed to get temperature ", eResult, ADI_ADC_SUCCESS);	
    
   
    adi_adc_Close (hDevice); 
    
 /******************************************************************************/
/***********adi_adc_EnableTemperatureSensor & ADI_ADC_INVALID_DEVICE_HANDLE ***/
/******************************************************************************/    
   UNIT_TEST( adi_adc_EnableTemperatureSensor(hDevice_1, false),ADI_ADC_INVALID_DEVICE_HANDLE)
 
   
#endif   
    
    
    
    
    
    
    
    
#if 0
    eResult = adi_adc_Open(ADC_DEV_NUM, DeviceMemory, sizeof(DeviceMemory), &hDevice);
    DEBUG_RESULT("Failed to open ADC device",eResult, ADI_ADC_SUCCESS);

    /* Register Callback */
    eResult = adi_adc_RegisterCallback (hDevice, ADCCallback, NULL);
    DEBUG_RESULT("Failed to register callback", eResult, ADI_ADC_SUCCESS);

    /* Go through the initial setup of the ADC */
    SetUpADC(hDevice);
    
#if ADC_WAIT_FOR_LIMIT  == 1
    WaitForLimit(hDevice);
#endif
    
    /* A test to see if the chip is at, or around, ambient office temperature */
#if ADC_GET_TEMP == 1
    GetChipTemperature(hDevice);
#endif
    
    /* The battery voltage should be fairly constant. */
#if ADC_GET_BATTERY == 1 
    GetBatteryVoltage(hDevice);
#endif
    
    /* Sample the ADC */
    SampleData(hDevice);
  
    ReadChannels(hDevice);
    
    /* Disable the ADC */
    eResult = adi_adc_Enable (hDevice, false);
    DEBUG_RESULT("Failed to disable ADC for sampling ", eResult, ADI_ADC_SUCCESS);

    /* Close the ADC */
    eResult = adi_adc_Close (hDevice);
    DEBUG_RESULT("Failed to close ADC", eResult, ADI_ADC_SUCCESS);
#endif     
    /* Test Passed */

    return ADI_UNIT_SUCCESS;  
}

/* Approximately wait for minimum 1 usec */
static void usleep(uint32_t usec)
{
    volatile int y = 0;
    while (y++ < usec) {
        volatile int x = 0;
        while (x < 16) {
        x++;
        }
    }
}


