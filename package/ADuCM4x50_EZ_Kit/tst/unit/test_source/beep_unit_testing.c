#include <drivers/gpio/adi_gpio.h>
#include <common.h>
#include <stdio.h>
/*********************************************************UNIT TEST***********************************************************/ 


#ifndef UNIT_TEST_H
#define UNIT_TEST_H

typedef enum
{
  
  ADI_UNIT_SUCCESS = 0u,
  ADI_UNIT_FAILURE = 1u
    
} ADI_UNIT_RESULT;


#define UNIT_TEST(func, dbg) \
do \
{ \
    if ((func) != (dbg)) \
    { \
        DEBUG_MESSAGE("Unit test fail at line %d of %s\r\n", __LINE__, __FILE__); \
        return (ADI_UNIT_FAILURE); \
    } \
} while(0u);

#endif /* UNIT_TEST_H */

/************************************************************ UNIT_TEST_H *****************************************************/


/******************************************************************************
 * @file:    BeepTest.c
 * @brief:   Beeper Driver Test forADuC302X
 *-----------------------------------------------------------------------------
 *
 Copyright (c) 2016 Analog Devices, Inc.
/**************************/
#include <stdio.h>
#include <drivers/pwr/adi_pwr.h>
#include <drivers/beep/adi_beep.h>
#include "common.h"

/* prototypes */
extern int32_t   adi_initpinmux (void);
ADI_BEEP_RESULT  beep_Init      (void);
ADI_BEEP_RESULT  beep_Start     (void);
void             beepCallback(void *pCBUnused, uint32_t Event, void *pvUnused);

/* Device handle */
ADI_BEEP_HANDLE hBeep= NULL,hBeep1= NULL,hBeep2= NULL,hBeep3= NULL,hBeep4= NULL;

/* required beeper driver memory */
uint8_t BeepMemory[ADI_BEEP_MEMORY_SIZE];


/* Various playlist lengths */
#define PLAY_TUNE1 1
#define PLAY_TUNE2 1
#define PLAY_TUNE3 1
#define PLAY_TUNE4 1
#define PLAY_TUNE5 1

/* Basic functionality */
#define PLAY_NOTE     1
//#define PLAY_TWONOTES 1
#define BEEP_OPEN
#define BEEP_REGISTER_CALLBACK
#define BEEP_PLAYSEQUENCE
#define BEEP_PLAYNOTE
#define BEEP_CLOSE

ADI_BEEP_RESULT PlayOne(void);
ADI_BEEP_RESULT PlayTwo(void);
ADI_BEEP_RESULT TuneTest(ADI_BEEP_NOTE tune[], uint8_t size);
ADI_BEEP_RESULT CheckStatic(void);


/* the playlist */
#if PLAY_TUNE1 == 1
ADI_BEEP_NOTE playList1[] = {
    /* Close Encounters: D-E-C-C(octave lower)-G */
    { ADI_BEEP_FREQ_D6,        ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_E6,        ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_C6,        ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_C5,        ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_G5,        ADI_BEEP_DUR_32_32 },
    { ADI_BEEP_FREQ_REST,      ADI_BEEP_DUR_1_32 },
};
uint8_t playLength1 = sizeof(playList1)/sizeof(playList1[0]);
#endif

#if PLAY_TUNE2 == 1
#define DURATION ADI_BEEP_DUR_1_32
ADI_BEEP_NOTE playList2[] = {
    /* Scales - step up two, then one down. */
    {ADI_BEEP_FREQ_C4  , DURATION},
    {ADI_BEEP_FREQ_D4  , DURATION},
    {ADI_BEEP_FREQ_Cs4 , DURATION},
    {ADI_BEEP_FREQ_Ds4 , DURATION},
    {ADI_BEEP_FREQ_F4  , DURATION},
    {ADI_BEEP_FREQ_E4  , DURATION},
    {ADI_BEEP_FREQ_Fs4 , DURATION},
    {ADI_BEEP_FREQ_Gs4 , DURATION},
    {ADI_BEEP_FREQ_G4  , DURATION},
    {ADI_BEEP_FREQ_A4  , DURATION},
    {ADI_BEEP_FREQ_B4  , DURATION},
    {ADI_BEEP_FREQ_As4 , DURATION},
    {ADI_BEEP_FREQ_C5  , DURATION},
    {ADI_BEEP_FREQ_D5  , DURATION},
    {ADI_BEEP_FREQ_Cs5 , DURATION},
    {ADI_BEEP_FREQ_Ds5 , DURATION},
    {ADI_BEEP_FREQ_F5  , DURATION},
    {ADI_BEEP_FREQ_E5  , DURATION},
    {ADI_BEEP_FREQ_Fs5 , DURATION},
    {ADI_BEEP_FREQ_Gs5 , DURATION},
    {ADI_BEEP_FREQ_G5  , DURATION},
    {ADI_BEEP_FREQ_A5  , DURATION},
    {ADI_BEEP_FREQ_B5  , DURATION},
    {ADI_BEEP_FREQ_As5 , DURATION},
    {ADI_BEEP_FREQ_C6  , DURATION},
    {ADI_BEEP_FREQ_D6  , DURATION},
    {ADI_BEEP_FREQ_Cs6 , DURATION},
    {ADI_BEEP_FREQ_Ds6 , DURATION},
    {ADI_BEEP_FREQ_F6  , DURATION},
    {ADI_BEEP_FREQ_E6  , DURATION},
    {ADI_BEEP_FREQ_Fs6 , DURATION},
    {ADI_BEEP_FREQ_Gs6 , DURATION},
    {ADI_BEEP_FREQ_G6  , DURATION},
    {ADI_BEEP_FREQ_A6  , DURATION},
    {ADI_BEEP_FREQ_B6  , DURATION},
    {ADI_BEEP_FREQ_As6 , DURATION},
    {ADI_BEEP_FREQ_C7  , DURATION},
    {ADI_BEEP_FREQ_D7  , DURATION},
    {ADI_BEEP_FREQ_Cs7 , DURATION},
    {ADI_BEEP_FREQ_Ds7 , DURATION},
    {ADI_BEEP_FREQ_F7  , DURATION},
    {ADI_BEEP_FREQ_E7  , DURATION},
    {ADI_BEEP_FREQ_Fs7 , DURATION},
    {ADI_BEEP_FREQ_Gs7 , DURATION},
    {ADI_BEEP_FREQ_G7  , DURATION},
    {ADI_BEEP_FREQ_A7  , DURATION},
    {ADI_BEEP_FREQ_B7  , DURATION},
    {ADI_BEEP_FREQ_As7 , DURATION},
    {ADI_BEEP_FREQ_C8  , DURATION},
    {ADI_BEEP_FREQ_D8  , DURATION},
    {ADI_BEEP_FREQ_Cs8 , DURATION},
    {ADI_BEEP_FREQ_Ds8 , DURATION},
    {ADI_BEEP_FREQ_F8  , DURATION},
    {ADI_BEEP_FREQ_E8  , DURATION},
    {ADI_BEEP_FREQ_Fs8 , DURATION},
    {ADI_BEEP_FREQ_G8  , DURATION},
};

uint8_t playLength2 = sizeof(playList2)/sizeof(playList2[0]);

#endif 

#if PLAY_TUNE3 == 1
ADI_BEEP_NOTE playList3[] = {
    /* Short fanfare */
    { ADI_BEEP_FREQ_REST,      ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_REST,      ADI_BEEP_DUR_16_32 },
};
uint8_t playLength3 = sizeof(playList3)/sizeof(playList3[0]);
#endif

#if PLAY_TUNE4 == 1
ADI_BEEP_NOTE playList4[] = {
    /* Fanfare */
    { ADI_BEEP_FREQ_C6,        ADI_BEEP_DUR_4_32 },
    { ADI_BEEP_FREQ_E6,        ADI_BEEP_DUR_4_32 },
    { ADI_BEEP_FREQ_G6,        ADI_BEEP_DUR_4_32 },
    { ADI_BEEP_FREQ_REST,      ADI_BEEP_DUR_2_32 },
    { ADI_BEEP_FREQ_E6,        ADI_BEEP_DUR_4_32 },
    { ADI_BEEP_FREQ_G6,        ADI_BEEP_DUR_16_32 },
};
uint8_t playLength4 = sizeof(playList4)/sizeof(playList4[0]);
#endif 

#if PLAY_TUNE5 == 1
ADI_BEEP_NOTE playList5[] = {
    /* Twinkle */
    { ADI_BEEP_FREQ_REST,      ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_E6,        ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_E6,        ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_B6,        ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_B6,        ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_Cs7,       ADI_BEEP_DUR_16_32 },
    { ADI_BEEP_FREQ_Cs7,       ADI_BEEP_DUR_16_32 },    
    { ADI_BEEP_FREQ_B6,        ADI_BEEP_DUR_32_32 },
    { ADI_BEEP_FREQ_REST,      ADI_BEEP_DUR_8_32 },
    { ADI_BEEP_FREQ_REST,      ADI_BEEP_DUR_8_32 },
};
uint8_t playLength5 = sizeof(playList5)/sizeof(playList5[0]);
#endif

/* Boolean flag indicating end of sequence */
volatile bool bDoneFlag     = false;
volatile bool bNoteDone     = false;
volatile bool bSequenceDone = false;

ADI_UNIT_RESULT beep_unit_testing(void)
{ 
  
ADI_BEEP_RESULT eResult;
ADI_CALLBACK          pfCallback=0;
ADI_BEEP_HANDLE const hDevice;
ADI_BEEP_NOTE noise = {ADI_BEEP_FREQ_Gs6, ADI_BEEP_DUR_2_32};
    /* enable the beeper pins */
    //adi_initpinmux();
    /* test system initialization. This functions will also add the 
       pinmuxing  required for the UART device. */
     
   common_Init();  
   adi_pwr_Init();
   adi_pwr_SetClockDivider(ADI_CLOCK_HCLK,1);
   adi_pwr_SetClockDivider(ADI_CLOCK_PCLK,1);
   adi_pwr_SetLFClockMux(ADI_CLOCK_MUX_LFCLK_LFOSC);
   
   /***************BEEP_OPEN*******************************/
   
   /* adi_beep_open pass */
 
   UNIT_TEST(adi_beep_Open( ADI_BEEP_DEVID_0, NULL,sizeof(BeepMemory),&hBeep),ADI_BEEP_NULL_PTR)
   UNIT_TEST(adi_beep_Open( ADI_BEEP_MAX_DEVID, &BeepMemory,sizeof(BeepMemory),&hBeep),ADI_BEEP_BAD_DEV_ID)
   UNIT_TEST(adi_beep_Open(ADI_BEEP_DEVID_0, &BeepMemory,sizeof(BeepMemory),&hBeep),ADI_BEEP_SUCCESS) 
   adi_beep_Close(hBeep);  

   
   /**********BEEP_REGISTER_CALLBACK****************/
     
/*********************ADI_BEEP_SUCCESS*******************************/
 adi_beep_Open(ADI_BEEP_DEVID_0, &BeepMemory,sizeof(BeepMemory),&hBeep);
 UNIT_TEST(adi_beep_RegisterCallback(hBeep, pfCallback,NULL),ADI_BEEP_SUCCESS);                                                
 adi_beep_Close(hBeep);
 
  /*********************ADI_BEEP_NOT_INITIALIZED*******************************/
              /*  Need to develope logic */
   /***************************************************************************/
 
  /*****************ADI_BEEP_BAD_DEV_HANDLE*******************************/
 
  adi_beep_Open( ADI_BEEP_MAX_DEVID, &BeepMemory,sizeof(BeepMemory),&hBeep);
   UNIT_TEST(adi_beep_RegisterCallback(hBeep,pfCallback,NULL),ADI_BEEP_BAD_DEV_HANDLE);                                                                                                                        

  
/******************BEEP_PLAYSEQUENCE*********************/
 
  /***************** ADI_BEEP_SUCCESS*******************************/
 adi_beep_Open(ADI_BEEP_DEVID_0,&BeepMemory,sizeof(BeepMemory),&hBeep);
 UNIT_TEST(adi_beep_PlaySequence(hBeep,playList2, playLength2),ADI_BEEP_SUCCESS)                                                                                                
 adi_beep_Close(hBeep);
  
 /*****************ADI_BEEP_INVALID_COUNT *******************************/
 
adi_beep_Open(ADI_BEEP_DEVID_0, &BeepMemory,sizeof(BeepMemory),&hBeep);
UNIT_TEST(adi_beep_PlaySequence(hBeep,playList2,13),ADI_BEEP_INVALID_COUNT)
adi_beep_Close(hBeep); 
 
 /*****************ADI_BEEP_NULL_PTR *******************************/
  adi_beep_Open(ADI_BEEP_DEVID_0,&BeepMemory,sizeof(BeepMemory),&hBeep);
  UNIT_TEST(adi_beep_PlaySequence(hBeep,NULL,playLength2),ADI_BEEP_NULL_PTR) 
  adi_beep_Close(hBeep);   
    
 /*********************ADI_BEEP_BAD_DEV_HANDLE*******************************/
 
 adi_beep_Open( ADI_BEEP_MAX_DEVID, &BeepMemory,sizeof(BeepMemory),&hBeep);
 UNIT_TEST(adi_beep_PlaySequence(hBeep,playList2, playLength2),ADI_BEEP_BAD_DEV_HANDLE)
 adi_beep_Close(hBeep); 
 
/************* BEEP_PLAYNOTE ******************/  
 
 /***************************ADI_BEEP_SUCCESS*******************************/
 adi_beep_Open(ADI_BEEP_DEVID_0, &BeepMemory,sizeof(BeepMemory),&hBeep);
 UNIT_TEST(adi_beep_PlayNote(hBeep, noise),ADI_BEEP_SUCCESS)
 adi_beep_Close(hBeep);  
 
 /*********************ADI_BEEP_BAD_DEV_HANDLE*******************************/
 
   adi_beep_Open( ADI_BEEP_MAX_DEVID, &BeepMemory,sizeof(BeepMemory),&hBeep);
   UNIT_TEST(adi_beep_PlayNote(hBeep, noise),ADI_BEEP_BAD_DEV_HANDLE)
  
  /*********************ADI_BEEP_NOT_INITIALIZED*******************************/

        /* Need To Develop Logic */

  return ADI_UNIT_SUCCESS;  
}
