/*!****************************************************************************
 * @file    unit_testing.c
 * @brief   Top-level unit testing file
 * @details Runs the unit tests for each driver in series
******************************************************************************/

#include <stdbool.h>
#include <common.h>
#include <unit_testing.h>


extern ADI_UNIT_RESULT gpio_unit_testing(void);

#ifndef ADI_DEBUG
#error "Release mode testing not supported. All APIs return success in release mode."
#endif


int main(void)
{
    ADI_UNIT_RESULT eResult = ADI_UNIT_SUCCESS;
    bool            bFailed = false;
    
  
   
   
   eResult = gpio_unit_testing();
   if (eResult == ADI_UNIT_FAILURE) 
   {
       common_Perf("GPIO Unit Test Failed.\r\n");
       bFailed = true;
   }

  
  
    
    if (bFailed == false) {
        common_Pass();
        return 0;
    } else {
        common_Fail("Unit testing failed.\r\n");
        return 1;
    }
}
