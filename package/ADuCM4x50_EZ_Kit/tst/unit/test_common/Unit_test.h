#ifndef UNIT_TEST_H
#define UNIT_TEST_H

#include <common.h>

typedef enum
{
  
  ADI_UNIT_SUCCESS = 0u,
  ADI_UNIT_FAILURE = 1u
    
} ADI_UNIT_RESULT;


#define UNIT_TEST(func, dbg) \
do \
{ \
    if ((func) != (dbg)) \
    { \
        DEBUG_MESSAGE("Unit test fail at line %d of %s\r\n", __LINE__, __FILE__); \
        return (ADI_UNIT_FAILURE); \
    } \
} while(0u);

#endif /* UNIT_TEST_H */#ifndef UNIT_TEST_H
#define UNIT_TEST_H

#include <common.h>

typedef enum
{
  
  ADI_UNIT_SUCCESS = 0u,
  ADI_UNIT_FAILURE = 1u
    
} ADI_UNIT_RESULT;


#define UNIT_TEST(func, dbg) \
do \
{ \
    if ((func) != (dbg)) \
    { \
      printf("PASS");\
        DEBUG_MESSAGE("Unit test fail at line %d of %s\r\n", __LINE__, __FILE__); \
        return (ADI_UNIT_FAILURE); \
    } \
} while(0u);

#endif /* UNIT_TEST_H */