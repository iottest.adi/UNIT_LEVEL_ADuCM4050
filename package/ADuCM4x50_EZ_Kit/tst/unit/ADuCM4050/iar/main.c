/* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Test for muska automation<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*/

#include <stdbool.h>
#include <common.h>
#include <stdio.h>

/*********************************************************UNIT TEST***********************************************************/ 

#ifndef UNIT_TEST_H
#define UNIT_TEST_H

typedef enum
{
  
  ADI_UNIT_SUCCESS = 0u,
  ADI_UNIT_FAILURE = 1u
    
} ADI_UNIT_RESULT;


#define UNIT_TEST(func, dbg) \
do \
{ \
    if ((func) != (dbg)) \
    { \
        DEBUG_MESSAGE("Unit test fail at line %d of %s\r\n", __LINE__, __FILE__); \
        return (ADI_UNIT_FAILURE); \
    } \
} while(0u);

#endif /* UNIT_TEST_H */

/************************************************************ UNIT_TEST_H *****************************************************/

extern ADI_UNIT_RESULT gpio_unit_testing(void);
extern ADI_UNIT_RESULT uart_unit_testing(void);
extern ADI_UNIT_RESULT i2c_unit_testing(void);
extern ADI_UNIT_RESULT pwr_unit_testing(void);
extern ADI_UNIT_RESULT tmr_unit_testing(void);
extern ADI_UNIT_RESULT spi_unit_testing(void);
extern ADI_UNIT_RESULT wdt_unit_testing(void);
extern ADI_UNIT_RESULT xint_unit_testing(void);
extern ADI_UNIT_RESULT beep_unit_testing(void);
extern ADI_UNIT_RESULT sport_unit_testing(void);
extern ADI_UNIT_RESULT crc_unit_testing(void);
extern ADI_UNIT_RESULT rng_unit_testing(void);
extern ADI_UNIT_RESULT adc_unit_testing(void);
int main(void)

{
  
 
  ADI_UNIT_RESULT eResult = ADI_UNIT_SUCCESS;
  bool  bFailed = false;
    
  
 printf("Unit Testing for GPIO start \n");
   eResult = gpio_unit_testing();
   if (eResult == ADI_UNIT_FAILURE) 
   {
       common_Perf("GPIO Unit Test Failed.\r\n");
       bFailed = true;
   }
   
    printf("Unit Testing for GPIO End  \n");
   
    printf("\n");
    printf("Unit Testing for UART start\n");
    eResult = uart_unit_testing();
    if (eResult == ADI_UNIT_FAILURE) 
    {
        common_Perf("UART Unit Test Failed.\r\n");
        bFailed = true;
    } 
    printf("Unit Testing for UART End\n");
    printf("\n");
    
    printf("Unit Testing for I2C start\n");
    eResult = i2c_unit_testing();
    if (eResult == ADI_UNIT_FAILURE) 
    {
        common_Perf("I2C Unit Test Failed.\r\n");
        bFailed = true;
    } 
    printf("Unit Testing for I2C End\n");
  
    printf("\n");
    
    printf("Unit Testing for PWR start\n");
    eResult = pwr_unit_testing();
    if (eResult == ADI_UNIT_FAILURE) 
    {
        common_Perf("PWR Unit Test Failed.\r\n");
        bFailed = true;
    } 
    printf("Unit Testing for PWR End\n");
    
    printf("\n");
    printf("Unit Testing for TIMER start\n");
    eResult = tmr_unit_testing();
    if (eResult == ADI_UNIT_FAILURE) 
    {
        common_Perf("TIMER Unit Test Failed.\r\n");
        bFailed = true;
    } 
    printf("Unit Testing for TIMER End\n");
    
    
    printf("\n");
    printf("Unit Testing for SPI start\n");
    eResult = spi_unit_testing();
    if (eResult == ADI_UNIT_FAILURE) 
    {
        common_Perf("SPI Unit Test Failed.\r\n");
        bFailed = true;
    } 
    printf("Unit Testing for SPI End\n");
  
   printf("\n");
   printf("Unit Testing for WDT start\n");
   eResult = wdt_unit_testing();
    if (eResult == ADI_UNIT_FAILURE) 
    {
        common_Perf("WDT Unit Test Failed.\r\n");
        bFailed = true;
    } 
    printf("Unit Testing for WDT End\n");
    
    
   printf("\n");
   printf("Unit Testing for XINT start\n");
   eResult = xint_unit_testing();
    if (eResult == ADI_UNIT_FAILURE) 
    {
        common_Perf("XINT Unit Test Failed.\r\n");
        bFailed = true;
    } 
    printf("Unit Testing for XINT End\n");
  
    
   printf("\n");
   printf("Unit Testing for BEEP start\n");
   eResult = beep_unit_testing();
    if (eResult == ADI_UNIT_FAILURE) 
    {
        common_Perf("BEEP Unit Test Failed.\r\n");
        bFailed = true;
    } 
    printf("Unit Testing for BEEP End\n");
    
   printf("\n");
   printf("Unit Testing for SPORT start\n");
   eResult = sport_unit_testing();
    if (eResult == ADI_UNIT_FAILURE) 
    {
        common_Perf("SPORT Unit Test Failed.\r\n");
        bFailed = true;
    } 
    printf("Unit Testing for SPORT End\n");
    
    
   printf("\n");
   printf("Unit Testing for CRC start\n");
   eResult = crc_unit_testing();
    if (eResult == ADI_UNIT_FAILURE) 
    {
        common_Perf("CRC Unit Test Failed.\r\n");
        bFailed = true;
    } 
    printf("Unit Testing for CRC End\n");

   printf("\n");
   printf("Unit Testing for RNG start\n");
   eResult = rng_unit_testing();
    if (eResult == ADI_UNIT_FAILURE) 
    {
        common_Perf("RNG Unit Test Failed.\r\n");
        bFailed = true;
    } 
    printf("Unit Testing for RNG End\n");
    
    printf("\n");
   printf("Unit Testing for ADC start\n");
   eResult = adc_unit_testing();
    if (eResult == ADI_UNIT_FAILURE) 
    {
        common_Perf("ADC Unit Test Failed.\r\n");
        bFailed = true;
    } 
    printf("Unit Testing for ADC End\n");
    return 0;
}
