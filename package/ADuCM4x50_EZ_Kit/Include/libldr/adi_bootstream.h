/*
 * adi_bootstream.h
 *
 * (c) Copyright 2015 Analog Devices, Inc. All Rights Reserved.
 * This software is proprietary to Analog Devices, Inc. and its
 * licensors.
 */

#pragma once
#if defined(__ECC__)
#pragma system_header  /* adi_bootstream.h */
#endif /* defined(__ECC__) */

#ifndef __ADI_BOOTSTREAM_H__
#define __ADI_BOOTSTREAM_H__

#include <stdint.h>

#if defined(_MISRA_2004_RULES) || defined(_MISRA_2012_RULES)
#pragma diag(push)
#ifdef _MISRA_2004_RULES
#pragma diag(suppress:misra_rule_19_4:"We use macros for structure members to allow for the different names on different platforms.")
#endif /* _MISRA_2004_RULES */
#endif /* MISRA RULES */

#if    defined(__ADSPBF54x__) \
    || defined(__ADSPBF52x__) \
    || defined(__ADSPBF51x__) \
    || defined(__ADSPBF50x__) \
    || defined(__ADSPBF59x__) \
    || defined(__ADSPBF60x__) \
    || defined(__ADSPBF70x__)

#include <bfrom.h>

#if    defined(__ADSPBF60x__)


typedef STRUCT_ROM_BOOT_HEADER HDRTYPE;
#define BFLG_FINAL       BITM_ROM_HDR_FINAL
#define BFLG_INDIRECT    BITM_ROM_HDR_INDIRECT
#define BFLG_IGNORE      BITM_ROM_HDR_IGNORE
#define BFLG_INIT        BITM_ROM_HDR_INIT
#define BFLG_CALLBACK    BITM_ROM_HDR_CALLBACK
#define BFLG_QUICKBOOT   BITM_ROM_HDR_QUICKBOOT
#define BFLG_FILL        BITM_ROM_HDR_FILL
#define BFLG_AUX         BITM_ROM_HDR_AUX
#define BFLG_SAVE        BITM_ROM_HDR_SAVE

#elif defined(__ADSPBF70x__)

typedef ADI_ROM_BOOT_HEADER HDRTYPE;
#define BFLG_FINAL       BITM_ROM_BFLAG_FINAL
#define BFLG_INDIRECT    BITM_ROM_BFLAG_INDIRECT
#define BFLG_IGNORE      BITM_ROM_BFLAG_IGNORE
#define BFLG_INIT        BITM_ROM_BFLAG_INIT
#define BFLG_CALLBACK    BITM_ROM_BFLAG_CALLBACK
#define BFLG_QUICKBOOT   BITM_ROM_BFLAG_QUICKBOOT
#define BFLG_FILL        BITM_ROM_BFLAG_FILL
#define BFLG_AUX         BITM_ROM_BFLAG_AUX
#define BFLG_SAVE        BITM_ROM_BFLAG_SAVE

#else

typedef ADI_BOOT_HEADER HDRTYPE;
#define BFLG_FINAL       ((uint32_t)BFLAG_FINAL)
#define BFLG_INDIRECT    ((uint32_t)BFLAG_INDIRECT)
#define BFLG_IGNORE      ((uint32_t)BFLAG_IGNORE)
#define BFLG_INIT        ((uint32_t)BFLAG_INIT)
#define BFLG_CALLBACK    ((uint32_t)BFLAG_CALLBACK)
#define BFLG_QUICKBOOT   ((uint32_t)BFLAG_QUICKBOOT)
#define BFLG_FILL        ((uint32_t)BFLAG_FILL)
#define BFLG_AUX         ((uint32_t)BFLAG_AUX)
#define BFLG_SAVE        ((uint32_t)BFLAG_SAVE)

#endif

#else

#ifdef __BA_SHARC__
#pragma default_addressed(push)
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Local declaration of bootstream structure and flags.
** This is available in ROM headers for some processors,
** but not for all, so we need a declaration here.
*/

typedef struct _adi_boot_header {
  uint32_t dBlockCode;
  uint32_t pTargetAddress;
  uint32_t dByteCount;
  uint32_t dArgument;
} adi_boot_header;

typedef adi_boot_header HDRTYPE;

#define BFLG_FINAL       0x00008000uL
#define BFLG_INDIRECT    0x00002000uL
#define BFLG_IGNORE      0x00001000uL
#define BFLG_INIT        0x00000800uL
#define BFLG_CALLBACK    0x00000400uL
#define BFLG_QUICKBOOT   0x00000200uL
#define BFLG_FILL        0x00000100uL
#define BFLG_AUX         0x00000020uL
#define BFLG_SAVE        0x00000010uL

#ifdef __cplusplus
}
#endif

#ifdef __BA_SHARC__
#pragma default_addressed(pop)
#endif

#endif /* include bfrom.h if we can */

#define BLKKIND dBlockCode
#define BLKADDR pTargetAddress
#define BLKSIZE dByteCount
#define BLKARGS dArgument

#if defined(_MISRA_2004_RULES) || defined(_MISRA_2012_RULES)
#pragma diag(pop)
#endif /* MISRA RULES */

#endif /* __ADI_BOOTSTREAM_H__ */
