/*
 * adi_libldr.h
 *
 * (c) Copyright 2015-2016 Analog Devices, Inc. All Rights Reserved.
 * This software is proprietary to Analog Devices, Inc. and its
 * licensors.
 */

#pragma once
#if defined(__ECC__)
#pragma system_header  /* adi_libldr.h */
#endif /* defined(__ECC__) */

#ifndef __ADI_LIBLDR_H__
#define __ADI_LIBLDR_H__

#if defined(_MISRA_2004_RULES) || defined(_MISRA_2012_RULES)
#pragma diag(push)
#ifdef _MISRA_2012_RULES
#pragma diag(suppress:misra2012_rule_21_6:"I/O is used to read the loader stream.")
#endif /* _MISRA_2012_RULES */
#ifdef _MISRA_2004_RULES
#pragma diag(suppress:misra_rule_2_4:"A comment includes example code.")
#pragma diag(suppress:misra_rule_11_3:"Casts from integer to pointer are used for the symbol table's magic numbers.")
#pragma diag(suppress:misra_rule_20_9:"I/O is used to read the loader stream.")
#endif /* _MISRA_2004_RULES */
#endif /* MISRA RULES */

#include <stdio.h>
#include <stdbool.h>
#include <adi_types.h>

#ifndef BYTES_PER_WORD
#define BYTES_PER_WORD 1
#define adi_code_cache_invalidate() do { } while (0)
#endif

#ifdef __BA_SHARC__
#pragma default_addressed(push)
#endif

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
  ADI_LIBLDR_SUCCESS = 0,         /* All okay */
  ADI_LIBLDR_NULL_PTR,            /* input fp is NULL */
  ADI_LIBLDR_READ_ERROR,          /* Could not read data from input fp */
  ADI_LIBLDR_SEEK_ERROR,          /* Could not skip ahead in stream */
  ADI_LIBLDR_UNSUPP_BLOCK,        /* Unsupported block type */
  ADI_LIBLDR_CHECKSUM_ERROR,      /* The checksum did not match */
  ADI_LIBLDR_NO_SYMTAB,           /* No symbol table in ldr file */
  ADI_LIBLDR_NO_SYMBOL,           /* Could not look up symbol in ldr file */
  ADI_LIBLDR_BAD_SYMBOL,          /* The parameter passed is invalid */
  ADI_LIBLDR_MEMCPY_L1_ERROR,     /* _memcpy_l1 failed */
  ADI_LIBLDR_MALLOC_FAILED,       /* failed to allocate buffer for l1 code
                                     initialization on Blackfin */
  ADI_LIBLDR_MAX_RESULT
} ADI_LIBLDR_RESULT;

/* Process a boot stream from the specified file pointer.
 * This will result in the contents of the boot stream being
 * copied into the addresses indicated by the boot stream.
 * It is the application developer's responsibility to ensure
 * that sufficient address space is set aside.
 * Returns ADI_LIBLDR_SUCCESS if everything goes to plan.
 */

#ifdef __BA_SHARC__
#pragma byte_addressed
#endif
ADI_LIBLDR_RESULT adi_libldr_Load(FILE *_fp);

/* Process a boot stream that is already in memory.
** This has the same behavior as adi_libldr_Load(),
** except that the parameter is the pointer to the
** first byte of the bootstream, already in contiguous
** (byte-addressed) memory.
*/

#if defined(__BYTE_ADDRESSING__) || defined(__ADSPBLACKFIN__) || defined(ADI_ADSP_CM41X) ||defined(ADI_ADSP_CM4X5X)
#ifdef __BA_SHARC__
#pragma byte_addressed
#endif
ADI_LIBLDR_RESULT adi_libldr_Move(char_t *_cp);
#endif /* __BYTE_ADDRESSING__ || __ADSPBLACKFIN__ || ADI_ADSP_CM41X */


/* Process a boot stream from the specified file pointer.
 * This has the same basic behavior as adi_libldr_Load(),
 * but the additional parameters affect operation:
 * - logfp:   if non-NULL, a summary of the boot stream will be
 *            written to this stream, in human-readable form.
 * - do_copy: only copy content into the address space if true.
 *            A word-addressed bool is used so we can call from char-size-8
 *            or char-size-32 code on SHARC+ parts.
 */
#ifdef __BA_SHARC__
#if defined(_MISRA_2004_RULES)
#pragma diag(push)
#pragma diag(suppress:misra_rule_6_3:"To permit use of __word_addressed bool.")
#endif /* MISRA RULES */
#pragma byte_addressed
ADI_LIBLDR_RESULT adi_libldr_LoadDbg(FILE *_fp, FILE *_logfp,
                                     __word_addressed bool _do_copy);
#if defined(_MISRA_2004_RULES)
#pragma diag(pop)
#endif /* MISRA RULES */
#else
ADI_LIBLDR_RESULT adi_libldr_LoadDbg(FILE *_fp, FILE *_logfp, bool _do_copy);
#endif


/* Process a boot stream that is already in memory.
 * This has the same basic behavior as adi_libldr_LoadDbg(),
 * but the function operates on a boot stream that is already
 * in contiguous, byte-addressed memory.
 */
#ifdef __BYTE_ADDRESSING__
#if defined(_MISRA_2004_RULES)
#pragma diag(push)
#pragma diag(suppress:misra_rule_6_3:"To permit use of __word_addressed bool.")
#endif /* MISRA RULES */
#pragma byte_addressed
ADI_LIBLDR_RESULT adi_libldr_MoveDbg(char_t *_cp, FILE *_logfp,
                                     __word_addressed bool _do_copy);
#if defined(_MISRA_2004_RULES)
#pragma diag(pop)
#endif /* MISRA RULES */
#endif /* __BYTE_ADDRESSING__ */

#if defined(__ADSPBLACKFIN__) || defined(ADI_ADSP_CM41X) ||defined(ADI_ADSP_CM4X5X)
ADI_LIBLDR_RESULT adi_libldr_MoveDbg(char_t *_cp, FILE *_logfp, bool _do_copy);
#endif

/* Look up the named symbol in a symbol/addr table at the
** given address, which typically will be the start of a section
** loaded from the ldr file.
** If the symbol is found, then this will return ADI_LIBLDR_SUCCESS,
** and *addr will be set to the address of the symbol. If the symbol
** is not found, then ADI_LIBLDR_NO_SYMBOL will be returned. If the
** address does not look like it points to a symbol table, then
** ADI_LIBLDR_NO_SYMTAB will be returned.
*/
ADI_LIBLDR_RESULT adi_libldr_LookUpSym(const char_t *_symbol,
                                       const void *_symtab,
                                       void **_addr);

/* Magic numbers to indicate that we are indeed looking at a
** symbol table.
*/
#define ADI_LIBLDR_MAGIC_SYMNAME ((const char_t *)0xFEED0AD1)
#define ADI_LIBLDR_MAGIC_SYMADDR ((const void *)0xAD1FACEF)

/* The structure we use for a symbol table.
** There should be an array of at least three such structures at
** the "symtab" address passed to adi_libldr_LookupSym():
**   { ADI_LIBLDR_MAGIC_SYMNAME, ADI_LIBLDR_MAGIC_SYMADDR },
**   { your_sym_name, your_sym_addr },
**   { 0x00000000, 0x00000000 }
** The NULL pointer for symname will terminate the search.
*/

typedef struct {
  const char_t *symname;
  const void *symaddr;
} adi_libldr_Symbol;

#ifdef __cplusplus
}
#endif

#ifdef __BA_SHARC__
#pragma default_addressed(pop)
#endif

#if defined(_MISRA_2004_RULES) || defined(_MISRA_2012_RULES)
#pragma diag(pop)
#endif /* MISRA RULES */
#endif /* __ADI_LIBLDR_H__ */
