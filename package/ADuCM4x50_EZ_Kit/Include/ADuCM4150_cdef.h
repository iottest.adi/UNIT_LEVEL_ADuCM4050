/*
** ADuCM4150_cdef.h
**
** Copyright (C) 2016 Analog Devices, Inc. All Rights Reserved.
**
*/

#ifndef _WRAP_ADUCM4150_CDEF_H
#define _WRAP_ADUCM4150_CDEF_H

#include <ADuCM4150_def.h>

#include <stdint.h>

#include <sys/adi_cio_macros.h>
#include <sys/adi_ADuCM4150_cdef.h>

#endif /* _WRAP_ADUCM4150_CDEF_H */
