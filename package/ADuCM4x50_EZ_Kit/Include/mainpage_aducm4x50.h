/*!
 *****************************************************************************
 * @file:    mainpage_aducm4x50.h
 * @brief:   CMSIS Cortex-M4 Main Documentation Page for
 *           ADI ADuCM4x50 Device Series
 *-----------------------------------------------------------------------------
 *
Copyright (c) 2010-2016 Analog Devices, Inc.

All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
  - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  - Modified versions of the software must be conspicuously marked as such.
  - This software is licensed solely and exclusively for use with processors
    manufactured by or for Analog Devices, Inc.
  - This software may not be combined or merged with other code in any manner
    that would cause the software to become subject to terms and conditions
    which differ from those listed here.
  - Neither the name of Analog Devices, Inc. nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
  - The use of this software may or may not infringe the patent rights of one
    or more patent holders.  This license does not release you from the
    requirement that you obtain separate licenses from these patent holders
    to use this software.

THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS "AS IS" AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT,
TITLE, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
NO EVENT SHALL ANALOG DEVICES, INC. OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF CLAIMS OF INTELLECTUAL
PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *****************************************************************************/

/* "Mainpage" is the main page for the DoxyGen output.
   Use it to present overview of the chip, drivers, release notes, etc.
   Use the "\section" directive to create section headers.
   Use the "\htmlinclude" directive to import html documentation.
 */

/*! \mainpage ADuCM4x50_EZ-KIT Device Drivers API Reference Manual Overview

   \section ChipIntro ADuCM4x50 Introduction

    The ADuCM4x50 is an ultra-low-power integrated mixed-signal micro-controller system for processing, control and connectivity.
    The MCU system is based on an ARM Cortex-M4 processor, a collection of digital peripherals, embedded SRAM and flash memory,
    and an analog subsystem which provides clocking, reset and power management capability along with an ADC.

    System features include (device drivers are not provided for all system peripherals):
       - 52 MHz ARM Cortex-M4F processor
       - Up to 512 KB of embedded flash memory with ECC
       - 128 KB system SRAM with parity
       - 32 KB user configurable instruction/data SRAM with parity
       - Power Management Unit (PMU)
       - A buck converter for improved energy efficiency during active state
       - 12-bit 1MSPS SAR analog-to-digital converter
       - Temperature sensor
       - Power-On Reset (POR) and Power-Supply Monitor (PSM)
       - Real time clock (RTC) capable of maintaining accurate wall clock time
       - A flexible real-time clock (FLEXI RTC) that supports a wide range of wakeup times
       - Three SPI interfaces capable of interfacing gluelessly with a range of sensors and converters
       - SPORT. Two single direction half SPORT or one bi-directional full SPORT
       - Multilayer Advanced Microcontroller Bus Architecture (AMBA) bus matrix
       - Central Direct Memory Access (DMA) controller
       - I2C
       - UART
       - Beeper interface
       - Crypto hardware support with AES and SHA256
       - 4 general-purpose and 1 watchdog timers
       - Programmable general-purpose I/O (GPIO) pins
       - True random number generator (TRNG)

      In order to support extremely low dynamic and standby power management,
      ADuCM4x50 provides a collection of power modes and features such as dynamic
      and software-controlled clock gating and power gating.

   \section DriverIntro API Reference Manual
    This "ADuCM4x50 Device Drivers API Reference Manual" documents a high-level language C
    Programming environment providing an Application Programming Interface (API)
    supporting a quick and easy functional interface to the ADuCM4x50 peripherals.

    Detailed knowledge of the various controller internals (control registers,
    register mapping, bitmaps, etc.) is not required to make effective use of the
    peripherals; simply open the device driver and use it.  Basic familiarity with
    the Cortex M4 architecture, relevant communications protocols and the third-party
    development tools is assumed.

    Various example are included to demonstrate use of the Device Driver programming model.

   \section SettingStarted Getting Started Guide
    Please read the "ADuCM4x50_EZ-KIT_BSP_GettingStartedGuide.pdf" for general
    information on the ADuCM4x50 Device Drivers installation and how to use it.

   \section ReleaseNotes Release Notes

    Please read the "ADuCM4x50 BSP Release Notes" document(s) for the latest
    information on the current release describing features, limitations, requirements,
    supported drivers, usage notes, examples, tests, etc.

   \section UserGuide Hardware User Guide and Device Data Sheet
    Please read the latest "ADuCM4x50 User Guide" and device Data Sheet for device
    reference information and performance specifications for the ADuCM4x50 microcontroller.


   \section Support Technical or Customer Support
    You can reach Analog Devices, Inc. Customer Support at:

        -    Web access at
                 - http://www.analog.com/support

        -    For IAR tool chain support please visit
                 -http://www.iar.com/support

        -    For Keil tool chain support please visit
                 -http://www.keil.com/support

        -    For CCES tool chain support please visit
                 -http://www.analog.com/cces

        -    E-mail processor questions to
                 - processor.support@analog.com
                 - processor.china@analog.com (China and Taiwan only)

        -    Phone questions to 1-800-ANALOGD

        -    Contact your Analog Devices, Inc. local sales office or authorized distributor

        -    Send questions by mail to:
   \code
   Analog Devices, Inc.
   3 Technology Way
   P.O. Box 9106
   Norwood, MA 02062-9106 USA
   \endcode

 */

/*
** EOF
*/

/*@}*/
