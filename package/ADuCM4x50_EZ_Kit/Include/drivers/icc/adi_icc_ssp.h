/*! *****************************************************************************
 * @file    adi_icc_ssp.h
 * @brief   Inter-core communication API for SSP (Sensor Signal Processor).
 * @details The APIs in this file should only be called from the SSP.
 -----------------------------------------------------------------------------
Copyright (c) 2016 Analog Devices, Inc.

All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
  - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  - Modified versions of the software must be conspicuously marked as such.
  - This software is licensed solely and exclusively for use with processors
    manufactured by or for Analog Devices, Inc.
  - This software may not be combined or merged with other code in any manner
    that would cause the software to become subject to terms and conditions
    which differ from those listed here.
  - Neither the name of Analog Devices, Inc. nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
  - The use of this software may or may not infringe the patent rights of one
    or more patent holders.  This license does not release you from the
    requirement that you obtain separate licenses from these patent holders
    to use this software.

THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-
INFRINGEMENT, TITLE, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL ANALOG DEVICES, INC. OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF
CLAIMS OF INTELLECTUAL PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*****************************************************************************/

#ifndef ADI_ICC_SSP_H
#define ADI_ICC_SSP_H

#include <stdint.h>
#include <stdbool.h>
#include "adi_icc_common.h"

#if !defined(__XTENSA__)
#error "This file cannot be compiled with this toolchain"
#endif

/*! @addtogroup ICC_Driver ICC driver for SSP
 *  @{
 */

/******************************************************************************
   Data structures
*******************************************************************************/

/*! Memory required for the operation of the ICC driver */
#define ADI_ICC_MEM_SIZE             (264)

/*! Enumeration of return codes from the ICC driver API */
typedef enum
{
    ADI_ICC_SUCCESS,                    /*!< The ICC API is successful executed. */
    ADI_ICC_FAILURE,                    /*!< The ICC API failed. */
    ADI_ICC_BAD_DEVICE_NUM,             /*!< The given device number is invalid. */
    ADI_ICC_NULL_POINTER,               /*!< The given pointer(s) is pointing to NULL. */
    ADI_ICC_INVALID_HANDLE,             /*!< The given device handle is invalid */
    ADI_ICC_ALREADY_INITIALIZED,        /*!< The device instance is already initialized and hence cannot be opened. */
    ADI_ICC_INSUFFICIENT_MEM,           /*!< The given memory is insufficient. */
    ADI_ICC_SEMAPHORE_FAILED            /*!< Semaphore related failure occurred. */
    
} ADI_ICC_RESULT;   

/**
 * @brief ICC Task function definition.
 */
typedef void (* ADI_ICC_TASK_FUNC) (  /*!< Task function pointer. */
    void      *pCBParam,         	  /*!< Client supplied callback param. */
    void      *pTaskArg,              /*!< Pointer to task argument supplied by Cortex. */
    uint32_t   nArgLen);              /*!< Size of the given argument in bytes. */

/*! ICC device driver handle */
typedef void * ADI_ICC_HANDLE;
    
/*****************************************************************************************************
                                            API functions 
******************************************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* Open the ICC driver. Before calling any other API the driver should be opened */
ADI_ICC_RESULT adi_icc_Open (const uint32_t nDeviceNum,
                             void *pMemory,
                             const uint32_t nMemorySize,
                             ADI_ICC_HANDLE *phDevice);

/* Close the ICC driver. */
ADI_ICC_RESULT adi_icc_Close (ADI_ICC_HANDLE hDevice);

/* Indicates that the task is done and updates the result.*/
ADI_ICC_RESULT adi_icc_TaskDone (ADI_ICC_HANDLE hDevice, 
                                void *pResult, 
                                uint32_t nResultLen);

/* Enables or disables expire timer.*/
ADI_ICC_RESULT adi_icc_ExpiryTmrEnable(ADI_ICC_HANDLE hDevice, bool bEnable);

/* Restart the Expiry timer, so that it starts recounting. */
ADI_ICC_RESULT adi_icc_ExpiryTmrRestart(ADI_ICC_HANDLE hDevice);

/* Start the ICC task dispatcher.*/
ADI_ICC_RESULT adi_icc_Start(ADI_ICC_HANDLE hDevice);

/* Register/un-register the task */
ADI_ICC_RESULT adi_icc_RegisterTask(ADI_ICC_HANDLE    hDevice,
                                    ADI_ICC_TASK 	  eTask,
                                    ADI_ICC_TASK_FUNC pfTask,
                                    void *pCBParam);

#ifdef __cplusplus
}
#endif /* __cplusplus */

/**@}*/

#endif /* ADI_ICC_SSP_H */



