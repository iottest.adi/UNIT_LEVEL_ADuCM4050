/*! *****************************************************************************
 * @file    adi_icc_cm.h
 * @brief   Inter-core communication API for Cortex.
 * @details The APIs in this file can only be called from the Cortex core.
 -----------------------------------------------------------------------------
Copyright (c) 2016 Analog Devices, Inc.

All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
  - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  - Modified versions of the software must be conspicuously marked as such.
  - This software is licensed solely and exclusively for use with processors
    manufactured by or for Analog Devices, Inc.
  - This software may not be combined or merged with other code in any manner
    that would cause the software to become subject to terms and conditions
    which differ from those listed here.
  - Neither the name of Analog Devices, Inc. nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
  - The use of this software may or may not infringe the patent rights of one
    or more patent holders.  This license does not release you from the
    requirement that you obtain separate licenses from these patent holders
    to use this software.

THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-
INFRINGEMENT, TITLE, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL ANALOG DEVICES, INC. OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF
CLAIMS OF INTELLECTUAL PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*****************************************************************************/
#ifndef ADI_ICC_CM_H
#define ADI_ICC_CM_H


 /*! \cond PRIVATE */
#include <adi_processor.h>
#include <adi_callback.h>
#include <rtos_map/adi_rtos_map.h>  /* for ADI_SEM_SIZE */
/*! \endcond */

#include "adi_icc_common.h"

#if !defined(__CORTEX_M)
#error "This file cannot be included in the applications running on this core"
#endif

/*! @addtogroup ICC_Driver ICC driver for Cortex
 *  @{
 */


/******************************************************************************
   Data structures
*******************************************************************************/

/*! Memory required for the operation of the ICC driver. */
#define ADI_ICC_MEM_SIZE             (12u + ADI_SEM_SIZE)

/*! Enumeration of return codes from the ICC driver API */
typedef enum 
{
    ADI_ICC_SUCCESS,                    /*!< The ICC API is successful executed. */     
    ADI_ICC_BAD_DEVICE_NUM,             /*!< The given device number is invalid. */
    ADI_ICC_NULL_POINTER,               /*!< The given pointer(s) is pointing to NULL. */
    ADI_ICC_INVALID_HANDLE,             /*!< The given device handle is invalid. */
    ADI_ICC_INVALID_PARAMETER,          /*!< The given parameter is not valid. */
    ADI_ICC_ALREADY_INITIALIZED,        /*!< The device instance is already initialized and hence cannot be opened. */
    ADI_ICC_INSUFFICIENT_MEM,           /*!< The given memory is insufficient. */
    ADI_ICC_SEMAPHORE_FAILED,           /*!< Semaphore related failure occurred. */
    ADI_ICC_HARDWARE_ERROR              /*!< A hardware error has occurred. */   
    
} ADI_ICC_RESULT;

   
/*! Enumeration of SRAM errors */
typedef enum
{
    ADI_ICC_SRAM_ERR_NONE                   = 0x00, /*!< No SRAM error is detected */
    ADI_ICC_SRAM_ERR_PARITY_CORTEX          = 0x01, /*!< Parity error status for Cortex */
    ADI_ICC_SRAM_ERR_IRAM_WRITE_CORTEX      = 0x02, /*!< IRAM Bus Write error for Cortex */
    ADI_ICC_SRAM_ERR_OUT_OF_BOUND_CORTEX    = 0x04, /*!< Out of bound access error for Cortex */
    ADI_ICC_SRAM_ERR_PARITY_SSP             = 0x10, /*!< Parity error status for SSP */
    ADI_ICC_SRAM_ERR_IRAM_WRITE_SSP         = 0x20, /*!< IRAM Bus Write error for SSP  */
    ADI_ICC_SRAM_ERR_IRAM_OUT_OF_BOUND_SSP  = 0x40  /*!< Out of bound access error for SSP */
    
} ADI_ICC_SRAM_ERR;

/*! Enumeration of DMA events that can trigger a task to run */
typedef enum
{
    ADI_ICC_EVENT_CTRL_NONE,                        /*!< DMA events do not trigger task to run. */
    ADI_ICC_EVENT_CTRL_BASIC_DMA_DONE,              /*!< Basic DMA mode done event triggers task to run. */
    ADI_ICC_EVENT_CTRL_SG_DMA_DONE,                 /*!< Scatter Gather DMA mode done event triggers task to run. */
    ADI_ICC_EVENT_CTRL_BASIC_AND_SG_DMA_DONE        /*!< Scatter Gather mode and Basic DMA mode done events triggers task to run. */
    
}ADI_ICC_EVENT_CTRL;


/*! Structure which holds information regarding the shared SRAM error */ 
typedef struct
{
    ADI_ICC_SRAM_ERR eError;          /*!< Error type */
    void *           pErrorAddr;      /*!< Address which caused the error */
    
} ADI_ICC_SRAM_ERROR_INFO;

/*! Events that are reported through the callback */
typedef enum
{   
    /*! No error events. */
    ADI_ICC_EVENT_ERROR_NONE,
    
    /*! The task was started when the previously started task is not done.*/
    ADI_ICC_EVENT_ERROR_TASK_OVR,
    
    /*! The Expiry timer has timed out. This can because software running in SSP 
        stuck in a loop or crashed. The Event argument points to NULL in this case.
    */
    ADI_ICC_EVENT_ERROR_TIMER_TIMEOUT,
    
    /*! An error occurred in the shared SRAM. The Event argument will point to the 
        ADI_ICC_SRAM_ERROR_INFO structure which will have more details regarding the error. */
    ADI_ICC_EVENT_ERROR_SHARED_SRAM
    
} ADI_ICC_EVENT;

/*! ICC device driver handle */
typedef void * ADI_ICC_HANDLE;

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */
/*****************************************************************************************************
                                            API functions 
******************************************************************************************************/

/* Open the ICC driver. Before calling any other API the driver should be opened */
ADI_ICC_RESULT adi_icc_Open(const uint32_t nDeviceNum, void *pMemory, const uint32_t nMemorySize, ADI_ICC_HANDLE *phDevice);

/* Close the ICC driver. */
ADI_ICC_RESULT adi_icc_Close(ADI_ICC_HANDLE hDevice);

/* Starts the execution of the task on SSP which is associated with the given task id (eTask) */
ADI_ICC_RESULT adi_icc_StartTask(ADI_ICC_HANDLE hDevice, const ADI_ICC_TASK eTask, void *pTaskArg, const uint32_t nArgLen);

/* Starts the execution of the task on SSP upon completion of DMA. */
ADI_ICC_RESULT adi_icc_StartTaskOnDmaDone(ADI_ICC_HANDLE hDevice, const ADI_ICC_TASK eTask, const ADI_ICC_EVENT_CTRL eEventCtrl, void *pTaskArg, const uint32_t nArgLen);

/* Waits until the completion of the previously started task.*/
ADI_ICC_RESULT adi_icc_WaitForTaskDone(ADI_ICC_HANDLE hDevice, void **pResult, uint32_t *pResultLen, uint32_t *pHwError);

/* Power up SSP and enable the clocks. */
ADI_ICC_RESULT adi_icc_EnableSSP(ADI_ICC_HANDLE hDevice, const bool bStall);

/* Power down SSP and disable the clocks */
ADI_ICC_RESULT adi_icc_DisableSSP(ADI_ICC_HANDLE hDevice);

/* Start executing code in SSP */
ADI_ICC_RESULT adi_icc_StartSSP(ADI_ICC_HANDLE hDevice);

/* Register/un-register the callback  */
ADI_ICC_RESULT adi_icc_RegisterCallback(ADI_ICC_HANDLE hDevice, ADI_CALLBACK pfCallback, void *pCBParam);

/* Checks if SSP is ready to run tasks.*/
ADI_ICC_RESULT adi_icc_QueryReadyStatus(ADI_ICC_HANDLE hDevice, bool *pStatus);
  
#ifdef __cplusplus
}
#endif

/**@}*/

#endif /* ADI_ICC_CM_H */   


