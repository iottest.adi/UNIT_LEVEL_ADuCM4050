/*! *****************************************************************************
 * @file    adi_icc_common.h
 * @brief   Inter-core communication common API file.
 * @details This file will be included by both SSP and the Cortex API files.
 -----------------------------------------------------------------------------
Copyright (c) 2016 Analog Devices, Inc.

All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
  - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  - Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  - Modified versions of the software must be conspicuously marked as such.
  - This software is licensed solely and exclusively for use with processors
    manufactured by or for Analog Devices, Inc.
  - This software may not be combined or merged with other code in any manner
    that would cause the software to become subject to terms and conditions
    which differ from those listed here.
  - Neither the name of Analog Devices, Inc. nor the names of its
    contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.
  - The use of this software may or may not infringe the patent rights of one
    or more patent holders.  This license does not release you from the
    requirement that you obtain separate licenses from these patent holders
    to use this software.

THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES, INC. AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-
INFRINGEMENT, TITLE, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL ANALOG DEVICES, INC. OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, PUNITIVE OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, DAMAGES ARISING OUT OF
CLAIMS OF INTELLECTUAL PROPERTY RIGHTS INFRINGEMENT; PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

*****************************************************************************/

#ifndef ADI_ICC_COMMON_H
#define ADI_ICC_COMMON_H

/*! 
 *  @addtogroup ICC_Driver
 *  @{
 */

/*! Enumeration of tasks that can be started by CortexM on SSP core. 
    Tasks 0 to 15 can be triggered to start by HW upon completion of the 
    DMA. This can be configured using the API adi_icc_StartTaskOnDmaDone.
   
    Please refer HRM for mapping between DMA channel and the task.
*/
typedef enum
{
    ADI_ICC_TASK_0 = 0, /*!< Task 0  */
    ADI_ICC_TASK_1,     /*!< Task 1  */
    ADI_ICC_TASK_3,     /*!< Task 3  */
    ADI_ICC_TASK_2,     /*!< Task 2  */
    ADI_ICC_TASK_4,     /*!< Task 4  */
    ADI_ICC_TASK_5,     /*!< Task 5  */
    ADI_ICC_TASK_6,     /*!< Task 6  */
    ADI_ICC_TASK_7,     /*!< Task 7  */
    ADI_ICC_TASK_8,     /*!< Task 8  */
    ADI_ICC_TASK_9,     /*!< Task 9  */
    ADI_ICC_TASK_10,    /*!< Task 10 */
    ADI_ICC_TASK_11,    /*!< Task 11 */
    ADI_ICC_TASK_12,    /*!< Task 12 */
    ADI_ICC_TASK_13,    /*!< Task 13 */
    ADI_ICC_TASK_14,    /*!< Task 14 */
    ADI_ICC_TASK_15,    /*!< Task 15 */
    ADI_ICC_TASK_16,    /*!< Task 16 */
    ADI_ICC_TASK_17,    /*!< Task 17 */
    ADI_ICC_TASK_18,    /*!< Task 18 */
    ADI_ICC_TASK_19,    /*!< Task 19 */
    ADI_ICC_TASK_20,    /*!< Task 20 */
    ADI_ICC_TASK_21,    /*!< Task 21 */
    ADI_ICC_TASK_22,    /*!< Task 22 */
    ADI_ICC_TASK_23,    /*!< Task 23 */
    ADI_ICC_TASK_24,    /*!< Task 24 */
    ADI_ICC_TASK_25,    /*!< Task 25 */
    ADI_ICC_TASK_26,    /*!< Task 26 */
    ADI_ICC_TASK_27,    /*!< Task 27 */
    ADI_ICC_TASK_28,    /*!< Task 28 */
    ADI_ICC_TASK_29,    /*!< Task 29 */
    ADI_ICC_TASK_30,    /*!< Task 30 */
    ADI_ICC_TASK_31,    /*!< Task 31 */
    ADI_ICC_TASK_MAX    /*!< Maximum number of ICC tasks that are supported by the hardware */                                 
} ADI_ICC_TASK;

/*! Maximum task number that can be used to start the task upon DMA completion. 
   Only tasks 0-15 are mapped to peripheral DMA channels. 
   
   Please refer Hardware Reference Manual to know the mapping between the tasks and the
   DMA channel numbers.   
   */
#define ADI_ICC_MAX_DMA_TASK  ADI_ICC_TASK_15

/*! Bit position for the SSP ready status. */
#define BITP_SSP_READY_STATUS   0u

/*! Bit mask for the SSP ready status. */
#define BITM_SSP_READY_STATUS   1u

/**@}*/

#endif /* ADI_ICC_COMMON_H */


