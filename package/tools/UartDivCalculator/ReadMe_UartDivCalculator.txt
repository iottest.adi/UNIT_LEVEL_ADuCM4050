How to compile UartDivCalculator.c

Requirements:
Visual Studio command line

Process:
1. In the command prompt window run "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\bin\vcvars32.bat"
2. change directory to the location of UartDivCalculator.c
3. Compile UartDivCalculator using the -O2 level of optimization


Example code: 
#Change Directory to VC\bin
cd "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\bin\"

#Run vcvars32.bat
vcvars32.bat

#Change directory to location of UartDivCalculator.c source code
cd C:\Desktop\aducm4x50\package\tools\UartDivCalculator

#Compile
cl UartDivCalculator.c -O2 -o UartDivCalculator.exe
