#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
void main(int argc, char *argv[])
{
	unsigned int baudrate []= { 9600,19200,38400,57600,115200,230400,460800};
    const unsigned int K1 = 1 << 11;
	unsigned int i,divC,br,M,N,minM=0,minN=0,C=0,clock,OSR,minOSR;
    float rBr;
	int mdivQ;
	int err;
	int prevErr = INT_MAX;
        
    if(argc < 2||argc > 3 )
    {
	printf("\n\t Usage: UartDivCalculator <input clock> [input clock baudrate]\n\n");
		exit(0);
    }
    clock = atoi(argv[1]);
    printf("\n+-----------------------------------------------------------------------+");
    printf("\n| Calculating UART DIV register value for the input clock = %8d\t|",clock);
    printf("\n|-----------------------------------------------------------------------|");
    printf("\n|\tBAUDRATE\tDIV-C\tDIV-M\tDIV-N\tOSR\tDIFF\t\t|");
    printf("\n|-----------------------------------------------------------------------|");
    for(i=0; i<sizeof(baudrate)/sizeof(baudrate[0]); i++)
    {
        if (argc == 2)
        {	    
            br = baudrate[i];
        }
        else
        {
            br = atoi(argv[2]);
        }

        mdivQ=INT_MAX;
		for(OSR=0;OSR<4;OSR++)
		{
			for(M=1;M<4;M++)
            {
                for(N=0;N<2048;N++)
                { 
					for(C=1;C<65536;C++)
					{
						const unsigned int K2 = 1 << (9 - OSR);
                        rBr = clock * (float)K2 / ((M*K1+N)*C); 
						
						err = abs((long)(rBr-br));
                        if(mdivQ > err)
                        { 
							divC   = C;
                            mdivQ  = err;
                            minM   = M;
                            minN   = N;
                            minOSR = OSR;
					    }

						/* The formula for calculating the error is a monotonic function. 
						This means that if the error starts to increase, it will continue to do so.
						There is therefor no point in continuing to calculate it if this happens.*/
						if (err > prevErr)
						{   //Error is increasing. Bail out.
							prevErr = INT_MAX;
							break;
						} 
						else 
						{
							prevErr = err;
						}
						//No point in continuing since we have the answer.
						if (err == 0) break;
					}
					if (err == 0) break;
                }
				if (err == 0) break;
			}
			if (err == 0) break;
        }

        
        printf("\n|\t%8d\t%04d\t%4d\t%4d\t%4d\t%4d\t\t|",br,divC,minM,minN,minOSR,mdivQ);
        printf("\n|-----------------------------------------------------------------------|");
		if(argc == 3)
        {
			break;
        }
    }
}


