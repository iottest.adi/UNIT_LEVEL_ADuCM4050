         Analog Devices, Inc. ADuCM4x50 Application 

Description:  Template for no-OS applications in ADuCM4x50 processors. 

Overview:
=========    
    Basic project template for ADuCM4x50 projects. 

    If the driver configuration files need to be changed from the default, this
    template expects them to be added to the config folder which is already
    added to the path so the files will be found before the ones in the BSP.

    This template includes some device drivers. If they are not required they
    can be removed from the project.

    Note that if changes are made to the project file the IAR toolchain may
    replace the environment variables used with the corresponding full paths.
    The toolchain only does this for files that are added to the project,
    leaving the include search path to use the environment variable.

Environment Setup:
=====================
  Environment variables required:
  * ADUCM4x50_BSP_DIR to point to the ADuCM4x50 BSP. 
   (C:\Analog Devices\ADuCM4x50\ADuCM4x50_EZ_Kit
   in the default installation)

References:
===========
    ADuCM4x50 Hardware Reference Manual
