/*
****************************************************************************
*
*                    APPLICATION CONFIGURATION
*
* This is the application configuration file required by uC/OS-III for the 
* application.
* It augments port-specific configuration information in os_cfg.h.
*
*****************************************************************************
*/

#if !defined(APP_CFG_H)
#define APP_CFG_H

/* application specific RTOS configurations */

#define TASK_STARTUP_STK_SIZE   (200u)
#define TASK_STARTUP_PRIO        (10u)

#endif    /* APP_CFG_H */
