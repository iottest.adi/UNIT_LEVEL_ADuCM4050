/*********************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you
agree to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*!
 * @brief     Brief example information
 * @details   More example information
 *
 */
/*=============  I N C L U D E S   =============*/

#include <os.h>
#include <app_cfg.h>
#include <stdio.h>
#include <common.h>
#include <drivers/pwr/adi_pwr.h>

#include <drivers/mydriver/mydriver.h>
#include "example.h"

/*=============  M A C R O    D E F I N I T I O N S   =============*/

/*=============  G L O B A L  V A R I A B L E S       =============*/
/* Handle for driver */
static ADI_DRIVER_HANDLE hDevice;

/* Memory for driver */
static uint8_t DriverDeviceMem[ADI_DRIVER_MEMORY_SIZE];

/*=============  R T O S D A T A   =============*/
static OS_TCB g_Task_Startup_TCB;

static CPU_STK g_Task_Startup_Stack  [TASK_STARTUP_STK_SIZE];


/*=============  E X T E R N A L  F U N C T I O N S   =============*/
extern int32_t adi_initpinmux(void);

/*=============  L O C A L       F U N C T I O N S   =============*/
void StartUpTask(void* arg)
{
    /* Create rest of the tasks and signals */ 
    /* The location where the example passes or fails is example-specific */ 
    while (1)
    {
      /* Example body here or in other tasks */

      /* if success */
      common_Pass();
      /* if there are no other actions this task should perform it can
       * be destroyed
       */
    }
}
/*********************************************************************
*
*   Function:   main
*
*********************************************************************/
int main (void)
{
    OS_ERR OSRetVal;

    /* pinmux init */
    adi_initpinmux();

    /* Initialize the RTOS */
    OSInit(&OSRetVal);
    if (OSRetVal != OS_ERR_NONE)
    {
        printf("Failed to initialize the RTOS \n");
        return(0);
    }

    /* Power Driver initialization if required */
    if(ADI_PWR_SUCCESS != adi_pwr_Init())
    {
        DEBUG_MESSAGE("Failure initializing the power driver");
        return (1);
    }

    /* example framework initialization */
    common_Init();

    /* Create the Start-up Task*/
    OSTaskCreate (&g_Task_Startup_TCB, /* address of TCB */
        "Start-up Task",               /* Task name */
        StartUpTask,                   /* Task "Run" function */
        NULL,                          /* arg for the "Run" function*/
        TASK_STARTUP_PRIO,             /* priority */
        g_Task_Startup_Stack,          /* base of the stack */
        TASK_STARTUP_STK_SIZE - 1u ,   /* limit for stack growth */
        TASK_STARTUP_STK_SIZE,         /* stack size in CPU_STK */
        0u,                            /* number of messages allowed */
        (OS_TICK)  0u,                 /* time_quanta */
        NULL,                          /* extension pointer */
        (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
        &OSRetVal);

    if (OSRetVal != OS_ERR_NONE)
    {
        DEBUG_MESSAGE("Failure creating start-up task");
        return (1);
    }

    /* Start OS */
    OSStart(&OSRetVal);

    if (OSRetVal != OS_ERR_NONE)
    {
        DEBUG_MESSAGE("Failure starting the RTOS ");
        return (1);
    }

    common_Fail("Application should not return to main after RTOS start");
    return (0);
}
