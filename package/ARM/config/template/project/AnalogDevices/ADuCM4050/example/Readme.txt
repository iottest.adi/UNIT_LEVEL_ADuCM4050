    Basic project template for ADuCM4x50 examples. The example should be created
    in the repository in the examples/<driver>/<example_name>/ADuCM4x50/iar
    folder.

Instructions: 
=============
    Sources
    -------
    * Move example.c and example.h to the <example_name> folder and add it to 
      your project in the Source folder
    * Add your example content to the files

    Drivers
    -------
    * Add the required drivers to the "Driver" group
    * If you don't need the DMA or PWR you can remove the files.

    Config
    -------
    * Move the config folder to <example_name>. 
    * Remove the readme from the config folder. It is there so the folder can
      be in svn. 
    * If you need any new config files different from the default add them 
      to the config folder and link them in the "config" group.

    Testing
    -------
    * In config.txt replace the project/exe named example with your own
    * Change pass/fail messages if required

    Readme
    -------
    * Rename this file Readme_<example_name>.txt
    * Move this file to the ADuCM4x50 folder
    * Link the moved file to your project
    * Fill in the appropriate sections for your example
    * Delete lines from the top to this one

-------------------------------------------------------------

         Analog Devices, Inc. ADuCM4x50 Application Example

Project Name: 

Description: 
            


Overview:
=========

User Configuration Macros:
==========================

Hardware Setup:
===============

External connections:
=====================

How to build and run:
=====================

Expected Result:
=================

References:
===========
    ADuCM4x50 Hardware Reference Manual.
