/*********************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you
agree to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*!
 * @brief     Brief example information
 * @details   More example information
 *
 */
/*=============  I N C L U D E S   =============*/

#include <common.h>
#include <drivers/pwr/adi_pwr.h>

#include <drivers/mydriver/mydriver.h>
#include "example.h"

/*=============  M A C R O    D E F I N I T I O N S   =============*/

/*=============  G L O B A L  V A R I A B L E S       =============*/
/* Handle for driver */
static ADI_DRIVER_HANDLE hDevice;

/* Memory for driver */
static uint8_t DriverDeviceMem[ADI_DRIVER_MEMORY_SIZE];

/*=============  E X T E R N A L  F U N C T I O N S   =============*/
extern int32_t adi_initpinmux(void);

/*
 * main
 */
int main(void)
{
    /* pinmux init */
    adi_initpinmux();

    /* Power Driver initialization if required */
    if(ADI_PWR_SUCCESS != adi_pwr_Init())
    {
        DEBUG_MESSAGE("Failure initializing the power driver");
        common_Fail("Failure initializing the power driver");
        return (1);
    }

    /* example framework initialization */
    common_Init();

    /* Example body */

    /* if success */
    common_Pass();

    return 0;
}
