/*********************************************************************************
Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you
agree to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/

#ifndef __EXAMPLE_H__
#define __EXAMPLE_H__

/*=============  M A C R O    D E F I N I T I O N S   =============*/

/*=============  G L O B A L  V A R I A B L E S       =============*/

/*=============  E X T E R N A L  F U N C T I O N S   =============*/

#endif /* __EXAMPLE_H__ */
