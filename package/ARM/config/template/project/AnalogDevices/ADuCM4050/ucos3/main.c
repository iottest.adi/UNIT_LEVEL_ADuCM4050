/*********************************************************************************

Copyright(c) 2016 Analog Devices, Inc. All Rights Reserved.

This software is proprietary and confidential.  By using this software you agree
to the terms of the associated Analog Devices License Agreement.

*********************************************************************************/
/*!
 * @file      main.c
 * @brief     Main template to create Micrium's projects with ADuCM4x50 processors 
 */

/*=============  I N C L U D E S   =============*/

#include "os.h"
#include <stdio.h>
#include <stdlib.h>

/*=============  D E F I N E S   =============*/
#define TASK_STARTUP_STK_SIZE   (200u)
#define TASK_STARTUP_PRIO        (10u)

/*=============  R T O S D A T A   =============*/

static OS_TCB g_Task_Startup_TCB;

static CPU_STK g_Task_Startup_Stack  [TASK_STARTUP_STK_SIZE];

void StartUpTask(void* arg)
{
    /* Create rest of the tasks and signals */ 
    while (1)
    {
      /* if there are no other actions this task should perform it can
       * be destroyed
       */
    }
}

/*********************************************************************
*
*   Function:   main
*
*********************************************************************/
int main (void)
{
    OS_ERR OSRetVal;

    /* Initialize the RTOS */
    OSInit(&OSRetVal);
    if (OSRetVal != OS_ERR_NONE)
    {
        printf("Failed to initialize the RTOS \n");
        return(0);
    }

    /* Create the Start-up Task*/
    OSTaskCreate (&g_Task_Startup_TCB, /* address of TCB */
        "Start-up Task",               /* Task name */
        StartUpTask,                   /* Task "Run" function */
        NULL,                          /* arg for the "Run" function*/
        TASK_STARTUP_PRIO,             /* priority */
        g_Task_Startup_Stack,          /* base of the stack */
        TASK_STARTUP_STK_SIZE - 1u ,   /* limit for stack growth */
        TASK_STARTUP_STK_SIZE,         /* stack size in CPU_STK */
        0u,                            /* number of messages allowed */
        (OS_TICK)  0u,                 /* time_quanta */
        NULL,                          /* extension pointer */
        (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR),
        &OSRetVal);

    if (OSRetVal != OS_ERR_NONE)
    {
        printf("Failed to create startup task\n");
        return(0);
    }

    /* Start OS */
    OSStart(&OSRetVal);

    if (OSRetVal != OS_ERR_NONE)
    {
        printf("Failed to start RTOS\n");
    }
}
               
/*****/

