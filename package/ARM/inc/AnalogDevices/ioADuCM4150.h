/*
** ioADuCM4150.h
**
** Copyright (C) 2016 Analog Devices, Inc. All Rights Reserved.
**
*/

#ifndef _WRAP_IOADUCM4150_H
#define _WRAP_IOADUCM4150_H

#ifdef __ICCARM__
/* IAR MISRA C 2004 error suppressions:
 *
 * Pm127 (rule 10.6): a 'U' suffix shall be applied to all constants of 'unsigned' type.
 *   Literals >= 0x80000000 used in address arguments for __IO_REG32 macro,
 *   which uses them only with '@' direct placement operator.
 */
_Pragma("diag_suppress=Pm127")
#endif /* __ICCARM__ */

#include <sys/adi_ioADuCM4150.h>

#ifdef __ICCARM__
_Pragma("diag_default=Pm127")
#endif /* __ICCARM__ */

#endif /* _WRAP_IOADUCM4150_H */
