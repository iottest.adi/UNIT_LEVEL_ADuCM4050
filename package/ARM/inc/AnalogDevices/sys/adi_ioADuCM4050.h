/* ================================================================================

     Project      :   ADuCM4050
     File         :   ioADuCM4050.h
     Description  :   IAR I/O Register Access Definitions

     Date         :   Feb 7, 2017

     Copyright (c) 2014-2017 Analog Devices, Inc.  All Rights Reserved.
     This software is proprietary and confidential to Analog Devices, Inc. and
     its licensors.

     This file was auto-generated. Do not make local changes to this file.

   ================================================================================ */


#ifndef _IAR_IO_ADUCM4050_H
#define _IAR_IO_ADUCM4050_H

#include "io_macros.h"


/* =========================
         TMR0
   ========================= */
__IO_REG16(TMR0_LOAD,                    0x40000000, __READ_WRITE);
__IO_REG16(TMR0_CURCNT,                  0x40000004,       __READ);
__IO_REG16(TMR0_CTL,                     0x40000008, __READ_WRITE);
__IO_REG16(TMR0_CLRINT,                  0x4000000c,      __WRITE);
__IO_REG16(TMR0_CAPTURE,                 0x40000010,       __READ);
__IO_REG16(TMR0_ALOAD,                   0x40000014, __READ_WRITE);
__IO_REG16(TMR0_ACURCNT,                 0x40000018,       __READ);
__IO_REG16(TMR0_STAT,                    0x4000001c,       __READ);
__IO_REG16(TMR0_PWMCTL,                  0x40000020, __READ_WRITE);
__IO_REG16(TMR0_PWMMATCH,                0x40000024, __READ_WRITE);
__IO_REG16(TMR0_EVENTSELECT,             0x40000028, __READ_WRITE);

/* =========================
         TMR1
   ========================= */
__IO_REG16(TMR1_LOAD,                    0x40000400, __READ_WRITE);
__IO_REG16(TMR1_CURCNT,                  0x40000404,       __READ);
__IO_REG16(TMR1_CTL,                     0x40000408, __READ_WRITE);
__IO_REG16(TMR1_CLRINT,                  0x4000040c,      __WRITE);
__IO_REG16(TMR1_CAPTURE,                 0x40000410,       __READ);
__IO_REG16(TMR1_ALOAD,                   0x40000414, __READ_WRITE);
__IO_REG16(TMR1_ACURCNT,                 0x40000418,       __READ);
__IO_REG16(TMR1_STAT,                    0x4000041c,       __READ);
__IO_REG16(TMR1_PWMCTL,                  0x40000420, __READ_WRITE);
__IO_REG16(TMR1_PWMMATCH,                0x40000424, __READ_WRITE);
__IO_REG16(TMR1_EVENTSELECT,             0x40000428, __READ_WRITE);

/* =========================
         TMR2
   ========================= */
__IO_REG16(TMR2_LOAD,                    0x40000800, __READ_WRITE);
__IO_REG16(TMR2_CURCNT,                  0x40000804,       __READ);
__IO_REG16(TMR2_CTL,                     0x40000808, __READ_WRITE);
__IO_REG16(TMR2_CLRINT,                  0x4000080c,      __WRITE);
__IO_REG16(TMR2_CAPTURE,                 0x40000810,       __READ);
__IO_REG16(TMR2_ALOAD,                   0x40000814, __READ_WRITE);
__IO_REG16(TMR2_ACURCNT,                 0x40000818,       __READ);
__IO_REG16(TMR2_STAT,                    0x4000081c,       __READ);
__IO_REG16(TMR2_PWMCTL,                  0x40000820, __READ_WRITE);
__IO_REG16(TMR2_PWMMATCH,                0x40000824, __READ_WRITE);
__IO_REG16(TMR2_EVENTSELECT,             0x40000828, __READ_WRITE);

/* =========================
         TMR_RGB
   ========================= */
__IO_REG16(TMR_RGB_LOAD,                 0x40000c00, __READ_WRITE);
__IO_REG16(TMR_RGB_CURCNT,               0x40000c04,       __READ);
__IO_REG16(TMR_RGB_CTL,                  0x40000c08, __READ_WRITE);
__IO_REG16(TMR_RGB_CLRINT,               0x40000c0c,      __WRITE);
__IO_REG16(TMR_RGB_CAPTURE,              0x40000c10,       __READ);
__IO_REG16(TMR_RGB_ALOAD,                0x40000c14, __READ_WRITE);
__IO_REG16(TMR_RGB_ACURCNT,              0x40000c18,       __READ);
__IO_REG16(TMR_RGB_STAT,                 0x40000c1c,       __READ);
__IO_REG16(TMR_RGB_PWM0CTL,              0x40000c20, __READ_WRITE);
__IO_REG16(TMR_RGB_PWM0MATCH,            0x40000c24, __READ_WRITE);
__IO_REG16(TMR_RGB_EVENTSELECT,          0x40000c28, __READ_WRITE);
__IO_REG16(TMR_RGB_PWM1CTL,              0x40000c2c, __READ_WRITE);
__IO_REG16(TMR_RGB_PWM1MATCH,            0x40000c30, __READ_WRITE);
__IO_REG16(TMR_RGB_PWM2CTL,              0x40000c34, __READ_WRITE);
__IO_REG16(TMR_RGB_PWM2MATCH,            0x40000c38, __READ_WRITE);

/* =========================
         RTC0
   ========================= */
__IO_REG16(RTC0_CR0,                     0x40001000, __READ_WRITE);
__IO_REG16(RTC0_SR0,                     0x40001004, __READ_WRITE);
__IO_REG16(RTC0_SR1,                     0x40001008,       __READ);
__IO_REG16(RTC0_CNT0,                    0x4000100c, __READ_WRITE);
__IO_REG16(RTC0_CNT1,                    0x40001010, __READ_WRITE);
__IO_REG16(RTC0_ALM0,                    0x40001014, __READ_WRITE);
__IO_REG16(RTC0_ALM1,                    0x40001018, __READ_WRITE);
__IO_REG16(RTC0_TRM,                     0x4000101c, __READ_WRITE);
__IO_REG16(RTC0_GWY,                     0x40001020,      __WRITE);
__IO_REG16(RTC0_CR1,                     0x40001028, __READ_WRITE);
__IO_REG16(RTC0_SR2,                     0x4000102c, __READ_WRITE);
__IO_REG16(RTC0_SNAP0,                   0x40001030,       __READ);
__IO_REG16(RTC0_SNAP1,                   0x40001034,       __READ);
__IO_REG16(RTC0_SNAP2,                   0x40001038,       __READ);
__IO_REG16(RTC0_MOD,                     0x4000103c,       __READ);
__IO_REG16(RTC0_CNT2,                    0x40001040,       __READ);
__IO_REG16(RTC0_ALM2,                    0x40001044, __READ_WRITE);
__IO_REG16(RTC0_SR3,                     0x40001048, __READ_WRITE);
__IO_REG16(RTC0_CR2IC,                   0x4000104c, __READ_WRITE);
__IO_REG16(RTC0_CR3SS,                   0x40001050, __READ_WRITE);
__IO_REG16(RTC0_CR4SS,                   0x40001054, __READ_WRITE);
__IO_REG16(RTC0_SSMSK,                   0x40001058, __READ_WRITE);
__IO_REG16(RTC0_IC2,                     0x40001064,       __READ);
__IO_REG16(RTC0_IC3,                     0x40001068,       __READ);
__IO_REG16(RTC0_IC4,                     0x4000106c,       __READ);
__IO_REG16(RTC0_SS1,                     0x40001070, __READ_WRITE);
__IO_REG16(RTC0_SS2,                     0x40001074, __READ_WRITE);
__IO_REG16(RTC0_SS3,                     0x40001078, __READ_WRITE);
__IO_REG16(RTC0_SS4,                     0x4000107c, __READ_WRITE);
__IO_REG16(RTC0_SR4,                     0x40001080,       __READ);
__IO_REG16(RTC0_SR5,                     0x40001084,       __READ);
__IO_REG16(RTC0_SR6,                     0x40001088,       __READ);
__IO_REG16(RTC0_SS1TGT,                  0x4000108c,       __READ);
__IO_REG16(RTC0_FRZCNT,                  0x40001090,       __READ);
__IO_REG16(RTC0_SS2TGT,                  0x40001094,       __READ);
__IO_REG16(RTC0_SS3TGT,                  0x40001098,       __READ);
__IO_REG16(RTC0_SS1LOWDUR,               0x400010a0, __READ_WRITE);
__IO_REG16(RTC0_SS2LOWDUR,               0x400010a4, __READ_WRITE);
__IO_REG16(RTC0_SS3LOWDUR,               0x400010a8, __READ_WRITE);
__IO_REG16(RTC0_SS1HIGHDUR,              0x400010b0, __READ_WRITE);
__IO_REG16(RTC0_SS2HIGHDUR,              0x400010b4, __READ_WRITE);
__IO_REG16(RTC0_SS3HIGHDUR,              0x400010b8, __READ_WRITE);
__IO_REG16(RTC0_SSMSKOT,                 0x400010c0, __READ_WRITE);
__IO_REG16(RTC0_CR5SSS,                  0x400010c4, __READ_WRITE);
__IO_REG16(RTC0_CR6SSS,                  0x400010c8, __READ_WRITE);
__IO_REG16(RTC0_CR7SSS,                  0x400010cc, __READ_WRITE);
__IO_REG16(RTC0_SR7,                     0x400010d0, __READ_WRITE);
__IO_REG16(RTC0_SR8,                     0x400010d4,       __READ);
__IO_REG16(RTC0_SR9,                     0x400010d8,       __READ);
__IO_REG16(RTC0_GPMUX0,                  0x400010e0, __READ_WRITE);
__IO_REG16(RTC0_GPMUX1,                  0x400010e4, __READ_WRITE);

/* =========================
         RTC1
   ========================= */
__IO_REG16(RTC1_CR0,                     0x40001400, __READ_WRITE);
__IO_REG16(RTC1_SR0,                     0x40001404, __READ_WRITE);
__IO_REG16(RTC1_SR1,                     0x40001408,       __READ);
__IO_REG16(RTC1_CNT0,                    0x4000140c, __READ_WRITE);
__IO_REG16(RTC1_CNT1,                    0x40001410, __READ_WRITE);
__IO_REG16(RTC1_ALM0,                    0x40001414, __READ_WRITE);
__IO_REG16(RTC1_ALM1,                    0x40001418, __READ_WRITE);
__IO_REG16(RTC1_TRM,                     0x4000141c, __READ_WRITE);
__IO_REG16(RTC1_GWY,                     0x40001420,      __WRITE);
__IO_REG16(RTC1_CR1,                     0x40001428, __READ_WRITE);
__IO_REG16(RTC1_SR2,                     0x4000142c, __READ_WRITE);
__IO_REG16(RTC1_SNAP0,                   0x40001430,       __READ);
__IO_REG16(RTC1_SNAP1,                   0x40001434,       __READ);
__IO_REG16(RTC1_SNAP2,                   0x40001438,       __READ);
__IO_REG16(RTC1_MOD,                     0x4000143c,       __READ);
__IO_REG16(RTC1_CNT2,                    0x40001440,       __READ);
__IO_REG16(RTC1_ALM2,                    0x40001444, __READ_WRITE);
__IO_REG16(RTC1_SR3,                     0x40001448, __READ_WRITE);
__IO_REG16(RTC1_CR2IC,                   0x4000144c, __READ_WRITE);
__IO_REG16(RTC1_CR3SS,                   0x40001450, __READ_WRITE);
__IO_REG16(RTC1_CR4SS,                   0x40001454, __READ_WRITE);
__IO_REG16(RTC1_SSMSK,                   0x40001458, __READ_WRITE);
__IO_REG16(RTC1_IC2,                     0x40001464,       __READ);
__IO_REG16(RTC1_IC3,                     0x40001468,       __READ);
__IO_REG16(RTC1_IC4,                     0x4000146c,       __READ);
__IO_REG16(RTC1_SS1,                     0x40001470, __READ_WRITE);
__IO_REG16(RTC1_SS2,                     0x40001474, __READ_WRITE);
__IO_REG16(RTC1_SS3,                     0x40001478, __READ_WRITE);
__IO_REG16(RTC1_SS4,                     0x4000147c, __READ_WRITE);
__IO_REG16(RTC1_SR4,                     0x40001480,       __READ);
__IO_REG16(RTC1_SR5,                     0x40001484,       __READ);
__IO_REG16(RTC1_SR6,                     0x40001488,       __READ);
__IO_REG16(RTC1_SS1TGT,                  0x4000148c,       __READ);
__IO_REG16(RTC1_FRZCNT,                  0x40001490,       __READ);
__IO_REG16(RTC1_SS2TGT,                  0x40001494,       __READ);
__IO_REG16(RTC1_SS3TGT,                  0x40001498,       __READ);
__IO_REG16(RTC1_SS1LOWDUR,               0x400014a0, __READ_WRITE);
__IO_REG16(RTC1_SS2LOWDUR,               0x400014a4, __READ_WRITE);
__IO_REG16(RTC1_SS3LOWDUR,               0x400014a8, __READ_WRITE);
__IO_REG16(RTC1_SS1HIGHDUR,              0x400014b0, __READ_WRITE);
__IO_REG16(RTC1_SS2HIGHDUR,              0x400014b4, __READ_WRITE);
__IO_REG16(RTC1_SS3HIGHDUR,              0x400014b8, __READ_WRITE);
__IO_REG16(RTC1_SSMSKOT,                 0x400014c0, __READ_WRITE);
__IO_REG16(RTC1_CR5SSS,                  0x400014c4, __READ_WRITE);
__IO_REG16(RTC1_CR6SSS,                  0x400014c8, __READ_WRITE);
__IO_REG16(RTC1_CR7SSS,                  0x400014cc, __READ_WRITE);
__IO_REG16(RTC1_SR7,                     0x400014d0, __READ_WRITE);
__IO_REG16(RTC1_SR8,                     0x400014d4,       __READ);
__IO_REG16(RTC1_SR9,                     0x400014d8,       __READ);
__IO_REG16(RTC1_GPMUX0,                  0x400014e0, __READ_WRITE);
__IO_REG16(RTC1_GPMUX1,                  0x400014e4, __READ_WRITE);

/* =========================
         SYS
   ========================= */
__IO_REG16(SYS_ADIID,                    0x40002020,       __READ);
__IO_REG16(SYS_CHIPID,                   0x40002024,       __READ);
__IO_REG16(SYS_SWDEN,                    0x40002040,      __WRITE);

/* =========================
         WDT0
   ========================= */
__IO_REG16(WDT0_LOAD,                    0x40002c00, __READ_WRITE);
__IO_REG16(WDT0_CCNT,                    0x40002c04,       __READ);
__IO_REG16(WDT0_CTL,                     0x40002c08, __READ_WRITE);
__IO_REG16(WDT0_RESTART,                 0x40002c0c,      __WRITE);
__IO_REG16(WDT0_STAT,                    0x40002c18,       __READ);

/* =========================
         I2C0
   ========================= */
__IO_REG16(I2C0_MCTL,                    0x40003000, __READ_WRITE);
__IO_REG16(I2C0_MSTAT,                   0x40003004, __READ_WRITE);
__IO_REG16(I2C0_MRX,                     0x40003008,       __READ);
__IO_REG16(I2C0_MTX,                     0x4000300c, __READ_WRITE);
__IO_REG16(I2C0_MRXCNT,                  0x40003010, __READ_WRITE);
__IO_REG16(I2C0_MCRXCNT,                 0x40003014,       __READ);
__IO_REG16(I2C0_ADDR1,                   0x40003018, __READ_WRITE);
__IO_REG16(I2C0_ADDR2,                   0x4000301c, __READ_WRITE);
__IO_REG16(I2C0_BYT,                     0x40003020, __READ_WRITE);
__IO_REG16(I2C0_DIV,                     0x40003024, __READ_WRITE);
__IO_REG16(I2C0_SCTL,                    0x40003028, __READ_WRITE);
__IO_REG16(I2C0_SSTAT,                   0x4000302c, __READ_WRITE);
__IO_REG16(I2C0_SRX,                     0x40003030,       __READ);
__IO_REG16(I2C0_STX,                     0x40003034, __READ_WRITE);
__IO_REG16(I2C0_ALT,                     0x40003038, __READ_WRITE);
__IO_REG16(I2C0_ID0,                     0x4000303c, __READ_WRITE);
__IO_REG16(I2C0_ID1,                     0x40003040, __READ_WRITE);
__IO_REG16(I2C0_ID2,                     0x40003044, __READ_WRITE);
__IO_REG16(I2C0_ID3,                     0x40003048, __READ_WRITE);
__IO_REG16(I2C0_STAT,                    0x4000304c, __READ_WRITE);
__IO_REG16(I2C0_SHCTL,                   0x40003050,      __WRITE);
__IO_REG16(I2C0_TCTL,                    0x40003054, __READ_WRITE);
__IO_REG16(I2C0_ASTRETCH_SCL,            0x40003058, __READ_WRITE);

/* =========================
         SPI0
   ========================= */
__IO_REG16(SPI0_STAT,                    0x40004000, __READ_WRITE);
__IO_REG16(SPI0_RX,                      0x40004004,       __READ);
__IO_REG16(SPI0_TX,                      0x40004008,      __WRITE);
__IO_REG16(SPI0_DIV,                     0x4000400c, __READ_WRITE);
__IO_REG16(SPI0_CTL,                     0x40004010, __READ_WRITE);
__IO_REG16(SPI0_IEN,                     0x40004014, __READ_WRITE);
__IO_REG16(SPI0_CNT,                     0x40004018, __READ_WRITE);
__IO_REG16(SPI0_DMA,                     0x4000401c, __READ_WRITE);
__IO_REG16(SPI0_FIFO_STAT,               0x40004020,       __READ);
__IO_REG16(SPI0_RD_CTL,                  0x40004024, __READ_WRITE);
__IO_REG16(SPI0_FLOW_CTL,                0x40004028, __READ_WRITE);
__IO_REG16(SPI0_WAIT_TMR,                0x4000402c, __READ_WRITE);
__IO_REG16(SPI0_CS_CTL,                  0x40004030, __READ_WRITE);
__IO_REG16(SPI0_CS_OVERRIDE,             0x40004034, __READ_WRITE);

/* =========================
         SPI1
   ========================= */
__IO_REG16(SPI1_STAT,                    0x40004400, __READ_WRITE);
__IO_REG16(SPI1_RX,                      0x40004404,       __READ);
__IO_REG16(SPI1_TX,                      0x40004408,      __WRITE);
__IO_REG16(SPI1_DIV,                     0x4000440c, __READ_WRITE);
__IO_REG16(SPI1_CTL,                     0x40004410, __READ_WRITE);
__IO_REG16(SPI1_IEN,                     0x40004414, __READ_WRITE);
__IO_REG16(SPI1_CNT,                     0x40004418, __READ_WRITE);
__IO_REG16(SPI1_DMA,                     0x4000441c, __READ_WRITE);
__IO_REG16(SPI1_FIFO_STAT,               0x40004420,       __READ);
__IO_REG16(SPI1_RD_CTL,                  0x40004424, __READ_WRITE);
__IO_REG16(SPI1_FLOW_CTL,                0x40004428, __READ_WRITE);
__IO_REG16(SPI1_WAIT_TMR,                0x4000442c, __READ_WRITE);
__IO_REG16(SPI1_CS_CTL,                  0x40004430, __READ_WRITE);
__IO_REG16(SPI1_CS_OVERRIDE,             0x40004434, __READ_WRITE);

/* =========================
         SPI2
   ========================= */
__IO_REG16(SPI2_STAT,                    0x40024000, __READ_WRITE);
__IO_REG16(SPI2_RX,                      0x40024004,       __READ);
__IO_REG16(SPI2_TX,                      0x40024008,      __WRITE);
__IO_REG16(SPI2_DIV,                     0x4002400c, __READ_WRITE);
__IO_REG16(SPI2_CTL,                     0x40024010, __READ_WRITE);
__IO_REG16(SPI2_IEN,                     0x40024014, __READ_WRITE);
__IO_REG16(SPI2_CNT,                     0x40024018, __READ_WRITE);
__IO_REG16(SPI2_DMA,                     0x4002401c, __READ_WRITE);
__IO_REG16(SPI2_FIFO_STAT,               0x40024020,       __READ);
__IO_REG16(SPI2_RD_CTL,                  0x40024024, __READ_WRITE);
__IO_REG16(SPI2_FLOW_CTL,                0x40024028, __READ_WRITE);
__IO_REG16(SPI2_WAIT_TMR,                0x4002402c, __READ_WRITE);
__IO_REG16(SPI2_CS_CTL,                  0x40024030, __READ_WRITE);
__IO_REG16(SPI2_CS_OVERRIDE,             0x40024034, __READ_WRITE);

/* =========================
         UART0
   ========================= */
__IO_REG16(UART0_RX,                     0x40005000,       __READ);
__IO_REG16(UART0_TX,                     0x40005000,      __WRITE);
__IO_REG16(UART0_IEN,                    0x40005004, __READ_WRITE);
__IO_REG16(UART0_IIR,                    0x40005008,       __READ);
__IO_REG16(UART0_LCR,                    0x4000500c, __READ_WRITE);
__IO_REG16(UART0_MCR,                    0x40005010, __READ_WRITE);
__IO_REG16(UART0_LSR,                    0x40005014,       __READ);
__IO_REG16(UART0_MSR,                    0x40005018,       __READ);
__IO_REG16(UART0_SCR,                    0x4000501c, __READ_WRITE);
__IO_REG16(UART0_FCR,                    0x40005020, __READ_WRITE);
__IO_REG16(UART0_FBR,                    0x40005024, __READ_WRITE);
__IO_REG16(UART0_DIV,                    0x40005028, __READ_WRITE);
__IO_REG16(UART0_LCR2,                   0x4000502c, __READ_WRITE);
__IO_REG16(UART0_CTL,                    0x40005030, __READ_WRITE);
__IO_REG16(UART0_RFC,                    0x40005034,       __READ);
__IO_REG16(UART0_TFC,                    0x40005038,       __READ);
__IO_REG16(UART0_RSC,                    0x4000503c, __READ_WRITE);
__IO_REG16(UART0_ACR,                    0x40005040, __READ_WRITE);
__IO_REG16(UART0_ASRL,                   0x40005044,       __READ);
__IO_REG16(UART0_ASRH,                   0x40005048,       __READ);

/* =========================
         UART1
   ========================= */
__IO_REG16(UART1_RX,                     0x40005400,       __READ);
__IO_REG16(UART1_TX,                     0x40005400,      __WRITE);
__IO_REG16(UART1_IEN,                    0x40005404, __READ_WRITE);
__IO_REG16(UART1_IIR,                    0x40005408,       __READ);
__IO_REG16(UART1_LCR,                    0x4000540c, __READ_WRITE);
__IO_REG16(UART1_MCR,                    0x40005410, __READ_WRITE);
__IO_REG16(UART1_LSR,                    0x40005414,       __READ);
__IO_REG16(UART1_MSR,                    0x40005418,       __READ);
__IO_REG16(UART1_SCR,                    0x4000541c, __READ_WRITE);
__IO_REG16(UART1_FCR,                    0x40005420, __READ_WRITE);
__IO_REG16(UART1_FBR,                    0x40005424, __READ_WRITE);
__IO_REG16(UART1_DIV,                    0x40005428, __READ_WRITE);
__IO_REG16(UART1_LCR2,                   0x4000542c, __READ_WRITE);
__IO_REG16(UART1_CTL,                    0x40005430, __READ_WRITE);
__IO_REG16(UART1_RFC,                    0x40005434,       __READ);
__IO_REG16(UART1_TFC,                    0x40005438,       __READ);
__IO_REG16(UART1_RSC,                    0x4000543c, __READ_WRITE);
__IO_REG16(UART1_ACR,                    0x40005440, __READ_WRITE);
__IO_REG16(UART1_ASRL,                   0x40005444,       __READ);
__IO_REG16(UART1_ASRH,                   0x40005448,       __READ);

/* =========================
         BEEP0
   ========================= */
__IO_REG16(BEEP0_CFG,                    0x40005c00, __READ_WRITE);
__IO_REG16(BEEP0_STAT,                   0x40005c04, __READ_WRITE);
__IO_REG16(BEEP0_TONEA,                  0x40005c08, __READ_WRITE);
__IO_REG16(BEEP0_TONEB,                  0x40005c0c, __READ_WRITE);

/* =========================
         ADC0
   ========================= */
__IO_REG16(ADC0_CFG,                     0x40007000, __READ_WRITE);
__IO_REG16(ADC0_PWRUP,                   0x40007004, __READ_WRITE);
__IO_REG16(ADC0_CAL_WORD,                0x40007008, __READ_WRITE);
__IO_REG16(ADC0_CNV_CFG,                 0x4000700c, __READ_WRITE);
__IO_REG16(ADC0_CNV_TIME,                0x40007010, __READ_WRITE);
__IO_REG16(ADC0_AVG_CFG,                 0x40007014, __READ_WRITE);
__IO_REG16(ADC0_IRQ_EN,                  0x40007020, __READ_WRITE);
__IO_REG16(ADC0_STAT,                    0x40007024, __READ_WRITE);
__IO_REG16(ADC0_OVF,                     0x40007028, __READ_WRITE);
__IO_REG16(ADC0_ALERT,                   0x4000702c, __READ_WRITE);
__IO_REG16(ADC0_CH0_OUT,                 0x40007030,       __READ);
__IO_REG16(ADC0_CH1_OUT,                 0x40007034,       __READ);
__IO_REG16(ADC0_CH2_OUT,                 0x40007038,       __READ);
__IO_REG16(ADC0_CH3_OUT,                 0x4000703c,       __READ);
__IO_REG16(ADC0_CH4_OUT,                 0x40007040,       __READ);
__IO_REG16(ADC0_CH5_OUT,                 0x40007044,       __READ);
__IO_REG16(ADC0_CH6_OUT,                 0x40007048,       __READ);
__IO_REG16(ADC0_CH7_OUT,                 0x4000704c,       __READ);
__IO_REG16(ADC0_BAT_OUT,                 0x40007050,       __READ);
__IO_REG16(ADC0_TMP_OUT,                 0x40007054,       __READ);
__IO_REG16(ADC0_TMP2_OUT,                0x40007058,       __READ);
__IO_REG16(ADC0_DMA_OUT,                 0x4000705c,       __READ);
__IO_REG16(ADC0_LIM0_LO,                 0x40007060, __READ_WRITE);
__IO_REG16(ADC0_LIM0_HI,                 0x40007064, __READ_WRITE);
__IO_REG16(ADC0_HYS0,                    0x40007068, __READ_WRITE);
__IO_REG16(ADC0_LIM1_LO,                 0x40007070, __READ_WRITE);
__IO_REG16(ADC0_LIM1_HI,                 0x40007074, __READ_WRITE);
__IO_REG16(ADC0_HYS1,                    0x40007078, __READ_WRITE);
__IO_REG16(ADC0_LIM2_LO,                 0x40007080, __READ_WRITE);
__IO_REG16(ADC0_LIM2_HI,                 0x40007084, __READ_WRITE);
__IO_REG16(ADC0_HYS2,                    0x40007088, __READ_WRITE);
__IO_REG16(ADC0_LIM3_LO,                 0x40007090, __READ_WRITE);
__IO_REG16(ADC0_LIM3_HI,                 0x40007094, __READ_WRITE);
__IO_REG16(ADC0_HYS3,                    0x40007098, __READ_WRITE);
__IO_REG16(ADC0_CFG1,                    0x400070c0, __READ_WRITE);

/* =========================
         DMA0
   ========================= */
__IO_REG32(DMA0_STAT,                    0x40010000,       __READ);
__IO_REG32(DMA0_CFG,                     0x40010004,      __WRITE);
__IO_REG32(DMA0_PDBPTR,                  0x40010008, __READ_WRITE);
__IO_REG32(DMA0_ADBPTR,                  0x4001000c,       __READ);
__IO_REG32(DMA0_SWREQ,                   0x40010014,      __WRITE);
__IO_REG32(DMA0_RMSK_SET,                0x40010020, __READ_WRITE);
__IO_REG32(DMA0_RMSK_CLR,                0x40010024,      __WRITE);
__IO_REG32(DMA0_EN_SET,                  0x40010028, __READ_WRITE);
__IO_REG32(DMA0_EN_CLR,                  0x4001002c,      __WRITE);
__IO_REG32(DMA0_ALT_SET,                 0x40010030, __READ_WRITE);
__IO_REG32(DMA0_ALT_CLR,                 0x40010034,      __WRITE);
__IO_REG32(DMA0_PRI_SET,                 0x40010038,      __WRITE);
__IO_REG32(DMA0_PRI_CLR,                 0x4001003c,      __WRITE);
__IO_REG32(DMA0_ERRCHNL_CLR,             0x40010048, __READ_WRITE);
__IO_REG32(DMA0_ERR_CLR,                 0x4001004c, __READ_WRITE);
__IO_REG32(DMA0_INVALIDDESC_CLR,         0x40010050, __READ_WRITE);
__IO_REG32(DMA0_BS_SET,                  0x40010800, __READ_WRITE);
__IO_REG32(DMA0_BS_CLR,                  0x40010804,      __WRITE);
__IO_REG32(DMA0_SRCADDR_SET,             0x40010810, __READ_WRITE);
__IO_REG32(DMA0_SRCADDR_CLR,             0x40010814,      __WRITE);
__IO_REG32(DMA0_DSTADDR_SET,             0x40010818, __READ_WRITE);
__IO_REG32(DMA0_DSTADDR_CLR,             0x4001081c,      __WRITE);
__IO_REG32(DMA0_REVID,                   0x40010fe0,       __READ);

/* =========================
         FLCC0
   ========================= */
__IO_REG32(FLCC0_STAT,                   0x40018000, __READ_WRITE);
__IO_REG32(FLCC0_IEN,                    0x40018004, __READ_WRITE);
__IO_REG32(FLCC0_CMD,                    0x40018008, __READ_WRITE);
__IO_REG32(FLCC0_KH_ADDR,                0x4001800c, __READ_WRITE);
__IO_REG32(FLCC0_KH_DATA0,               0x40018010, __READ_WRITE);
__IO_REG32(FLCC0_KH_DATA1,               0x40018014, __READ_WRITE);
__IO_REG32(FLCC0_PAGE_ADDR0,             0x40018018, __READ_WRITE);
__IO_REG32(FLCC0_PAGE_ADDR1,             0x4001801c, __READ_WRITE);
__IO_REG32(FLCC0_KEY,                    0x40018020,      __WRITE);
__IO_REG32(FLCC0_WR_ABORT_ADDR,          0x40018024,       __READ);
__IO_REG32(FLCC0_WRPROT,                 0x40018028, __READ_WRITE);
__IO_REG32(FLCC0_SIGNATURE,              0x4001802c,       __READ);
__IO_REG32(FLCC0_UCFG,                   0x40018030, __READ_WRITE);
__IO_REG32(FLCC0_TIME_PARAM0,            0x40018034, __READ_WRITE);
__IO_REG32(FLCC0_TIME_PARAM1,            0x40018038, __READ_WRITE);
__IO_REG32(FLCC0_ABORT_EN_LO,            0x4001803c, __READ_WRITE);
__IO_REG32(FLCC0_ABORT_EN_HI,            0x40018040, __READ_WRITE);
__IO_REG32(FLCC0_ECC_CFG,                0x40018044, __READ_WRITE);
__IO_REG32(FLCC0_ECC_ADDR,               0x40018048,       __READ);
__IO_REG32(FLCC0_POR_SEC,                0x40018050, __READ_WRITE);
__IO_REG32(FLCC0_VOL_CFG,                0x40018054, __READ_WRITE);

/* =========================
         FLCC0_CACHE
   ========================= */
__IO_REG32(FLCC0_CACHE_STAT,             0x40018058,       __READ);
__IO_REG32(FLCC0_CACHE_SETUP,            0x4001805c, __READ_WRITE);
__IO_REG32(FLCC0_CACHE_KEY,              0x40018060,      __WRITE);

/* =========================
         GPIO0
   ========================= */
__IO_REG32(GPIO0_CFG,                    0x40020000, __READ_WRITE);
__IO_REG16(GPIO0_OEN,                    0x40020004, __READ_WRITE);
__IO_REG16(GPIO0_PE,                     0x40020008, __READ_WRITE);
__IO_REG16(GPIO0_IEN,                    0x4002000c, __READ_WRITE);
__IO_REG16(GPIO0_IN,                     0x40020010,       __READ);
__IO_REG16(GPIO0_OUT,                    0x40020014, __READ_WRITE);
__IO_REG16(GPIO0_SET,                    0x40020018,      __WRITE);
__IO_REG16(GPIO0_CLR,                    0x4002001c,      __WRITE);
__IO_REG16(GPIO0_TGL,                    0x40020020,      __WRITE);
__IO_REG16(GPIO0_POL,                    0x40020024, __READ_WRITE);
__IO_REG16(GPIO0_IENA,                   0x40020028, __READ_WRITE);
__IO_REG16(GPIO0_IENB,                   0x4002002c, __READ_WRITE);
__IO_REG16(GPIO0_INT,                    0x40020030, __READ_WRITE);
__IO_REG16(GPIO0_DS,                     0x40020034, __READ_WRITE);

/* =========================
         GPIO1
   ========================= */
__IO_REG32(GPIO1_CFG,                    0x40020040, __READ_WRITE);
__IO_REG16(GPIO1_OEN,                    0x40020044, __READ_WRITE);
__IO_REG16(GPIO1_PE,                     0x40020048, __READ_WRITE);
__IO_REG16(GPIO1_IEN,                    0x4002004c, __READ_WRITE);
__IO_REG16(GPIO1_IN,                     0x40020050,       __READ);
__IO_REG16(GPIO1_OUT,                    0x40020054, __READ_WRITE);
__IO_REG16(GPIO1_SET,                    0x40020058,      __WRITE);
__IO_REG16(GPIO1_CLR,                    0x4002005c,      __WRITE);
__IO_REG16(GPIO1_TGL,                    0x40020060,      __WRITE);
__IO_REG16(GPIO1_POL,                    0x40020064, __READ_WRITE);
__IO_REG16(GPIO1_IENA,                   0x40020068, __READ_WRITE);
__IO_REG16(GPIO1_IENB,                   0x4002006c, __READ_WRITE);
__IO_REG16(GPIO1_INT,                    0x40020070, __READ_WRITE);
__IO_REG16(GPIO1_DS,                     0x40020074, __READ_WRITE);

/* =========================
         GPIO2
   ========================= */
__IO_REG32(GPIO2_CFG,                    0x40020080, __READ_WRITE);
__IO_REG16(GPIO2_OEN,                    0x40020084, __READ_WRITE);
__IO_REG16(GPIO2_PE,                     0x40020088, __READ_WRITE);
__IO_REG16(GPIO2_IEN,                    0x4002008c, __READ_WRITE);
__IO_REG16(GPIO2_IN,                     0x40020090,       __READ);
__IO_REG16(GPIO2_OUT,                    0x40020094, __READ_WRITE);
__IO_REG16(GPIO2_SET,                    0x40020098,      __WRITE);
__IO_REG16(GPIO2_CLR,                    0x4002009c,      __WRITE);
__IO_REG16(GPIO2_TGL,                    0x400200a0,      __WRITE);
__IO_REG16(GPIO2_POL,                    0x400200a4, __READ_WRITE);
__IO_REG16(GPIO2_IENA,                   0x400200a8, __READ_WRITE);
__IO_REG16(GPIO2_IENB,                   0x400200ac, __READ_WRITE);
__IO_REG16(GPIO2_INT,                    0x400200b0, __READ_WRITE);
__IO_REG16(GPIO2_DS,                     0x400200b4, __READ_WRITE);

/* =========================
         GPIO3
   ========================= */
__IO_REG32(GPIO3_CFG,                    0x400200c0, __READ_WRITE);
__IO_REG16(GPIO3_OEN,                    0x400200c4, __READ_WRITE);
__IO_REG16(GPIO3_PE,                     0x400200c8, __READ_WRITE);
__IO_REG16(GPIO3_IEN,                    0x400200cc, __READ_WRITE);
__IO_REG16(GPIO3_IN,                     0x400200d0,       __READ);
__IO_REG16(GPIO3_OUT,                    0x400200d4, __READ_WRITE);
__IO_REG16(GPIO3_SET,                    0x400200d8,      __WRITE);
__IO_REG16(GPIO3_CLR,                    0x400200dc,      __WRITE);
__IO_REG16(GPIO3_TGL,                    0x400200e0,      __WRITE);
__IO_REG16(GPIO3_POL,                    0x400200e4, __READ_WRITE);
__IO_REG16(GPIO3_IENA,                   0x400200e8, __READ_WRITE);
__IO_REG16(GPIO3_IENB,                   0x400200ec, __READ_WRITE);
__IO_REG16(GPIO3_INT,                    0x400200f0, __READ_WRITE);
__IO_REG16(GPIO3_DS,                     0x400200f4, __READ_WRITE);

/* =========================
         SPORT0
   ========================= */
__IO_REG32(SPORT0_CTL_A,                 0x40038000, __READ_WRITE);
__IO_REG32(SPORT0_DIV_A,                 0x40038004, __READ_WRITE);
__IO_REG32(SPORT0_IEN_A,                 0x40038008, __READ_WRITE);
__IO_REG32(SPORT0_STAT_A,                0x4003800c, __READ_WRITE);
__IO_REG32(SPORT0_NUMTRAN_A,             0x40038010, __READ_WRITE);
__IO_REG32(SPORT0_CNVT_A,                0x40038014, __READ_WRITE);
__IO_REG32(SPORT0_TX_A,                  0x40038020,      __WRITE);
__IO_REG32(SPORT0_RX_A,                  0x40038028,       __READ);
__IO_REG32(SPORT0_CTL_B,                 0x40038040, __READ_WRITE);
__IO_REG32(SPORT0_DIV_B,                 0x40038044, __READ_WRITE);
__IO_REG32(SPORT0_IEN_B,                 0x40038048, __READ_WRITE);
__IO_REG32(SPORT0_STAT_B,                0x4003804c, __READ_WRITE);
__IO_REG32(SPORT0_NUMTRAN_B,             0x40038050, __READ_WRITE);
__IO_REG32(SPORT0_CNVT_B,                0x40038054, __READ_WRITE);
__IO_REG32(SPORT0_TX_B,                  0x40038060,      __WRITE);
__IO_REG32(SPORT0_RX_B,                  0x40038068,       __READ);

/* =========================
         CRC0
   ========================= */
__IO_REG32(CRC0_CTL,                     0x40040000, __READ_WRITE);
__IO_REG32(CRC0_IPDATA,                  0x40040004,      __WRITE);
__IO_REG32(CRC0_RESULT,                  0x40040008, __READ_WRITE);
__IO_REG32(CRC0_POLY,                    0x4004000c, __READ_WRITE);
__IO_REG8(CRC0_IPBITS0,                  0x40040010,      __WRITE);
__IO_REG8(CRC0_IPBITS1,                  0x40040011,      __WRITE);
__IO_REG8(CRC0_IPBITS2,                  0x40040012,      __WRITE);
__IO_REG8(CRC0_IPBITS3,                  0x40040013,      __WRITE);
__IO_REG8(CRC0_IPBITS4,                  0x40040014,      __WRITE);
__IO_REG8(CRC0_IPBITS5,                  0x40040015,      __WRITE);
__IO_REG8(CRC0_IPBITS6,                  0x40040016,      __WRITE);
__IO_REG8(CRC0_IPBITS7,                  0x40040017,      __WRITE);
__IO_REG8(CRC0_IPBYTE,                   0x40040010,      __WRITE);

/* =========================
         RNG0
   ========================= */
__IO_REG16(RNG0_CTL,                     0x40040400, __READ_WRITE);
__IO_REG16(RNG0_LEN,                     0x40040404, __READ_WRITE);
__IO_REG16(RNG0_STAT,                    0x40040408, __READ_WRITE);
__IO_REG32(RNG0_DATA,                    0x4004040c,       __READ);
__IO_REG32(RNG0_OSCCNT,                  0x40040410,       __READ);
__IO_REG8(RNG0_OSCDIFF0,                 0x40040414,       __READ);
__IO_REG8(RNG0_OSCDIFF1,                 0x40040415,       __READ);
__IO_REG8(RNG0_OSCDIFF2,                 0x40040416,       __READ);
__IO_REG8(RNG0_OSCDIFF3,                 0x40040417,       __READ);

/* =========================
         CRYPT0
   ========================= */
__IO_REG32(CRYPT0_CFG,                   0x40044000, __READ_WRITE);
__IO_REG32(CRYPT0_DATALEN,               0x40044004, __READ_WRITE);
__IO_REG32(CRYPT0_PREFIXLEN,             0x40044008, __READ_WRITE);
__IO_REG32(CRYPT0_INTEN,                 0x4004400c, __READ_WRITE);
__IO_REG32(CRYPT0_STAT,                  0x40044010, __READ_WRITE);
__IO_REG32(CRYPT0_INBUF,                 0x40044014,      __WRITE);
__IO_REG32(CRYPT0_OUTBUF,                0x40044018,       __READ);
__IO_REG32(CRYPT0_NONCE0,                0x4004401c, __READ_WRITE);
__IO_REG32(CRYPT0_NONCE1,                0x40044020, __READ_WRITE);
__IO_REG32(CRYPT0_NONCE2,                0x40044024, __READ_WRITE);
__IO_REG32(CRYPT0_NONCE3,                0x40044028, __READ_WRITE);
__IO_REG32(CRYPT0_AESKEY0,               0x4004402c,      __WRITE);
__IO_REG32(CRYPT0_AESKEY1,               0x40044030,      __WRITE);
__IO_REG32(CRYPT0_AESKEY2,               0x40044034,      __WRITE);
__IO_REG32(CRYPT0_AESKEY3,               0x40044038,      __WRITE);
__IO_REG32(CRYPT0_AESKEY4,               0x4004403c,      __WRITE);
__IO_REG32(CRYPT0_AESKEY5,               0x40044040,      __WRITE);
__IO_REG32(CRYPT0_AESKEY6,               0x40044044,      __WRITE);
__IO_REG32(CRYPT0_AESKEY7,               0x40044048,      __WRITE);
__IO_REG32(CRYPT0_CNTRINIT,              0x4004404c, __READ_WRITE);
__IO_REG32(CRYPT0_SHAH0,                 0x40044050, __READ_WRITE);
__IO_REG32(CRYPT0_SHAH1,                 0x40044054, __READ_WRITE);
__IO_REG32(CRYPT0_SHAH2,                 0x40044058, __READ_WRITE);
__IO_REG32(CRYPT0_SHAH3,                 0x4004405c, __READ_WRITE);
__IO_REG32(CRYPT0_SHAH4,                 0x40044060, __READ_WRITE);
__IO_REG32(CRYPT0_SHAH5,                 0x40044064, __READ_WRITE);
__IO_REG32(CRYPT0_SHAH6,                 0x40044068, __READ_WRITE);
__IO_REG32(CRYPT0_SHAH7,                 0x4004406c, __READ_WRITE);
__IO_REG32(CRYPT0_SHA_LAST_WORD,         0x40044070, __READ_WRITE);
__IO_REG32(CRYPT0_CCM_NUM_VALID_BYTES,   0x40044074, __READ_WRITE);
__IO_REG32(CRYPT0_PRKSTORCFG,            0x40044078, __READ_WRITE);
__IO_REG32(CRYPT0_KUW0,                  0x40044080,      __WRITE);
__IO_REG32(CRYPT0_KUW1,                  0x40044084,      __WRITE);
__IO_REG32(CRYPT0_KUW2,                  0x40044088,      __WRITE);
__IO_REG32(CRYPT0_KUW3,                  0x4004408c,      __WRITE);
__IO_REG32(CRYPT0_KUW4,                  0x40044090,      __WRITE);
__IO_REG32(CRYPT0_KUW5,                  0x40044094,      __WRITE);
__IO_REG32(CRYPT0_KUW6,                  0x40044098,      __WRITE);
__IO_REG32(CRYPT0_KUW7,                  0x4004409c,      __WRITE);
__IO_REG32(CRYPT0_KUW8,                  0x400440a0,      __WRITE);
__IO_REG32(CRYPT0_KUW9,                  0x400440a4,      __WRITE);
__IO_REG32(CRYPT0_KUW10,                 0x400440a8,      __WRITE);
__IO_REG32(CRYPT0_KUW11,                 0x400440ac,      __WRITE);
__IO_REG32(CRYPT0_KUW12,                 0x400440b0,      __WRITE);
__IO_REG32(CRYPT0_KUW13,                 0x400440b4,      __WRITE);
__IO_REG32(CRYPT0_KUW14,                 0x400440b8,      __WRITE);
__IO_REG32(CRYPT0_KUW15,                 0x400440bc,      __WRITE);
__IO_REG32(CRYPT0_KUWValStr1,            0x400440c0,      __WRITE);
__IO_REG32(CRYPT0_KUWValStr2,            0x400440c4,      __WRITE);

/* =========================
         PMG0
   ========================= */
__IO_REG32(PMG0_IEN,                     0x4004c000, __READ_WRITE);
__IO_REG32(PMG0_PSM_STAT,                0x4004c004, __READ_WRITE);
__IO_REG32(PMG0_PWRMOD,                  0x4004c008, __READ_WRITE);
__IO_REG32(PMG0_PWRKEY,                  0x4004c00c,      __WRITE);
__IO_REG32(PMG0_SHDN_STAT,               0x4004c010,       __READ);
__IO_REG32(PMG0_SRAMRET,                 0x4004c014, __READ_WRITE);
__IO_REG32(PMG0_TRIM,                    0x4004c038, __READ_WRITE);
__IO_REG32(PMG0_RST_STAT,                0x4004c040, __READ_WRITE);
__IO_REG32(PMG0_CTL1,                    0x4004c044, __READ_WRITE);

/* =========================
         XINT0
   ========================= */
__IO_REG32(XINT0_CFG0,                   0x4004c080, __READ_WRITE);
__IO_REG32(XINT0_EXT_STAT,               0x4004c084,       __READ);
__IO_REG32(XINT0_CLR,                    0x4004c090, __READ_WRITE);
__IO_REG32(XINT0_NMICLR,                 0x4004c094, __READ_WRITE);

/* =========================
         CLKG0_OSC
   ========================= */
__IO_REG32(CLKG0_OSC_KEY,                0x4004c10c,      __WRITE);
__IO_REG32(CLKG0_OSC_CTL,                0x4004c110, __READ_WRITE);

/* =========================
         PMG0_TST
   ========================= */
__IO_REG32(PMG0_TST_SRAM_CTL,            0x4004c260, __READ_WRITE);
__IO_REG32(PMG0_TST_SRAM_INITSTAT,       0x4004c264,       __READ);
__IO_REG16(PMG0_TST_CLR_LATCH_GPIOS,     0x4004c268,      __WRITE);
__IO_REG32(PMG0_TST_SCRPAD_IMG,          0x4004c26c, __READ_WRITE);
__IO_REG32(PMG0_TST_SCRPAD_3V_RD,        0x4004c270,       __READ);
__IO_REG32(PMG0_TST_FAST_SHT_WAKEUP,     0x4004c274, __READ_WRITE);

/* =========================
         CLKG0_CLK
   ========================= */
__IO_REG32(CLKG0_CLK_CTL0,               0x4004c300, __READ_WRITE);
__IO_REG32(CLKG0_CLK_CTL1,               0x4004c304, __READ_WRITE);
__IO_REG32(CLKG0_CLK_CTL2,               0x4004c308, __READ_WRITE);
__IO_REG32(CLKG0_CLK_CTL3,               0x4004c30c, __READ_WRITE);
__IO_REG32(CLKG0_CLK_CTL5,               0x4004c314, __READ_WRITE);
__IO_REG32(CLKG0_CLK_STAT0,              0x4004c318, __READ_WRITE);

/* =========================
         BUSM0
   ========================= */
__IO_REG32(BUSM0_ARBIT0,                 0x4004c800, __READ_WRITE);
__IO_REG32(BUSM0_ARBIT1,                 0x4004c804, __READ_WRITE);
__IO_REG32(BUSM0_ARBIT2,                 0x4004c808, __READ_WRITE);
__IO_REG32(BUSM0_ARBIT3,                 0x4004c80c, __READ_WRITE);
__IO_REG32(BUSM0_ARBIT4,                 0x4004c814, __READ_WRITE);

/* =========================
         PTI0
   ========================= */
__IO_REG32(PTI0_RST_ISR_STARTADDR,       0x4004cd00, __READ_WRITE);
__IO_REG32(PTI0_RST_STACK_PTR,           0x4004cd04, __READ_WRITE);
__IO_REG32(PTI0_CTL,                     0x4004cd08, __READ_WRITE);

/* =========================
         NVIC0
   ========================= */
__IO_REG32(NVIC0_INTNUM,                 0xe000e004, __READ_WRITE);
__IO_REG32(NVIC0_STKSTA,                 0xe000e010, __READ_WRITE);
__IO_REG32(NVIC0_STKLD,                  0xe000e014, __READ_WRITE);
__IO_REG32(NVIC0_STKVAL,                 0xe000e018, __READ_WRITE);
__IO_REG32(NVIC0_STKCAL,                 0xe000e01c, __READ_WRITE);
__IO_REG32(NVIC0_INTSETE0,               0xe000e100, __READ_WRITE);
__IO_REG32(NVIC0_INTSETE1,               0xe000e104, __READ_WRITE);
__IO_REG32(NVIC0_INTCLRE0,               0xe000e180, __READ_WRITE);
__IO_REG32(NVIC0_INTCLRE1,               0xe000e184, __READ_WRITE);
__IO_REG32(NVIC0_INTSETP0,               0xe000e200, __READ_WRITE);
__IO_REG32(NVIC0_INTSETP1,               0xe000e204, __READ_WRITE);
__IO_REG32(NVIC0_INTCLRP0,               0xe000e280, __READ_WRITE);
__IO_REG32(NVIC0_INTCLRP1,               0xe000e284, __READ_WRITE);
__IO_REG32(NVIC0_INTACT0,                0xe000e300, __READ_WRITE);
__IO_REG32(NVIC0_INTACT1,                0xe000e304, __READ_WRITE);
__IO_REG32(NVIC0_INTPRI0,                0xe000e400, __READ_WRITE);
__IO_REG32(NVIC0_INTPRI1,                0xe000e404, __READ_WRITE);
__IO_REG32(NVIC0_INTPRI2,                0xe000e408, __READ_WRITE);
__IO_REG32(NVIC0_INTPRI3,                0xe000e40c, __READ_WRITE);
__IO_REG32(NVIC0_INTPRI4,                0xe000e410, __READ_WRITE);
__IO_REG32(NVIC0_INTPRI5,                0xe000e414, __READ_WRITE);
__IO_REG32(NVIC0_INTPRI6,                0xe000e418, __READ_WRITE);
__IO_REG32(NVIC0_INTPRI7,                0xe000e41c, __READ_WRITE);
__IO_REG32(NVIC0_INTPRI8,                0xe000e420, __READ_WRITE);
__IO_REG32(NVIC0_INTPRI9,                0xe000e424, __READ_WRITE);
__IO_REG32(NVIC0_INTPRI10,               0xe000e428, __READ_WRITE);
__IO_REG32(NVIC0_INTCPID,                0xe000ed00, __READ_WRITE);
__IO_REG32(NVIC0_INTSTA,                 0xe000ed04, __READ_WRITE);
__IO_REG32(NVIC0_INTVEC,                 0xe000ed08, __READ_WRITE);
__IO_REG32(NVIC0_INTAIRC,                0xe000ed0c, __READ_WRITE);
__IO_REG16(NVIC0_INTCON0,                0xe000ed10, __READ_WRITE);
__IO_REG32(NVIC0_INTCON1,                0xe000ed14, __READ_WRITE);
__IO_REG32(NVIC0_INTSHPRIO0,             0xe000ed18, __READ_WRITE);
__IO_REG32(NVIC0_INTSHPRIO1,             0xe000ed1c, __READ_WRITE);
__IO_REG32(NVIC0_INTSHPRIO3,             0xe000ed20, __READ_WRITE);
__IO_REG32(NVIC0_INTSHCSR,               0xe000ed24, __READ_WRITE);
__IO_REG32(NVIC0_INTCFSR,                0xe000ed28, __READ_WRITE);
__IO_REG32(NVIC0_INTHFSR,                0xe000ed2c, __READ_WRITE);
__IO_REG32(NVIC0_INTDFSR,                0xe000ed30, __READ_WRITE);
__IO_REG32(NVIC0_INTMMAR,                0xe000ed34, __READ_WRITE);
__IO_REG32(NVIC0_INTBFAR,                0xe000ed38, __READ_WRITE);
__IO_REG32(NVIC0_INTAFSR,                0xe000ed3c, __READ_WRITE);
__IO_REG32(NVIC0_INTPFR0,                0xe000ed40, __READ_WRITE);
__IO_REG32(NVIC0_INTPFR1,                0xe000ed44, __READ_WRITE);
__IO_REG32(NVIC0_INTDFR0,                0xe000ed48, __READ_WRITE);
__IO_REG32(NVIC0_INTAFR0,                0xe000ed4c, __READ_WRITE);
__IO_REG32(NVIC0_INTMMFR0,               0xe000ed50, __READ_WRITE);
__IO_REG32(NVIC0_INTMMFR1,               0xe000ed54, __READ_WRITE);
__IO_REG32(NVIC0_INTMMFR2,               0xe000ed58, __READ_WRITE);
__IO_REG32(NVIC0_INTMMFR3,               0xe000ed5c, __READ_WRITE);
__IO_REG32(NVIC0_INTISAR0,               0xe000ed60, __READ_WRITE);
__IO_REG32(NVIC0_INTISAR1,               0xe000ed64, __READ_WRITE);
__IO_REG32(NVIC0_INTISAR2,               0xe000ed68, __READ_WRITE);
__IO_REG32(NVIC0_INTISAR3,               0xe000ed6c, __READ_WRITE);
__IO_REG32(NVIC0_INTISAR4,               0xe000ed70, __READ_WRITE);
__IO_REG32(NVIC0_INTTRGI,                0xe000ef00, __READ_WRITE);
__IO_REG32(NVIC0_INTPID4,                0xe000efd0, __READ_WRITE);
__IO_REG32(NVIC0_INTPID5,                0xe000efd4, __READ_WRITE);
__IO_REG32(NVIC0_INTPID6,                0xe000efd8, __READ_WRITE);
__IO_REG32(NVIC0_INTPID7,                0xe000efdc, __READ_WRITE);
__IO_REG32(NVIC0_INTPID0,                0xe000efe0, __READ_WRITE);
__IO_REG32(NVIC0_INTPID1,                0xe000efe4, __READ_WRITE);
__IO_REG32(NVIC0_INTPID2,                0xe000efe8, __READ_WRITE);
__IO_REG32(NVIC0_INTPID3,                0xe000efec, __READ_WRITE);
__IO_REG32(NVIC0_INTCID0,                0xe000eff0, __READ_WRITE);
__IO_REG32(NVIC0_INTCID1,                0xe000eff4, __READ_WRITE);
__IO_REG32(NVIC0_INTCID2,                0xe000eff8, __READ_WRITE);
__IO_REG32(NVIC0_INTCID3,                0xe000effc, __READ_WRITE);


#endif

