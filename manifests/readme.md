# Manifest Editing Guide

## IAR ZIP: MSKEW-00

* Examples and source files go into `ADuCM4x50_EZ_Kit-manifest.xml`

* Tests and `config.txt` files go into `ADuCM4x50_EZ_Kit-support-manifest.xml`

* ARM directory (IAR install) files go into iot-iar-manifest.xml

### ADuCM4150

* Examples and source files go into `ADuCM4150_EZ_Kit-manifest.xml`

* Tests, `config.txt`, and 4050 sources needed to recreate those tests go into `ADuCM4150_EZ_Kit-support-manifest.xml`

* ARM directory (IAR install) files go into iot-aducm4150-iar-manifest.xml

### DTE

Similar semantics, but with seperate manifest for staging. No DTE support manifests since all run-time testing is from IoT.

## CMSIS DFP PACK: MSKUV-00

* Source files, excluding off-chip drivers, go into `AnalogDevices.ADuCM4x50_DFP-manifest.xml`

* Tests, excluding tests for off-chip drivers, go into `AnalogDevices.ADuCM4x50_DFP-support-manifest.xml`


## CMSIS BSP PACK: MSKUV-01

* Examples and off-chip drivers go into `AnalogDevices.ADuCM4x50_EZ_KIT_BSP-manifest.xml`

* `COM.cfg`, `config.txt`, and tests for off-chip drivers go into `AnalogDevices.ADuCM4x50_EZ_KIT_BSP-support-manifest.xml`

# More Information About Manifests

## Files
Manifests contain a list of files to include in a software package.

Support manifests contain a list of files to include with the regular manifest during testing, but should 
not be included in the final product. Some examples of these files are `config.txt` files, anything in the `tst`
directory, and other files that a package are needed for testing.

Manifest files are organized into sub-folders depending on the package they go into. However, this organization is for 
human readability only and can be easily changed by updating `manifest.xml`. 

Some packages have multiple manifest files in order to distinguish between origin (IoT or DTE) and part (4050 or 4150).

## Paths
All paths in manifests use the following syntax:
`<f p="My\Special\Path" />`

All paths are rooted from the `package` directory. Do not include `package` in the path.

## Nuances 
One file that misbehaves is the `example_pdsc.txt` file. This file is not in the final CMSIS package and not needed for
testing. However, the file is needed for the build process. This file goes in the regular CMSIS manifest (not the support).
The build system will ensure that it is not included in the final product.

Support files for the 4150 products are tricky as well. The 4150 DriverSW package is a new set of personality files, 
and an additional set of drivers and examples. The 4150 DriverSW package does not include the 4x50 drivers, since it is 
designed to be overlayed on top of the 4x50 DriverSW package. Since the 4150 DriverSW package has tests that assume the
4x50 drivers are present (e.g. MISRA), they are considered support files for this product. This is why the driver is added
to the 4150 support manifest in Example A.

